/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.exceptions;

/**
 *
 * @author macbook
 */
public class ExceptionJR extends BaseJRException {

    private String className = "";
    private String code = "";
    private String message = "";
    
    public ExceptionJR(String pstrClass, String pstrCode, String pstrMessage) {
        super(pstrMessage);
        this.className = pstrClass;
        this.code = pstrCode;
        this.message = pstrMessage;
    }
    
    public ExceptionJR(String pstrClass, String pstrCode, String pstrMessage, Throwable pthrCause) {
        super(pstrMessage, pthrCause);
        this.className = pstrClass;
        this.code = pstrCode;
        this.message = pstrMessage;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("className: ").append(this.className);
        sb.append(" code: ").append(this.code);
        sb.append(" message: ").append(this.message);
        return sb.toString();
    }
    
}
