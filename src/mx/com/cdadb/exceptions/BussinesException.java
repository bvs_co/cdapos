/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.exceptions;

/**
 *
 * @author macbook
 */
public class BussinesException extends BaseException{
    
    public BussinesException(String code) {
        super("", code, "default");
    }
    
    public BussinesException(String code, String message) {
        super("", code, message);
    }
    
    public BussinesException(String className, String code, String message) {
        super(className, code, "default");
    }
}
