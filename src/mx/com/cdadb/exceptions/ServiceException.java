/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.exceptions;

/**
 *
 * @author 
 */
public class ServiceException extends Exception{
    
    /**
	 * Manejador de excepcion
	 * @param message
	 */
	public ServiceException(String message) {
		super(message);
	}
}
