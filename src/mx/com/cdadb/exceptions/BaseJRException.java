/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.exceptions;

import java.util.Locale;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReportsContext;

/**
 *
 * @author macbook
 */
public abstract class BaseJRException extends JRException{
    
    public BaseJRException(String message) {
        super(message);
    }

    public BaseJRException(Throwable t) {
        super(t);
    }

    public BaseJRException(String message, Throwable t) {
        super(message, t);
    }

    public BaseJRException(String messageKey, Object[] args, JasperReportsContext jasperReportsContext, Locale locale) {
        super(messageKey, args, jasperReportsContext, locale);
    }
    
    
    
}
