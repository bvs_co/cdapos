package mx.com.cdadb;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.service.impl.ServiceBaseImpl;
import mx.com.cdadb.util.Log;
import mx.com.cdadb.view.FrMenu;

/**
 *
 * @author unknown
 */
public class CdaDb {

    public static void main(String[] args) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            Log.error(ex.getMessage());
        }
        try {
            Log.init();
            Log.info("################################## INICIANDO CDA v1.2.11.2020 ################################");
            ServiceBaseImpl service = new ServiceBaseImpl();
            App app = new App();
            try {
                app = (App) service.getBy(new App(), "t.appNombre = '" + ConstantConfig.CONFIG_APP_PORT + "'");
                Log.info("app.port:" + app.toString());
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, "Error de conexión:\n*Compruebe la conexión con la base de datos.\n*Compruebe que el servidor local esta encendido.",
                        e.getMessage(), JOptionPane.ERROR_MESSAGE);
                Log.error(e.getMessage());
                System.exit(0);
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error de conexión:\n*Compruebe la conexión con la base de datos.\n*Compruebe que el servidor local esta encendido.",
                        ex.getMessage(), JOptionPane.ERROR_MESSAGE);
                Log.error(ex.getMessage());
            }
            ServerSocket SERVER_SOCKET = new ServerSocket(Integer.parseInt(app.getAppValor()));
            FrMenu frMenu = new FrMenu();
            frMenu.setVisible(true);
        } catch (IOException ioEx) {
            JOptionPane.showMessageDialog(null, "Ya hay una instancia de la aplicación que se está ejecutando actualmente...");
            Log.error(ioEx.getMessage());
        } catch (NumberFormatException ex) {
            Log.error(ex.getMessage());
            JOptionPane.showMessageDialog(null, "Error en parámetros de inicio.", "Error.", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }
}
