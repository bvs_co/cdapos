/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.constants;

/**
 *
 * @author
 */
public class ConstantConfig {
    
    public static final String CONFIG_LABORATORY_AREA = "app.config.area.lab";
    public static final String CONFIG_BRANCH_ID = "app.config.sucursal";
    public static final String CONFIG_COMPANY_RFC = "app.company.rfc";
    public static final String CONFIG_COMPANY_BUSSINES_NAME = "app.company.razonSocial";
    public static final String CONFIG_COMPANY_BUSSINES_ADDRES = "app.company.domFiscal";
    public static final String CONFIG_COMPANY_TELEPHONE = "app.company.tel";
    public static final String CONFIG_ORDER_INIT = "app.order.ini";
    public static final String CONFIG_ORDER_FINALIZE = "app.order.fin";
    public static final String CONFIG_FOLIO_DEVOLUTION_INIT = "app.pos.dev.ini";
    public static final String CONFIG_FOLIO_SALE_INIT = "app.pos.ini";
    public static final String CONFIG_APP_VERSION = "app.ver";
    public static final String CONFIG_APP_NAME = "app.Name";
    public static final String CONFIG_ENTERPRISE_ORDER_SAVE = "app.order.save";
    public static final String CONFIG_APP_ONLINE_ASSIST = "app.config.asistOnline";
    public static final String CONFIG_ORDER_HOST = "app.order.host";
    public static final String CONFIG_ORDER_PORT = "app.order.puerto";
    public static final String CONFIG_APP_PORT = "app.puerto";
    public static final String CONFIG_POS_CREDIT_CARD_ID = "app.config.tj";
    public static final String CONFIG_SQL_BACKUP = "app.sql.backup";
    public static final String CONFIG_ORDER_SEND_DOUBLE = "app.order.send.double";
    public static final String CONFIG_ORDER_SEND_DOUBLE_PATH = "app.order.send.double.path";
    public static final String CONFIG_ORDER_SEND_ENCRYPT = "app.order.send.encrypt";
}
