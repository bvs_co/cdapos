/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.constants;

/**
 *
 * @author macbook
 */
public class ConstantReport {
    
    public static final String CASHBOX_CUT = "/mx/com/cdadb/resources/report/cashbox_cut.jasper";
    public static final String PATIENT_VOUCHER = "/mx/com/cdadb/resources/report/patient_voucher.jasper";
    public static final String DONATION_VOUCHER = "/mx/com/cdadb/resources/report/donation_voucher.jasper";
    public static final String SERVICE_ORDER = "/mx/com/cdadb/resources/report/service_order.jasper";
    public static final String DEVOLUTION_VOUCHER = "/mx/com/cdadb/resources/report/devolution_voucher.jasper";
    public static final String DAY_SALES_REPORT = "/mx/com/cdadb/resources/report/day_sales_report.jasper";
}
