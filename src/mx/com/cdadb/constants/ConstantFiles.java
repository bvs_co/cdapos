/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.constants;

/**
 *
 * @author
 */
public class ConstantFiles {
    
    public static final String LOCAL_CONNECTION = "dbLocal.properties";
    public static final String CUSTOMER_CONNECTION = "dbPacientes.properties";
    public static final String POS_PROPERTIES = "caja.properties";
    public static final String DIR_PROPERTIES = "dir.properties";
}
