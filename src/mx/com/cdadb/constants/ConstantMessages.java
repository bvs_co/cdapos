/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.constants;

/**
 *
 * @author macbook
 */
public class ConstantMessages {

    public static final String UNEXPECTED_ERROR = "Ocurrió un error inesperado!!";
    public static final String ERROR = "Error: ";
    public static final String ERROR_DESCRIPTION = "Descripción: \n";
    public static final String ERROR_TITLE = "Error.";
}
