package mx.com.cdadb.entities;
// Generated 23/04/2018 09:40:19 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Modulo generated by hbm2java
 */
public class Modulo  implements java.io.Serializable {


     private Integer moduloId;
     private String moduloNombre;
     private String moduloMenu;
     private String moduloSubmenu;
     private Set<Accion> accions = new HashSet<Accion>(0);

    public Modulo() {
    }

    public Modulo(String moduloNombre, String moduloMenu, String moduloSubmenu, Set<Accion> accions) {
       this.moduloNombre = moduloNombre;
       this.moduloMenu = moduloMenu;
       this.moduloSubmenu = moduloSubmenu;
       this.accions = accions;
    }
   
    public Integer getModuloId() {
        return this.moduloId;
    }
    
    public void setModuloId(Integer moduloId) {
        this.moduloId = moduloId;
    }
    public String getModuloNombre() {
        return this.moduloNombre;
    }
    
    public void setModuloNombre(String moduloNombre) {
        this.moduloNombre = moduloNombre;
    }
    public String getModuloMenu() {
        return this.moduloMenu;
    }
    
    public void setModuloMenu(String moduloMenu) {
        this.moduloMenu = moduloMenu;
    }
    public String getModuloSubmenu() {
        return this.moduloSubmenu;
    }
    
    public void setModuloSubmenu(String moduloSubmenu) {
        this.moduloSubmenu = moduloSubmenu;
    }
    public Set<Accion> getAccions() {
        return this.accions;
    }
    
    public void setAccions(Set<Accion> accions) {
        this.accions = accions;
    }




}


