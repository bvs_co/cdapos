package mx.com.cdadb.entities;
// Generated 23/04/2018 09:40:19 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Infoenvio generated by hbm2java
 */
public class Infoenvio  implements java.io.Serializable {


     private Integer infoenvioId;
     private Ordenlab ordenlab;
     private Date infoenvioDate;
     private String infoenvioDesc;

    public Infoenvio() {
    }

	
    public Infoenvio(Ordenlab ordenlab) {
        this.ordenlab = ordenlab;
    }
    public Infoenvio(Ordenlab ordenlab, Date infoenvioDate, String infoenvioDesc) {
       this.ordenlab = ordenlab;
       this.infoenvioDate = infoenvioDate;
       this.infoenvioDesc = infoenvioDesc;
    }
   
    public Integer getInfoenvioId() {
        return this.infoenvioId;
    }
    
    public void setInfoenvioId(Integer infoenvioId) {
        this.infoenvioId = infoenvioId;
    }
    public Ordenlab getOrdenlab() {
        return this.ordenlab;
    }
    
    public void setOrdenlab(Ordenlab ordenlab) {
        this.ordenlab = ordenlab;
    }
    public Date getInfoenvioDate() {
        return this.infoenvioDate;
    }
    
    public void setInfoenvioDate(Date infoenvioDate) {
        this.infoenvioDate = infoenvioDate;
    }
    public String getInfoenvioDesc() {
        return this.infoenvioDesc;
    }
    
    public void setInfoenvioDesc(String infoenvioDesc) {
        this.infoenvioDesc = infoenvioDesc;
    }




}


