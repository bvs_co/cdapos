package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableRowSorter;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Clave;
import mx.com.cdadb.entities.ClaveId;
import mx.com.cdadb.entities.Enterprise;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceClave;
import mx.com.cdadb.service.ServiceEnterprise;
import mx.com.cdadb.service.ServiceProducto;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.service.impl.ServiceClaveImpl;
import mx.com.cdadb.service.impl.ServiceEnterpriseImpl;
import mx.com.cdadb.service.impl.ServiceProductoImpl;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author unknown
 */
public class DialogNuevoConnEnterprise extends javax.swing.JDialog {

    private int opc = -1;
    private PaneConnEnterprise paneConnEnterprise = null;
    private Enterprise enterprise = null;
    private final ServiceEnterprise sEnterprise = new ServiceEnterpriseImpl();
    private Logger log = LoggerFactory.getLogger(DialogNuevoConnEnterprise.class);

    public DialogNuevoConnEnterprise(java.awt.Frame parent, boolean modal, int op, PaneConnEnterprise pane) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setTitle("Nuevo código Enterprise.");
        this.opc = op;
        this.paneConnEnterprise = pane;
        initData();
        log.info("Iniciando: Agregar nueva clave Enterprise.....");
        //quitarSeleccionados();
    }

    public DialogNuevoConnEnterprise(java.awt.Frame parent, boolean modal, int op, PaneConnEnterprise pane, Enterprise ent) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setTitle("Editar código Enterprise.");
        this.opc = op;
        this.paneConnEnterprise = pane;
        this.enterprise = ent;
        initData();
        fillData();
        log.info("Iniciando: Editar clave Enterprise["+enterprise.getEnterpriseClave()+"].....");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtClave = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtDescripcion = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        txtBuscar = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setText("Clave");

        txtClave.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtClave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtClaveKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel2.setText("Descripción");

        txtDescripcion.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtDescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDescripcionKeyPressed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Seleccione productos que lo conforman", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 1, 11))); // NOI18N

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CLAVE", "NOMBRE", "SERVICIO", "SELECC"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(100);
        }

        txtBuscar.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jLabel3.setText("Buscar:");

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 286, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnGuardar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/save_accept_x16.png"))); // NOI18N
        btnGuardar.setMnemonic('G');
        btnGuardar.setText("Guardar [F5]");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        btnGuardar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGuardarKeyPressed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cancel_x16.png"))); // NOI18N
        btnCancelar.setMnemonic('C');
        btnCancelar.setText("Cancelar [Esc]");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 647, Short.MAX_VALUE)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar))
                    .addComponent(txtClave)
                    .addComponent(txtDescripcion))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                        .addGap(3, 3, 3)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtClaveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtClaveKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtClaveKeyPressed

    private void txtDescripcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripcionKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtDescripcionKeyPressed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            btnGuardar.requestFocus();
        } else {
            checkKeyPressed(evt);
        }
    }//GEN-LAST:event_jTable1KeyPressed

    private void txtBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtBuscarKeyPressed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        txtBuscar.setText("");
        txtBuscarKeyReleased(null);
        switch (this.opc) {
            case 1:
                if (save()) {
                    JOptionPane.showMessageDialog(this, "Registro guardado correctamente.");
                    this.paneConnEnterprise.initData();
                    log.info("Nueva clave Enterprise guardada.......");
                    this.dispose();
                }
                break;
            case 2:
                if (refresh()) {
                    JOptionPane.showMessageDialog(this, "Cambios guardados correctamente.");
                    this.paneConnEnterprise.initData();
                    log.info("Clave Enterprise ["+this.enterprise.getEnterpriseClave()+"] modificada.......");
                    this.dispose();
                }
                break;
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnGuardarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGuardarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnGuardarKeyPressed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased

    }//GEN-LAST:event_jTable1KeyReleased

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        txtBuscar.setText(txtBuscar.getText().toUpperCase());
        findCodes(txtBuscar.getText());
    }//GEN-LAST:event_txtBuscarKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoConnEnterprise.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoConnEnterprise.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoConnEnterprise.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoConnEnterprise.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogNuevoConnEnterprise dialog = new DialogNuevoConnEnterprise(new javax.swing.JFrame(), true, 0, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextField txtClave;
    private javax.swing.JTextField txtDescripcion;
    // End of variables declaration//GEN-END:variables

    private void initData() {
        try {
            ServiceApp sApp = new ServiceAppImpl();
            App app = sApp.getBy("a.appNombre = '" + ConstantConfig.CONFIG_LABORATORY_AREA + "'");

            ServiceProducto sProducto = new ServiceProductoImpl();
            List<Producto> arr = sProducto.findBy("p.area.areaId = " + app.getAppValor());
            
            if(this.getTitle().contains("Editar")){
                txtClave.setEnabled(false);
            }
            
            int i = 0;
            Object[][] datos = new Object[arr.size()][4];
            for (Producto producto : arr) {
                datos[i][0] = producto.getProductoClave();
                datos[i][1] = producto.getProductoNombre();
                datos[i][2] = producto.getArea().getAreaNombre();
                datos[i][3] = false;
                i++;
            }

            jTable1.setModel(new javax.swing.table.DefaultTableModel(
                    datos,
                    new String[]{
                        "CLAVE", "NOMBRE", "SERVICIO", "SELECC"
                    }
            ) {
                Class[] types = new Class[]{
                    java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
                };
                boolean[] canEdit = new boolean[]{
                    false, false, false, true
                };

                @Override
                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            });

            jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);

            jScrollPane1.setViewportView(jTable1);

            jTable1.getTableHeader().setReorderingAllowed(false);

            if (jTable1.getColumnModel().getColumnCount() > 0) {
                jTable1.getColumnModel().getColumn(0).setResizable(true);
                jTable1.getColumnModel().getColumn(0).setPreferredWidth(80);
                jTable1.getColumnModel().getColumn(1).setResizable(true);
                jTable1.getColumnModel().getColumn(1).setPreferredWidth(280);
                jTable1.getColumnModel().getColumn(2).setResizable(true);
                jTable1.getColumnModel().getColumn(2).setPreferredWidth(150);
                jTable1.getColumnModel().getColumn(3).setResizable(true);
                jTable1.getColumnModel().getColumn(3).setPreferredWidth(100);
            }

            JTableHeader jtableHeader = jTable1.getTableHeader();
            jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
            jTable1.setTableHeader(jtableHeader);

            jTable1.setRowHeight(30);

            jTable1.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ORDER_ID.toString()));
            jTable1.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
            jTable1.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        } catch (Exception ex) {
            if (ex.getMessage() != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            log.error(ex.getMessage());
        }
    }

    private boolean save() {
        boolean logrado = false;

        try {
            if (isRepeat()) {
                JOptionPane.showMessageDialog(this, "Esta clave Enterprise ya se encuentra registrada.");
            } else {
                Enterprise ent = new Enterprise();
                ent.setEnterpriseClave(txtClave.getText());
                ent.setEnterpriseDesc(txtDescripcion.getText());
                ent.setEnterpriseFcreacion(new java.util.Date());
                ent.setEnterpriseFmodif(new java.util.Date());
                int id = sEnterprise.add(ent);
                if (id > 0) {
                    logrado = true;
                    ServiceClave sClave = new ServiceClaveImpl();
                    sClave.add(getClaves(id));
                }
            }
        } catch (Exception ex) {
            if (ex.getMessage() != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            log.error(ex.getMessage());
        }
        return logrado;
    }

    private List<Clave> getClaves(int id) {
        List<Clave> claves = new ArrayList<>();
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            if (Boolean.valueOf(jTable1.getValueAt(i, 3).toString())) {
                Clave clave = new Clave();
                ClaveId idd = new ClaveId();
                idd.setCalveEnterpriseId(id);
                idd.setClaveProductoClave(jTable1.getValueAt(i, 0).toString());
                clave.setId(idd);
                clave.setClaveStatus("ACT");
                clave.setClaveFcreacion(new java.util.Date());
                clave.setClaveFmodif(new java.util.Date());
                claves.add(clave);
            }
        }
        return claves;
    }

//    private Producto getProducto(String clave) {
//        ServiceProducto sPr = new ServiceProductoImpl();
//        return sPr.obtenerProducto("p.productoClave = '" + clave + "'");
//    }
//
//    private Enterprise getEnterPrise(int id) {
//        ServiceEnterprise sEnt = new ServiceEnterpriseImpl();
//        return sEnt.obtenerEnterprise("e.enterpriseId = " + id);
//    }
    private void fillData() {
        txtClave.setText(this.enterprise.getEnterpriseClave());
        txtDescripcion.setText(this.enterprise.getEnterpriseDesc());

        this.enterprise.getClaves().stream().map(o -> (Clave) o).forEachOrdered(clave -> {
            for (int i = 0; i < jTable1.getRowCount(); i++) {
                if (clave.getProducto().getProductoClave().equals(jTable1.getValueAt(i, 0).toString())) {
                    jTable1.setValueAt(true, i, 3);
                }
            }
        });
    }

    private boolean refresh() {
        boolean logrado = false;
        try {
            ServiceEnterprise sEnt = new ServiceEnterpriseImpl();
            Enterprise ent = new Enterprise();
            ent.setEnterpriseClave(txtClave.getText());
            ent.setEnterpriseDesc(txtDescripcion.getText());
            ent.setEnterpriseId(this.enterprise.getEnterpriseId());
            ent.setEnterpriseFmodif(new java.util.Date());
            List<Producto> nuevos = getNewProducts();
            List<Producto> eliminar = getDeleteProducts();//Revisar este punto. Aunque exista uno igual, lo guarda
            if (sEnt.edit(ent, nuevos, eliminar)) {
                logrado = true;
            }
        } catch (Exception ex) {
            if (ex.getMessage() != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            log.error(ex.getMessage());
        }
        return logrado;
    }

    private List<Producto> getNewProducts() {
        ServiceProducto sPr = new ServiceProductoImpl();
        List<Producto> arr = new ArrayList<>();
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            if (Boolean.valueOf(jTable1.getValueAt(i, 3).toString())) {
                try {
                    Producto pr = sPr.getBy("p.productoClave = '" + jTable1.getValueAt(i, 0).toString() + "'");
                    if (!addArr(arr, pr)) {
                        arr.add(pr);
                    }
                } catch (Exception ex) {
                    if (ex.getMessage() != null) {
                        JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                    }
                    log.error(ex.getMessage());
                }
            }
        }
        return arr;
    }

    private List<Producto> getDeleteProducts() {
        List<Producto> arr = new ArrayList<>();
        if (this.enterprise.getClaves().size() > 0) {
            this.enterprise.getClaves().forEach(clave -> {
                for (int i = 0; i < jTable1.getRowCount(); i++) {
                    if (!addArr(arr, clave.getProducto())) {
                        arr.add(clave.getProducto());
                    }
                }
            });
        }
        return arr;
    }

    //Evento para usar las teclas especiales 
    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 113://F2 Nuevo

                break;
            case 114://F3 Eliminar

                break;
            case 115://F4

                break;
            case 116://F5 Guardar
                btnGuardarActionPerformed(null);
                break;
            case 117://F6 
                break;
            case 118://F7 
                break;
            case 119://F8
                break;
            case 120://F9 
                break;
            case 121://F10
                break;
            case 27://Esc
                this.dispose();
                break;
        }
    }

    private void findCodes(String text) {
        List<RowFilter<Object, Object>> row = null;
        row = new ArrayList();
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>((DefaultTableModel) jTable1.getModel());
        try {
            row.add(RowFilter.regexFilter(text, 0));
            row.add(RowFilter.regexFilter(text, 1));
            row.add(RowFilter.regexFilter(text, 2));
        } catch (java.util.regex.PatternSyntaxException e) {
            JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + e.getMessage());
        }
        jTable1.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.orFilter(row));
    }

    private boolean addArr(List<Producto> arr, Producto pr) {
        boolean enc = false;
        for (Producto p : arr) {
            if (p.getProductoClave().equals(pr.getProductoClave())) {
                enc = true;
                break;
            }
        }
        return enc;
    }

    private boolean isRepeat() {
        List<Enterprise> results = new ArrayList<>();
        try {
            //Revisar si la clave ya existe
            results = sEnterprise.findBy("e.enterpriseClave = '" + txtClave.getText() + "'");
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return results.size() > 0;
    }
}
