package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableRowSorter;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Clave;
import mx.com.cdadb.entities.Detallepaquete;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceClave;
import mx.com.cdadb.service.ServiceDetallePaquete;
import mx.com.cdadb.service.ServiceProducto;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.service.impl.ServiceClaveImpl;
import mx.com.cdadb.service.impl.ServiceDetallePaqueteImpl;
import mx.com.cdadb.service.impl.ServiceProductoImpl;
import mx.com.cdadb.view.FrMenu;
import mx.com.cdadb.util.DialogWait;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;

/**
 *
 * @author unknown
 */
public class DialogProductos extends javax.swing.JDialog {

    private PanePos panePos = null;
    private FrMenu frMenu = null;
    private static final String PRODUCT_ID = "0";
    private static final String PACKAGE_ID = "1";
    private App app = new App();

    public DialogProductos(java.awt.Frame parent, boolean modal, String clave, PanePos p) {
        super(parent, modal);
        try {
            initComponents();
            this.panePos = p;
            this.frMenu = (FrMenu) parent;
            this.setLocationRelativeTo(null);
            DialogWait dlg = new DialogWait(this.frMenu, true, this, clave);
            dlg.setVisible(true);

            ServiceApp sApp = new ServiceAppImpl();
            app = sApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_LABORATORY_AREA+"'");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this.frMenu, "Ha ocurrido un error al intentar extraer el identificador del laboratorio.");
            LoggerUtil.registrarError(ex, this.frMenu.user);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        txtBuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaProductos = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        btnDetalle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Seleccione estudio");
        setIconImage(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_x32.png")).getImage());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel1KeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        txtBuscar.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        tablaProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CLAVE", "DESCRIPCIÓN", "C.M.R.", "SELECC", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaProductos.setColumnSelectionAllowed(true);
        tablaProductos.getTableHeader().setReorderingAllowed(false);
        tablaProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaProductosMouseClicked(evt);
            }
        });
        tablaProductos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaProductosKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaProductosKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaProductos);
        tablaProductos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (tablaProductos.getColumnModel().getColumnCount() > 0) {
            tablaProductos.getColumnModel().getColumn(0).setMinWidth(150);
            tablaProductos.getColumnModel().getColumn(1).setMinWidth(300);
            tablaProductos.getColumnModel().getColumn(2).setMinWidth(150);
            tablaProductos.getColumnModel().getColumn(3).setMinWidth(100);
            tablaProductos.getColumnModel().getColumn(4).setResizable(false);
            tablaProductos.getColumnModel().getColumn(4).setPreferredWidth(0);
        }
        JTableHeader jtableHeader = tablaProductos.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaProductos.setTableHeader(jtableHeader);

        tablaProductos.setRowHeight(30);

        tablaProductos.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ORDER_ID.toString()));
        tablaProductos.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tablaProductos.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));
        tablaProductos.getColumnModel().getColumn(4).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ORDER_ID.toString()));

        jButton1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/accept_x32.png"))); // NOI18N
        jButton1.setText("Aceptar [F5]");
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerifyInputWhenFocusTarget(false);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        btnDetalle.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        btnDetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/view_detail_x32.png"))); // NOI18N
        btnDetalle.setText("Detalle [F2]");
        btnDetalle.setEnabled(false);
        btnDetalle.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnDetalle.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnDetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDetalleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jSeparator2)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 827, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnDetalle)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(btnDetalle))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtBuscarKeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jButton1KeyPressed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        txtBuscar.setText(txtBuscar.getText().toUpperCase());
        buscarProductos(txtBuscar.getText());
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void tablaProductosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaProductosKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_tablaProductosKeyPressed

    private void jPanel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jPanel1KeyPressed

    private void tablaProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaProductosMouseClicked
        if (evt.getClickCount() == 2) {
            tablaProductos.setValueAt(true, tablaProductos.getSelectedRow(), 3);
            seleccionaProductos(Boolean.valueOf(tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 3).toString()));
        }
        activaDetalle();
        if(Boolean.valueOf(tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 3).toString())
                && !txtBuscar.getText().isEmpty()){
            txtBuscar.setText("");
            //txtBuscarKeyReleased(null);
            txtBuscar.requestFocus();
        }
    }//GEN-LAST:event_tablaProductosMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (getSeleccionados() == 1) {
            seleccionaProductos();
        } else if (getSeleccionados() > 1) {
            seleccionaProductos();
        } else if (tablaProductos.getSelectedRowCount() == 1) {
            seleccionaProductos(true);
        } else {
            dispose();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tablaProductosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaProductosKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            txtBuscar.selectAll();
            txtBuscar.requestFocus();
        }
        if (tablaProductos.getSelectedRowCount() > 0) {
            if (evt.getKeyCode() == KeyEvent.VK_UP || evt.getKeyCode() == KeyEvent.VK_DOWN || evt.getKeyCode() == KeyEvent.VK_TAB) {
                activaDetalle();
            }
        }
    }//GEN-LAST:event_tablaProductosKeyReleased

    private void btnDetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDetalleActionPerformed
        try {
            ServiceDetallePaquete sdp = new ServiceDetallePaqueteImpl();
            Detallepaquete dtp = sdp.getBy("p.detallePaqueteClave='" + tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 0) + "'");
            new DialogProductosPaq(null, true, dtp).setVisible(true);
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }//GEN-LAST:event_btnDetalleActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogProductos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogProductos dialog = new DialogProductos(new javax.swing.JFrame(), true, "", null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDetalle;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable tablaProductos;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 113://F2
                if (btnDetalle.isEnabled()) {
                    btnDetalleActionPerformed(null);
                }
                break;
            case 116://F5 Aceptar
                if (getSeleccionados() == 1) {
                    try {
                        seleccionaProductos(Boolean.valueOf(tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 3).toString()));
                    } catch (Exception ex) {
                        Logger.getLogger(DialogProductos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (getSeleccionados() > 1) {
                    seleccionaProductos();
                } else if (tablaProductos.getSelectedRowCount() == 1) {
                    seleccionaProductos(true);
                } else {
                    dispose();
                }
                break;
            case 27://Esc
                this.dispose();
                break;
        }
    }

    public void initData(String clave) {
        try {
            ServiceProducto sProducto = new ServiceProductoImpl();
            ServiceDetallePaquete sPaquete = new ServiceDetallePaqueteImpl();

            List<Producto> productos = sProducto.findAll();
            List<Detallepaquete> paquetes = sPaquete.findAll();

            int i = 0;
            Object[][] datos = new Object[productos.size() + paquetes.size()][6];
            for (Producto producto : productos) {
                datos[i][0] = producto.getProductoClave();
                datos[i][1] = producto.getProductoNombre();
                datos[i][2] = producto.getProductoPrecio();
                datos[i][3] = false;
                datos[i][4] = PRODUCT_ID;//Producto
                datos[i][5] = producto.getArea().getAreaId().toString();//Area
                i++;
            }
            for (Detallepaquete paquete : paquetes) {
                datos[i][0] = paquete.getDetallePaqueteClave();
                datos[i][1] = paquete.getDetallePaqueteNombre();
                datos[i][2] = paquete.getDetallePaquetePrecio();
                datos[i][3] = false;
                datos[i][4] = PACKAGE_ID;//Paquete
                datos[i][5] = "";//Area
                i++;
            }

            tablaProductos.setModel(new javax.swing.table.DefaultTableModel(
                    datos,
                    new String[]{
                        "CLAVE", "DESCRIPCIÓN", "C.M.R.", "SELECC", "", ""
                    }
            ) {
                Class[] types = new Class[]{
                    java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class
                };
                boolean[] canEdit = new boolean[]{
                    false, false, false, true, false, false
                };

                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            });

            tablaProductos.setColumnSelectionAllowed(false);

            tablaProductos.getTableHeader().setReorderingAllowed(false);

            jScrollPane1.setViewportView(tablaProductos);

            tablaProductos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
            if (tablaProductos.getColumnModel().getColumnCount() > 0) {
                tablaProductos.getColumnModel().getColumn(0).setMinWidth(150);
                tablaProductos.getColumnModel().getColumn(1).setMinWidth(300);
                tablaProductos.getColumnModel().getColumn(2).setMinWidth(150);
                tablaProductos.getColumnModel().getColumn(3).setMinWidth(100);
                tablaProductos.getColumnModel().getColumn(4).setPreferredWidth(0);
                tablaProductos.getColumnModel().getColumn(4).setResizable(false);
                tablaProductos.getColumnModel().getColumn(5).setPreferredWidth(0);
                tablaProductos.getColumnModel().getColumn(5).setResizable(false);
            }
            JTableHeader jtableHeader = tablaProductos.getTableHeader();
            jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
            tablaProductos.setTableHeader(jtableHeader);

            tablaProductos.setRowHeight(30);

            tablaProductos.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ORDER_ID.toString()));
            tablaProductos.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
            tablaProductos.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));
            tablaProductos.getColumnModel().getColumn(4).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.PK_ID_HIDDEN.toString()));
            tablaProductos.getColumnModel().getColumn(5).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.PK_ID_HIDDEN.toString()));

            txtBuscar.setText(clave);
            txtBuscarKeyReleased(null);
            txtBuscar.selectAll();
            txtBuscar.requestFocus();
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }

    private void buscarProductos(String text) {
        List<RowFilter<Object, Object>> row = null;
        row = new ArrayList();
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>((DefaultTableModel) tablaProductos.getModel());
        try {
            row.add(RowFilter.regexFilter(text, 0));
            row.add(RowFilter.regexFilter(text, 1));
            row.add(RowFilter.regexFilter(text, 2));
        } catch (java.util.regex.PatternSyntaxException e) {
            JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + e.getMessage());
        }
        tablaProductos.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.orFilter(row));
    }

    private void seleccionaProductos(boolean b) {
        ServiceClave sClave = new ServiceClaveImpl();
        DefaultTableModel model = (DefaultTableModel) this.panePos.tablaProductos.getModel();

        if (PRODUCT_ID.equals(tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 4).toString()) 
                && app.getAppValor().equals(tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 5).toString())) {
            try {
                List<Clave> claves = sClave.findBy("c.id.claveProductoClave = '" + tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 0).toString() + "'");
                if (claves.size() > 0) {
                    if (b) {
                        Object nuevo[] = {
                            tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 4),//Tipo
                            tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 0).toString(),//Clave
                            tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 1).toString(),//Descripcion
                            tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 2).toString(),//P.U.
                            tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 2).toString()};//Importe
                        model.addRow(nuevo);
                    }
                } else {
                    if (b) {
                        JOptionPane.showMessageDialog(this, "Este producto no cuenta con clave enterprise para su procesamiento en laboratorio.");
                    }
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this, "Este producto no cuenta con clave enterprise para su procesamiento en laboratorio.");
                LoggerUtil.registrarError(ex, this.panePos.frMenu.user);
            }
        } else {
            Object nuevo[] = {
                tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 4),//Tipo
                tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 0).toString(),//Clave
                tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 1).toString(),//Descripcion
                tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 2).toString(),//P.U.
                tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 2).toString()};//Importe
            model.addRow(nuevo);
        }

        this.panePos.tablaProductos.setModel(model);
        this.panePos.actualizaTotal();
        this.panePos.repaint();
        this.panePos.txtClave.setText("");
        this.panePos.txtClave.requestFocus();
        this.dispose();
    }

    private void seleccionaProductos() {
        String noEnterprise = "";
        ServiceClave sClave = new ServiceClaveImpl();
        DefaultTableModel model = (DefaultTableModel) this.panePos.tablaProductos.getModel();
        for (int i = 0; i < tablaProductos.getRowCount(); i++) {
            if (Boolean.valueOf(tablaProductos.getValueAt(i, 3).toString())) {
                if (PRODUCT_ID.equals(tablaProductos.getValueAt(i, 4).toString())
                        && app.getAppValor().equals(tablaProductos.getValueAt(i, 5).toString())) {
                    try {
                        List<Clave> claves = sClave.findBy("c.id.claveProductoClave = '" + tablaProductos.getValueAt(i, 0).toString() + "'");
                        if (claves.size() > 0) {
                            //Tiene clave enterprise
                            Object nuevo[] = {
                                tablaProductos.getValueAt(i, 4),//Tipo
                                tablaProductos.getValueAt(i, 0).toString(),//Clave
                                tablaProductos.getValueAt(i, 1).toString(),//Descripcion
                                tablaProductos.getValueAt(i, 2).toString(),//P.U.
                                tablaProductos.getValueAt(i, 2).toString()};//Importe
                            model.addRow(nuevo);
                        } else {
                            //No tiene clave enterprise
                            noEnterprise = noEnterprise + "\n[" + tablaProductos.getValueAt(i, 0).toString() + "] " + tablaProductos.getValueAt(i, 1).toString();
                        }
                    } catch (Exception ex) {
                        Logger.getLogger(DialogProductos.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    Object nuevo[] = {
                        tablaProductos.getValueAt(i, 4),//Tipo
                        tablaProductos.getValueAt(i, 0).toString(),//Clave
                        tablaProductos.getValueAt(i, 1).toString(),//Descripcion
                        tablaProductos.getValueAt(i, 2).toString(),//P.U.
                        tablaProductos.getValueAt(i, 2).toString()};//Importe
                    model.addRow(nuevo);
                }
            }
            this.panePos.tablaProductos.setModel(model);
            this.panePos.actualizaTotal();
            this.panePos.repaint();
            this.panePos.txtClave.setText("");
            this.panePos.txtClave.requestFocus();
            this.dispose();
        }
        if (noEnterprise.length() > 0) {
            JOptionPane.showMessageDialog(this, "Los siguientes productos no cuentan con una clave enterprise para su procesamiento: " + noEnterprise);
        }

    }

    private void activaDetalle() {
        if ("1".equals(tablaProductos.getValueAt(tablaProductos.getSelectedRow(), 4).toString())) {
            btnDetalle.setEnabled(true);
        } else {
            btnDetalle.setEnabled(false);
        }
    }

    private int getSeleccionados() {
        int count = 0;
        for (int i = 0; i < tablaProductos.getRowCount(); i++) {
            if (Boolean.valueOf(tablaProductos.getValueAt(i, 3).toString())) {
                count++;
            }
        }
        return count;
    }
}
