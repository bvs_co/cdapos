package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.JTableHeader;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Area;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.entities.Xx;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceArea;
import mx.com.cdadb.service.ServiceSucursalLocal;
import mx.com.cdadb.service.ServiceX;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.service.impl.ServiceAreaImpl;
import mx.com.cdadb.service.impl.ServiceSucursalLocalImp;
import mx.com.cdadb.service.impl.ServiceXImpl;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;

/**
 *
 * @author unknown
 */
public class PaneConfig extends javax.swing.JPanel {

    private FrMenu frMenu = null;
    private int opc = 0;
    private Sucursal sucursal = null;
    private List<App> apps = null;
    boolean boolrespaldo;

    public PaneConfig(FrMenu fr, int op) {
        initComponents();
        this.frMenu = fr;
        this.opc = op;
        initData();
        repaintcomponent();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtRazonSocial = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtDomicilioFiscal = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtRfc = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        txtSucursalnombre = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtSucursalDireccion = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtSucursalTelefono = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtNumEnterprise = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtNumContabilidad = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jSeparator11 = new javax.swing.JSeparator();
        jCheckBox1 = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<String>();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtIniciaOrden = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtTerminaOrden = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jLabel21 = new javax.swing.JLabel();
        txtOrderSave = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jSeparator10 = new javax.swing.JSeparator();
        jLabel23 = new javax.swing.JLabel();
        txtOrderIp = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txtOrderPuerto = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel18 = new javax.swing.JLabel();
        txtIniciaFolioVenta = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        jLabel20 = new javax.swing.JLabel();
        txtIniciaFolioDevolucion = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jSeparator9 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btnAgregarFecha = new javax.swing.JButton();
        btnEliminarFecha = new javax.swing.JButton();
        jCalendar1 = new com.toedter.calendar.JCalendar();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTabbedPane1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTabbedPane1KeyPressed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel1.setText("Datos de matríz");

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Razón social:");
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        txtRazonSocial.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtRazonSocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRazonSocialKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Domicilio fiscal:");
        jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        txtDomicilioFiscal.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtDomicilioFiscal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDomicilioFiscalKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("RFC:");
        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        txtRfc.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtRfc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRfcKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Teléfono:");
        jLabel5.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);

        txtTelefono.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTelefonoKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel6.setText("Datos de sucursal");

        jLabel7.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Nombre:");

        txtSucursalnombre.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtSucursalnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSucursalnombreKeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Dirección:");

        txtSucursalDireccion.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtSucursalDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSucursalDireccionKeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Teléfono:");

        txtSucursalTelefono.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtSucursalTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSucursalTelefonoKeyPressed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("# Suc. Enterprise:");

        txtNumEnterprise.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtNumEnterprise.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNumEnterpriseKeyPressed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("# Suc. Contabilidad:");

        txtNumContabilidad.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtNumContabilidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNumContabilidadKeyPressed(evt);
            }
        });

        jLabel25.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel25.setText("Respaldos");

        jCheckBox1.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jCheckBox1.setText("Respaldo de BD");
        jCheckBox1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jCheckBox1StateChanged(evt);
            }
        });
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtDomicilioFiscal, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtRfc, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 739, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelefono)
                            .addComponent(txtRazonSocial)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator1))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator11))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtSucursalTelefono)
                            .addComponent(txtNumEnterprise)
                            .addComponent(txtNumContabilidad)
                            .addComponent(txtSucursalDireccion)
                            .addComponent(txtSucursalnombre, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(154, 154, 154)
                .addComponent(jCheckBox1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtRazonSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDomicilioFiscal, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtRfc, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtSucursalnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtSucursalDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtSucursalTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txtNumEnterprise, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtNumContabilidad, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox1)
                .addContainerGap(89, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Generales", jPanel2);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel12.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel12.setText("Área:");

        jComboBox1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel13.setText("Indique el área que generará las ordenes");

        jLabel14.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel14.setText("Rango de ordenes asignado");

        jLabel15.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel15.setText("Iniciar en:");

        txtIniciaOrden.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtIniciaOrden.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtIniciaOrdenKeyPressed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel16.setText("Terminar en:");

        txtTerminaOrden.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtTerminaOrden.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTerminaOrdenKeyPressed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel21.setText("Guardar ordenes en:");

        txtOrderSave.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N

        jButton1.setText("...");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel23.setText("Enviar ordenes a puerto:");

        txtOrderIp.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N

        jLabel24.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel24.setText("Enviar ordenes a direccion IP:");

        txtOrderPuerto.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator4)
            .addComponent(jSeparator5)
            .addComponent(jSeparator8)
            .addComponent(jSeparator10)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtOrderIp)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIniciaOrden))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel16)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTerminaOrden))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(txtOrderSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtOrderPuerto)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14)
                            .addComponent(jLabel21)
                            .addComponent(jLabel24)
                            .addComponent(jLabel23))
                        .addGap(0, 563, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txtIniciaOrden, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtTerminaOrden, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtOrderSave, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtOrderIp, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtOrderPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(142, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Ordenes de laboratorio", jPanel3);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel17.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel17.setText("De ventas");

        jLabel18.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel18.setText("Reiniciar en:");

        txtIniciaFolioVenta.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtIniciaFolioVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtIniciaFolioVentaKeyPressed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel19.setText("De cancelaciones:");

        jLabel20.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel20.setText("Reiniciar en:");

        txtIniciaFolioDevolucion.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtIniciaFolioDevolucion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtIniciaFolioDevolucionKeyPressed(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel22.setText("Excluir:");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CONT.", "FECHA"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        btnAgregarFecha.setText("Agregar");
        btnAgregarFecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarFechaActionPerformed(evt);
            }
        });

        btnEliminarFecha.setText("Eliminar");
        btnEliminarFecha.setEnabled(false);
        btnEliminarFecha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarFechaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator6)
            .addComponent(jSeparator7)
            .addComponent(jSeparator9)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel19)
                    .addComponent(jLabel22)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCalendar1, javax.swing.GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnAgregarFecha)
                                    .addComponent(btnEliminarFecha))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIniciaFolioVenta))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtIniciaFolioDevolucion)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txtIniciaFolioVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(txtIniciaFolioDevolucion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(btnAgregarFecha)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminarFecha)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCalendar1, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Folios", jPanel4);

        btnGuardar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/save_accept_x16.png"))); // NOI18N
        btnGuardar.setText("Guardar [F5]");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        btnGuardar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGuardarKeyPressed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cancel_x16.png"))); // NOI18N
        btnCancelar.setText("Cancelar [Esc]");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardar)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jTabbedPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        getAccessibleContext().setAccessibleDescription("config");
    }// </editor-fold>//GEN-END:initComponents

    private void txtRazonSocialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRazonSocialKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtRazonSocialKeyPressed

    private void txtDomicilioFiscalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDomicilioFiscalKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtDomicilioFiscalKeyPressed

    private void txtRfcKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRfcKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtRfcKeyPressed

    private void txtTelefonoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtTelefonoKeyPressed

    private void txtSucursalnombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSucursalnombreKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtSucursalnombreKeyPressed

    private void txtSucursalDireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSucursalDireccionKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtSucursalDireccionKeyPressed

    private void txtSucursalTelefonoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSucursalTelefonoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtSucursalTelefonoKeyPressed

    private void txtNumEnterpriseKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumEnterpriseKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtNumEnterpriseKeyPressed

    private void txtNumContabilidadKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNumContabilidadKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtNumContabilidadKeyPressed

    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jComboBox1KeyPressed

    private void txtIniciaOrdenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIniciaOrdenKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtIniciaOrdenKeyPressed

    private void txtTerminaOrdenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTerminaOrdenKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtTerminaOrdenKeyPressed

    private void txtIniciaFolioVentaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIniciaFolioVentaKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtIniciaFolioVentaKeyPressed

    private void txtIniciaFolioDevolucionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIniciaFolioDevolucionKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtIniciaFolioDevolucionKeyPressed

    private void jTabbedPane1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTabbedPane1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jTabbedPane1KeyPressed

    private void btnGuardarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGuardarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnGuardarKeyPressed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        try {
            save();
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        //FileNameExtensionFilter filtro = new FileNameExtensionFilter("*.xls", "xls");
        //fileChooser.setFileFilter(filtro);
        int returnValue = fileChooser.showOpenDialog(this);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getCurrentDirectory();
            setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
            //readXls(selectedFile.getAbsoluteFile().toString());
            txtOrderSave.setText(selectedFile.getAbsolutePath());
            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnAgregarFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarFechaActionPerformed
        try {
            ServiceX sx = new ServiceXImpl();
            Xx x = new Xx();
            x.setXxDate(jCalendar1.getDate());
            if (sx.add(x)) {
                fillXx();
            } else {
                JOptionPane.showMessageDialog(frMenu, "Ocurrió un error al intentar guardar la fecha.");
            }
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }//GEN-LAST:event_btnAgregarFechaActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        if (jTable1.getSelectedRowCount() > 0) {
            btnEliminarFecha.setEnabled(true);
        } else {
            btnEliminarFecha.setEnabled(false);
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void btnEliminarFechaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarFechaActionPerformed
        try {
            ServiceX sx = new ServiceXImpl();
            Xx x = sx.getBy("xxId = " + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString());
            x.setXxDate(jCalendar1.getDate());
            if (sx.delete(x)) {
                fillXx();
            } else {
                JOptionPane.showMessageDialog(frMenu, "Ocurrió un error al intentar eliminar la fecha.");
            }
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }//GEN-LAST:event_btnEliminarFechaActionPerformed

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jCheckBox1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jCheckBox1StateChanged
        if(jCheckBox1.isSelected()){
        boolrespaldo=true;
        }else{
        boolrespaldo=false;
        }
    }//GEN-LAST:event_jCheckBox1StateChanged


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarFecha;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminarFecha;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton jButton1;
    private com.toedter.calendar.JCalendar jCalendar1;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtDomicilioFiscal;
    private javax.swing.JTextField txtIniciaFolioDevolucion;
    private javax.swing.JTextField txtIniciaFolioVenta;
    private javax.swing.JTextField txtIniciaOrden;
    private javax.swing.JTextField txtNumContabilidad;
    private javax.swing.JTextField txtNumEnterprise;
    private javax.swing.JTextField txtOrderIp;
    private javax.swing.JTextField txtOrderPuerto;
    private javax.swing.JTextField txtOrderSave;
    private javax.swing.JTextField txtRazonSocial;
    private javax.swing.JTextField txtRfc;
    private javax.swing.JTextField txtSucursalDireccion;
    private javax.swing.JTextField txtSucursalTelefono;
    private javax.swing.JTextField txtSucursalnombre;
    private javax.swing.JTextField txtTelefono;
    private javax.swing.JTextField txtTerminaOrden;
    // End of variables declaration//GEN-END:variables

    private void initData() {
        try {
            ServiceSucursalLocal sSucursal = new ServiceSucursalLocalImp();
            List<Sucursal> sucursales = sSucursal.findAll();

            if (sucursales.size() > 0) {
                txtSucursalnombre.setText(sucursales.get(0).getSucursalNombre());
                txtSucursalDireccion.setText(sucursales.get(0).getSucursalDireccion());
                txtSucursalTelefono.setText(sucursales.get(0).getSucursalTel());
                txtNumEnterprise.setText(String.valueOf(sucursales.get(0).getSucursalEnterpriseId()));
                txtNumContabilidad.setText(String.valueOf(sucursales.get(0).getSucursalContaId()));
                this.sucursal = sucursales.get(0);
            }
            fillAreas();

            ServiceApp sApp = new ServiceAppImpl();
            List<App> apps = sApp.findAll();

            if (apps.size() > 0) {
                for (App app : apps) {
                    switch (app.getAppNombre()) {
                        case ConstantConfig.CONFIG_COMPANY_RFC:
                            txtRfc.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_COMPANY_BUSSINES_NAME:
                            txtRazonSocial.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_COMPANY_BUSSINES_ADDRES:
                            txtDomicilioFiscal.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_COMPANY_TELEPHONE:
                            txtTelefono.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_ORDER_INIT:
                            txtIniciaOrden.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_ORDER_FINALIZE:
                            txtTerminaOrden.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_FOLIO_DEVOLUTION_INIT:
                            txtIniciaFolioDevolucion.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_FOLIO_SALE_INIT:
                            txtIniciaFolioVenta.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_LABORATORY_AREA:
                            ServiceArea sArea = new ServiceAreaImpl();
                            Area area = sArea.getBy("a.areaId = " + app.getAppValor());
                            jComboBox1.setSelectedItem(area.getAreaId() + ".- " + area.getAreaNombre());
                            break;
                        case ConstantConfig.CONFIG_ENTERPRISE_ORDER_SAVE:
                            txtOrderSave.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_ORDER_HOST:
                            txtOrderIp.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_ORDER_PORT:
                            txtOrderPuerto.setText(app.getAppValor());
                            break;
                        case ConstantConfig.CONFIG_SQL_BACKUP:
                            boolrespaldo=Boolean.parseBoolean(app.getAppValor());
                            break;
                    }
                }
                this.apps = apps;
            }

            fillXx();
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }

    private void fillAreas() {
        try {
            ServiceArea sArea = new ServiceAreaImpl();
            List<Area> areas = sArea.findAll();
            jComboBox1.removeAllItems();
            jComboBox1.addItem("Seleccione...");
            for (Area area : areas) {
                jComboBox1.addItem(area.getAreaId() + ".- " + area.getAreaNombre());
            }
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 116://F5 Guardar
                btnCancelarActionPerformed(null);
                break;
            case 27://Esc
                btnCancelarActionPerformed(null);
                break;
        }
    }

    private void save() throws Exception {
        ServiceApp sApp = new ServiceAppImpl();
        if (apps.size() > 0) {
            for (App app : this.apps) {
                switch (app.getAppNombre()) {
                    case ConstantConfig.CONFIG_COMPANY_RFC:
                        app.setAppValor(txtRfc.getText());
                        break;
                    case ConstantConfig.CONFIG_COMPANY_BUSSINES_NAME:
                        app.setAppValor(txtRazonSocial.getText());
                        break;
                    case ConstantConfig.CONFIG_COMPANY_BUSSINES_ADDRES:
                        app.setAppValor(txtDomicilioFiscal.getText());
                        break;
                    case ConstantConfig.CONFIG_COMPANY_TELEPHONE:
                        app.setAppValor(txtTelefono.getText());
                        break;
                    case ConstantConfig.CONFIG_ORDER_INIT:
                        app.setAppValor(txtIniciaOrden.getText());
                        break;
                    case ConstantConfig.CONFIG_ORDER_FINALIZE:
                        app.setAppValor(txtTerminaOrden.getText());
                        break;
                    case ConstantConfig.CONFIG_FOLIO_DEVOLUTION_INIT:
                        app.setAppValor(txtIniciaFolioDevolucion.getText());
                        break;
                    case ConstantConfig.CONFIG_FOLIO_SALE_INIT:
                        app.setAppValor(txtIniciaFolioVenta.getText());
                        break;
                    case ConstantConfig.CONFIG_LABORATORY_AREA:
                        List<String> arr = Arrays.asList(jComboBox1.getSelectedItem().toString().split(".- "));
                        app.setAppValor(arr.get(0));
                        break;
                    case ConstantConfig.CONFIG_ENTERPRISE_ORDER_SAVE:
                        app.setAppValor(txtOrderSave.getText());
                        break;
                    case ConstantConfig.CONFIG_ORDER_HOST:
                        app.setAppValor(txtOrderIp.getText());
                        break;
                    case ConstantConfig.CONFIG_ORDER_PORT:
                        app.setAppValor(txtOrderPuerto.getText());
                        break;
                    case ConstantConfig.CONFIG_SQL_BACKUP:
                        app.setAppValor(String.valueOf(boolrespaldo));
                        break;
                }
            }
        }

        ServiceSucursalLocal ssuc = new ServiceSucursalLocalImp();
        List<Sucursal> sucursales = ssuc.findAll();
        if (sucursales.size() > 0) {
            sucursales.get(0).setSucursalDireccion(txtSucursalDireccion.getText());
            sucursales.get(0).setSucursalEnterpriseId(Integer.parseInt(txtNumEnterprise.getText()));
            sucursales.get(0).setSucursalContaId(Integer.parseInt(txtNumContabilidad.getText()));
            sucursales.get(0).setSucursalFmodif(new java.util.Date());
            sucursales.get(0).setSucursalNombre(txtSucursalnombre.getText());
            sucursales.get(0).setSucursalTel(txtSucursalTelefono.getText());
            ssuc.edit(sucursales.get(0));
        }
        try {
            if (sApp.editList(this.apps)) {
                JOptionPane.showMessageDialog(null, "Cambios guardados correctamente.");
            }
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }

    private void fillXx() {
        try {
            ServiceX sx = new ServiceXImpl();
            List<Xx> arr = sx.findAll();

            int i = 0;
            Object[][] datos = new Object[arr.size()][3];
            for (Xx x : arr) {
                datos[i][0] = x.getXxId();
                datos[i][1] = FormatDateUtil.SFD_YYYYMMDD.format(x.getXxDate());
                i++;
            }

            jTable1.setModel(new javax.swing.table.DefaultTableModel(
                    datos,
                    new String[]{
                        "CONT.", "FECHA"
                    }
            ) {
                boolean[] canEdit = new boolean[]{
                    false, false
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            });

            jScrollPane1.setViewportView(jTable1);
            JTableHeader jtableHeader = jTable1.getTableHeader();
            jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
            jTable1.setTableHeader(jtableHeader);

            jTable1.setRowHeight(30);

            jTable1.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.PK_ID.toString()));
            jTable1.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_CENTER.toString()));

        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }
    
     private void repaintcomponent() {
        jCheckBox1.setSelected(boolrespaldo);
        
    }
}
