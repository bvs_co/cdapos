package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashSet;
import javax.swing.JOptionPane;
import mx.com.cdadb.entities.Enterprise;
import mx.com.cdadb.service.ServiceEnterprise;
import mx.com.cdadb.service.impl.ServiceEnterpriseImpl;
import java.util.List;
import java.util.Set;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableRowSorter;
import mx.com.cdadb.entities.Clave;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.util.DialogWait;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author unknown
 */
public class PaneConnEnterprise extends javax.swing.JPanel {

    private FrMenu frMenu2 = null;
    private int opc = -1;
    private Logger log = LoggerFactory.getLogger(PaneConnEnterprise.class);

    public PaneConnEnterprise(FrMenu fr, int op) {
        initComponents();
        this.frMenu2 = fr;
        this.opc = op;
        DialogWait dw = new DialogWait(this.frMenu2, true, this);
        dw.setVisible(true);
        loadPermisions(this.frMenu2.user);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        btnBuscar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        txtBuscar.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBuscarActionPerformed(evt);
            }
        });
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        btnBuscar.setToolTipText("Buscar [F5]");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnNuevo.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/add_x16.png"))); // NOI18N
        btnNuevo.setMnemonic('N');
        btnNuevo.setText("Nuevo [F2]");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        btnNuevo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnNuevoKeyPressed(evt);
            }
        });

        btnEditar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/pen_write_edit_x16.png"))); // NOI18N
        btnEditar.setMnemonic('d');
        btnEditar.setText("Editar [F4]");
        btnEditar.setEnabled(false);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        btnEditar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEditarKeyPressed(evt);
            }
        });

        btnEliminar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/delete_x16.png"))); // NOI18N
        btnEliminar.setMnemonic('E');
        btnEliminar.setText("Eliminar [F3]");
        btnEliminar.setEnabled(false);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        btnEliminar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEliminarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNuevo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                    .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "CÓDIGO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable1.setAutoscrolls(false);
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscar))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 871, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addGap(3, 3, 3)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 219, Short.MAX_VALUE)
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleDescription("enterprise");
    }// </editor-fold>//GEN-END:initComponents

    private void txtBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBuscarActionPerformed

    private void txtBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtBuscarKeyPressed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        txtBuscar.setText(txtBuscar.getText().toUpperCase());
        findCodes(txtBuscar.getText());
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        txtBuscarKeyReleased(null);
        txtBuscar.requestFocus();
        jTable1MouseClicked(null);
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        managedButtons();
        if (evt.getClickCount() == 2) {
            edit();
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jTable1KeyPressed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        DialogNuevoConnEnterprise dialogNuevoConnEnterprise = new DialogNuevoConnEnterprise(this.frMenu2, true, 1, this);
        dialogNuevoConnEnterprise.setVisible(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnNuevoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnNuevoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnNuevoKeyPressed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        edit();
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnEditarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEditarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnEditarKeyPressed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        if (jTable1.getSelectedRowCount() > 0) {
            int op = JOptionPane.showConfirmDialog(this, "Esta seguro de eliminar la clave seleccionada.", "Confirme.", JOptionPane.YES_NO_OPTION);
            if (op == 0) {
                try {
                    ServiceEnterprise sEnterprise = new ServiceEnterpriseImpl();
                    Enterprise p = sEnterprise.getBy("e.enterpriseId = '" + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString() + "'");
                    if (sEnterprise.delete(p)) {
                        JOptionPane.showMessageDialog(this, "Clave eliminada, correctamente.");
                        initData();
                        jTable1MouseClicked(null);
                    }
                } catch (Exception ex) {
                    log.error(ex.getMessage());
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione el registro a eliminar.");
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnEliminarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEliminarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnEliminarKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables

    public void initData() {
        try {
            ServiceEnterprise sEnterprise = new ServiceEnterpriseImpl();
            List<Enterprise> arr = sEnterprise.findAll();
            log.info("Cargando claves enterprise existentes.......");
            //Para revisar, si es que hay id's en tabla clave
            /*arr.stream().forEach(p ->{
                System.out.println("BB:"+p.getEnterpriseId()+"-"+p.getEnterpriseClave());
                p.getClaves().stream().forEach(c->{
                    System.out.println("    CC:"+c.getProducto().getProductoNombre());
                });
            });*/
            fillTable(arr);
        } catch (Exception ex) {
            if (ex.getMessage() != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            log.error(ex.getMessage());
        }
    }

    public void fillTable(List<Enterprise> arr) {
        int i = 0;
        Object[][] datos = new Object[arr.size()][4];
        for (Enterprise e : arr) {
            datos[i][0] = e.getEnterpriseId();
            datos[i][1] = e.getEnterpriseClave();
            String desc = "";
            for (Object o : e.getClaves()) {
                Clave clave = (Clave) o;
                desc = desc + clave.getProducto().getProductoNombre() + "|";
            }
            datos[i][2] = desc.substring(0, desc.length() - 1);
            i++;
        }

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                new String[]{
                    "ID", "CÓDIGO", "DESCRIPCIÓN"
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);

        jTable1.setAutoscrolls(false);

        jTable1.getTableHeader().setReorderingAllowed(false);

        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });

        jScrollPane1.setViewportView(jTable1);

        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setMinWidth(100);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setMinWidth(150);
            jTable1.getColumnModel().getColumn(1).setMaxWidth(150);
        }

        JTableHeader jtableHeader = jTable1.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        jTable1.setTableHeader(jtableHeader);

        jTable1.setRowHeight(30);

        jTable1.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ORDER_ID.toString()));
        jTable1.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ENTERPRISE_ID.toString()));
        jTable1.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
    }

    private void managedButtons() {
        if (jTable1.getSelectedRowCount() > 0) {
            btnEditar.setEnabled(true);
            btnEliminar.setEnabled(true);
        } else {
            btnEditar.setEnabled(false);
            btnEliminar.setEnabled(false);
        }
    }
    //Evento para usar las teclas especiales 

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 113://F2 Nuevo
                if (btnNuevo.isVisible()) {
                    btnNuevoActionPerformed(null);
                }
                break;
            case 114://F3 Eliminar
                if (btnEliminar.isVisible()) {
                    btnEliminarActionPerformed(null);
                }
                break;
            case 115://F4
                if (btnEditar.isVisible()) {
                    btnEditarActionPerformed(null);
                }
                break;
        }
    }

    private void findCodes(String text) {
        List<RowFilter<Object, Object>> row = null;
        row = new ArrayList();
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>((DefaultTableModel) jTable1.getModel());
        try {
            row.add(RowFilter.regexFilter(text, 0));
            row.add(RowFilter.regexFilter(text, 1));
            row.add(RowFilter.regexFilter(text, 2));
        } catch (java.util.regex.PatternSyntaxException e) {
            JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + e.getMessage());
        }
        jTable1.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.orFilter(row));
    }

    private void loadPermisions(User usuario) {
        Set<String> permisions = new HashSet<>();
        //quitar duplicados
        usuario.getRol().getPermisos().stream().forEach(p -> {
            permisions.add(p.getAccion().getAccionDesc());
        });
        permisions.stream().forEach(permiso -> {
            if ("AGREGAR ENTERPRISE".equals(permiso)) {
                btnNuevo.setVisible(true);
            }
            if ("MODIFICAR ENTERPRISE".equals(permiso)) {
                btnEditar.setVisible(true);
            }
            if ("ELIMINAR ENTERPRISE".equals(permiso)) {
                btnEliminar.setVisible(true);
            }
        });
    }

    private void edit() {
        try {
            setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
            ServiceEnterprise sEnt = new ServiceEnterpriseImpl();
            Enterprise e = sEnt.getBy("e.enterpriseId = " + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString());
            DialogNuevoConnEnterprise dialogNuevoConnEnterprise = new DialogNuevoConnEnterprise(this.frMenu2, true, 2, this, e);
            dialogNuevoConnEnterprise.setVisible(true);
            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }
}
