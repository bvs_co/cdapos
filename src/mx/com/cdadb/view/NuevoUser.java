package mx.com.cdadb.view;

import java.awt.Event;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.swing.InputMap;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import mx.com.cdadb.entities.Rol;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.service.ServiceRol;
import mx.com.cdadb.service.ServiceUser;
import mx.com.cdadb.service.impl.ServiceRolImpl;
import mx.com.cdadb.service.impl.ServiceUserImpl;
import mx.com.cdadb.util.KeyGenerator;
import mx.com.cdadb.util.LoggerUtil;

public class NuevoUser extends javax.swing.JDialog {

    private PaneUsuarios paneUsuarios = null;
    private User usuarioEnviado = null;
    private FrMenu frMenu = null;
    int opc = -1;

    public NuevoUser(java.awt.Frame parent, boolean modal, int op, PaneUsuarios pane) {
        super(parent, modal);
        initComponents();
        this.opc = op;
        this.paneUsuarios = pane;
        this.setLocationRelativeTo(null);
        this.setTitle("Nuevo usuario.");
        lblValidaPasswNo.setVisible(false);
        lblValidaPasswOk.setVisible(false);
        lblValidaUserNo.setVisible(false);
        lblValidaUserOk.setVisible(false);
        initData();
    }

    public NuevoUser(java.awt.Frame parent, boolean modal, int op, FrMenu fr) {
        super(parent, modal);
        initComponents();
        this.opc = op;
        this.frMenu = fr;
        this.setLocationRelativeTo(null);
        this.setTitle("Nuevo usuario.");
        lblValidaPasswNo.setVisible(false);
        lblValidaPasswOk.setVisible(false);
        lblValidaUserNo.setVisible(false);
        lblValidaUserOk.setVisible(false);
        initData();
    }

    public NuevoUser(java.awt.Frame parent, boolean modal, int op, PaneUsuarios pane, User u) {
        super(parent, modal);
        initComponents();
        this.usuarioEnviado = u;
        this.opc = op;
        this.paneUsuarios = pane;
        this.setLocationRelativeTo(null);
        this.setTitle("Modificar usuario.");
        initData();
        llenaDatos(this.usuarioEnviado);
        txtNombre.grabFocus();
        lblValidaPasswNo.setVisible(false);
        lblValidaPasswOk.setVisible(false);
        lblValidaUserNo.setVisible(false);
        lblValidaUserOk.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtPaterno = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtMaterno = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtPassw = new javax.swing.JPasswordField();
        jLabel10 = new javax.swing.JLabel();
        txtRePassw = new javax.swing.JPasswordField();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        lblValidaUserOk = new javax.swing.JLabel();
        lblValidaUserNo = new javax.swing.JLabel();
        lblValidaPasswOk = new javax.swing.JLabel();
        lblValidaPasswNo = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Nombre:");

        txtNombre.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        txtNombre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNombreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNombreFocusLost(evt);
            }
        });
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNombreKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("A. Paterno:");

        txtPaterno.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        txtPaterno.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPaternoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPaternoFocusLost(evt);
            }
        });
        txtPaterno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPaternoKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("A. Materno:");

        txtMaterno.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        txtMaterno.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtMaternoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtMaternoFocusLost(evt);
            }
        });
        txtMaterno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMaternoKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Telefono:");

        txtTelefono.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        txtTelefono.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtTelefonoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtTelefonoFocusLost(evt);
            }
        });
        txtTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTelefonoKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        jLabel6.setText("Datos personales");

        jLabel7.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        jLabel7.setText("Datos de usuario");

        jLabel8.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Usuario:");

        txtUsuario.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        txtUsuario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUsuarioFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtUsuarioFocusLost(evt);
            }
        });
        txtUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUsuarioKeyPressed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Contraseña:");

        txtPassw.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        InputMap map2 = txtPassw.getInputMap(txtPassw.WHEN_FOCUSED);
        map2.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        txtPassw.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPasswFocusGained(evt);
            }
        });
        txtPassw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPasswKeyPressed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Repita contraseña:");

        txtRePassw.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        InputMap map3 = txtRePassw.getInputMap(txtRePassw.WHEN_FOCUSED);
        map3.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        txtRePassw.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtRePasswFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtRePasswFocusLost(evt);
            }
        });
        txtRePassw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRePasswKeyPressed(evt);
            }
        });

        btnGuardar.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/save_accept_x16.png"))); // NOI18N
        btnGuardar.setText("Guardar [F5]");
        btnGuardar.setEnabled(false);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        btnGuardar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGuardarKeyPressed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cancel_x16.png"))); // NOI18N
        btnCancelar.setText("Cancelar [Esc]");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        lblValidaUserOk.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblValidaUserOk.setForeground(new java.awt.Color(51, 153, 0));
        lblValidaUserOk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/accept_x10.png"))); // NOI18N
        lblValidaUserOk.setText("Usuario disponible");

        lblValidaUserNo.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        lblValidaUserNo.setForeground(new java.awt.Color(255, 0, 0));
        lblValidaUserNo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/delete_x10.png"))); // NOI18N
        lblValidaUserNo.setText("Usuario ocupado");

        lblValidaPasswOk.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblValidaPasswOk.setForeground(new java.awt.Color(51, 153, 0));
        lblValidaPasswOk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/accept_x10.png"))); // NOI18N
        lblValidaPasswOk.setText("Coinciden las contraseñas");

        lblValidaPasswNo.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        lblValidaPasswNo.setForeground(new java.awt.Color(255, 0, 0));
        lblValidaPasswNo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/delete_x10.png"))); // NOI18N
        lblValidaPasswNo.setText("Contraseñas no coinciden");

        jLabel5.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Rol:");

        jComboBox1.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jComboBox1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jSeparator2)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombre)
                            .addComponent(txtTelefono)
                            .addComponent(txtMaterno)
                            .addComponent(txtPaterno)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUsuario)
                            .addComponent(txtPassw)
                            .addComponent(txtRePassw)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblValidaPasswOk)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(lblValidaPasswNo))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblValidaUserOk)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(lblValidaUserNo)))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(jComboBox1, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPaterno, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtMaterno, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7)
                .addGap(7, 7, 7)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblValidaUserOk)
                    .addComponent(lblValidaUserNo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtPassw, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRePassw, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblValidaPasswOk)
                    .addComponent(lblValidaPasswNo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtNombreKeyPressed

    private void txtPaternoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaternoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtPaternoKeyPressed

    private void txtMaternoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMaternoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtMaternoKeyPressed

    private void txtTelefonoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtTelefonoKeyPressed

    private void txtUsuarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUsuarioKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtUsuarioKeyPressed

    private void txtPasswKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPasswKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtPasswKeyPressed

    private void txtRePasswKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRePasswKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtRePasswKeyPressed

    private void txtNombreFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreFocusLost
        Mayus(txtNombre);
    }//GEN-LAST:event_txtNombreFocusLost

    private void txtPaternoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPaternoFocusLost
        Mayus(txtPaterno);
    }//GEN-LAST:event_txtPaternoFocusLost

    private void txtMaternoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMaternoFocusLost
        Mayus(txtMaterno);
    }//GEN-LAST:event_txtMaternoFocusLost

    private void txtTelefonoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtTelefonoFocusLost
        Mayus(txtTelefono);
    }//GEN-LAST:event_txtTelefonoFocusLost

    private void txtUsuarioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUsuarioFocusLost
        try {
            if (!"".equals(txtUsuario.getText().trim())) {
                ServiceUser sUsuario = new ServiceUserImpl();
                List<User> arr = sUsuario.findBy("u.userUsuario = '" + txtUsuario.getText().trim() + "'");
                switch (this.opc) {
                    case 0:
                        if (arr.size() > 0 && arr != null) {
                            lblValidaUserNo.setVisible(true);
                            lblValidaUserOk.setVisible(false);
                        } else {
                            lblValidaUserNo.setVisible(false);
                            lblValidaUserOk.setVisible(true);
                        }
                        break;
                    case 1:
                        if (!this.usuarioEnviado.getUserUsuario().equals(txtUsuario.getText()) && arr != null) {
                            if (arr.size() > 1 && arr != null) {
                                lblValidaUserNo.setVisible(true);
                                lblValidaUserOk.setVisible(false);
                            } else {
                                lblValidaUserNo.setVisible(false);
                                lblValidaUserOk.setVisible(true);
                            }
                        } else {
                            lblValidaUserNo.setVisible(false);
                            lblValidaUserOk.setVisible(true);
                        }
                        break;
                    case 2:
                        if (arr.size() > 0 && arr != null) {
                            lblValidaUserNo.setVisible(true);
                            lblValidaUserOk.setVisible(false);
                        } else {
                            lblValidaUserNo.setVisible(false);
                            lblValidaUserOk.setVisible(true);
                        }
                        break;
                }
            }
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
    }//GEN-LAST:event_txtUsuarioFocusLost

    private void txtNombreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreFocusGained
        focus(txtNombre);
    }//GEN-LAST:event_txtNombreFocusGained

    private void txtPaternoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPaternoFocusGained
        focus(txtPaterno);
    }//GEN-LAST:event_txtPaternoFocusGained

    private void txtMaternoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMaternoFocusGained
        focus(txtMaterno);
    }//GEN-LAST:event_txtMaternoFocusGained

    private void txtTelefonoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtTelefonoFocusGained
        focus(txtTelefono);
    }//GEN-LAST:event_txtTelefonoFocusGained

    private void txtUsuarioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUsuarioFocusGained
        focus(txtUsuario);
    }//GEN-LAST:event_txtUsuarioFocusGained

    private void txtPasswFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPasswFocusGained
        focus(txtPassw);
    }//GEN-LAST:event_txtPasswFocusGained

    private void txtRePasswFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtRePasswFocusGained
        focus(txtRePassw);
    }//GEN-LAST:event_txtRePasswFocusGained

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        switch (this.opc) {
            case 0://Alta
                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                if (!txtNombre.getText().isEmpty() && !txtPaterno.getText().isEmpty()
                        && !txtMaterno.getText().isEmpty()
                        && !txtUsuario.getText().isEmpty()
                        && !txtPassw.getText().isEmpty()
                        && !txtRePassw.getText().isEmpty()) {
                    if (guardar()) {
                        JOptionPane.showMessageDialog(this, "Usuario guardado correctamente.");
                        this.paneUsuarios.initData();
                        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        this.dispose();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Todos los campos marcados con **, son obligatorios.");
                    txtNombre.selectAll();
                    txtNombre.requestFocus();
                }
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                break;
            case 1://Modificacion
                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                if (!txtNombre.getText().isEmpty() && !txtPaterno.getText().isEmpty()
                        && !txtMaterno.getText().isEmpty()
                        && !txtUsuario.getText().isEmpty()
                        && !txtPassw.getText().isEmpty()
                        && !txtRePassw.getText().isEmpty()) {
                    if (guardar()) {
                        JOptionPane.showMessageDialog(this, "Cambios guardados correctamente.");
                        this.paneUsuarios.initData();
                        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        this.dispose();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Todos los campos marcados con **, son obligatorios.");
                    txtNombre.selectAll();
                    txtNombre.requestFocus();
                }
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                break;
            case 2://Alta
                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                if (!txtNombre.getText().isEmpty() && !txtPaterno.getText().isEmpty()
                        && !txtMaterno.getText().isEmpty()
                        && !txtUsuario.getText().isEmpty()
                        && !txtPassw.getText().isEmpty()
                        && !txtRePassw.getText().isEmpty()) {
                    if (guardar()) {
                        JOptionPane.showMessageDialog(this, "Usuario guardado correctamente.");
                        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        this.dispose();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Todos los campos marcados con **, son obligatorios.");
                    txtNombre.selectAll();
                    txtNombre.requestFocus();
                }
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                break;
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void txtRePasswFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtRePasswFocusLost
        if (!"".equals(txtPassw.getText().trim()) && !"".equals(txtRePassw.getText().trim())) {
            if (txtPassw.getText().equals(txtRePassw.getText())) {
                lblValidaPasswOk.setVisible(true);
                lblValidaPasswNo.setVisible(false);
                if (lblValidaUserOk.isVisible()) {
                    btnGuardar.setEnabled(true);
                } else {
                    btnGuardar.setEnabled(false);
                }
            } else {
                lblValidaPasswOk.setVisible(false);
                lblValidaPasswNo.setVisible(true);
                btnGuardar.setEnabled(false);
            }
        }
    }//GEN-LAST:event_txtRePasswFocusLost

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void btnGuardarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGuardarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnGuardarKeyPressed

    private void jComboBox1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jComboBox1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jComboBox1KeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NuevoUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NuevoUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NuevoUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NuevoUser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                NuevoUser dialog = new NuevoUser(new javax.swing.JFrame(), true, 0, null, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblValidaPasswNo;
    private javax.swing.JLabel lblValidaPasswOk;
    private javax.swing.JLabel lblValidaUserNo;
    private javax.swing.JLabel lblValidaUserOk;
    private javax.swing.JTextField txtMaterno;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JPasswordField txtPassw;
    private javax.swing.JTextField txtPaterno;
    private javax.swing.JPasswordField txtRePassw;
    private javax.swing.JTextField txtTelefono;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 27://Esc
                dispose();
                break;
            case 116://F5
                break;
        }
    }

    private void focus(JTextField txt) {
        txt.selectAll();
        txt.grabFocus();
    }

    private boolean guardar() {
        boolean logrado = false;
        try {
            ServiceUser sCliente = new ServiceUserImpl();
            User usuario = getUsuario();
            switch (this.opc) {
                case 0://Nuevo registro
                    usuario.setUserFcreacion(new Date());
                    usuario.setUserFmodif(new Date());
                    if (sCliente.add(usuario)) {
                        logrado = true;
                    }
                    break;
                case 1://Editar registro
                    usuario.setUserId(usuarioEnviado.getUserId());
                    usuario.setUserStatus(usuarioEnviado.getUserStatus());
                    usuario.setUserFcreacion(usuarioEnviado.getUserFcreacion());
                    usuario.setUserFmodif(usuarioEnviado.getUserFmodif());
                    if (!compara(usuario, usuarioEnviado)) {//Hay algun cambio
                        usuario.setUserFmodif(new Date());
                        if (sCliente.edit(usuario)) {
                            logrado = true;
                        }
                    } else {//No hay cambios
                        JOptionPane.showMessageDialog(null, "No se detectaron cambios.");
                        this.dispose();
                        logrado = true;
                    }
                    break;
                case 2://Nuevo registro
                    usuario.setUserFcreacion(new Date());
                    usuario.setUserFmodif(new Date());
                    if (sCliente.add(usuario)) {
                        logrado = true;
                    }
                    break;
            }

        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
        return logrado;
    }

    private User getUsuario() {
        User usuario = new User();
        try {
            usuario.setUserId(-1);
            usuario.setUserNombre(txtNombre.getText().trim());
            usuario.setUserPaterno(txtPaterno.getText().trim());
            usuario.setUserMaterno(txtMaterno.getText().trim());
            usuario.setUserTelefono(txtTelefono.getText());
            usuario.setUserUsuario(txtUsuario.getText());

            StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
            encryptor.setPassword(KeyGenerator.PASSWORD_USER_CDA_KEY);
            String p = encryptor.encrypt(txtPassw.getText());
            usuario.setUserPassw(p);
            usuario.setUserStatus("1");

            ServiceRol sRol = new ServiceRolImpl();
            List<String> arr = Arrays.asList(jComboBox1.getSelectedItem().toString().split(".- "));
            Rol rol = sRol.getBy("r.rolId = " + Integer.parseInt(arr.get(0)));
            usuario.setRol(rol);
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
        return usuario;
    }

    private boolean compara(User usr1, User usr2) {
        boolean res = false;
        if (usr1.getUserPassw().equals(usr2.getUserPassw())
                && usr1.getUserMaterno().equals(usr2.getUserMaterno())
                && usr1.getUserNombre().equals(usr2.getUserNombre())
                && usr1.getUserUsuario().equals(usr2.getUserUsuario())
                && usr1.getUserPaterno().equals(usr2.getUserPaterno())
                && usr1.getUserTelefono().equals(usr2.getUserTelefono())) {
            res = true;
        }
        return res;
    }

    private void llenaDatos(User u) {
        txtMaterno.setText(u.getUserMaterno());
        txtNombre.setText(u.getUserNombre());
        StandardPBEStringEncryptor s = new StandardPBEStringEncryptor();
        s.setPassword(KeyGenerator.PASSWORD_USER_CDA_KEY);
        txtPassw.setText(s.decrypt(u.getUserPassw()));
        txtPaterno.setText(u.getUserPaterno());
        txtRePassw.setText(s.decrypt(u.getUserPassw()));
        txtTelefono.setText(u.getUserTelefono());
        txtUsuario.setText(u.getUserUsuario());
        jComboBox1.setSelectedItem(u.getRol().getRolId() + ".- " + u.getRol().getRolNombre());
    }

    public static void Mayus(javax.swing.JTextField c) {
        String toUpperCase = c.getText().toUpperCase();
        c.setText(toUpperCase);
    }

    private void initData() {
        try {
            ServiceRol sRol = new ServiceRolImpl();
            List<Rol> roles = sRol.findAll();
            jComboBox1.removeAllItems();
            jComboBox1.addItem("Seleccione...");
            roles.forEach(rol -> {
                jComboBox1.addItem(rol.getRolId() + ".- " + rol.getRolNombre());
            });
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
    }
}
