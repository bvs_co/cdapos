package mx.com.cdadb.view;

import java.awt.FlowLayout;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.JTableHeader;
import mx.com.cdadb.entities.Permiso;
import mx.com.cdadb.entities.Rol;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.enums.CellRendererEnum;
import mx.com.cdadb.service.ServiceRol;
import mx.com.cdadb.service.impl.ServiceRolImpl;
import mx.com.cdadb.util.CellRendererUtil;
import mx.com.cdadb.util.HeaderCellRenderer;

/**
 *
 * @author unknown
 */
public class PaneRoles extends javax.swing.JPanel {

    private FrMenu frMenu2 = null;
    private List<Rol> roles = null;
    private String idRoleSelected = "";
    private String menuSelected = "";

    public PaneRoles(FrMenu fr) {
        initComponents();
        this.frMenu2 = fr;
        //this.panelMenus.setLayout(new FlowLayout());
        this.panelMenus.revalidate();
        loadPermisos(this.frMenu2.user);
        loadData();
        repaint();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        panelMenus = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaMenus = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        panelSubmenues = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaSubMenus = new javax.swing.JTable();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaPermisos = new javax.swing.JTable();

        btnNuevo.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/add_x16.png"))); // NOI18N
        btnNuevo.setText("Agregar [F2]");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnEditar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/pen_write_edit_x16.png"))); // NOI18N
        btnEditar.setText("Editar [F4]");

        btnEliminar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/delete_x16.png"))); // NOI18N
        btnEliminar.setText("Eliminar [F3]");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNuevo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                    .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "NOMBRE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(50);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(300);
        }

        panelMenus.setBackground(new java.awt.Color(255, 255, 255));
        panelMenus.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelMenus.setLayout(new FlowLayout());

        tablaMenus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NOMBRE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaMenus.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaMenus.getTableHeader().setReorderingAllowed(false);
        tablaMenus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMenusMouseClicked(evt);
            }
        });
        JTableHeader jtableHeader = tablaMenus.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaMenus.setTableHeader(jtableHeader);
        jScrollPane2.setViewportView(tablaMenus);
        if (tablaMenus.getColumnModel().getColumnCount() > 0) {
            tablaMenus.getColumnModel().getColumn(0).setResizable(false);
            tablaMenus.getColumnModel().getColumn(0).setPreferredWidth(300);
        }

        javax.swing.GroupLayout panelMenusLayout = new javax.swing.GroupLayout(panelMenus);
        panelMenus.setLayout(panelMenusLayout);
        panelMenusLayout.setHorizontalGroup(
            panelMenusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        panelMenusLayout.setVerticalGroup(
            panelMenusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
        );

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("MENÚS");

        panelSubmenues.setBackground(new java.awt.Color(255, 255, 255));
        panelSubmenues.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelSubmenues.setLayout(new FlowLayout());

        tablaSubMenus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NOMBRE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaSubMenus.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaSubMenus.getTableHeader().setReorderingAllowed(false);
        tablaSubMenus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaSubMenusMouseClicked(evt);
            }
        });
        JTableHeader jtableHeader2 = tablaSubMenus.getTableHeader();
        jtableHeader2.setDefaultRenderer(new HeaderCellRenderer());
        tablaSubMenus.setTableHeader(jtableHeader2);
        jScrollPane3.setViewportView(tablaSubMenus);
        if (tablaSubMenus.getColumnModel().getColumnCount() > 0) {
            tablaSubMenus.getColumnModel().getColumn(0).setResizable(false);
            tablaSubMenus.getColumnModel().getColumn(0).setPreferredWidth(300);
        }

        javax.swing.GroupLayout panelSubmenuesLayout = new javax.swing.GroupLayout(panelSubmenues);
        panelSubmenues.setLayout(panelSubmenuesLayout);
        panelSubmenuesLayout.setHorizontalGroup(
            panelSubmenuesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        panelSubmenuesLayout.setVerticalGroup(
            panelSubmenuesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
        );

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("SUBMENÚS");

        jLabel5.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("PERMISOS");

        tablaPermisos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "NOMBRE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaPermisos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaPermisos.getTableHeader().setReorderingAllowed(false);
        JTableHeader jtableHeader3 = tablaPermisos.getTableHeader();
        jtableHeader3.setDefaultRenderer(new HeaderCellRenderer());
        tablaPermisos.setTableHeader(jtableHeader3);
        jScrollPane5.setViewportView(tablaPermisos);
        if (tablaPermisos.getColumnModel().getColumnCount() > 0) {
            tablaPermisos.getColumnModel().getColumn(0).setResizable(false);
            tablaPermisos.getColumnModel().getColumn(0).setPreferredWidth(300);
        }

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(panelMenus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator3)
                            .addComponent(jSeparator2)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jSeparator5)
                            .addComponent(panelSubmenues, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator4)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 922, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 922, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 922, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator4)
                    .addComponent(jSeparator2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator3)
                    .addComponent(jSeparator5))
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelSubmenues, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelMenus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 1023, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 527, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        getAccessibleContext().setAccessibleName("");
        getAccessibleContext().setAccessibleDescription("roles");
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        DialogNuevoRol nuevoRol = new DialogNuevoRol(this.frMenu2, true);
        nuevoRol.setVisible(true);
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        idRoleSelected = String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 0));
        paintVoidTables();
        paintMenues(idRoleSelected);
    }//GEN-LAST:event_jTable1MouseClicked

    private void tablaMenusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMenusMouseClicked
        menuSelected = String.valueOf(tablaMenus.getValueAt(tablaMenus.getSelectedRow(), 0));
        paintSubMenues(idRoleSelected, String.valueOf(tablaMenus.getValueAt(tablaMenus.getSelectedRow(), 0)));
    }//GEN-LAST:event_tablaMenusMouseClicked

    private void tablaSubMenusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaSubMenusMouseClicked
        paintPermissions(idRoleSelected, menuSelected, String.valueOf(tablaSubMenus.getValueAt(tablaSubMenus.getSelectedRow(), 0)));
    }//GEN-LAST:event_tablaSubMenusMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JPanel panelMenus;
    public javax.swing.JPanel panelSubmenues;
    private javax.swing.JTable tablaMenus;
    private javax.swing.JTable tablaPermisos;
    private javax.swing.JTable tablaSubMenus;
    // End of variables declaration//GEN-END:variables

    private void loadPermisos(User usuario) {
        for (Object p : usuario.getRol().getPermisos()) {
            Permiso permiso = (Permiso) p;
            if ("AGREGAR ROL".equals(permiso.getAccion().getAccionDesc())) {
                btnNuevo.setVisible(true);
            }
            if ("MODIFICAR ROL".equals(permiso.getAccion().getAccionDesc())) {
                btnEditar.setVisible(true);
            }
            if ("ELIMINAR ROL".equals(permiso.getAccion().getAccionDesc())) {
                btnEliminar.setVisible(true);
            }
        }
    }

    private void loadData() {
        try {
            ServiceRol serviceRol = new ServiceRolImpl();
            roles = serviceRol.findAll();

            int i = 0;
            Object[][] datos = new Object[roles.size()][2];
            for (Rol rol : roles) {
                datos[i][0] = rol.getRolId();
                datos[i][1] = rol.getRolNombre();
                i++;
            }

            jTable1.setModel(new javax.swing.table.DefaultTableModel(
                    datos,
                    new String[]{
                        "ID", "NOMBRE"
                    }
            ) {
                boolean[] canEdit = new boolean[]{
                    false, false
                };

                @Override
                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            });

            JTableHeader jtableHeader = jTable1.getTableHeader();
            jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
            jTable1.setTableHeader(jtableHeader);

            jTable1.setRowHeight(30);

            jScrollPane1.setViewportView(jTable1);

            if (jTable1.getColumnModel().getColumnCount() > 0) {
                jTable1.getColumnModel().getColumn(0).setResizable(false);
                jTable1.getColumnModel().getColumn(0).setPreferredWidth(50);
                jTable1.getColumnModel().getColumn(1).setPreferredWidth(300);
            }

            jTable1.getColumnModel().getColumn(0).setCellRenderer(new CellRendererUtil(CellRendererEnum.PK_ID.toString()));
            jTable1.getColumnModel().getColumn(1).setCellRenderer(new CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        } catch (Exception ex) {
            Logger.getLogger(PaneRoles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void paintMenues(String idRol) {
        for (Rol rol : roles) {
            if (rol.getRolId().toString().equals(idRol)) {
                Set<String> modules = new HashSet<>();
                //quitar duplicados
                rol.getPermisos().stream().forEach(p -> {
                    modules.add(p.getAccion().getModulo().getModuloMenu());
                });
                Object[][] datos = new Object[modules.size()][1];
                int i = 0;
                for (String module : modules) {
                    datos[i][0] = module;
                    i++;
                }

                tablaMenus.setModel(new javax.swing.table.DefaultTableModel(
                        datos,
                        new String[]{
                            "NOMBRE"
                        }
                ) {
                    boolean[] canEdit = new boolean[]{
                        false
                    };

                    @Override
                    public boolean isCellEditable(int rowIndex, int columnIndex) {
                        return canEdit[columnIndex];
                    }
                });

                JTableHeader jtableHeader = tablaMenus.getTableHeader();
                jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
                tablaMenus.setTableHeader(jtableHeader);

                tablaMenus.setRowHeight(30);

                jScrollPane2.setViewportView(tablaMenus);

                if (tablaMenus.getColumnModel().getColumnCount() > 0) {
                    tablaMenus.getColumnModel().getColumn(0).setResizable(false);
                    tablaMenus.getColumnModel().getColumn(0).setPreferredWidth(300);
                }

                tablaMenus.getColumnModel().getColumn(0).setCellRenderer(new CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
            }
        }
    }

    private void paintSubMenues(String idRol, String menu) {
        for (Rol rol : roles) {
            if (rol.getRolId().toString().equals(idRol)) {
                Set<String> subModules = new HashSet<>();
                //quitar duplicados
                rol.getPermisos().stream().forEach(p -> {
                    if (p.getAccion().getModulo().getModuloMenu().equals(menu)) {
                        subModules.add(p.getAccion().getModulo().getModuloSubmenu());
                    }
                });

                Object[][] datos = new Object[subModules.size()][1];
                int i = 0;
                for (String module : subModules) {
                    datos[i][0] = module;
                    i++;
                }

                tablaSubMenus.setModel(new javax.swing.table.DefaultTableModel(
                        datos,
                        new String[]{
                            "NOMBRE"
                        }
                ) {
                    boolean[] canEdit = new boolean[]{
                        false
                    };

                    @Override
                    public boolean isCellEditable(int rowIndex, int columnIndex) {
                        return canEdit[columnIndex];
                    }
                });

                JTableHeader jtableHeader = tablaSubMenus.getTableHeader();
                jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
                tablaSubMenus.setTableHeader(jtableHeader);

                tablaSubMenus.setRowHeight(30);

                jScrollPane3.setViewportView(tablaSubMenus);

                if (tablaSubMenus.getColumnModel().getColumnCount() > 0) {
                    tablaSubMenus.getColumnModel().getColumn(0).setResizable(false);
                    tablaSubMenus.getColumnModel().getColumn(0).setPreferredWidth(300);
                }

                tablaSubMenus.getColumnModel().getColumn(0).setCellRenderer(new CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
            }
        }
    }

    private void paintPermissions(String idRol, String menu, String submenu) {
        Set<String> permissions = new HashSet<>();
        for (Rol rol : roles) {
            for (Permiso p : rol.getPermisos()) {
                if (String.valueOf(rol.getRolId()).equals(idRol)) {
                    for (Permiso permiso : p.getRol().getPermisos()) {
                        if (permiso.getAccion().getModulo().getModuloMenu().equals(menu) && permiso.getAccion().getModulo().getModuloSubmenu().equals(submenu)) {
                            permissions.add(permiso.getAccion().getAccionDesc());
                        }
                    }
                    break;
                }
            }
        }

        Object[][] datos = new Object[permissions.size()][1];
        int i = 0;
        for (String module : permissions) {
            datos[i][0] = module;
            i++;
        }

        tablaPermisos.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                new String[]{
                    "NOMBRE"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean[]{
                false
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        JTableHeader jtableHeader = tablaPermisos.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaPermisos.setTableHeader(jtableHeader);

        tablaPermisos.setRowHeight(30);

        jScrollPane5.setViewportView(tablaPermisos);

        if (tablaPermisos.getColumnModel().getColumnCount() > 0) {
            tablaPermisos.getColumnModel().getColumn(0).setResizable(true);
            tablaPermisos.getColumnModel().getColumn(0).setPreferredWidth(500);
        }

        tablaPermisos.getColumnModel().getColumn(0).setCellRenderer(new CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));

    }

    private void paintVoidTables() {
        tablaMenus.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "NOMBRE"
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false
            };

        });

        tablaMenus.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);

        tablaMenus.getTableHeader().setReorderingAllowed(false);

        tablaMenus.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMenusMouseClicked(evt);
            }
        });

        jScrollPane2.setViewportView(tablaMenus);

        if (tablaMenus.getColumnModel().getColumnCount() > 0) {
            tablaMenus.getColumnModel().getColumn(0).setResizable(false);
            tablaMenus.getColumnModel().getColumn(0).setPreferredWidth(300);
        }

        tablaSubMenus.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "NOMBRE"
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        tablaSubMenus.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);

        tablaSubMenus.getTableHeader().setReorderingAllowed(false);

        jScrollPane3.setViewportView(tablaSubMenus);

        if (tablaSubMenus.getColumnModel().getColumnCount() > 0) {
            tablaSubMenus.getColumnModel().getColumn(0).setResizable(false);
            tablaSubMenus.getColumnModel().getColumn(0).setPreferredWidth(300);
        }

        tablaPermisos.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "NOMBRE"
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false
            };
        });

        tablaPermisos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);

        tablaPermisos.getTableHeader().setReorderingAllowed(false);

        jScrollPane5.setViewportView(tablaPermisos);

        if (tablaPermisos.getColumnModel().getColumnCount() > 0) {
            tablaPermisos.getColumnModel().getColumn(0).setResizable(false);
            tablaPermisos.getColumnModel().getColumn(0).setPreferredWidth(300);
        }

    }
}
