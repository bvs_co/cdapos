package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import mx.com.cdadb.entities.Caja;
import mx.com.cdadb.entities.Detalledevolucion;
import mx.com.cdadb.entities.Devolucion;
import mx.com.cdadb.entities.Folio;
import mx.com.cdadb.entities.Foliodevolucion;
import mx.com.cdadb.entities.FoliodevolucionId;
import mx.com.cdadb.entities.Orden;
import mx.com.cdadb.entities.customer.Cliente;
import mx.com.cdadb.service.ServiceCaja;
import mx.com.cdadb.service.ServiceDetalleDevolucion;
import mx.com.cdadb.service.ServiceFolio;
import mx.com.cdadb.service.ServiceFolioDevolucion;
import mx.com.cdadb.service.ServiceOrden;
import mx.com.cdadb.service.impl.ServiceCajaImpl;
import mx.com.cdadb.service.impl.ServiceDetalleDevolucionImpl;
import mx.com.cdadb.service.impl.ServiceFolioDevolucionImpl;
import mx.com.cdadb.service.impl.ServiceFolioImpl;
import mx.com.cdadb.service.impl.ServiceOrdenImpl;
import mx.com.cdadb.util.DialogWait;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.util.EncryptCashbox;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.constants.ConstantFiles;
import mx.com.cdadb.enums.CellRendererEnum;
import mx.com.cdadb.util.MonedaUtil;

public class FrDevolucion extends javax.swing.JDialog {

    private PanePos panePos = null;
    private int opc;
    private Folio folio = null;
    private Cliente cliente = null;
    public long autorizacion = 0;
    private final String DEVUELTO = "DEVUELTO";
    private final String DISPONIBLE = "DISPONIBLE";

    public FrDevolucion(java.awt.Frame parent, boolean modal, PanePos pos, int op) {
        super(parent, modal);
        initComponents();
        this.panePos = pos;
        this.opc = op;
        txtBuscar.requestFocus();
        this.cliente = new Cliente();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        bntBuscar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        btnDevuelve = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        lblTotalventa = new javax.swing.JLabel();
        lblTotalDevolver = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDescripcion = new javax.swing.JTextPane();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblPaciente = new javax.swing.JLabel();
        lblFolio = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Devoluciones.");
        setIconImage(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_x32.png")).getImage());
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel1KeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jLabel1.setText("Buscar folio:");

        txtBuscar.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        txtBuscar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtBuscarFocusGained(evt);
            }
        });
        txtBuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtBuscarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtBuscarMouseEntered(evt);
            }
        });
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBuscarKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        jTable1.setModel(new DefaultTableModel());
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTable1FocusLost(evt);
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jTable1MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTable1MouseEntered(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTable1KeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(100);
            jTable1.getColumnModel().getColumn(1).setMinWidth(250);
            jTable1.getColumnModel().getColumn(2).setMinWidth(150);
            jTable1.getColumnModel().getColumn(3).setMinWidth(150);
            jTable1.getColumnModel().getColumn(4).setMinWidth(150);
        }

        bntBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        bntBuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bntBuscarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                bntBuscarMouseEntered(evt);
            }
        });
        bntBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bntBuscarActionPerformed(evt);
            }
        });
        bntBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                bntBuscarKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                bntBuscarKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Courier New", 1, 22)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Total Venta: $");

        jLabel3.setFont(new java.awt.Font("Courier New", 1, 22)); // NOI18N
        jLabel3.setText("Total a devolver: $");

        btnDevuelve.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        btnDevuelve.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/accept_blue_x16.png"))); // NOI18N
        btnDevuelve.setText("Devolver [F5]");
        btnDevuelve.setEnabled(false);
        btnDevuelve.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnDevuelve.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                btnDevuelveFocusGained(evt);
            }
        });
        btnDevuelve.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnDevuelveMouseEntered(evt);
            }
        });
        btnDevuelve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDevuelveActionPerformed(evt);
            }
        });
        btnDevuelve.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnDevuelveKeyPressed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cancel_x16.png"))); // NOI18N
        btnCancelar.setText("CancelaR [Esc]");
        btnCancelar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnCancelar.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                btnCancelarFocusGained(evt);
            }
        });
        btnCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCancelarMouseEntered(evt);
            }
        });
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        lblTotalventa.setFont(new java.awt.Font("Courier New", 1, 22)); // NOI18N
        lblTotalventa.setForeground(new java.awt.Color(0, 51, 204));
        lblTotalventa.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalventa.setText("0.00");

        lblTotalDevolver.setFont(new java.awt.Font("Courier New", 1, 22)); // NOI18N
        lblTotalDevolver.setForeground(new java.awt.Color(255, 0, 0));
        lblTotalDevolver.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotalDevolver.setText("0.00");

        jLabel4.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/view_details_x16.png"))); // NOI18N
        jLabel4.setText("Descripción:");

        txtDescripcion.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        txtDescripcion.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDescripcionFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDescripcionFocusLost(evt);
            }
        });
        txtDescripcion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                txtDescripcionMouseEntered(evt);
            }
        });
        txtDescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDescripcionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDescripcionKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(txtDescripcion);

        jLabel5.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        jLabel5.setText("[Esc] Salir de la edición de la descripción");

        jLabel6.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        jLabel6.setText("[Esc] Salir de la selección de la tabla");

        jLabel7.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/people_x16.png"))); // NOI18N
        jLabel7.setText("Paciente:");

        lblPaciente.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N
        lblPaciente.setForeground(new java.awt.Color(32, 117, 32));
        lblPaciente.setText("...");

        lblFolio.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        lblFolio.setForeground(new java.awt.Color(255, 0, 0));
        lblFolio.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblFolio.setText("......");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jSeparator2)
            .addComponent(jSeparator4)
            .addComponent(jSeparator3)
            .addComponent(jSeparator5)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 827, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtBuscar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bntBuscar))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnCancelar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDevuelve))
                            .addComponent(jScrollPane2)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblPaciente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 243, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblTotalventa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblTotalDevolver, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(lblFolio, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(bntBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblFolio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lblPaciente))
                .addGap(20, 20, 20)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTotalventa, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(5, 5, 5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTotalDevolver, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnDevuelve)
                    .addComponent(btnCancelar))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            //iniBuscaFolio();
            DialogWait dw = new DialogWait(this.panePos.frMenu, true, this, txtBuscar.getText().trim());
            dw.setVisible(true);
        }
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        actualizaDevolucion();
    }//GEN-LAST:event_jTable1MouseClicked

    private void btnDevuelveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDevuelveActionPerformed
        DialogWait dw = new DialogWait(this.panePos.frMenu, true, this, "");
        dw.setVisible(true);
//        devuelve();
    }//GEN-LAST:event_btnDevuelveActionPerformed

    private void txtBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyPressed
        checkKeyPressed(evt, false);
    }//GEN-LAST:event_txtBuscarKeyPressed

    private void bntBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bntBuscarKeyPressed
        checkKeyPressed(evt, false);
    }//GEN-LAST:event_bntBuscarKeyPressed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        checkKeyPressed(evt, true);
        actualizaDevolucion();
    }//GEN-LAST:event_jTable1KeyPressed

    private void btnDevuelveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnDevuelveKeyPressed
        checkKeyPressed(evt, false);
    }//GEN-LAST:event_btnDevuelveKeyPressed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        checkKeyPressed(evt, false);
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void jPanel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel1KeyPressed
        checkKeyPressed(evt, false);
    }//GEN-LAST:event_jPanel1KeyPressed

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            txtDescripcion.selectAll();
            txtDescripcion.requestFocus();
        }
        actualizaDevolucion();
    }//GEN-LAST:event_jTable1KeyReleased

    private void txtBuscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyTyped
        isNoLetter(evt);
    }//GEN-LAST:event_txtBuscarKeyTyped

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtDescripcionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripcionKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            btnDevuelve.requestFocus();
        }
    }//GEN-LAST:event_txtDescripcionKeyReleased

    private void bntBuscarKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_bntBuscarKeyTyped
        checkKeyPressed(evt, false);
        txtBuscar.selectAll();
        txtBuscar.requestFocus();
    }//GEN-LAST:event_bntBuscarKeyTyped

    private void bntBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bntBuscarActionPerformed
        findFolio();
    }//GEN-LAST:event_bntBuscarActionPerformed

    private void txtDescripcionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDescripcionFocusLost
        txtDescripcion.setText(txtDescripcion.getText().toUpperCase());
    }//GEN-LAST:event_txtDescripcionFocusLost

    private void txtDescripcionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDescripcionFocusGained
        txtDescripcion.selectAll();
        actualizaDevolucion();
    }//GEN-LAST:event_txtDescripcionFocusGained

    private void txtDescripcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripcionKeyPressed
        checkKeyPressed(evt, false);
    }//GEN-LAST:event_txtDescripcionKeyPressed

    private void jTable1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyTyped
        actualizaDevolucion();
    }//GEN-LAST:event_jTable1KeyTyped

    private void jTable1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTable1FocusLost
        actualizaDevolucion();
    }//GEN-LAST:event_jTable1FocusLost

    private void btnDevuelveFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_btnDevuelveFocusGained
        actualizaDevolucion();
    }//GEN-LAST:event_btnDevuelveFocusGained

    private void btnCancelarFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_btnCancelarFocusGained
        actualizaDevolucion();
    }//GEN-LAST:event_btnCancelarFocusGained

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        actualizaDevolucion();
    }//GEN-LAST:event_formMouseClicked

    private void btnDevuelveMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnDevuelveMouseEntered
        actualizaDevolucion();
    }//GEN-LAST:event_btnDevuelveMouseEntered

    private void btnCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelarMouseEntered
        actualizaDevolucion();
    }//GEN-LAST:event_btnCancelarMouseEntered

    private void txtDescripcionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtDescripcionMouseEntered
        actualizaDevolucion();
    }//GEN-LAST:event_txtDescripcionMouseEntered

    private void txtBuscarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtBuscarMouseEntered
        actualizaDevolucion();
    }//GEN-LAST:event_txtBuscarMouseEntered

    private void txtBuscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtBuscarMouseClicked
        actualizaDevolucion();
    }//GEN-LAST:event_txtBuscarMouseClicked

    private void bntBuscarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bntBuscarMouseEntered
        actualizaDevolucion();
    }//GEN-LAST:event_bntBuscarMouseEntered

    private void bntBuscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bntBuscarMouseClicked
        actualizaDevolucion();
    }//GEN-LAST:event_bntBuscarMouseClicked

    private void jTable1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseEntered
        actualizaDevolucion();
    }//GEN-LAST:event_jTable1MouseEntered

    private void jTable1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseExited
        actualizaDevolucion();
    }//GEN-LAST:event_jTable1MouseExited

    private void txtBuscarFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtBuscarFocusGained
        txtBuscar.selectAll();
    }//GEN-LAST:event_txtBuscarFocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrDevolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrDevolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrDevolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrDevolucion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrDevolucion dialog = new FrDevolucion(new javax.swing.JFrame(), true, null, 0);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bntBuscar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnDevuelve;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lblFolio;
    private javax.swing.JLabel lblPaciente;
    private javax.swing.JLabel lblTotalDevolver;
    private javax.swing.JLabel lblTotalventa;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextPane txtDescripcion;
    // End of variables declaration//GEN-END:variables

    private void fillTable(Folio folio) {
        try {
            mx.com.cdadb.service.ServiceCliente sCliente = new mx.com.cdadb.service.impl.ServiceClienteImpl();
            ServiceDetalleDevolucion sdd = new ServiceDetalleDevolucionImpl();
            List<Detalledevolucion> detDevs = sdd.findAll();

            int i = 0;
            float total = 0;
            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            Object[][] datos = new Object[folio.getOrdens().size()][6];
            for (Object o : folio.getOrdens()) {
                Orden orden = (Orden) o;
                if (orden.getProducto() != null) {
                    mx.com.cdadb.entities.Cliente clienteObj = sCliente.getBy("p.clienteId = " + orden.getOrdenPacienteId());
                    datos[i][0] = orden.getProducto().getProductoClave();
                    datos[i][1] = orden.getProducto().getProductoNombre();
                    datos[i][2] = orden.getProducto().getProductoPrecio();
                    datos[i][3] = (isDevueltos(orden, detDevs)) ? DEVUELTO : DISPONIBLE;
                    datos[i][4] = (!isDevueltos(orden, detDevs));
                    datos[i][5] = orden.getOrdenId();
                    total = total + orden.getProducto().getProductoPrecio();
                    this.cliente.setClienteId(orden.getOrdenPacienteId());
                    this.cliente.setClienteNombre(clienteObj.getClienteNombre() + " " + clienteObj.getClientePaterno() + " " + clienteObj.getClienteMaterno());
                    i++;
                }

            }
            for (Object o : folio.getOrdens()) {
                Orden orden = (Orden) o;
                if (orden.getDetallepaquete() != null) {
                    mx.com.cdadb.entities.Cliente clienteObj = sCliente.getBy("p.clienteId = " + orden.getOrdenPacienteId());
                    datos[i][0] = orden.getDetallepaquete().getDetallePaqueteClave();
                    datos[i][1] = orden.getDetallepaquete().getDetallePaqueteNombre();
                    datos[i][2] = orden.getDetallepaquete().getDetallePaquetePrecio();
                    datos[i][3] = (isDevueltos(orden, detDevs)) ? DEVUELTO : DISPONIBLE;
                    datos[i][4] = (!isDevueltos(orden, detDevs));
                    datos[i][5] = orden.getOrdenId();
                    total = total + orden.getDetallepaquete().getDetallePaquetePrecio();
                    this.cliente.setClienteId(orden.getOrdenPacienteId());
                    this.cliente.setClienteNombre(clienteObj.getClienteNombre() + " " + clienteObj.getClientePaterno() + " " + clienteObj.getClienteMaterno());
                    i++;
                }
            }
            lblTotalventa.setText(MonedaUtil.separaMiles(String.format("%.2f", total)));
            jTable1.setModel(new javax.swing.table.DefaultTableModel(
                    datos,
                    new String[]{
                        "CLAVE", "PRODUCTO", "C.M.R.", "STATUS", "SELECC", ""
                    }
            ) {
                Class[] types = new Class[]{
                    java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class, java.lang.Object.class
                };
                boolean[] canEdit = new boolean[]{
                    false, false, false, false, true, false
                };

                public Class getColumnClass(int columnIndex) {
                    return types[columnIndex];
                }

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    for (Object[] dato : datos) {
                        if (datos[rowIndex][3] == DEVUELTO) {
                            return false;
                        }
                    }

                    return canEdit[columnIndex];
                }
            });

            jTable1.getTableHeader().setReorderingAllowed(false);

            jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    jTable1MouseClicked(evt);
                }
            });
            jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    jTable1KeyPressed(evt);
                }
            });

            jScrollPane1.setViewportView(jTable1);

            if (jTable1.getColumnModel().getColumnCount() > 0) {
                jTable1.getColumnModel().getColumn(0).setMinWidth(100);
                jTable1.getColumnModel().getColumn(1).setMinWidth(250);
                jTable1.getColumnModel().getColumn(2).setMinWidth(150);
                jTable1.getColumnModel().getColumn(3).setMinWidth(150);
                jTable1.getColumnModel().getColumn(4).setMinWidth(150);
                jTable1.getColumnModel().getColumn(5).setMinWidth(0);//0 para que no muestre el valor de la columna
            }
            JTableHeader jtableHeader = jTable1.getTableHeader();
            jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
            jTable1.setTableHeader(jtableHeader);
            jTable1.setRowHeight(30);
            jTable1.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ORDER_ID.toString()));
            jTable1.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
            jTable1.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));
            jTable1.getColumnModel().getColumn(3).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_CENTER.toString()));
            jTable1.getColumnModel().getColumn(5).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.PK_ID_HIDDEN.toString()));
            ActualizaTotal();
            lblPaciente.setText("[" + this.cliente.getClienteId() + "] " + this.cliente.getClienteNombre());
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }

    private void actualizaDevolucion() {
        float total = 0;
        if (jTable1.getRowCount() > 0) {
            for (int i = 0; i < jTable1.getRowCount(); i++) {
                if (Boolean.valueOf(jTable1.getValueAt(i, 4).toString())) {
                    total = total + Float.valueOf(jTable1.getValueAt(i, 2).toString().replaceAll(",", ""));
                }
            }
        }
        lblTotalDevolver.setText(MonedaUtil.separaMiles(String.format("%.2f", total)));
        btnDevuelve.setEnabled((total > 0));
    }

    private void checkKeyPressed(KeyEvent evt, boolean op) {
        switch (evt.getKeyCode()) {
            case 113://F2 Buscar producto
                txtBuscarKeyPressed(null);
                break;
            case 116://F5 Devuelve
                btnDevuelveActionPerformed(null);
                break;
            case 32://F11 
//                if (Boolean.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 4).toString())) {
//                    jTable1.setValueAt(false, jTable1.getSelectedRow(), 4);
//                    repaint();
//                } else {
//                    jTable1.setValueAt(true, jTable1.getSelectedRow(), 4);
//                    repaint();
//                }
                break;
            case 27://Esc
                if (!op) {
                    if(countChecked() > 0 || txtDescripcion.getText().length() > 0){
                        int resp = JOptionPane.showConfirmDialog(this, "Esta seguro de salir.", "Confirme.", JOptionPane.YES_NO_OPTION);
                        if(resp == 0){
                            dispose();
                        }
                    }else{
                        dispose();
                    }
                }
                break;
        }
    }

    public void devuelve() {
        ServiceFolioDevolucion sDevolucion = new ServiceFolioDevolucionImpl();
        Foliodevolucion fDevolucion = new Foliodevolucion();

        DialogFolioCancelacion dfc = new DialogFolioCancelacion(this.panePos.frMenu, true, this);
        dfc.setVisible(true);

        if (this.autorizacion > 0 && validaFolio(this.autorizacion)) {
            try {
                Devolucion devolucion = new Devolucion();
                devolucion.setFolio(folio);
                devolucion.setDevolucionAutorizacion(this.autorizacion);
                devolucion.setDevolucionDesc(txtDescripcion.getText().toUpperCase());
                devolucion.setDevolucionFcreacion(new java.util.Date());
                devolucion.setDevolucionPacienteId(getCliente(this.folio));
                devolucion.setDevolucionStatus("1");
                devolucion.setUser(this.panePos.frMenu.user);
                EncryptCashbox p = new EncryptCashbox();
                Caja caja = p.decrypt(ConstantFiles.POS_PROPERTIES);
                devolucion.setCaja(caja);
                //Falta el folio de la devolucion, se agregará una ves que el folio se cree
                Set<Detalledevolucion> detalleDevoluciones = new HashSet<Detalledevolucion>();
                for (int i = 0; i < jTable1.getRowCount(); i++) {
                    if (Boolean.valueOf(jTable1.getValueAt(i, 4).toString()) && !jTable1.getValueAt(i, 3).toString().equals(DEVUELTO)) {
                        Detalledevolucion detDev = new Detalledevolucion();
                        detDev.setOrden(getOrden(jTable1.getValueAt(i, 5).toString()));
                        detDev.setDetalleDevolucionFcreacion(new java.util.Date());
                        detalleDevoluciones.add(detDev);
                    }
                }//La devolucion se agregrá una ves guardada, ára saber el id
                devolucion.setDetalledevolucions(detalleDevoluciones);
                fDevolucion.setFolioDevolucionFcreacion(new java.util.Date());
                fDevolucion.setFolioDevolucionStatus("1");
                FoliodevolucionId fDevId = new FoliodevolucionId();
                fDevId.setFolioDevolucionValor(new Long(11));//Se cambia por el consecutivo en el service
                fDevId.setFolioDevolucionFecha(new java.util.Date());
                fDevolucion.setId(fDevId);
                Set<Devolucion> devolucions = new HashSet<Devolucion>();
                devolucions.add(devolucion);
                fDevolucion.setDevolucions(devolucions);
                this.folio.setFolioStatus("DEV");
                ServiceCaja sCaja = new ServiceCajaImpl();
                Caja c = sCaja.getBy("c.cajaId = " + caja.getCajaId());
                float res = c.getCajaTotal() - Float.valueOf(lblTotalDevolver.getText().replaceAll(",", ""));
                c.setCajaTotal(res);
                c.setCajaFmodif(new java.util.Date());
                if (sDevolucion.add(fDevolucion, this.cliente, lblTotalDevolver.getText(), txtDescripcion.getText(), this.folio, c)) {
                    JOptionPane.showMessageDialog(this, "Devolución realizada con éxito.");
                    updateCash(caja, res);
                    this.dispose();
                } else {
                    JOptionPane.showMessageDialog(this, "Devolución no realizada.", "Ocurrió un error.", JOptionPane.ERROR_MESSAGE);
                }
            } catch (Exception ex) {
                if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Código de autorización erróneo.");
        }

    }

    private boolean validaFolio(long autorizacion) {
        boolean x = false;
        long f = ((this.folio.getId().getFolioValor() + 77) * 2) - 14;
        if (f == autorizacion) {
            x = true;
        }
        return x;
    }

    private Long getCliente(Folio folio) {
        long idCliente = -1;
        for (Object o : folio.getOrdens()) {
            Orden orden = (Orden) o;
            idCliente = orden.getOrdenPacienteId();
        }
        return idCliente;
    }

    private Orden getOrden(String valor) throws Exception {
        //Revisar por que se comparaba contra paqutes. Se pueden devolver productos de un paquete? o el paquete completo?
        ServiceOrden sOrden = new ServiceOrdenImpl();
        Orden orden = sOrden.getBy("o.ordenId = " + valor);
        /*for (Object o : folio.getOrdens()) {
            Orden ord = (Orden) o;
            if ((ord.getDetallepaquete() != null)) {
                if (ord.getDetallepaquete().getDetallePaqueteClave().equals(valor)) {
                    return ord;
                }
            } else {
                if (ord.getProducto() != null) {
                    if (ord.getProducto().getProductoClave().equals(valor)) {
                        return ord;
                    }
                }
            }
        }*/
        return orden;
    }

    private boolean isDevueltos(Orden orden, List<Detalledevolucion> detDevs) {
        for (Detalledevolucion detDev : detDevs) {
            if (detDev.getOrden().getOrdenId() == orden.getOrdenId()) {
                return true;
            }
        }
        return false;
    }

    private void ActualizaTotal() {
        float total = 0;
        if (jTable1.getRowCount() > 0) {
            for (int i = 0; i < jTable1.getRowCount(); i++) {
                if (Boolean.valueOf(jTable1.getValueAt(i, 4).toString())) {
                    total = total + Float.valueOf(jTable1.getValueAt(i, 2).toString().replaceAll(",", ""));
                }
            }
        }
        lblTotalDevolver.setText(MonedaUtil.separaMiles(String.format("%.2f", total)));
        btnDevuelve.setEnabled((total > 0));
    }

    private void isNoLetter(java.awt.event.KeyEvent evt) {
        char c = evt.getKeyChar();
        if (Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();
        }
    }

    /**
     * Actualiza el total que se vendio en la caja
     *
     * @param caja Caja en la cual se hizo el cobro
     */
    private void updateCash(Caja caja, float total) {
        try {
            ServiceCaja sCaja = new ServiceCajaImpl();
            caja.setCajaFmodif(new java.util.Date());
            caja.setCajaTotal(total);
            sCaja.edit(caja);
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }

    public void findFolio() {
        try {
            ServiceFolio sFolio = new ServiceFolioImpl();
            if (!txtBuscar.getText().isEmpty()) {
                folio = sFolio.getBy("f.id.folioValor = " + txtBuscar.getText()/*+" and f.id.folioFecha = '"+Formatt.SFD_YYYYMMDD.format(new java.util.Date())+"'"*/);
                if (folio != null) {
                    lblFolio.setText(String.valueOf(folio.getId().getFolioValor()));
                    fillTable(folio);
                } else {
                    JOptionPane.showMessageDialog(this, "No se encontraron resultados.", "Información.", JOptionPane.WARNING_MESSAGE);
                    lblFolio.setText("......");
                    jTable1.setModel(new DefaultTableModel());
                    txtBuscar.requestFocus();
                }
            }else{
                JOptionPane.showMessageDialog(this, "Debe ingresar el forlio a buscar.", "Información.", JOptionPane.WARNING_MESSAGE);
                txtBuscar.requestFocus();
            }
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }

    private int countChecked() {
        int count = 0;
        
        for(int i = 0; i < jTable1.getRowCount(); i++){
            if("true".equals(jTable1.getValueAt(i, 4).toString())){
                count++;
            }
        }
        
        return count;
    }

}
