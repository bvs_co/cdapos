package mx.com.cdadb.view;

import com.mx.dto.PersonaRfcDto;
import com.mx.rfc_.Rfc;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import mx.com.cdadb.entities.customer.Cliente;
import mx.com.cdadb.entities.customer.Recomendacion;
import mx.com.cdadb.entities.customer.Sucursal;
import mx.com.cdadb.service.customer.impl.ServiceClienteImpl;
import mx.com.cdadb.service.customer.impl.ServiceDoctorImpl;
import mx.com.cdadb.util.CalculateAgeUtil;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.service.customer.ServiceCliente;
import mx.com.cdadb.service.customer.ServiceDoctor;
import mx.com.cdadb.util.CellRendererUtil;
import mx.com.cdadb.util.DialogWait;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;
import mx.com.cdadb.service.customer.impl.ServiceCustomerBaseImpl;

public class DialogAltaRapidaPaciente extends javax.swing.JDialog {

    private int opc = -1;
    public static PanePos panePos = null;
    private List<Cliente> customers = new ArrayList<>();
    private final ServiceCustomerBaseImpl service = new ServiceCustomerBaseImpl();
    private Cliente selectedCustomer;
    private Cliente senderCustomer = null;
    private static final String CUSTOMER_LASTNAME_LIKE = "%' and t.clientePaterno like '%";
    private static final String CUSTOMER_MOTHER_LASTNAME_LIKE = "%' and t.clienteMaterno like '%";
    private static final String CUSTOMER_NAME_LIKE = "t.clienteNombre like '%";

    public DialogAltaRapidaPaciente(java.awt.Frame parent, boolean modal, int op, PanePos pos/*, List<clientesModelo.Cliente> cltes*/) {
        super(parent, modal);
        try {
            initComponents();
            this.setLocationRelativeTo(null);
            //this.setIconImage(new ImageIcon(getClass().getResource("/seek/mx/pv/cliente/imagenes/iconos/iconLogo.png")).getImage());
            this.opc = op;
            this.panePos = pos;
            init_();
        } catch (SQLException | IOException ex) {
            JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage());
        }
    }

    public DialogAltaRapidaPaciente(java.awt.Frame parent, boolean modal, int op, PanePos pos, Cliente c) {
        super(parent, modal);
        try {
            initComponents();
            this.setLocationRelativeTo(null);
            //this.setIconImage(new ImageIcon(getClass().getResource("/seek/mx/pv/cliente/imagenes/iconos/iconLogo.png")).getImage());
            this.senderCustomer = c;
            this.opc = op;
            this.panePos = pos;
            init_();
        } catch (SQLException | IOException ex) {
            JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage());
        }
    }

    private DialogAltaRapidaPaciente(JFrame jFrame, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtLastname = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtSecondLastname = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtBirthdate = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        comboGender = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        txtRfc = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtBussinesname = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtAge = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtIdCustomer = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableCustomer = new javax.swing.JTable();
        jLabel16 = new javax.swing.JLabel();
        btnQuitarSeleccion = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        txtStreet = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtInterior = new javax.swing.JTextField();
        txtExterior = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtColony = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtCity = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtState = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtPostalcode = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtPhone = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Alta rápida de paciente.");
        setIconImage(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_x32.png")).getImage());
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Personal.", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 0, 14))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel1.setText("Nombre **");

        txtName.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtName.setToolTipText("Enter para buscar coincidenias");
        txtName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNameFocusLost(evt);
            }
        });
        txtName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNameKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNameKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel2.setText("Apellido Paterno **");

        txtLastname.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtLastname.setToolTipText("Enter para buscar coincidenias");
        txtLastname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtLastnameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtLastnameFocusLost(evt);
            }
        });
        txtLastname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtLastnameKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtLastnameKeyReleased(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel3.setText("Apellido Materno **");

        txtSecondLastname.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtSecondLastname.setToolTipText("Presiona Enter para buscar..");
        txtSecondLastname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtSecondLastnameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtSecondLastnameFocusLost(evt);
            }
        });
        txtSecondLastname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSecondLastnameKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSecondLastnameKeyReleased(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel4.setText("F. Nacimiento (DD/MM/AAAA) **");

        try {
            txtBirthdate.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtBirthdate.setToolTipText("Formato: DD/MM/AAAA");
        txtBirthdate.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtBirthdate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtBirthdateFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtBirthdateFocusLost(evt);
            }
        });
        txtBirthdate.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBirthdateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBirthdateKeyReleased(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel5.setText("Género **");

        comboGender.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        comboGender.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<Seleccione>", "Masculino", "Femenino" }));
        comboGender.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboGenderKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                comboGenderKeyReleased(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel6.setText("R.F.C.");

        txtRfc.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtRfc.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtRfcFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtRfcFocusLost(evt);
            }
        });
        txtRfc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRfcKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRfcKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtRfcKeyReleased(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel7.setText("Razón Social");

        txtBussinesname.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtBussinesname.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtBussinesnameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtBussinesnameFocusLost(evt);
            }
        });
        txtBussinesname.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBussinesnameKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBussinesnameKeyReleased(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel17.setText("Edad(Años)");

        txtAge.setEditable(false);
        txtAge.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtAge.setForeground(new java.awt.Color(255, 0, 0));
        txtAge.setFocusable(false);
        txtAge.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAgeKeyPressed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel18.setText("Núm. Paciente");

        txtIdCustomer.setEditable(false);
        txtIdCustomer.setBackground(java.awt.SystemColor.inactiveCaption);
        txtIdCustomer.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        txtIdCustomer.setForeground(new java.awt.Color(153, 0, 0));
        txtIdCustomer.setFocusable(false);
        txtIdCustomer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtIdCustomerKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtIdCustomerKeyReleased(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 0, 0));
        jLabel19.setText("-->");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtBussinesname, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtName)
                    .addComponent(txtLastname)
                    .addComponent(txtSecondLastname)
                    .addComponent(comboGender, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtRfc)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtBirthdate))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 9, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(txtAge))))
                    .addComponent(txtIdCustomer)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel18))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel19)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtIdCustomer, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtLastname, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtSecondLastname, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel17))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtAge)
                    .addComponent(txtBirthdate, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboGender, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addGap(4, 4, 4)
                .addComponent(txtRfc, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtBussinesname, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Pacientes actuales.", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 0, 14))); // NOI18N

        tableCustomer.setModel(new DefaultTableModel());
        tableCustomer.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tableCustomer.getTableHeader().setReorderingAllowed(false);
        tableCustomer.addHierarchyListener(new java.awt.event.HierarchyListener() {
            public void hierarchyChanged(java.awt.event.HierarchyEvent evt) {
                tableCustomerHierarchyChanged(evt);
            }
        });
        tableCustomer.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tableCustomerFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tableCustomerFocusLost(evt);
            }
        });
        tableCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableCustomerMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                tableCustomerMouseEntered(evt);
            }
        });
        tableCustomer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableCustomerKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tableCustomer);

        jLabel16.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        jLabel16.setText("[Esc] para salir de la selección de la busqueda.");

        btnQuitarSeleccion.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        btnQuitarSeleccion.setMnemonic('Q');
        btnQuitarSeleccion.setText("Quitar Selección [F3]");
        btnQuitarSeleccion.setEnabled(false);
        btnQuitarSeleccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarSeleccionActionPerformed(evt);
            }
        });
        btnQuitarSeleccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnQuitarSeleccionKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 716, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnQuitarSeleccion, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(btnQuitarSeleccion, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Contacto.", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 0, 14))); // NOI18N

        jLabel8.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel8.setText("Calle");

        txtStreet.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtStreet.setNextFocusableComponent(txtInterior);
        txtStreet.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtStreetFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtStreetFocusLost(evt);
            }
        });
        txtStreet.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtStreetKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtStreetKeyReleased(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel9.setText("# Interior");

        jLabel10.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel10.setText("# Exterior");

        txtInterior.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtInterior.setNextFocusableComponent(txtExterior);
        txtInterior.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtInteriorFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtInteriorFocusLost(evt);
            }
        });
        txtInterior.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtInteriorKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtInteriorKeyReleased(evt);
            }
        });

        txtExterior.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtExterior.setNextFocusableComponent(txtColony);
        txtExterior.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtExteriorFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtExteriorFocusLost(evt);
            }
        });
        txtExterior.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtExteriorKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtExteriorKeyReleased(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel11.setText("Colonia");

        txtColony.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtColony.setNextFocusableComponent(txtCity);
        txtColony.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtColonyFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtColonyFocusLost(evt);
            }
        });
        txtColony.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtColonyKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtColonyKeyReleased(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel12.setText("Ciudad");

        txtCity.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtCity.setNextFocusableComponent(txtState);
        txtCity.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtCityFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCityFocusLost(evt);
            }
        });
        txtCity.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCityKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCityKeyReleased(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel13.setText("Estado **");

        txtState.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtState.setNextFocusableComponent(txtPostalcode);
        txtState.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtStateFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtStateFocusLost(evt);
            }
        });
        txtState.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtStateKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtStateKeyReleased(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel14.setText("C.P.");

        txtPostalcode.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtPostalcode.setNextFocusableComponent(txtPhone);
        txtPostalcode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPostalcodeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPostalcodeFocusLost(evt);
            }
        });
        txtPostalcode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPostalcodeKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPostalcodeKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPostalcodeKeyReleased(evt);
            }
        });

        jLabel15.setFont(new java.awt.Font("Consolas", 1, 13)); // NOI18N
        jLabel15.setText("Teléfono **");

        txtPhone.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        txtPhone.setNextFocusableComponent(comboRecomendation);
        txtPhone.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPhoneFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPhoneFocusLost(evt);
            }
        });
        txtPhone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPhoneKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPhoneKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPhoneKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtStreet))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(55, 55, 55))
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(txtInterior)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addGap(34, 34, 34))
                            .addComponent(txtExterior)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addGap(196, 196, 196)
                                .addComponent(jLabel15)))
                        .addGap(0, 395, Short.MAX_VALUE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtPostalcode, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 218, Short.MAX_VALUE)
                            .addComponent(txtColony, javax.swing.GroupLayout.Alignment.LEADING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPhone)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCity)
                                    .addGroup(jPanel4Layout.createSequentialGroup()
                                        .addComponent(jLabel12)
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel13)
                                    .addComponent(txtState, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtStreet, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtInterior, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtExterior, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtColony, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCity, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtState, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPostalcode, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPhone, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 8, Short.MAX_VALUE))
        );

        btnSave.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/accept_blue_x16.png"))); // NOI18N
        btnSave.setMnemonic('G');
        btnSave.setText("Aceptar [F5]");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        btnSave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnSaveKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                btnSaveKeyReleased(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Recomendación.", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Consolas", 0, 14))); // NOI18N

        comboRecomendation.setFont(new java.awt.Font("Consolas", 0, 11)); // NOI18N
        comboRecomendation.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        comboRecomendation.setEnabled(false);
        comboRecomendation.setNextFocusableComponent(jButton1);
        comboRecomendation.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboRecomendationKeyPressed(evt);
            }
        });

        jButton1.setEnabled(false);
        jButton1.setNextFocusableComponent(btnSave);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(comboRecomendation, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(comboRecomendation))
                .addGap(0, 13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtSecondLastnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSecondLastnameKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER && !txtSecondLastname.getText().trim().isEmpty()) {
            DialogWait dw = new DialogWait(DialogAltaRapidaPaciente.panePos.frMenu, true, this, 1);
            dw.setVisible(true);
            txtSecondLastname.requestFocus();
        }
    }//GEN-LAST:event_txtSecondLastnameKeyReleased

    private void txtSecondLastnameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSecondLastnameFocusLost
        mayusConvert(txtSecondLastname);
        if(!txtName.getText().isEmpty()){
            DialogWait dw = new DialogWait(DialogAltaRapidaPaciente.panePos.frMenu, true, this, 1);
            dw.setVisible(true);
       }
    }//GEN-LAST:event_txtSecondLastnameFocusLost

    private void txtNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNameFocusLost
        mayusConvert(txtName);
//        if(!txtName.getText().isEmpty()){
//            DialogWait dw = new DialogWait(DialogAltaRapidaPaciente.panePos.frMenu, true, this, 0);
//            dw.setVisible(true);
//            txtLastname.requestFocus();
//        }
    }//GEN-LAST:event_txtNameFocusLost

    private void txtLastnameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtLastnameFocusLost
        mayusConvert(txtLastname);
    }//GEN-LAST:event_txtLastnameFocusLost

    private void txtBirthdateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtBirthdateFocusLost
        if (!txtLastname.getText().isEmpty()
                && !txtSecondLastname.getText().isEmpty()
                && !txtName.getText().isEmpty()
                && !"  /  /    ".equals(txtBirthdate.getText())
                && txtName.getText().length() > 1
                && txtLastname.getText().length() > 1
                && txtSecondLastname.getText().length() > 1) {
            try {
                Rfc rfc = new Rfc();
                PersonaRfcDto persona = new PersonaRfcDto();
                persona.setApMaterno(txtSecondLastname.getText());
                persona.setApPaterno(txtLastname.getText());
                persona.setNombre(txtName.getText());

                persona.setFecha(FormatDateUtil.SFD_YYYYMMDD_NS.format(FormatDateUtil.SFD_DDMMYYYY.parse(txtBirthdate.getText())));
                txtRfc.setText(rfc.generarRfc(persona));
            } catch (ParseException ex) {
                Logger.getLogger(DialogAltaRapidaPaciente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (!"  /  /    ".equals(txtBirthdate.getText())) {
            txtAge.setText(String.valueOf(CalculateAgeUtil.calculate(txtBirthdate.getText().replaceAll("/", "-"))));
        }
    }//GEN-LAST:event_txtBirthdateFocusLost

    private void txtNameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNameKeyReleased
//        if (evt.getKeyCode() == KeyEvent.VK_ENTER && !txtName.getText().trim().isEmpty() && evt != null) {
//            DialogWait dw = new DialogWait(DialogAltaRapidaPaciente.panePos.frMenu, true, this, 0);
//            dw.setVisible(true);
//            txtLastname.requestFocus();
//        }
    }//GEN-LAST:event_txtNameKeyReleased

    private void txtLastnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLastnameKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER && !txtLastname.getText().trim().isEmpty()) {
            DialogWait dw = new DialogWait(DialogAltaRapidaPaciente.panePos.frMenu, true, this, 1);
            dw.setVisible(true);
            txtSecondLastname.requestFocus();
        }
    }//GEN-LAST:event_txtLastnameKeyReleased

    private void tableCustomerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableCustomerMouseClicked
        if (tableCustomer.getSelectedRowCount() >= 0) {
            llenaDatos(Integer.parseInt(tableCustomer.getValueAt(tableCustomer.getSelectedRow(), 3).toString()));
            selectedCustomer = getSelectCustomer(Long.parseLong(tableCustomer.getValueAt(tableCustomer.getSelectedRow(), 3).toString()));
            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        }
        txtName.selectAll();
        txtName.requestFocus();
        if (evt.getClickCount() == 2) {
            btnSaveActionPerformed(null);
        }
    }//GEN-LAST:event_tableCustomerMouseClicked

    private void txtBirthdateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBirthdateKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtBirthdateKeyPressed

    private void txtBirthdateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBirthdateKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            comboGender.requestFocus();
        }
    }//GEN-LAST:event_txtBirthdateKeyReleased

    private void comboGenderKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboGenderKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtRfc.selectAll();
            txtRfc.requestFocus();
        }
    }//GEN-LAST:event_comboGenderKeyReleased

    private void txtRfcKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRfcKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtBussinesname.selectAll();
            txtBussinesname.requestFocus();
        }
    }//GEN-LAST:event_txtRfcKeyReleased

    private void txtBussinesnameKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBussinesnameKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtStreet.selectAll();
            txtStreet.requestFocus();
        }
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            if (tableCustomer.getRowCount() <= 0) {
                txtStreet.selectAll();
                txtStreet.requestFocus();
            }
        }
    }//GEN-LAST:event_txtBussinesnameKeyReleased

    private void txtStreetKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStreetKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtInterior.selectAll();
            txtInterior.requestFocus();
        }
    }//GEN-LAST:event_txtStreetKeyReleased

    private void txtInteriorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInteriorKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtExterior.selectAll();
            txtExterior.requestFocus();
        }
    }//GEN-LAST:event_txtInteriorKeyReleased

    private void txtExteriorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtExteriorKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtColony.selectAll();
            txtColony.requestFocus();
        }
    }//GEN-LAST:event_txtExteriorKeyReleased

    private void txtColonyKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtColonyKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtCity.selectAll();
            txtCity.requestFocus();
        }
    }//GEN-LAST:event_txtColonyKeyReleased

    private void txtStateKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStateKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            detectGro(txtState.getText());
            txtPostalcode.selectAll();
            txtPostalcode.requestFocus();
        }
    }//GEN-LAST:event_txtStateKeyReleased

    private void txtPostalcodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPostalcodeKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtPhone.selectAll();
            txtPhone.requestFocus();
        }
    }//GEN-LAST:event_txtPostalcodeKeyReleased

    private void txtPhoneKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPhoneKeyReleased
        txtPhone.setText(txtPhone.getText().toUpperCase());
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnSave.requestFocus();
        }
    }//GEN-LAST:event_txtPhoneKeyReleased

    private void tableCustomerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tableCustomerKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            txtStreet.selectAll();
            txtStreet.requestFocus();
        }
        if (tableCustomer.getSelectedRowCount() > 0) {
            llenaDatos(Integer.parseInt(tableCustomer.getValueAt(tableCustomer.getSelectedRow(), 3).toString()));
            selectedCustomer = getSelectCustomer(Long.parseLong(tableCustomer.getValueAt(tableCustomer.getSelectedRow(), 3).toString()));
            btnQuitarSeleccion.setEnabled(true);
        }
    }//GEN-LAST:event_tableCustomerKeyReleased

    private void btnQuitarSeleccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarSeleccionActionPerformed
        if (tableCustomer.getSelectedRowCount() > 0) {
            fillTable(customers);
            selectedCustomer = null;
            cleanControls();
            txtName.setText("");
            txtNameKeyReleased(null);
            txtName.requestFocus();
            comboRecomendation.setSelectedIndex(0);
            txtName.grabFocus();
        }
    }//GEN-LAST:event_btnQuitarSeleccionActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        switch (this.opc) {
            case 0://Alta desde el pos
                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                if (!txtName.getText().isEmpty() && !txtLastname.getText().isEmpty()
                        //&& !txtMaterno.getText().isEmpty()
                        && !"  /  /    ".equals(txtBirthdate.getText())
                        && comboGender.getSelectedIndex() > 0
                        && !txtState.getText().isEmpty()
                        && !txtPhone.getText().isEmpty()) {
                    if (save()) {
                        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        this.dispose();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Todos los campos marcados con **, son obligatorios.");
                    txtName.selectAll();
                    txtName.requestFocus();
                }
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                break;
            case 1://Alta desde el doble clic del pos
                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                if (!txtName.getText().isEmpty() && !txtLastname.getText().isEmpty()
                        //&& !txtMaterno.getText().isEmpty()
                        && !"  /  /    ".equals(txtBirthdate.getText())
                        && comboGender.getSelectedIndex() > 0
                        && !txtState.getText().isEmpty()
                        && !txtPhone.getText().isEmpty()) {
                    if (save()) {
                        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        this.dispose();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Todos los campos marcados con **, son obligatorios.");
                    txtName.selectAll();
                    txtName.requestFocus();
                }
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                break;
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void tableCustomerFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tableCustomerFocusGained
        if (tableCustomer.getSelectedColumnCount() > 0) {
            btnQuitarSeleccion.setEnabled(true);
        } else {
            btnQuitarSeleccion.setEnabled(false);
        }
        if (tableCustomer.getRowCount() <= 0) {
            txtStreet.selectAll();
            txtStreet.grabFocus();
        }
    }//GEN-LAST:event_tableCustomerFocusGained

    private void txtCityKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCityKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtState.selectAll();
            txtState.requestFocus();
        }
    }//GEN-LAST:event_txtCityKeyReleased

    private void txtPostalcodeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPostalcodeKeyTyped
        isNoLetter(evt);
        if (txtPostalcode.getText().length() == 5) {
            evt.consume();
        }
    }//GEN-LAST:event_txtPostalcodeKeyTyped

    private void txtRfcKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRfcKeyTyped
        if (txtRfc.getText().length() == 13) {
            evt.consume();
        }
    }//GEN-LAST:event_txtRfcKeyTyped

    private void txtPhoneKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPhoneKeyTyped
        isNoLetter(evt);
        if (txtPhone.getText().length() == 13) {
            evt.consume();
        }
    }//GEN-LAST:event_txtPhoneKeyTyped

    private void txtBussinesnameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtBussinesnameFocusLost
        mayusConvert(txtBussinesname);
        //if(jTable1.getSelectedRow()>0){
        txtStreet.grabFocus();
        //}
    }//GEN-LAST:event_txtBussinesnameFocusLost

    private void tableCustomerFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tableCustomerFocusLost

    }//GEN-LAST:event_tableCustomerFocusLost

    private void txtNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNameFocusGained
        focus(txtName);
    }//GEN-LAST:event_txtNameFocusGained

    private void txtLastnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtLastnameFocusGained
        focus(txtLastname);
    }//GEN-LAST:event_txtLastnameFocusGained

    private void txtSecondLastnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtSecondLastnameFocusGained
        focus(txtSecondLastname);
    }//GEN-LAST:event_txtSecondLastnameFocusGained

    private void txtRfcFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtRfcFocusGained
        focus(txtRfc);
    }//GEN-LAST:event_txtRfcFocusGained

    private void txtBussinesnameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtBussinesnameFocusGained
        focus(txtBussinesname);
    }//GEN-LAST:event_txtBussinesnameFocusGained

    private void txtStreetFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtStreetFocusGained
        focus(txtStreet);
    }//GEN-LAST:event_txtStreetFocusGained

    private void txtInteriorFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtInteriorFocusGained
        focus(txtInterior);
    }//GEN-LAST:event_txtInteriorFocusGained

    private void txtExteriorFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtExteriorFocusGained
        focus(txtExterior);
    }//GEN-LAST:event_txtExteriorFocusGained

    private void txtColonyFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtColonyFocusGained
        focus(txtColony);
    }//GEN-LAST:event_txtColonyFocusGained

    private void txtCityFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCityFocusGained
        focus(txtCity);
    }//GEN-LAST:event_txtCityFocusGained

    private void txtStateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtStateFocusGained
        focus(txtState);
    }//GEN-LAST:event_txtStateFocusGained

    private void txtPostalcodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPostalcodeFocusGained
        focus(txtPostalcode);
    }//GEN-LAST:event_txtPostalcodeFocusGained

    private void txtPhoneFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPhoneFocusGained
        focus(txtPhone);
    }//GEN-LAST:event_txtPhoneFocusGained

    private void txtRfcFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtRfcFocusLost
        mayusConvert(txtRfc);
    }//GEN-LAST:event_txtRfcFocusLost

    private void txtStreetFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtStreetFocusLost
        mayusConvert(txtStreet);
    }//GEN-LAST:event_txtStreetFocusLost

    private void txtInteriorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtInteriorFocusLost
        mayusConvert(txtInterior);
    }//GEN-LAST:event_txtInteriorFocusLost

    private void txtColonyFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtColonyFocusLost
        mayusConvert(txtColony);
    }//GEN-LAST:event_txtColonyFocusLost

    private void txtCityFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCityFocusLost
        mayusConvert(txtCity);
    }//GEN-LAST:event_txtCityFocusLost

    private void txtStateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtStateFocusLost
        mayusConvert(txtState);
        detectGro(txtState.getText());
    }//GEN-LAST:event_txtStateFocusLost

    private void txtPostalcodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPostalcodeFocusLost
        mayusConvert(txtPostalcode);
    }//GEN-LAST:event_txtPostalcodeFocusLost

    private void txtPhoneFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPhoneFocusLost
        mayusConvert(txtPhone);
    }//GEN-LAST:event_txtPhoneFocusLost

    private void txtExteriorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtExteriorFocusLost
        mayusConvert(txtExterior);
    }//GEN-LAST:event_txtExteriorFocusLost

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
//        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
//        altaDoctor = new DialogAltaDoctor(this.panePos, true, this, 2);
//        altaDoctor.setVisible(true);
//        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tableCustomerHierarchyChanged(java.awt.event.HierarchyEvent evt) {//GEN-FIRST:event_tableCustomerHierarchyChanged

    }//GEN-LAST:event_tableCustomerHierarchyChanged

    private void txtIdCustomerKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIdCustomerKeyReleased

    }//GEN-LAST:event_txtIdCustomerKeyReleased

    private void txtIdCustomerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIdCustomerKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtIdCustomerKeyPressed

    private void txtNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNameKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtNameKeyPressed

    private void txtLastnameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLastnameKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtLastnameKeyPressed

    private void txtSecondLastnameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSecondLastnameKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtSecondLastnameKeyPressed

    private void txtAgeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAgeKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtAgeKeyPressed

    private void comboGenderKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboGenderKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_comboGenderKeyPressed

    private void txtRfcKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRfcKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtRfcKeyPressed

    private void txtBussinesnameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBussinesnameKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtBussinesnameKeyPressed

    private void txtStreetKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStreetKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtStreetKeyPressed

    private void txtInteriorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtInteriorKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtInteriorKeyPressed

    private void txtExteriorKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtExteriorKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtExteriorKeyPressed

    private void txtColonyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtColonyKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtColonyKeyPressed

    private void txtCityKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCityKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtCityKeyPressed

    private void txtStateKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtStateKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtStateKeyPressed

    private void txtPostalcodeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPostalcodeKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtPostalcodeKeyPressed

    private void txtPhoneKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPhoneKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtPhoneKeyPressed

    private void comboRecomendationKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboRecomendationKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_comboRecomendationKeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jButton1KeyPressed

    private void btnSaveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnSaveKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnSaveKeyPressed

    private void btnQuitarSeleccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnQuitarSeleccionKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnQuitarSeleccionKeyPressed

    private void tableCustomerMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableCustomerMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_tableCustomerMouseEntered

    private void btnSaveKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnSaveKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnSaveActionPerformed(null);
        }
    }//GEN-LAST:event_btnSaveKeyReleased

    private void txtBirthdateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtBirthdateFocusGained
        txtBirthdate.selectAll();
    }//GEN-LAST:event_txtBirthdateFocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogAltaRapidaPaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogAltaRapidaPaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogAltaRapidaPaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogAltaRapidaPaciente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogAltaRapidaPaciente dialog = new DialogAltaRapidaPaciente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnQuitarSeleccion;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox comboGender;
    public static final javax.swing.JComboBox comboRecomendation = new javax.swing.JComboBox();
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableCustomer;
    private javax.swing.JTextField txtAge;
    private javax.swing.JFormattedTextField txtBirthdate;
    private javax.swing.JTextField txtBussinesname;
    private javax.swing.JTextField txtCity;
    private javax.swing.JTextField txtColony;
    private javax.swing.JTextField txtExterior;
    private javax.swing.JTextField txtIdCustomer;
    private javax.swing.JTextField txtInterior;
    private javax.swing.JTextField txtLastname;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPhone;
    private javax.swing.JTextField txtPostalcode;
    private javax.swing.JTextField txtRfc;
    private javax.swing.JTextField txtSecondLastname;
    private javax.swing.JTextField txtState;
    private javax.swing.JTextField txtStreet;
    // End of variables declaration//GEN-END:variables

    public void init_() throws SQLException, IOException {
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        this.setLocationRelativeTo(null);
        extractDoctors();
        //revisaRquerimentos();
        tableCustomer.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        switch (this.opc) {
            case 0:
                break;
            case 1://Viene del doble clic en uno de los campos del pos
                fillCustomers(this.senderCustomer);
                break;
        }
    }

    private void fillTable(List<Cliente> clientes) {
        Object[][] datos = new Object[clientes.size()][4];
        int i = 0;
        for (Cliente cliente : clientes) {
            datos[i][0] = cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno();
            datos[i][1] = cliente.getClienteCalle() + " "
                    + cliente.getClienteNumExt() + " "
                    + cliente.getClienteNumInt() + " "
                    + cliente.getClienteColonia() + " "
                    + cliente.getClienteCiudad() + " "
                    + cliente.getClienteEdo();
            if (cliente.getClienteFnac() != null) {
                datos[i][2] = FormatDateUtil.SFD_DDMMYYYY.format(cliente.getClienteFnac());
            } else {
                datos[i][2] = null;
            }
            datos[i][3] = cliente.getClienteId();
            i++;
        }

        tableCustomer.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                new String[]{
                    "Nombre", "Dirección", "F. Nac.", ""
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        tableCustomer.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);

        tableCustomer.getTableHeader().setReorderingAllowed(false);

        tableCustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableCustomerMouseClicked(evt);
            }
        });
        tableCustomer.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tableCustomerKeyReleased(evt);
            }
        });

        jScrollPane1.setViewportView(tableCustomer);

        if (tableCustomer.getColumnModel().getColumnCount() > 0) {
            tableCustomer.getColumnModel().getColumn(0).setResizable(false);
            tableCustomer.getColumnModel().getColumn(0).setPreferredWidth(300);
            tableCustomer.getColumnModel().getColumn(1).setResizable(false);
            tableCustomer.getColumnModel().getColumn(1).setPreferredWidth(350);
            tableCustomer.getColumnModel().getColumn(2).setResizable(false);
            tableCustomer.getColumnModel().getColumn(2).setPreferredWidth(100);
            tableCustomer.getColumnModel().getColumn(3).setResizable(false);
            tableCustomer.getColumnModel().getColumn(3).setPreferredWidth(0);
        }

        JTableHeader jtableHeader = tableCustomer.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tableCustomer.setTableHeader(jtableHeader);

        tableCustomer.setRowHeight(25);

        tableCustomer.getColumnModel().getColumn(0).setCellRenderer(new CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tableCustomer.getColumnModel().getColumn(1).setCellRenderer(new CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tableCustomer.getColumnModel().getColumn(2).setCellRenderer(new CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tableCustomer.getColumnModel().getColumn(3).setCellRenderer(new CellRendererUtil(CellRendererEnum.PK_ID_HIDDEN.toString()));
        //seek.mx.pv.cliente.util.table.addFilter(jTable1);
    }

    private void llenaDatos(int id) {
        customers.stream().filter(cliente -> (cliente.getClienteId() == id)).map(cliente -> {
            fillData(cliente);
            return cliente;
        }).forEachOrdered(_item -> {
            comboRecomendation.setSelectedIndex(0);
        });
    }

    private void cleanControls() {
        txtAge.setText("");
        txtName.setText("");
        txtLastname.setText("");
        txtSecondLastname.setText("");
        txtBirthdate.setText("");
        txtRfc.setText("");
        txtBussinesname.setText("");
        txtStreet.setText("");
        txtInterior.setText("");
        txtExterior.setText("");
        txtColony.setText("");
        txtCity.setText("");
        txtState.setText("");
        txtPostalcode.setText("");
        txtPhone.setText("");
        comboGender.setSelectedIndex(0);
        if (tableCustomer.getSelectedRowCount() == 0) {
            txtIdCustomer.setText("");
        }
    }

    private void isNoLetter(java.awt.event.KeyEvent evt) {
        char c = evt.getKeyChar();
        if (Character.isLetter(c)) {
            getToolkit().beep();
            evt.consume();
        }
    }

    private void focus(JTextField txt) {
        txt.selectAll();
        txt.grabFocus();
    }

    public static void mayusConvert(javax.swing.JTextField c) {
        String toUpperCase = c.getText().toUpperCase();
        c.setText(toUpperCase);
    }

    private Cliente getSelectCustomer(long id) {
        for (Cliente c : customers) {
            if (c.getClienteId() == id) {
                return c;
            }
        }
        return null;
    }

    public void extractDoctors() {
//        ServiceDoctor sDoctor = new ServiceDoctorImpl();
//        List<Doctor> arr = sDoctor.mostrarDoctores();
//        jcbRecomienda.removeAllItems();
//        jcbRecomienda.addItem("<Seleccione>");
//        for (Doctor dr : arr) {
//            jcbRecomienda.addItem(dr.getDoctorId() + ". " + dr.getDoctorNombre() + " " + dr.getDoctorPaterno() + " " + dr.getDoctorMaterno());
//        }
    }

    private boolean compareCustomers(Cliente cl1, Cliente cl2) {
        boolean res = false;
        if (cl1.getClienteGenero().equals(cl2.getClienteGenero())
                && cl1.getClienteCalle().equals(cl2.getClienteCalle())
                && cl1.getClienteCiudad().equals(cl2.getClienteCiudad())
                && cl1.getClienteColonia().equals(cl2.getClienteColonia())
                && cl1.getClienteCp().equals(cl2.getClienteCp())
                && cl1.getClienteEdo().equals(cl2.getClienteEdo())
                && cl1.getClienteFnac().equals(cl2.getClienteFnac())
                && cl1.getClienteMaterno().equals(cl2.getClienteMaterno())
                && cl1.getClienteNombre().equals(cl2.getClienteNombre())
                && cl1.getClienteNumExt().equals(cl2.getClienteNumExt())
                && cl1.getClienteNumInt().equals(cl2.getClienteNumInt())
                && cl1.getClientePaterno().equals(cl2.getClientePaterno())
                && cl1.getClienteRfc().equals(cl2.getClienteRfc())
                && cl1.getClienteTel().equals(cl2.getClienteTel())) {
            res = true;
        }
        return res;
    }

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 113://F2 
                break;
            case 114://F3
                btnQuitarSeleccionActionPerformed(null);
                break;
            case 115://F4 
                break;
            case 116://F5
                btnSaveActionPerformed(null);
                break;
            case 117://F6
                break;
            case 118://F7
                break;
            case 119://F8
                break;
            case 120://F9
                break;
            case 121://F10
                break;
            case 27://Esc
                this.dispose();
                break;
        }
    }

    private void fillCustomers(Cliente cliente) {
        try {
            fillData(cliente);
            comboRecomendation.setSelectedIndex(0);
//        ServiceCliente cd = new ServiceClienteImpl();
//        customers = cd.mostrarClientes(CUSTOMER_NAME_LIKE + txtNombre.getText().toUpperCase()
//                + CUSTOMER_LASTNAME_LIKE + txtPaterno.getText().toLowerCase()
//                + CUSTOMER_MOTHER_LASTNAME_LIKE + txtMaterno.getText().toLowerCase() + "%')");

            customers = (List<Cliente>) service.findBy(new Cliente(), CUSTOMER_NAME_LIKE + txtName.getText().toUpperCase()
                    + CUSTOMER_LASTNAME_LIKE + txtLastname.getText().toLowerCase()
                    + CUSTOMER_MOTHER_LASTNAME_LIKE + txtSecondLastname.getText().toLowerCase() + "%')");
            fillTable(customers);
            tableCustomer.setRowSelectionInterval(0, 0);
            this.selectedCustomer = senderCustomer;
        } catch (Exception ex) {
            Logger.getLogger(DialogAltaRapidaPaciente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean save() {
        ServiceCliente sCliente = new ServiceClienteImpl();
        Cliente cliente = getCustomerFromControls();
        boolean logrado = false;
        if (selectedCustomer != null) {//Actualizacion de datos
            cliente.setClienteId(selectedCustomer.getClienteId());
            cliente.setClienteRazonSoc(selectedCustomer.getClienteRazonSoc());
            cliente.setClienteMpio(selectedCustomer.getClienteMpio());
            cliente.setClientePais(selectedCustomer.getClientePais());
            cliente.setClienteStatus(selectedCustomer.getClienteStatus());
            cliente.setClienteEmail(selectedCustomer.getClienteEmail());
            cliente.setClienteFechRegistro(selectedCustomer.getClienteFechRegistro());
            cliente.setSucursal(selectedCustomer.getSucursal());
            if (!compareCustomers(cliente, selectedCustomer)) {//Hay algun cambio
                if (sCliente.modificarCliente(cliente)) {
                    DialogAltaRapidaPaciente.panePos.txtPaciente.setText("[" + cliente.getClienteId() + "] " + cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
                    DialogAltaRapidaPaciente.panePos.txtDireccion.setText(cliente.getClienteCalle() + ", # " + cliente.getClienteNumExt() + " - " + cliente.getClienteNumInt() + ", COL. " + cliente.getClienteColonia() + ", LUGAR: " + cliente.getClienteCiudad() + ", " + cliente.getClienteEdo());
                    DialogAltaRapidaPaciente.panePos.txtRfc.setText(cliente.getClienteRfc());
                    DialogAltaRapidaPaciente.panePos.cliente = cliente;
                    DialogAltaRapidaPaciente.panePos.txtPaciente.requestFocus();
                    logrado = true;
                }
            } else {//No hay cambios
                DialogAltaRapidaPaciente.panePos.txtPaciente.setText("[" + cliente.getClienteId() + "] " + cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
                DialogAltaRapidaPaciente.panePos.txtDireccion.setText(cliente.getClienteCalle() + ", # " + cliente.getClienteNumExt() + " - " + cliente.getClienteNumInt() + ", COL. " + cliente.getClienteColonia() + ", LUGAR: " + cliente.getClienteCiudad() + ", " + cliente.getClienteEdo());
                DialogAltaRapidaPaciente.panePos.txtRfc.setText(cliente.getClienteRfc());
                DialogAltaRapidaPaciente.panePos.cliente = cliente;
                DialogAltaRapidaPaciente.panePos.txtPaciente.requestFocus();
                logrado = true;
            }
        } else {//Nuevo registro
            StringBuilder sb = new StringBuilder();
            List<Cliente> customerRepeats = new ArrayList<>();
            if (!"".equals(cliente.getClienteRfc())) {
                customerRepeats = sCliente.mostrarClientes("clienteRfc = '" + cliente.getClienteRfc() + "'");
            }
            if (customerRepeats.size() > 0) {
                String clientesString = "";
                for (Cliente cl : customerRepeats) {
                    sb.append(clientesString);
                    sb.append("\n[");
                    sb.append(cl.getClienteId());
                    sb.append("] ");
                    sb.append(cl.getClienteNombre());
                    sb.append(" ");
                    sb.append(cl.getClientePaterno());
                    sb.append(" ");
                    sb.append(cl.getClienteMaterno());
                    sb.append(" [");
                    sb.append(cl.getClienteRfc());
                    sb.append("]");
                    clientesString = sb.toString();
                    this.customers.add(cl);
                }
                JOptionPane.showMessageDialog(panePos, "El paciente ya se encuentra registrado: "
                        + clientesString);
                fillTable(customerRepeats);
                tableCustomer.grabFocus();
            } else {
                long id = sCliente.insertarCliente(cliente);
                if (id > -1) {
                    Cliente clte = sCliente.obtenerCliente("clienteId = " + id);
                    DialogAltaRapidaPaciente.panePos.txtPaciente.setText("[" + clte.getClienteId() + "] " + clte.getClienteNombre() + " " + clte.getClientePaterno() + " " + clte.getClienteMaterno());
                    DialogAltaRapidaPaciente.panePos.txtDireccion.setText(clte.getClienteCalle() + ", # " + clte.getClienteNumExt() + " - " + clte.getClienteNumInt() + ", COL. " + clte.getClienteColonia() + ", LUGAR: " + clte.getClienteCiudad() + ", " + clte.getClienteEdo());
                    DialogAltaRapidaPaciente.panePos.txtRfc.setText(clte.getClienteRfc());
                    DialogAltaRapidaPaciente.panePos.cliente = clte;
                    DialogAltaRapidaPaciente.panePos.txtPaciente.requestFocus();
                    logrado = true;
                }
            }
        }
        return logrado;
    }

    private Cliente getCustomerFromControls() {
        try {
            Cliente cliente = new Cliente();
            cliente.setClienteId(-1L);
            cliente.setClienteNombre(txtName.getText().trim());
            cliente.setClientePaterno(txtLastname.getText().trim());
            cliente.setClienteMaterno((!txtSecondLastname.getText().isEmpty()) ? txtSecondLastname.getText() : " ");
            cliente.setClienteRazonSoc("");
            if (!txtInterior.getText().isEmpty()) {
                cliente.setClienteNumInt(txtInterior.getText());
            } else {
                cliente.setClienteNumInt("");
            }
            if (!txtExterior.getText().isEmpty()) {
                cliente.setClienteNumExt(txtExterior.getText());
            } else {
                cliente.setClienteNumExt("");
            }
            cliente.setClienteCalle(txtStreet.getText());
            cliente.setClienteColonia(txtColony.getText());
            cliente.setClienteCiudad(txtCity.getText());
            cliente.setClienteMpio("");
            cliente.setClienteEdo(txtState.getText());
            cliente.setClienteTel((txtPhone.getText().length() == 10)?txtPhone.getText():"");
            cliente.setClienteRfc(txtRfc.getText());
            cliente.setClienteTalla(1);//Borrar este campo
            cliente.setClienteCp(txtPostalcode.getText());
            cliente.setClientePais("");
            cliente.setClienteFnac(FormatDateUtil.SFD_DDMMYYYY.parse(txtBirthdate.getText()));
            switch (comboGender.getSelectedIndex()) {
                case 1:
                    cliente.setClienteGenero(true);
                    break;
                case 2:
                    cliente.setClienteGenero(false);
                    break;
                default:
                    cliente.setClienteGenero(null);
                    break;
            }
            cliente.setClienteEmail("");
            cliente.setClienteStatus("1");
            cliente.setClienteFechRegistro(new java.util.Date());

            mx.com.cdadb.service.ServiceSucursalLocal serviceSucursal = new mx.com.cdadb.service.impl.ServiceSucursalLocalImp();
            List<mx.com.cdadb.entities.Sucursal> sucursales = serviceSucursal.findAll();
            Sucursal suc = new Sucursal();
            suc.setSucursalId(sucursales.get(0).getSucursalId());
            cliente.setSucursal(suc);

            Recomendacion recomendacion = new Recomendacion();
            ServiceDoctor ddao = new ServiceDoctorImpl();

            if (comboRecomendation.getSelectedIndex() > 0) {
                List<String> arr = Arrays.asList(comboRecomendation.getSelectedItem().toString().split("\\. "));
                recomendacion.setRecomendacionId((long) -1);
                recomendacion.setDoctor(ddao.obtenerDoctor("doctorId = " + arr.get(0)));
                recomendacion.setRecomendacionStatus("A");
                recomendacion.setCliente(cliente);
                Set<Recomendacion> recomendaciones = new HashSet<>();
                recomendaciones.add(recomendacion);
                cliente.setRecomendacions(recomendaciones);
            } else {
                cliente.setRecomendacions(null);
            }
            return cliente;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            LoggerUtil.registrarError(ex);
        }
        return null;
    }

    public void findByNameOrderByPaternoMaternoAsc() {
        try {
            //ServiceCliente cd = new ServiceClienteImpl();
            //clientes = cd.mostrarClientes(CUSTOMER_NAME_LIKE + txtNombre.getText().trim().toUpperCase() + "%'");
            customers = (List<Cliente>) service.findBy(new Cliente(), CUSTOMER_NAME_LIKE + txtName.getText().trim().toUpperCase() + "%' order by t.clientePaterno, t.clienteMaterno asc");
            fillTable(customers);
        } catch (Exception ex) {
            Logger.getLogger(DialogAltaRapidaPaciente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
        public void findByNameAndPaternoOrderByPaternoMaternoAsc() {
        try {
            //ServiceCliente cd = new ServiceClienteImpl();
            //clientes = cd.mostrarClientes(CUSTOMER_NAME_LIKE + txtNombre.getText().trim().toUpperCase() + "%'");
            customers = (List<Cliente>) service.findBy(new Cliente(), CUSTOMER_NAME_LIKE + txtName.getText().trim().toUpperCase() + "%' and t.clientePaterno like '%"+txtLastname.getText().trim().toUpperCase()+"%' and t.clientePaterno like '%"+txtLastname.getText().trim().toUpperCase()+"%' order by t.clienteNombre, t.clientePaterno, t.clienteMaterno asc");
            fillTable(customers);
        } catch (Exception ex) {
            Logger.getLogger(DialogAltaRapidaPaciente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void detectGro(String text) {
        List<String> list = Arrays.asList("GRO", " GRO", "GRO ", "G R O", " G R O", "G R O ", "GRRO", " GRRO", "GRRO ", "GERR", " GUERR", "GEO", "GERO", "GGRO", "GR", "GRO.", " GRO.", "GRO. ");
        list.forEach(cad -> {
            if (cad.contains(text)) {
                txtState.setText("GUERRERO");
            }
        });
    }

    private void fillData(Cliente customer) {
        txtIdCustomer.setText(String.format("%010d", customer.getClienteId()));
        txtName.setText(customer.getClienteNombre());
        txtLastname.setText(customer.getClientePaterno());
        txtSecondLastname.setText(customer.getClienteMaterno());
        if (customer.getClienteFnac() != null) {
            txtBirthdate.setText(FormatDateUtil.SFD_DDMMYYYY.format(customer.getClienteFnac()));
            txtBirthdateFocusLost(null);
        }
        txtRfc.setText(customer.getClienteRfc());
        txtBussinesname.setText(customer.getClienteRazonSoc());
        txtStreet.setText(customer.getClienteCalle());
        txtInterior.setText(customer.getClienteNumInt());
        txtExterior.setText(customer.getClienteNumExt());
        txtColony.setText(customer.getClienteColonia());
        txtCity.setText(customer.getClienteCiudad());
        txtState.setText(customer.getClienteEdo());
        txtPostalcode.setText(customer.getClienteCp());
        txtPhone.setText(customer.getClienteTel());
        if (customer.getClienteGenero()) {
            comboGender.setSelectedIndex(1);
        } else if (!customer.getClienteGenero()) {
            comboGender.setSelectedIndex(2);
        } else {
            comboGender.setSelectedIndex(0);
        }
    }
}
