package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.constants.ConstantFiles;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceSucursalLocal;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.service.impl.ServiceSucursalLocalImp;
import mx.com.cdadb.util.Directory;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.util.PropertiesDirectories;
import mx.com.cdadb.util.SendFile;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.util.KeyGenerator;

/**
 * 
 * @author macbook
 */
public class ReportView extends javax.swing.JDialog {

    private JasperPrint jasperPrint = null;
    private int opc = -1;
    private List<Producto> productos = null;

    public ReportView(java.awt.Frame parent, boolean modal, JasperPrint jasp, int op, List<Producto> productos) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(0);
        this.getRootPane().setDefaultButton(jButton1);
        this.productos = productos;
        this.jasperPrint = jasp;
        this.opc = op;
        initGui();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        panelReport = new javax.swing.JPanel();
        btnPdf = new javax.swing.JButton();
        btnExcel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setIconImage(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_x32.png")).getImage());
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jButton1.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jButton1.setText("Salir [Esc]");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        panelReport.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        panelReport.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                panelReportKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout panelReportLayout = new javax.swing.GroupLayout(panelReport);
        panelReport.setLayout(panelReportLayout);
        panelReportLayout.setHorizontalGroup(
            panelReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1083, Short.MAX_VALUE)
        );
        panelReportLayout.setVerticalGroup(
            panelReportLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 519, Short.MAX_VALUE)
        );

        btnPdf.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        btnPdf.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/pdfx24.png"))); // NOI18N
        btnPdf.setText("PDF [F5]");
        btnPdf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPdfActionPerformed(evt);
            }
        });
        btnPdf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                btnPdfKeyReleased(evt);
            }
        });

        btnExcel.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        btnExcel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/excelx24.png"))); // NOI18N
        btnExcel.setText("Excel [F6]");
        btnExcel.setEnabled(false);
        btnExcel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPdf, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExcel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(panelReport, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnPdf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExcel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelReport, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void panelReportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_panelReportKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_panelReportKeyPressed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_formKeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int o = JOptionPane.showConfirmDialog(null, "Esta seguro que desea cerrar el reporte?", "Aviso importante.", JOptionPane.YES_NO_OPTION);
        if (o == 0) {
            if (this.productos != null) {
                createXls();
            }
            dispose();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnPdfActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPdfActionPerformed
        exportPdf();
    }//GEN-LAST:event_btnPdfActionPerformed

    private void btnPdfKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnPdfKeyReleased
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnPdfKeyReleased

    private void btnExcelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcelActionPerformed
        switch (this.opc) {
            case 1:
                break;
            case 2:
                iniExportToExcel();
                break;
        }
    }//GEN-LAST:event_btnExcelActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ReportView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ReportView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ReportView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ReportView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ReportView dialog = new ReportView(new javax.swing.JFrame(), true, null, -1, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExcel;
    private javax.swing.JButton btnPdf;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    public javax.swing.JPanel panelReport;
    // End of variables declaration//GEN-END:variables

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 27://Esc
                jButton1ActionPerformed(null);
                break;
            case 116://F5 PDF
                if (btnPdf.isEnabled()) {
                    exportPdf();
                }
                break;
            case 117://F6 EXCEL

                break;
        }
    }

    private void exportPdf() {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(this.getParent()) == JFileChooser.APPROVE_OPTION) {
            File directorio = fileChooser.getSelectedFile();
            //Se guarda copia PDF en Servidor
            try {
                //se crea el archivo PDF
                JasperExportManager.exportReportToPdfFile(jasperPrint, directorio + ".pdf");
                JOptionPane.showMessageDialog(null, "Archivo, guardado correctamente.");
            } catch (JRException ex) {
                //Logger.getLogger(ReportView.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
            }
        }

    }

    private void initGui() {
        switch (this.opc) {
            case 1:
                btnExcel.setEnabled(false);
                break;
            case 2:
                btnExcel.setEnabled(true);
                break;
        }
    }

    private void iniExportToExcel() {
//        JFileChooser fileChooser = new JFileChooser();
//        if (fileChooser.showSaveDialog(this.getParent()) == JFileChooser.APPROVE_OPTION) {
//            File directorio = fileChooser.getSelectedFile();
//            if (createXls(directorio.toString())) {
//                JOptionPane.showMessageDialog(null, "Archivo guardado correctamente.");
//            }
//        }
    }

    private boolean createXls() {
        boolean logrado = false;
        float devolucion = 0;
        float total = 0;

        Directory dir = PropertiesDirectories.decryptDirectories(ConstantFiles.DIR_PROPERTIES);
        
        File directorio = new File(dir.getDirTMP());

        directorio.mkdir();

        java.util.Date hoy = new java.util.Date();
        String nombre = "";

        try {
            ServiceApp sApp = new ServiceAppImpl();
            App rSocial = sApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_COMPANY_BUSSINES_NAME+"'");
            App direccion = sApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_COMPANY_BUSSINES_ADDRES+"'");
            App rfc = sApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_COMPANY_RFC+"'");
            App sucursalId = sApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_BRANCH_ID+"'");
            ServiceSucursalLocal sSuc = new ServiceSucursalLocalImp();
            Sucursal sucursal = sSuc.getBy("s.sucursalId = " + sucursalId.getAppValor());

            nombre = "Ventas_sucursal_" + sucursal.getSucursalNombre() + "_" + FormatDateUtil.SFD_DMY.format(hoy) + "_" + FormatDateUtil.SFD_KKMMSS_SIN.format(hoy);

            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet("Reporte de ventas " + FormatDateUtil.SFD_DMY.format(hoy));
            File archivo = new File(directorio.getAbsolutePath() + "/" + nombre + ".xls");
            CreationHelper createHelper = workbook.getCreationHelper();

            /**
             * Estilos de celda
             * **************************************************
             */
            // Titulo principal
            HSSFCellStyle stylePrincipal = workbook.createCellStyle();
            stylePrincipal = workbook.createCellStyle();
            HSSFFont f = workbook.createFont();
            f.setFontName(HSSFFont.FONT_ARIAL);
            f.setFontHeightInPoints((short) 10);
            f.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            f.setColor(HSSFColor.BLACK.index);
            stylePrincipal.setFont(f);

            //Subtitulos
            HSSFCellStyle styleSubt = workbook.createCellStyle();
            styleSubt = workbook.createCellStyle();
            HSSFFont f1 = workbook.createFont();
            f1.setFontName(HSSFFont.FONT_ARIAL);
            f1.setFontHeightInPoints((short) 8);
            f1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            f1.setColor(HSSFColor.BLACK.index);
            styleSubt.setFont(f1);

            //Logo 10px
            HSSFCellStyle styleLogoCenterX10 = workbook.createCellStyle();
            HSSFFont f2 = workbook.createFont();
            f2.setFontName(HSSFFont.FONT_ARIAL);
            f2.setFontHeightInPoints((short) 10);
            f2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            f2.setColor(HSSFColor.RED.index);
            styleLogoCenterX10.setFont(f2);
            styleLogoCenterX10.setAlignment((short) 2);

            //Logo 16px
            HSSFCellStyle styleLogoCenterX16 = workbook.createCellStyle();
            HSSFFont f3 = workbook.createFont();
            f3.setFontName(HSSFFont.FONT_ARIAL);
            f3.setFontHeightInPoints((short) 16);
            f3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            f3.setColor(HSSFColor.RED.index);
            styleLogoCenterX16.setFont(f3);
            styleLogoCenterX16.setAlignment((short) 2);

            //totales
            HSSFCellStyle styleTotal = workbook.createCellStyle();
            styleTotal = workbook.createCellStyle();
            HSSFFont f4 = workbook.createFont();
            f4.setFontName(HSSFFont.FONT_ARIAL);
            f4.setFontHeightInPoints((short) 10);
            f4.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            f4.setColor(HSSFColor.BLACK.index);
            stylePrincipal.setFont(f4);
            styleTotal.setAlignment((short) 1);

            //Estilo para celdas numericas Negritas
            HSSFCellStyle styleNumericBold = workbook.createCellStyle();
            HSSFFont f5 = workbook.createFont();
//        styleNumericBold.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//        styleNumericBold.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
            styleNumericBold.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleNumericBold.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
            styleNumericBold.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleNumericBold.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleNumericBold.setBorderTop(HSSFCellStyle.BORDER_THIN);
            styleNumericBold.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            styleNumericBold.setBorderRight(HSSFCellStyle.BORDER_THIN);
            styleNumericBold.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            styleNumericBold.setTopBorderColor(IndexedColors.BLACK.getIndex());
            styleNumericBold.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            styleNumericBold.setRightBorderColor(IndexedColors.BLACK.getIndex());
            f5.setFontName(HSSFFont.FONT_ARIAL);
            f5.setFontHeightInPoints((short) 10);
            f5.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            f5.setColor(HSSFColor.WHITE.index);
            styleNumericBold.setFont(f5);
            styleNumericBold.setDataFormat(createHelper.createDataFormat().getFormat("0.00"));

            //Estilo para celdas numericas Plain
            HSSFCellStyle styleNumericPlain = workbook.createCellStyle();
            styleNumericPlain.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleNumericPlain.setFillForegroundColor(HSSFColor.WHITE.index);
            styleNumericPlain.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleNumericPlain.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleNumericPlain.setBorderTop(HSSFCellStyle.BORDER_THIN);
            styleNumericPlain.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            styleNumericPlain.setBorderRight(HSSFCellStyle.BORDER_THIN);
            styleNumericPlain.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            styleNumericPlain.setTopBorderColor(IndexedColors.BLACK.getIndex());
            styleNumericPlain.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            styleNumericPlain.setRightBorderColor(IndexedColors.BLACK.getIndex());
            HSSFFont f6 = workbook.createFont();
            f6.setFontName(HSSFFont.FONT_ARIAL);
            f6.setFontHeightInPoints((short) 10);
            f6.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            f6.setColor(HSSFColor.BLACK.index);
            styleNumericPlain.setFont(f6);
            styleNumericPlain.setDataFormat(createHelper.createDataFormat().getFormat("0.00"));

            // Encabezado
            HSSFCellStyle styleHeader = workbook.createCellStyle();
            styleHeader = workbook.createCellStyle();
            styleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleHeader.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
            styleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleHeader.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleHeader.setBorderTop(HSSFCellStyle.BORDER_THIN);
            styleHeader.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            styleHeader.setBorderRight(HSSFCellStyle.BORDER_THIN);
            styleHeader.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            styleHeader.setTopBorderColor(IndexedColors.BLACK.getIndex());
            styleHeader.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            styleHeader.setRightBorderColor(IndexedColors.BLACK.getIndex());
            HSSFFont f7 = workbook.createFont();
            f7.setFontName(HSSFFont.FONT_ARIAL);
            f7.setFontHeightInPoints((short) 10);
            f7.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            f7.setColor(HSSFColor.WHITE.index);
            styleHeader.setFont(f7);

            //Estilo para celdas numericas Negritas negativas
            HSSFCellStyle styleNumericBoldM = workbook.createCellStyle();
            HSSFFont f8 = workbook.createFont();
            styleNumericBoldM.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleNumericBoldM.setFillForegroundColor(HSSFColor.ORANGE.index);
            styleNumericBoldM.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleNumericBoldM.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleNumericBoldM.setBorderTop(HSSFCellStyle.BORDER_THIN);
            styleNumericBoldM.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            styleNumericBoldM.setBorderRight(HSSFCellStyle.BORDER_THIN);
            styleNumericBoldM.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            styleNumericBoldM.setTopBorderColor(IndexedColors.BLACK.getIndex());
            styleNumericBoldM.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            styleNumericBoldM.setRightBorderColor(IndexedColors.BLACK.getIndex());
            f8.setFontName(HSSFFont.FONT_ARIAL);
            f8.setFontHeightInPoints((short) 10);
            f8.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            f8.setColor(HSSFColor.WHITE.index);
            styleNumericBoldM.setFont(f8);

            //Estilo para celdas normales
            HSSFCellStyle styleNormal = workbook.createCellStyle();
            //HSSFFont f8 = workbook.createFont();
            styleNormal.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleNormal.setFillForegroundColor(HSSFColor.WHITE.index);
            styleNormal.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            styleNormal.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleNormal.setBorderTop(HSSFCellStyle.BORDER_THIN);
            styleNormal.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            styleNormal.setBorderRight(HSSFCellStyle.BORDER_THIN);
            styleNormal.setBottomBorderColor(IndexedColors.BLACK.getIndex());
            styleNormal.setTopBorderColor(IndexedColors.BLACK.getIndex());
            styleNormal.setLeftBorderColor(IndexedColors.BLACK.getIndex());
            styleNormal.setRightBorderColor(IndexedColors.BLACK.getIndex());
            //f8.setFontName(HSSFFont.FONT_ARIAL);
            //f8.setFontHeightInPoints((short) 10);
            //f8.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            //f8.setColor(HSSFColor.WHITE.index);
            //styleNumericBoldM.setFont(f8);

            styleNumericBoldM.setDataFormat(createHelper.createDataFormat().getFormat("0.00"));
            //**********************************************************************

            HSSFRow fila1 = sheet.createRow(0);
            HSSFRow fila2 = sheet.createRow(1);
            HSSFRow fila3 = sheet.createRow(2);
            HSSFRow fila4 = sheet.createRow(4);
            //Crea las filas
            HSSFCell celda1 = fila1.createCell(0);
            HSSFCell celda2 = fila2.createCell(0);
            HSSFCell celda3 = fila3.createCell(0);
            HSSFCell celda4 = fila4.createCell(0);
            HSSFCell celda5 = fila4.createCell(4);
            HSSFCell celda6 = fila4.createCell(5);
            //HSSFCell celda7 = fila2.createCell(4);
            HSSFCell celda8 = fila2.createCell(6);
            HSSFCell celda9 = fila1.createCell(6);

            celda1.setCellValue(rSocial.getAppValor());
            celda2.setCellValue("DIRECCIÓN: " + direccion.getAppValor());
            celda3.setCellValue("R.F.C.: " + rfc.getAppValor());
            celda4.setCellValue("Reporte de ventas del día " + FormatDateUtil.SFD_DDMMYYYY.format(hoy) + ".");
            celda5.setCellValue("Fecha de emisión: ");
            celda6.setCellValue(FormatDateUtil.SFD_DDMMYYYY_HHMMSS.format(hoy));

            celda8.setCellValue("SUCURSAL: " + sucursal.getSucursalNombre());
            celda8.setCellStyle(styleLogoCenterX10);

            celda9.setCellValue("CDA");
            celda9.setCellStyle(styleLogoCenterX16);

            //Titulos de columnas
            String[] titulos = {"AREA", "CLAVE", "PRODUCTO", "CANTIDAD", "DEVOLUCIONES", "PRECIO", "SUBTOTAL"};

            celda1.setCellStyle(stylePrincipal);
            celda2.setCellStyle(styleSubt);
            celda3.setCellStyle(styleSubt);

            //Crea la fila del encabezado
            HSSFRow fila = sheet.createRow(5);
            HSSFCell celdaEncabezado = fila.createCell(0);
            HSSFCell cellAux = null;

            //Agrega el titulo de las columnas en la fila 1
            for (int i = 0; i < titulos.length; i++) {
                celdaEncabezado = fila.createCell(i);
                celdaEncabezado.setCellValue(titulos[i]);
                celdaEncabezado.setCellStyle(styleHeader);
            }

            int i = 6;
            for (Producto p : this.productos) {
                fila = sheet.createRow(i);

                cellAux = fila.createCell(0);
                cellAux.setCellValue(p.getArea().getAreaNombre());
                cellAux.setCellStyle(styleNormal);

                cellAux = fila.createCell(1);
                cellAux.setCellValue(p.getProductoClave());
                cellAux.setCellStyle(styleNormal);

                cellAux = fila.createCell(2);
                cellAux.setCellValue(p.getProductoNombre());
                cellAux.setCellStyle(styleNormal);

                List<String> arr = Arrays.asList(p.getArea().getAreaStatus().split("\\,"));
                int v = Integer.parseInt(arr.get(1));
                HSSFCellStyle style = workbook.createCellStyle();
                style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                style.setFillForegroundColor(HSSFColor.WHITE.index);
                style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
                style.setTopBorderColor(IndexedColors.BLACK.getIndex());
                style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
                style.setRightBorderColor(IndexedColors.BLACK.getIndex());
                style.setAlignment((short) 2);//Centrado
                cellAux = fila.createCell(3);
                cellAux.setCellValue(v);
                cellAux.setCellStyle(style);

                String arr2[] = p.getArea().getAreaStatus().split("\\,");
                int v2 = Integer.parseInt(arr2[0]);
                cellAux = fila.createCell(4);
                cellAux.setCellValue(v2);
                cellAux.setCellStyle(style);

                cellAux = fila.createCell(5);
                cellAux.setCellValue(p.getProductoPrecio());
                cellAux.setCellStyle(styleNumericPlain);

                float dev = Integer.parseInt(arr2[0]) * p.getProductoPrecio();
                float tot = (p.getProductoPrecio() * Integer.parseInt(arr.get(1)));
                cellAux = fila.createCell(6);
                cellAux.setCellValue(tot);
                cellAux.setCellStyle(styleNumericPlain);
                devolucion = devolucion + dev;
                total = total + tot;
                i++;
            }

            fila = sheet.createRow(i);
            cellAux = fila.createCell(5);
            stylePrincipal.setAlignment((short) 1);
            cellAux.setCellValue("TOTAL:");
            cellAux.setCellStyle(styleNumericBold);
            cellAux = fila.createCell(6);
            cellAux.setCellValue(total);
            cellAux.setCellStyle(styleNumericBold);

            fila = sheet.createRow(i + 1);
            cellAux = fila.createCell(5);
            cellAux.setCellValue("TOTAL DEVOLUCIONES:");
            cellAux.setCellStyle(styleNumericBoldM);
            cellAux = fila.createCell(6);
            cellAux.setCellValue(devolucion);
            cellAux.setCellStyle(styleNumericBoldM);

            fila = sheet.createRow(i + 2);
            cellAux = fila.createCell(5);
            cellAux.setCellValue("TOTAL NETO:");
            cellAux.setCellStyle(styleNumericBold);
            cellAux = fila.createCell(6);
            cellAux.setCellValue(total - devolucion);
            cellAux.setCellStyle(styleNumericBold);

            sheet.autoSizeColumn((short) 1);
            sheet.autoSizeColumn((short) 2);
            sheet.autoSizeColumn((short) 3);
            sheet.autoSizeColumn((short) 4);
            sheet.autoSizeColumn((short) 5);
            sheet.autoSizeColumn((short) 6);

            //Escribimos el archivo
            try {
                FileOutputStream out = new FileOutputStream(archivo);
                workbook.write(out);
                logrado = true;
                out.close();

                ZipFile zipFile = new ZipFile(directorio + "/" + nombre + ".zip");
                ArrayList filesToAdd = new ArrayList();
                filesToAdd.add(new File(directorio + "/" + nombre + ".xls"));
                ZipParameters parameters = new ZipParameters();
                parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE); // set compression method to deflate compression
                parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
                parameters.setEncryptFiles(true);
                parameters.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
                parameters.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
                parameters.setPassword(KeyGenerator.PASSWORD_ZIP_123);
                zipFile.addFiles(filesToAdd, parameters);

                SendFile ea = new SendFile(directorio + "/" + nombre + ".zip");
                ea.send();
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + e.getMessage());
            }
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
        return logrado;
    }
}
