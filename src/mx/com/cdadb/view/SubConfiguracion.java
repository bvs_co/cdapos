package mx.com.cdadb.view;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import mx.com.cdadb.entities.User;

/**
 *
 * @author unknown
 */
public class SubConfiguracion extends javax.swing.JPanel {

    private FrMenu frMenu2 = null;

    public SubConfiguracion(FrMenu fr) {
        initComponents();
        this.frMenu2 = fr;
        loadPermisos(this.frMenu2.user);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(254, 254, 254));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/role_x80.png"))); // NOI18N
        jLabel1.setText("Roles");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.setEnabled(false);
        jLabel1.setIconTextGap(1);
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel1MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel1MouseEntered(evt);
            }
        });
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(-4, 0, 150, -1));

        jPanel3.setBackground(new java.awt.Color(254, 254, 254));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/users_x80.png"))); // NOI18N
        jLabel2.setText("Usuarios");
        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel2.setEnabled(false);
        jLabel2.setIconTextGap(1);
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel2MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel2MouseEntered(evt);
            }
        });
        jPanel3.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 170, -1));

        jPanel4.setBackground(new java.awt.Color(254, 254, 254));
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/company_x75.png"))); // NOI18N
        jLabel3.setText("Sucursal");
        jLabel3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel3.setEnabled(false);
        jLabel3.setIconTextGap(1);
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel3MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel3MouseEntered(evt);
            }
        });
        jPanel4.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 170, 100));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(136, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(165, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(231, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(195, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel1.getAccessibleContext().setAccessibleName("");

        getAccessibleContext().setAccessibleName("");
        getAccessibleContext().setAccessibleDescription("config");
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseEntered
        if (jLabel1.isEnabled()) {
            //underline(jLabel1, 1);
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/role_x96.png"))); // NOI18N
        }
    }//GEN-LAST:event_jLabel1MouseEntered

    private void jLabel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseExited
        if (jLabel1.isEnabled()) {
            //underline(jLabel1, 2);
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/role_x80.png"))); // NOI18N
        }
    }//GEN-LAST:event_jLabel1MouseExited

    private void jLabel2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseEntered
        if (jLabel2.isEnabled()) {
            //underline(jLabel2, 1);
            jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/users_x96.png"))); // NOI18N
        }
    }//GEN-LAST:event_jLabel2MouseEntered

    private void jLabel2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseExited
        if (jLabel2.isEnabled()) {
            //underline(jLabel2, 2);
            jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/users_x80.png"))); // NOI18N
        }
    }//GEN-LAST:event_jLabel2MouseExited

    private void jLabel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseEntered
        if (jLabel3.isEnabled()) {
            //underline(jLabel3, 1);
            jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/company_x90.png"))); // NOI18N
        }
    }//GEN-LAST:event_jLabel3MouseEntered

    private void jLabel3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseExited
        if (jLabel3.isEnabled()) {
            //underline(jLabel3, 2);
            jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/company_x75.png"))); // NOI18N
        }
    }//GEN-LAST:event_jLabel3MouseExited


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    // End of variables declaration//GEN-END:variables

    private void underline(JLabel label, int op) {
        switch (op) {
            case 1:
                Font fuente = label.getFont();
                //Creamos una nueva fuente con la fuente y tamaño original, pero en negrita
                Font fuenteNueva = new Font(fuente.getName(), Font.BOLD, fuente.getSize());
                //Creamos un objeto atributos para que se pueda subrayar
                Map atributos = new java.util.HashMap<>();
                //Seteo el atributo que me dice que debemos subrayar
                atributos.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                //Seteamos al textarea la fuente en negrita y subrayada
                label.setFont(fuenteNueva.deriveFont(atributos));
                break;
            case 2:
                label.setFont(new java.awt.Font("Consolas", 1, 12));
                break;
        }
    }

    public void requesttFocus() {
        JTextField txt = null;
        for (int i = 0; i < this.frMenu2.principalPane.getComponentCount(); i++) {
            if (this.frMenu2.principalPane.getComponent(i) instanceof JPanel) {
                JPanel p = (JPanel) this.frMenu2.principalPane.getComponent(i);
                for (int j = 0; j < p.getComponentCount(); j++) {
                    if (p.getComponent(i) instanceof JPanel) {
                        JPanel p2 = (JPanel) p.getComponent(j);
                        for (int k = 0; k < p2.getComponentCount(); k++) {
                            if (p2.getComponent(k) instanceof JTextField) {
                                txt = (JTextField) p2.getComponent(k);
                            }
                        }
                    }
                }
            }
        }
        if (txt != null) {
            txt.requestFocus();
        }
    }

    private void loadPermisos(User usuario) {
        Set<String> permisions = new HashSet<>();
        //quitar duplicados
        usuario.getRol().getPermisos().stream().forEach(p -> {
            permisions.add(p.getAccion().getModulo().getModuloSubmenu());
        });
        permisions.stream().forEach(p -> {
            if ("ROLES".equals(p)) {
                jLabel1.setEnabled(true);
                jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
                    @Override
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        paintSubmenuRoles();
                    }
                });
            }
            if ("ROLES".equals(p)) {
                jLabel1.setEnabled(true);
                jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
                    @Override
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        paintSubmenuRoles();
                    }
                });
            }
            if ("USUARIOS".equals(p)) {
                jLabel2.setEnabled(true);
                jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
                    @Override
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        paintSubmenuUsuarios();
                    }
                });
            }
            if ("SUCURSAL".equals(p)) {
                jLabel3.setEnabled(true);
                jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
                    @Override
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        paintSubmenuSucursal();
                    }
                });
            }
        });
    }

    private void paintSubmenuRoles() {
        PaneRoles paneRoles = new PaneRoles(this.frMenu2);
        paneRoles.setSize(this.frMenu2.principalPane.getSize());
        this.frMenu2.principalPane.removeAll();
        this.frMenu2.principalPane.add(paneRoles, BorderLayout.CENTER);
        this.frMenu2.lblNavegador.setIcon(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/role_x32.png")));
        this.frMenu2.lblNavegador.setText("Roles");
        this.frMenu2.lblAyuda.setText(" Doble clic en un registro, para ver su contenido.");
        this.frMenu2.principalPane.revalidate();
        this.frMenu2.principalPane.repaint();
        requesttFocus();
    }

    private void paintSubmenuUsuarios() {
        PaneUsuarios paneUsuarios = new PaneUsuarios(this.frMenu2);
        paneUsuarios.setSize(this.frMenu2.principalPane.getSize());
        this.frMenu2.principalPane.removeAll();
        this.frMenu2.principalPane.add(paneUsuarios, BorderLayout.CENTER);
        this.frMenu2.lblNavegador.setIcon(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/users_x32.png")));
        this.frMenu2.lblNavegador.setText("Usuarios");
        this.frMenu2.lblAyuda.setText(" Doble clic en un registro, para ver su contenido.");
        this.frMenu2.principalPane.revalidate();
        this.frMenu2.principalPane.repaint();
        this.requesttFocus();
    }

    private void paintSubmenuSucursal() {
        PaneConfig paneConfig = new PaneConfig(this.frMenu2, 1);
        paneConfig.setSize(this.frMenu2.principalPane.getSize());
        this.frMenu2.principalPane.removeAll();
        this.frMenu2.principalPane.add(paneConfig, BorderLayout.CENTER);
        this.frMenu2.lblNavegador.setIcon(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/company_x32.png")));
        this.frMenu2.lblNavegador.setText("Sucursal");
        this.frMenu2.lblAyuda.setText(" ...");
        this.frMenu2.principalPane.revalidate();
        this.frMenu2.principalPane.repaint();
        requesttFocus();
    }
}
