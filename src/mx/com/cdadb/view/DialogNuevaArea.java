package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import mx.com.cdadb.entities.Area;
import mx.com.cdadb.service.ServiceArea;
import mx.com.cdadb.service.impl.ServiceAreaImpl;
import mx.com.cdadb.util.TextfieldUtil;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.constants.ConstantMessages;

/**
 *
 * @author unknown
 */
public class DialogNuevaArea extends javax.swing.JDialog {

    private int opc = -1;
    private Area area = null;
    private ServiceArea sArea = new ServiceAreaImpl();
    private PaneAreas paneAreas = null;

    public DialogNuevaArea(java.awt.Frame parent, boolean modal, int opc, PaneAreas fr) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.opc = opc;
        this.paneAreas = fr;
        initGui();
    }

    public DialogNuevaArea(java.awt.Frame parent, boolean modal, int opc, Area area, PaneAreas fr) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.opc = opc;
        this.area = area;
        this.paneAreas = fr;
        initGui();
        initData();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel2KeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel1.setText("Nombre");

        txtNombre.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtNombre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNombreFocusGained(evt);
            }
        });
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNombreKeyPressed(evt);
            }
        });

        btnGuardar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/save_accept_x16.png"))); // NOI18N
        btnGuardar.setMnemonic('G');
        btnGuardar.setText("Guardar [F5]");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        btnGuardar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGuardarKeyPressed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cancel_x16.png"))); // NOI18N
        btnCancelar.setMnemonic('C');
        btnCancelar.setText("Cancelar [Esc]");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txtNombre)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 108, Short.MAX_VALUE)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar)
                    .addComponent(btnCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtNombreKeyPressed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        switch (this.opc) {
            case 1://Alta
                List<Area> areas;
                try {
                    areas = sArea.findBy("a.areaNombre = '" + txtNombre.getText() + "'");
                    if (areas.size() == 0) {
                        Area a = new Area();
                        a.setAreaNombre(txtNombre.getText().trim());
                        a.setAreaStatus("ACT");
                        a.setAreaFcreacion(new java.util.Date());
                        a.setAreaFmodif(new java.util.Date());
                        if (sArea.add(a)) {
                            JOptionPane.showMessageDialog(this, "Se agregó correctamente la nueva area.");
                            this.paneAreas.initData();
                            this.dispose();
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Ya existe una area con ese nombre,");
                        txtNombre.selectAll();
                        txtNombre.grabFocus();
                    }
                } catch (Exception ex) {
                    if (ex != null) {
                        JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                    }
                    LoggerUtil.registrarError(ex);
                }
                break;
            case 2:
                try {
                List<Area> arr;
                arr = sArea.findBy("a.areaNombre = '" + txtNombre.getText() + "' and a.areaId <> " + this.area.getAreaId());

                if (arr.size() == 0) {
                    this.area.setAreaNombre(txtNombre.getText());
                    this.area.setAreaFmodif(new java.util.Date());
                    if (sArea.edit(this.area)) {
                        JOptionPane.showMessageDialog(this, "Se modificó correctamente el area seleccionada.");
                        this.paneAreas.initData();
                        this.dispose();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Ya existe una area con ese nombre,");
                    txtNombre.selectAll();
                    txtNombre.grabFocus();
                }
            } catch (Exception ex) {
                if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
            }
            break;
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnGuardarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGuardarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnGuardarKeyPressed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void jPanel2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel2KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jPanel2KeyPressed

    private void txtNombreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreFocusGained
        TextfieldUtil.textBoxFocus(txtNombre);
    }//GEN-LAST:event_txtNombreFocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogNuevaArea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogNuevaArea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogNuevaArea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogNuevaArea.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogNuevaArea dialog = new DialogNuevaArea(new javax.swing.JFrame(), true, 0, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtNombre;
    // End of variables declaration//GEN-END:variables

    private void initGui() {
        switch (this.opc) {
            case 1:
                this.setTitle("Nueva área.");
                break;
            case 2:
                this.setTitle("Modificando área.");
                break;
        }
    }

    private void initData() {
        txtNombre.setText(this.area.getAreaNombre());
    }

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 113://F2
                break;
            case 114://F3 
                break;
            case 115://F4
                break;
            case 116://F5 Guardar
                btnGuardarActionPerformed(null);
                break;
            case 117://F6 
                break;
            case 118://F7 
                break;
            case 119://F8
                break;
            case 120://F9 
                break;
            case 121://F10
                break;
            case 27://Esc
                this.dispose();
                break;
        }
    }

}
