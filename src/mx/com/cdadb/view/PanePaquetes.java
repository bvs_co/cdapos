package mx.com.cdadb.view;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableRowSorter;
import mx.com.cdadb.entities.Detallepaquete;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.service.ServiceDetallePaquete;
import mx.com.cdadb.service.impl.ServiceDetallePaqueteImpl;
import mx.com.cdadb.util.DialogWait;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;

/**
 *
 * @author unknown
 */
public class PanePaquetes extends javax.swing.JPanel {

    private int opc = -1;
    private FrMenu frMenu2 = null;
    private ServiceDetallePaquete sDetallePaquete = new ServiceDetallePaqueteImpl();

    public PanePaquetes(FrMenu fr, int op) {
        initComponents();
        this.opc = op;
        this.frMenu2 = fr;
        DialogWait dw = new DialogWait(this.frMenu2, true, this);
        dw.setVisible(true);
        btnEliminar.setVisible(false);
        loadPermisos(this.frMenu2.user);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        btnBuscar.setToolTipText("Buscar [F5]");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel2.setText("[F6] ó Doble clic para Ver contenido del paquete");

        txtBuscar.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CLAVE", "NOMBRE", "INICIA", "TERMINA", "PRECIO"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnNuevo.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/add_x16.png"))); // NOI18N
        btnNuevo.setMnemonic('N');
        btnNuevo.setText("Nuevo [F2]");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        btnNuevo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnNuevoKeyPressed(evt);
            }
        });

        btnEditar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/pen_write_edit_x16.png"))); // NOI18N
        btnEditar.setMnemonic('d');
        btnEditar.setText("Editar [F4]");
        btnEditar.setEnabled(false);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        btnEditar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEditarKeyPressed(evt);
            }
        });

        btnEliminar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/delete_x16.png"))); // NOI18N
        btnEliminar.setMnemonic('E');
        btnEliminar.setText("Eliminar [F3]");
        btnEliminar.setEnabled(false);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        btnEliminar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEliminarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNuevo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnEditar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnNuevo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBuscar))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(381, 592, Short.MAX_VALUE)))))
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addGap(5, 5, 5)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
                .addGap(15, 15, 15)
                .addComponent(jLabel2)
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleDescription("paquetes");
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        if (jTable1.getSelectedRowCount() > 0) {
            int op = JOptionPane.showConfirmDialog(this, "Esta seguro de eliminar el producto seleccionado?\nCon ello se eliminaran todos los productos asociados a este paquete.", "Confirme.", JOptionPane.YES_NO_OPTION);
            if (op == 0) {
                try {
                    ServiceDetallePaquete sDetallePaquete = new ServiceDetallePaqueteImpl();
                    Detallepaquete p = sDetallePaquete.getBy("p.detallePaqueteClave = '" + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString() + "'");
                    p.setDetallePaqueteStatus("DEL");
                    if (sDetallePaquete.edit(p)) {
                        JOptionPane.showMessageDialog(this, "Paquete eliminado, correctamente.");
                        initData();
                        jTable1MouseClicked(null);
                    }
                } catch (Exception ex) {
                    LoggerUtil.registrarError(ex);
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione el registro a eliminar.");
            txtBuscar.selectAll();
            txtBuscar.requestFocus();
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnEliminarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEliminarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnEliminarKeyPressed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        txtBuscarKeyReleased(null);
        txtBuscar.selectAll();
        txtBuscar.requestFocus();
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void txtBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtBuscarKeyPressed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        txtBuscar.setText(txtBuscar.getText().toUpperCase());
        buscarPaquetes(txtBuscar.getText());
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        managedButtons();
    }//GEN-LAST:event_jTable1MouseClicked

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        managedButtons();
    }//GEN-LAST:event_jTable1KeyReleased

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        DialogNuevoDetallePaquete dialog = new DialogNuevoDetallePaquete(this.frMenu2, true, 1, this);
        dialog.setVisible(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnNuevoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnNuevoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnNuevoKeyPressed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        if (jTable1.getSelectedRowCount() > 0) {
            try {
                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                Detallepaquete dp = sDetallePaquete.getBy("p.detallePaqueteClave = '" + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString() + "'");
                DialogNuevoDetallePaquete dialog = new DialogNuevoDetallePaquete(this.frMenu2, true, 2, this, dp);
                dialog.setVisible(true);
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            } catch (Exception ex) {
                LoggerUtil.registrarError(ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione el registro a editar.");
            txtBuscar.selectAll();
            txtBuscar.requestFocus();
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnEditarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEditarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnEditarKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables

    public void initData() {
        switch (this.opc) {
            case 1:
                try {
                ServiceDetallePaquete sDetallepaquete = new ServiceDetallePaqueteImpl();
                List<Detallepaquete> paqs = sDetallepaquete.findBy(" p.detallePaqueteStatus = 'ACT'");
                fillTable(paqs);
            } catch (Exception ex) {
                LoggerUtil.registrarError(ex);
            }
            break;
            case 2:
                break;
        }
    }

    private void fillTable(List<Detallepaquete> paqs) {
        int i = 0;
        Object[][] datos = new Object[paqs.size()][5];
        for (Detallepaquete p : paqs) {
            datos[i][0] = p.getDetallePaqueteClave();
            datos[i][1] = p.getDetallePaqueteNombre();
            datos[i][2] = FormatDateUtil.SFD_DDMMYYYY.format(p.getDetallePaqueteInicia());
            datos[i][3] = FormatDateUtil.SFD_DDMMYYYY.format(p.getDetallePaqueteTermina());
            datos[i][4] = p.getDetallePaquetePrecio();
            i++;
        }

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                new String[]{
                    "CLAVE", "NOMBRE", "INICIA", "TERMINA", "PRECIO"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);

        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });

        jScrollPane1.setViewportView(jTable1);
        jTable1.getTableHeader().setReorderingAllowed(false);

        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(1).setResizable(false);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(300);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(3).setResizable(false);
            jTable1.getColumnModel().getColumn(3).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(4).setResizable(false);
            jTable1.getColumnModel().getColumn(4).setPreferredWidth(100);
        }

        JTableHeader jtableHeader = jTable1.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        jTable1.setTableHeader(jtableHeader);

        jTable1.setRowHeight(30);

        jTable1.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ORDER_ID.toString()));
        jTable1.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        jTable1.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_CENTER.toString()));
        jTable1.getColumnModel().getColumn(3).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_CENTER.toString()));
        jTable1.getColumnModel().getColumn(4).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));

        add_event(jTable1, this);
    }

    private void managedButtons() {
        if (jTable1.getSelectedRowCount() > 0) {
            btnEditar.setEnabled(true);
            btnEliminar.setEnabled(true);
        } else {
            btnEditar.setEnabled(false);
            btnEliminar.setEnabled(false);
        }
    }

    private void add_event(JTable b, PanePaquetes dlg) {
        b.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    switch (dlg.opc) {
                        case 1:
                            try {
                            setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                            ServiceDetallePaquete sdp = new ServiceDetallePaqueteImpl();
                            Detallepaquete dpq = sdp.getBy("p.detallePaqueteClave = '" + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString() + "'");
                            PaneProductos paneProductos = new PaneProductos(3, dlg.frMenu2, dpq, dlg);
                            paneProductos.setSize(dlg.frMenu2.principalPane.getSize());
                            dlg.frMenu2.principalPane.removeAll();
                            dlg.frMenu2.principalPane.add(paneProductos, BorderLayout.CENTER);
                            dlg.frMenu2.principalPane.revalidate();
                            dlg.frMenu2.principalPane.repaint();
                            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        } catch (Exception ex) {
                            LoggerUtil.registrarError(ex);
                        }
                        break;
                        case 2:
                            break;
                        case 3:
                        case 4:
                            break;
                    }

                }
            }
        });
    }

    private void buscarPaquetes(String text) {
        List<RowFilter<Object, Object>> row = null;
        row = new ArrayList();
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>((DefaultTableModel) jTable1.getModel());
        try {
            row.add(RowFilter.regexFilter(text, 0));
            row.add(RowFilter.regexFilter(text, 1));
            row.add(RowFilter.regexFilter(text, 2));
            row.add(RowFilter.regexFilter(text, 3));
            row.add(RowFilter.regexFilter(text, 4));
        } catch (java.util.regex.PatternSyntaxException e) {
            JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + e.getMessage());
        }
        jTable1.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.orFilter(row));
    }

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 113://F2 Nuevo
                if (btnNuevo.isVisible()) {
                    btnNuevoActionPerformed(null);
                }
                break;
            case 114://F3 
                if (btnEliminar.isVisible()) {
                    if (btnEliminar.isEnabled()) {
                        btnEliminarActionPerformed(null);
                    }
                }
                break;
            case 115://F4
                if (btnEditar.isVisible()) {
                    if (btnEditar.isEnabled()) {
                        btnEditarActionPerformed(null);
                    }
                }
                break;
            case 116://F5 Buscar
                btnBuscarActionPerformed(null);
                jTable1KeyReleased(null);
                break;
            case 117://F6 
                if (btnNuevo.isVisible()) {
                    if (jTable1.getSelectedColumnCount() > 0) {
                        try {
                            setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                            ServiceDetallePaquete sdp = new ServiceDetallePaqueteImpl();
                            Detallepaquete dpq = sdp.getBy("p.detallePaqueteClave = '" + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString() + "'");
                            PaneProductos paneProductos = new PaneProductos(3, this.frMenu2, dpq, null, null);
                            paneProductos.setSize(this.frMenu2.principalPane.getSize());
                            this.frMenu2.principalPane.removeAll();
                            this.frMenu2.principalPane.add(paneProductos, BorderLayout.CENTER);
                            this.frMenu2.principalPane.revalidate();
                            this.frMenu2.principalPane.repaint();
                            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        } catch (Exception ex) {
                            LoggerUtil.registrarError(ex);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Primero debe seleccionar el paquete que desea consultar.");
                        txtBuscar.requestFocus();
                    }
                }
                break;
        }
    }

    private void loadPermisos(User usuario) {
        Set<String> permisions = new HashSet<>();
        //quitar duplicados
        usuario.getRol().getPermisos().stream().forEach(p -> {
            permisions.add(p.getAccion().getAccionDesc());
        });

        permisions.stream().forEach(permiso -> {
            if ("AGREGAR PAQUETE".equals(permiso)) {
                btnNuevo.setVisible(true);
            }
            if ("MODIFICAR PAQUETE".equals(permiso)) {
                btnEditar.setVisible(true);
            }
            if ("ELIMINAR PAQUETE".equals(permiso)) {
                btnEliminar.setVisible(true);
            }
        });
    }

}
