package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableRowSorter;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Ordenlab;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.util.GeneratexlsFilesUtil;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;
import mx.com.cdadb.service.impl.ServiceBaseImpl;
import mx.com.cdadb.util.HeaderCellRenderer;

/**
 *
 * @author unknown
 */
public class DialogExportOrder extends javax.swing.JDialog {

    private int opc = -1;
    private List<Ordenlab> currentOrders = null;
    private final ServiceBaseImpl service = new ServiceBaseImpl();

    public DialogExportOrder(java.awt.Frame parent, boolean modal, int op) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        currentOrders = new ArrayList<>();
        this.opc = op;
        init_gui(this.opc);
        txtBuscar.selectAll();
        txtBuscar.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnExport = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        btnSelTodos = new javax.swing.JButton();
        btnQuitSel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel1KeyPressed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        btnExport.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        btnExport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/excelx24.png"))); // NOI18N
        btnExport.setText("Exportar [F5]");
        btnExport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportActionPerformed(evt);
            }
        });
        btnExport.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnExportKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnExport)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnExport, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Orden", "Paciente", "Fecha", "Hora", "U. Modificacion", "Status", "Selecc"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(90);
            jTable1.getColumnModel().getColumn(1).setMinWidth(150);
            jTable1.getColumnModel().getColumn(2).setMinWidth(100);
            jTable1.getColumnModel().getColumn(3).setMinWidth(100);
            jTable1.getColumnModel().getColumn(4).setMinWidth(150);
            jTable1.getColumnModel().getColumn(5).setMinWidth(100);
            jTable1.getColumnModel().getColumn(6).setMinWidth(70);
        }

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        txtBuscar.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        btnSelTodos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/check_allx16.png"))); // NOI18N
        btnSelTodos.setToolTipText("Seleccionar todos [F2]");
        btnSelTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelTodosActionPerformed(evt);
            }
        });
        btnSelTodos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnSelTodosKeyPressed(evt);
            }
        });

        btnQuitSel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/quit_check_allx16.png"))); // NOI18N
        btnQuitSel.setToolTipText("Quitar seleccion [F3]");
        btnQuitSel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitSelActionPerformed(evt);
            }
        });
        btnQuitSel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnQuitSelKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSelTodos, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnQuitSel, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 841, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnSelTodos)
                    .addComponent(btnQuitSel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
                .addGap(6, 6, 6))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSelTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelTodosActionPerformed
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            jTable1.setValueAt(true, i, 6);
        }
    }//GEN-LAST:event_btnSelTodosActionPerformed

    private void btnQuitSelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitSelActionPerformed
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            jTable1.setValueAt(false, i, 6);
        }
    }//GEN-LAST:event_btnQuitSelActionPerformed

    private void btnExportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportActionPerformed
        if (getRowsChecked() > 0) {
            try {
                //ServiceSucursalLocal sSuc = new ServiceSucursalLocalImp();
                //ServiceApp sApp = new ServiceAppImpl();
                java.util.Date hoy = new java.util.Date();
                //App app = sApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_BRANCH_ID+"'");
                App app = (App)service.getBy(new App(), "t.appNombre = '"+ConstantConfig.CONFIG_BRANCH_ID+"'");
                App encrypt = (App)service.getBy(new App(), "t.appNombre = '"+ConstantConfig.CONFIG_ORDER_SEND_ENCRYPT+"'");
                //Sucursal s = sSuc.getBy("s.sucursalId = " + app.getAppValor());
                Sucursal s = (Sucursal)service.getBy(new Sucursal(), "t.sucursalId = " + app.getAppValor());

                JFileChooser fileChooser = new JFileChooser();
                if (fileChooser.showSaveDialog(this.getParent()) == JFileChooser.APPROVE_OPTION) {
                    File directorio = fileChooser.getSelectedFile();

                    List<Ordenlab> comandasExcel = extractOrders();
                    GeneratexlsFilesUtil excel = new GeneratexlsFilesUtil();

                    if (excel.generatexlsOrdersForExport(comandasExcel, directorio.toString() + " Sucursal " + s.getSucursalNombre() + " [" + FormatDateUtil.SFD_YYYYMMDD.format(hoy) + " " + FormatDateUtil.SFD_KKMMSS.format(hoy).replaceAll(":", "") + "].xls", encrypt.getAppValor())) {
                        JOptionPane.showMessageDialog(null, "Archivo con ordenes, generado exitosamente, en " + fileChooser.getCurrentDirectory());
                        this.dispose();
                        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                    } else {
                        JOptionPane.showMessageDialog(this, "ocurrio un error.");
                        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                    }
                }
            } catch (Exception ex) {
                if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccionar los registros a exportar");
            jTable1.requestFocus();
        }
    }//GEN-LAST:event_btnExportActionPerformed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        txtBuscar.setText(txtBuscar.getText().toUpperCase());
        filterData(txtBuscar.getText());
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void btnExportKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnExportKeyPressed
        checkKeyPressed(evt);

    }//GEN-LAST:event_btnExportKeyPressed

    private void txtBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtBuscarKeyPressed

    private void btnSelTodosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnSelTodosKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnSelTodosKeyPressed

    private void btnQuitSelKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnQuitSelKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnQuitSelKeyPressed

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jTable1KeyPressed

    private void jPanel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jPanel1KeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogExportOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogExportOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogExportOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogExportOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogExportOrder dialog = new DialogExportOrder(new javax.swing.JFrame(), true, -1);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExport;
    private javax.swing.JButton btnQuitSel;
    private javax.swing.JButton btnSelTodos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables

    public void init_gui(int op) {
        switch (op) {
            case 0:
                try {
                this.setTitle("Exportar ordenes a otra sucursal.");
                txtBuscar.setText("");
                txtBuscar.grabFocus();
                //ServiceOrdenlab sOrderLab = new ServiceordenLabImpl();
                //List<Ordenlab> ordenes = sOrderLab.findAll();
                List<Ordenlab> ordenes = service.findAll(new Ordenlab());
                this.currentOrders = getCurrentOrders(ordenes);
                fillTabla(this.currentOrders);
                txtBuscar.requestFocus();
            } catch (Exception ex) {
                if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
            }
            break;
            case 1:
//                this.setTitle("Seleccione una orden.");
//
//                txtBuscar.grabFocus();
//                cargaTabla(this.pos.cliente);
//                JOptionPane.showMessaDialog(null, "Solo se agregarán a la orden los estudios que correspondan a laboratorio.");
//                txtBuscar.requestFocus();
                break;
        }
    }

    private void fillTabla(List<Ordenlab> ordenes) {
        Object[][] datos = new Object[ordenes.size()][7];
        int i = 0;
        for (Ordenlab o : ordenes) {
            datos[i][0] = o.getId().getOrdenlabContDiario();
            datos[i][1] = o.getOrdenLabPacienteId();
            datos[i][2] = FormatDateUtil.SFD_DDMMYYYY.format(o.getId().getOrdenLabFecha());
            datos[i][3] = FormatDateUtil.SFD_KKMMSS.format(o.getOrdenHora());
            datos[i][4] = FormatDateUtil.SFD_DDMMYYYY_HHMMSS_GUION.format(o.getOrdenlabFmodif());
            datos[i][5] = o.getOrdenlabStatus();
            datos[i][6] = false;
            i++;
        }

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                new String[]{
                    "Orden", "Paciente", "Fecha", "Hora", "U. Modificacion", "Status", "Selecc"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, true
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        jTable1.getTableHeader().setReorderingAllowed(false);

        jScrollPane1.setViewportView(jTable1);

        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(90);
            jTable1.getColumnModel().getColumn(1).setMinWidth(150);
            jTable1.getColumnModel().getColumn(2).setMinWidth(100);
            jTable1.getColumnModel().getColumn(3).setMinWidth(100);
            jTable1.getColumnModel().getColumn(4).setMinWidth(200);
            jTable1.getColumnModel().getColumn(5).setMinWidth(100);
            jTable1.getColumnModel().getColumn(6).setMinWidth(70);
        }

        JTableHeader jtableHeader = jTable1.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        jTable1.setTableHeader(jtableHeader);

        jTable1.setRowHeight(30);

        jTable1.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ORDER_ID.toString()));
        jTable1.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.PATIENT_ID.toString()));
        jTable1.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_CENTER.toString()));
        jTable1.getColumnModel().getColumn(3).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_CENTER.toString()));
        jTable1.getColumnModel().getColumn(4).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_CENTER.toString()));
        jTable1.getColumnModel().getColumn(5).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_CENTER.toString()));
    }

    private List<Ordenlab> getCurrentOrders(List<Ordenlab> ordenes) {
        List<Ordenlab> orders = new ArrayList<>();
        ordenes.stream().filter(o -> (FormatDateUtil.SFD_YYYYMMDD.format(o.getId().getOrdenLabFecha()).equals(FormatDateUtil.SFD_YYYYMMDD.format(new java.util.Date()))
                && !"...".equals(o.getOrdenlabDesc()) && !"SEP".equals(o.getOrdenlabStatus()))).forEachOrdered(o -> {
                    orders.add(o);
        });
        return orders;
    }

    private int getRowsChecked() {
        int check = 0;
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            if (Boolean.valueOf(jTable1.getValueAt(i, 6).toString())) {
                check++;
            }
        }
        return check;
    }

    private List<Ordenlab> extractOrders() {
        List<Ordenlab> backOrders = new ArrayList<>();
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            if (Boolean.parseBoolean(jTable1.getValueAt(i, 6).toString())) {
                backOrders.add(findOrder(Integer.valueOf(jTable1.getValueAt(i, 0).toString())));
            }
        }
        return backOrders;
    }

    private Ordenlab findOrder(Integer ord) {
        for (Ordenlab o : currentOrders) {
            if (Objects.equals(o.getId().getOrdenlabContDiario(), ord)) {
                return o;
            }
        }
        return null;
    }

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 27://F2 
                this.dispose();
                break;
            case 113://F2 
                btnSelTodosActionPerformed(null);
                break;
            case 114://F3;
                btnQuitSelActionPerformed(null);
                break;
            case 116://F5
                if (btnExport.isEnabled()) {
                    btnExportActionPerformed(null);
                }
                break;
        }
    }

    private void filterData(String text) {
        List<RowFilter<Object, Object>> row = null;
        row = new ArrayList();
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>((DefaultTableModel) jTable1.getModel());
        try {
            row.add(RowFilter.regexFilter(text, 0));
            row.add(RowFilter.regexFilter(text, 1));
            row.add(RowFilter.regexFilter(text, 2));
            row.add(RowFilter.regexFilter(text, 3));
            row.add(RowFilter.regexFilter(text, 4));
            row.add(RowFilter.regexFilter(text, 5));
        } catch (java.util.regex.PatternSyntaxException e) {
            JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + e.getMessage());
        }
        jTable1.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.orFilter(row));
    }
}
