package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Area;
import mx.com.cdadb.entities.Caja;
import mx.com.cdadb.entities.Clave;
import mx.com.cdadb.entities.Detallepaquete;
import mx.com.cdadb.entities.Folio;
import mx.com.cdadb.entities.Orden;
import mx.com.cdadb.entities.Ordenlab;
import mx.com.cdadb.entities.Paquete;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.entities.Tipopago;
import mx.com.cdadb.entities.Tr;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.entities.Xx;
import mx.com.cdadb.entities.customer.Cliente;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceCaja;
import mx.com.cdadb.service.ServiceClave;
import mx.com.cdadb.service.ServiceDetallePaquete;
import mx.com.cdadb.service.ServiceFolio;
import mx.com.cdadb.service.ServiceOrden;
import mx.com.cdadb.service.ServicePaquete;
import mx.com.cdadb.service.ServiceProducto;
import mx.com.cdadb.service.ServiceSucursalLocal;
import mx.com.cdadb.service.ServiceTr;
import mx.com.cdadb.service.ServiceX;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.service.impl.ServiceCajaImpl;
import mx.com.cdadb.service.impl.ServiceClaveImpl;
import mx.com.cdadb.service.impl.ServiceDetallePaqueteImpl;
import mx.com.cdadb.service.impl.ServiceFolioImpl;
import mx.com.cdadb.service.impl.ServiceOrdenImpl;
import mx.com.cdadb.service.impl.ServicePaqueteImpl;
import mx.com.cdadb.service.impl.ServiceProductoImpl;
import mx.com.cdadb.service.impl.ServiceSucursalLocalImp;
import mx.com.cdadb.service.impl.ServiceTrImpl;
import mx.com.cdadb.service.impl.ServiceXImpl;
import mx.com.cdadb.service.customer.impl.ServiceClienteImpl;
import mx.com.cdadb.service.customer.ServiceCliente;
import mx.com.cdadb.util.CalculateAgeUtil;
import mx.com.cdadb.util.DialogWait;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.GeneralGetters;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.util.NumberToLetterConverter;
import mx.com.cdadb.util.PrintReportImpl;
import mx.com.cdadb.util.EncryptCashbox;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.constants.ConstantFiles;
import mx.com.cdadb.enums.CellRendererEnum;
import mx.com.cdadb.util.MonedaUtil;


/**
 *
 * @author unknown
 */
public class PanePos extends javax.swing.JPanel {

    public FrMenu frMenu = null;
    private final int opc;
    public Cliente cliente = null;
    public long idTrVentaAnterior = 0;
    public long idFolioVentaAnterior = 0;
    private List<Xx> arrX = new ArrayList<>();

    public PanePos(FrMenu men, int op) {
        initComponents();
        this.opc = op;
        this.frMenu = men;
        txtClave.requestFocus();
        initData();
        loadPermisos(this.frMenu.user);
        txtClave.grabFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnBuscar = new javax.swing.JButton();
        btnPaciente = new javax.swing.JButton();
        btnOk = new javax.swing.JButton();
        btnLab = new javax.swing.JButton();
        btndev = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnLentes = new javax.swing.JButton();
        btnReimprimir = new javax.swing.JButton();
        btnImportar = new javax.swing.JButton();
        btnExportar = new javax.swing.JButton();
        btnQuitar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtPaciente = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jFormattedTextField1 = new javax.swing.JFormattedTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        txtClave = new javax.swing.JTextField();
        jButton6 = new javax.swing.JButton();
        btnReimprimeUltima = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtRfc = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaProductos = new javax.swing.JTable();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel8 = new javax.swing.JLabel();
        txtCajero = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        lblTotal = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        btnBuscar.setBackground(new java.awt.Color(51, 153, 255));
        btnBuscar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x32.png"))); // NOI18N
        btnBuscar.setText("Buscar [F2]");
        btnBuscar.setToolTipText("Buscar estudio");
        btnBuscar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnBuscar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBuscar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnPaciente.setBackground(new java.awt.Color(51, 153, 255));
        btnPaciente.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnPaciente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/people_x32.png"))); // NOI18N
        btnPaciente.setMnemonic('L');
        btnPaciente.setText("Pacientes [F3]");
        btnPaciente.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPaciente.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPaciente.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPacienteActionPerformed(evt);
            }
        });

        btnOk.setBackground(new java.awt.Color(51, 153, 255));
        btnOk.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnOk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/donate_x32.png"))); // NOI18N
        btnOk.setMnemonic('R');
        btnOk.setText("Donativo [F5]");
        btnOk.setToolTipText("Recibir donativo");
        btnOk.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnOk.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnOk.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOkActionPerformed(evt);
            }
        });

        btnLab.setBackground(new java.awt.Color(51, 153, 255));
        btnLab.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnLab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/laboratory_x32.png"))); // NOI18N
        btnLab.setText("Laboratorio [F4]");
        btnLab.setToolTipText("Visualiza ordenes enviadas a laboratorio");
        btnLab.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLab.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnLab.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnLab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLabActionPerformed(evt);
            }
        });
        btnLab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnLabKeyPressed(evt);
            }
        });

        btndev.setBackground(new java.awt.Color(51, 153, 255));
        btndev.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btndev.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/devolucion_x32.png"))); // NOI18N
        btndev.setText("Devolución [F6]");
        btndev.setToolTipText("Procesa devolución");
        btndev.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btndev.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btndev.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btndev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndevActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnLab, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnOk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPaciente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btndev, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLab, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btndev, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(159, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        btnLentes.setBackground(new java.awt.Color(51, 153, 255));
        btnLentes.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnLentes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/glasses_sunglasses_x32.png"))); // NOI18N
        btnLentes.setText("Lentes [F7]");
        btnLentes.setToolTipText("Lista de lentes por cobrar");
        btnLentes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLentes.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnLentes.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnLentes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLentesActionPerformed(evt);
            }
        });

        btnReimprimir.setBackground(new java.awt.Color(51, 153, 255));
        btnReimprimir.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnReimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/document-print_x32.png"))); // NOI18N
        btnReimprimir.setText("Reimpresión [F8]");
        btnReimprimir.setToolTipText("Reimprime una venta");
        btnReimprimir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnReimprimir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnReimprimir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnReimprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReimprimirActionPerformed(evt);
            }
        });

        btnImportar.setBackground(new java.awt.Color(51, 153, 255));
        btnImportar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnImportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/import_x32.png"))); // NOI18N
        btnImportar.setText("Importar [F9]");
        btnImportar.setToolTipText("Importar ordenes de ortras sucursales");
        btnImportar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnImportar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImportar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnImportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImportarActionPerformed(evt);
            }
        });

        btnExportar.setBackground(new java.awt.Color(51, 153, 255));
        btnExportar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/export_32.png"))); // NOI18N
        btnExportar.setText("Exportar [F10]");
        btnExportar.setToolTipText("Exportar ordenes para otras suscursales");
        btnExportar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExportar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnExportar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });

        btnQuitar.setBackground(new java.awt.Color(51, 153, 255));
        btnQuitar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnQuitar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/Delete_X32.png"))); // NOI18N
        btnQuitar.setText("Quitar[Sup]");
        btnQuitar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnQuitar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnQuitar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnQuitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQuitarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnLentes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnReimprimir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnImportar, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExportar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnQuitar, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnLentes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnReimprimir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnImportar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnExportar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnQuitar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/note-accept_x16.png"))); // NOI18N
        jLabel2.setText("Documento:");

        jTextField1.setEditable(false);
        jTextField1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jTextField1.setForeground(new java.awt.Color(0, 0, 255));
        jTextField1.setText("PAPELETA");
        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextField1KeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/people_x16.png"))); // NOI18N
        jLabel3.setText("Paciente:");

        txtPaciente.setEditable(false);
        txtPaciente.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtPaciente.setForeground(new java.awt.Color(0, 0, 255));
        txtPaciente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtPacienteMouseClicked(evt);
            }
        });
        txtPaciente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPacienteKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/calendar_date_x16.png"))); // NOI18N
        jLabel4.setText("Fecha:");

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        jFormattedTextField1.setValue(sdf.format(new java.util.Date()));
        jFormattedTextField1.setEditable(false);
        jFormattedTextField1.setForeground(new java.awt.Color(0, 0, 255));
        jFormattedTextField1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/bar-code_x32.png"))); // NOI18N
        jLabel7.setText("Clave:");

        txtClave.setFont(new java.awt.Font("Courier New", 1, 24)); // NOI18N
        txtClave.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtClaveMouseClicked(evt);
            }
        });
        txtClave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtClaveKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtClaveKeyReleased(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x32.png"))); // NOI18N
        jButton6.setToolTipText("Buscar");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jButton6.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton6KeyPressed(evt);
            }
        });

        btnReimprimeUltima.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/printer_x32.png"))); // NOI18N
        btnReimprimeUltima.setToolTipText("Reimprimir ultima venta [F11]");
        btnReimprimeUltima.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReimprimeUltimaActionPerformed(evt);
            }
        });
        btnReimprimeUltima.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnReimprimeUltimaKeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/rfc_x16.png"))); // NOI18N
        jLabel1.setText("RFC:");

        txtRfc.setEditable(false);
        txtRfc.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtRfc.setForeground(new java.awt.Color(0, 0, 255));
        txtRfc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtRfcMouseClicked(evt);
            }
        });
        txtRfc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRfcActionPerformed(evt);
            }
        });
        txtRfc.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtRfcKeyPressed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/Map alt_x16.png"))); // NOI18N
        jLabel11.setText("Dirección:");

        txtDireccion.setEditable(false);
        txtDireccion.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtDireccion.setForeground(new java.awt.Color(0, 0, 255));
        txtDireccion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtDireccionMouseClicked(evt);
            }
        });
        txtDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDireccionKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator3)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(367, 367, 367)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtClave, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnReimprimeUltima, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtRfc)
                                    .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel5Layout.createSequentialGroup()
                                        .addComponent(txtDireccion)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel4)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtPaciente))))
                        .addContainerGap())))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txtPaciente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jFormattedTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txtRfc, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel11)
                        .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnReimprimeUltima, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        tablaProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "Clave", "Descripción", "C.M.R.U.", "C.M.R."
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaProductos.getTableHeader().setReorderingAllowed(false);
        tablaProductos.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                tablaProductosComponentAdded(evt);
            }
        });
        tablaProductos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaProductosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaProductos);
        if (tablaProductos.getColumnModel().getColumnCount() > 0) {
            tablaProductos.getColumnModel().getColumn(1).setResizable(false);
            tablaProductos.getColumnModel().getColumn(4).setResizable(false);
        }
        JTableHeader jtableHeader = tablaProductos.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaProductos.setTableHeader(jtableHeader);

        tablaProductos.setRowHeight(20);

        tablaProductos.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.PK_ID_HIDDEN.toString()));
        tablaProductos.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tablaProductos.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tablaProductos.getColumnModel().getColumn(3).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));
        tablaProductos.getColumnModel().getColumn(4).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));

        jLabel8.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cajero_x16.png"))); // NOI18N
        jLabel8.setText("Cajero:");

        txtCajero.setEditable(false);
        txtCajero.setFont(new java.awt.Font("Courier New", 1, 12)); // NOI18N

        lblTotal.setFont(new java.awt.Font("Courier New", 1, 36)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(0, 0, 255));
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTotal.setText("0.00");
        lblTotal.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);

        jLabel10.setFont(new java.awt.Font("Courier New", 1, 36)); // NOI18N
        jLabel10.setText("Total:");

        jLabel9.setFont(new java.awt.Font("Courier New", 1, 36)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 0, 255));
        jLabel9.setText("$");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator4, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jSeparator5)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCajero, javax.swing.GroupLayout.PREFERRED_SIZE, 394, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jSeparator6, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtCajero, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel10)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jToolBar1.setBackground(new java.awt.Color(255, 255, 255));
        jToolBar1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        getAccessibleContext().setAccessibleDescription("donativos");
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        new DialogProductos(this.frMenu, true, txtClave.getText(), this).setVisible(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnPacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPacienteActionPerformed
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        DialogAltaRapidaPaciente dalp = new DialogAltaRapidaPaciente(null, true, 0, this);
        dalp.setVisible(true);
        txtClave.requestFocus();
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btnPacienteActionPerformed

    private void btnOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOkActionPerformed
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        if (this.cliente != null) {
            if (tablaProductos.getRowCount() > 0) {
                FrCobro frCobro = new FrCobro(null, true, 1, Float.valueOf(this.lblTotal.getText().replace(",", "")), this);
                frCobro.setVisible(true);
                txtClave.requestFocus();
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            } else {
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                JOptionPane.showMessageDialog(null, "No hay productos que cobrar.");
                txtClave.selectAll();
                txtClave.requestFocus();
            }
        } else {
            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            JOptionPane.showMessageDialog(null, "Debe ingresar los datos del paciente, antes de continuar.");
            txtClave.selectAll();
            txtClave.requestFocus();
        }
    }//GEN-LAST:event_btnOkActionPerformed

    private void btnLabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLabActionPerformed
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        DialogOrdenesLab dOrdenes = new DialogOrdenesLab(null, true, 1, this.frMenu);
        dOrdenes.setVisible(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_btnLabActionPerformed

    private void btnLabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnLabKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnLabKeyPressed

    private void btndevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndevActionPerformed
        FrDevolucion fr = new FrDevolucion(null, true, this, 1);
        fr.setVisible(true);
    }//GEN-LAST:event_btndevActionPerformed

    private void btnLentesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLentesActionPerformed
        JOptionPane.showMessageDialog(null, "Lentes...");
        txtClave.selectAll();
        txtClave.requestFocus();
    }//GEN-LAST:event_btnLentesActionPerformed

    private void btnReimprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReimprimirActionPerformed
        new FrReprint(null, true, this).setVisible(true);
    }//GEN-LAST:event_btnReimprimirActionPerformed

    private void btnQuitarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnQuitarActionPerformed
        borrarRegistro();
    }//GEN-LAST:event_btnQuitarActionPerformed

    private void jTextField1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jTextField1KeyPressed

    private void txtPacienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtPacienteMouseClicked
        if (!txtPaciente.getText().isEmpty() && !txtRfc.getText().isEmpty() && !txtDireccion.getText().isEmpty()) {
            if (evt.getClickCount() == 2) {
                DialogAltaRapidaPaciente dalp = new DialogAltaRapidaPaciente(null, true, 1, this, this.cliente);
                dalp.setVisible(true);
                txtClave.requestFocus();
            }
        }
    }//GEN-LAST:event_txtPacienteMouseClicked

    private void txtPacienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPacienteKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtPacienteKeyPressed

    private void txtClaveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtClaveMouseClicked
        if (evt.getClickCount() == 2) {
            this.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
            txtClave.setText(txtClave.getText().toUpperCase());
            new DialogProductos(this.frMenu, true, txtClave.getText(), this).setVisible(true);
            this.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_txtClaveMouseClicked

    private void txtClaveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtClaveKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtClaveKeyPressed

    private void txtClaveKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtClaveKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
            txtClave.setText(txtClave.getText().toUpperCase());
            new DialogProductos(this.frMenu, true, txtClave.getText(), this).setVisible(true);
            this.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_txtClaveKeyReleased

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        btnBuscarActionPerformed(evt);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton6KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton6KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jButton6KeyPressed

    private void btnReimprimeUltimaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReimprimeUltimaActionPerformed
        //imprimeUltima();
        DialogWait dw = new DialogWait(this.frMenu, true, this);
        dw.setVisible(true);
    }//GEN-LAST:event_btnReimprimeUltimaActionPerformed

    private void btnReimprimeUltimaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnReimprimeUltimaKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnReimprimeUltimaKeyPressed

    private void txtRfcMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtRfcMouseClicked
        if (!txtPaciente.getText().isEmpty() && !txtRfc.getText().isEmpty() && !txtDireccion.getText().isEmpty()) {
            if (evt.getClickCount() == 2) {
                DialogAltaRapidaPaciente dalp = new DialogAltaRapidaPaciente(null, true, 1, this, this.cliente);
                dalp.setVisible(true);
                txtClave.requestFocus();
            }
        }
    }//GEN-LAST:event_txtRfcMouseClicked

    private void txtRfcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRfcActionPerformed

    }//GEN-LAST:event_txtRfcActionPerformed

    private void txtRfcKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRfcKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtRfcKeyPressed

    private void txtDireccionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtDireccionMouseClicked
        if (!txtPaciente.getText().isEmpty() && !txtRfc.getText().isEmpty() && !txtDireccion.getText().isEmpty()) {
            if (evt.getClickCount() == 2) {
                DialogAltaRapidaPaciente dalp = new DialogAltaRapidaPaciente(null, true, 1, this, this.cliente);
                dalp.setVisible(true);
                txtClave.requestFocus();
            }
        }
    }//GEN-LAST:event_txtDireccionMouseClicked

    private void txtDireccionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDireccionKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtDireccionKeyPressed

    private void tablaProductosComponentAdded(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_tablaProductosComponentAdded

    }//GEN-LAST:event_tablaProductosComponentAdded

    private void tablaProductosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaProductosKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_tablaProductosKeyPressed

    private void btnImportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImportarActionPerformed
        DialogImportOrder de = new DialogImportOrder(frMenu, true, 0);
        de.setVisible(true);
    }//GEN-LAST:event_btnImportarActionPerformed

    private void btnExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarActionPerformed
        DialogExportOrder de = new DialogExportOrder(frMenu, true, 0);
        de.setVisible(true);
    }//GEN-LAST:event_btnExportarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnExportar;
    private javax.swing.JButton btnImportar;
    private javax.swing.JButton btnLab;
    private javax.swing.JButton btnLentes;
    private javax.swing.JButton btnOk;
    private javax.swing.JButton btnPaciente;
    private javax.swing.JButton btnQuitar;
    private javax.swing.JButton btnReimprimeUltima;
    private javax.swing.JButton btnReimprimir;
    private javax.swing.JButton btndev;
    private javax.swing.JButton jButton6;
    private javax.swing.JFormattedTextField jFormattedTextField1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel lblTotal;
    public javax.swing.JTable tablaProductos;
    private javax.swing.JTextField txtCajero;
    public javax.swing.JTextField txtClave;
    public javax.swing.JTextField txtDireccion;
    public javax.swing.JTextField txtPaciente;
    public javax.swing.JTextField txtRfc;
    // End of variables declaration//GEN-END:variables

    /**
     * Agrega los eventos de la teclas de acceso rapido
     *
     * @param evt evento del teclado
     */
    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 113://F2 Buscar producto
                if (btnBuscar.isVisible()) {
                    btnBuscarActionPerformed(null);
                }
                break;
            case 114://F3 Pacientes
                if (btnPaciente.isVisible()) {
                    btnPacienteActionPerformed(null);
                }
                break;
            case 115://F4 Laboratorio
                if (btnLab.isVisible()) {
                    btnLabActionPerformed(null);
                }
                break;
            case 116://F5 Cobro
                if (btnOk.isVisible()) {
                    btnOkActionPerformed(null);
                }
                break;
            case 117://F6 Devolucion
                if (btndev.isVisible()) {
                    btndevActionPerformed(null);
                }
                break;
            case 118://F7 Lentes
                if (btnLentes.isVisible()) {
                    btnOkActionPerformed(null);
                }
                break;
            case 119://F8 Reimpresion
                if (btnReimprimir.isVisible()) {
                    btnReimprimirActionPerformed(null);
                }
                break;
            case 120://F9 Importar
                if (btnImportar.isVisible()) {
                    btnOkActionPerformed(null);
                }
                break;
            case 121://F10 Exportar
                if (btnExportar.isVisible()) {
                    btnExportarActionPerformed(null);
                }
                break;
            case 122://F11 Reimprimir ultima
                btnReimprimeUltimaActionPerformed(null);
                break;
            case 127://Suprimir
                if (btnQuitar.isVisible()) {
                    borrarRegistro();
                }
                break;
        }
    }

    /**
     * Quita un registro de la tabla de venta
     */
    private void borrarRegistro() {
        if (tablaProductos.getSelectedRowCount() > 0) {
            DefaultTableModel model = (DefaultTableModel) tablaProductos.getModel();
            model.removeRow(tablaProductos.getSelectedRow());
            tablaProductos.setModel(model);
            actualizaTotal();
            repaint();
            txtClave.requestFocus();
        } else {
            JOptionPane.showMessageDialog(this, "Primero debe seleccionar un registro a eliminar.");
            txtClave.selectAll();
            txtClave.requestFocus();
        }
    }

    /**
     * *
     * Actualiza el total de la venta
     */
    public void actualizaTotal() {
        float total = 0;
        for (int i = 0; i < tablaProductos.getRowCount(); i++) {
            total = total + Float.valueOf(tablaProductos.getValueAt(i, 4).toString());
        }
        lblTotal.setText(MonedaUtil.separaMiles(String.format("%.2f", total).replaceAll(",", "")));
    }

    /**
     * Guarda la venta del pos
     *
     * @param camb Cambio a entregar al cliente
     * @param totalLetra Total con letra
     * @param tipoPago Tipo de pago
     * @return Map con el id de la Transacción y rl id del folio de la venta
     */
    public Map guardaVenta(float camb, String totalLetra, Tipopago tipoPago) {
        Map param = null;
        try {
            java.util.Date hoy = new java.util.Date();
            List<Detallepaquete> paquetes = new ArrayList<>();
            List<Producto> productos = new ArrayList<>();
            List<Orden> ordenes = new ArrayList<>();
            EncryptCashbox prop = new EncryptCashbox();
            Caja caja = prop.decrypt(ConstantFiles.POS_PROPERTIES);
            ServiceCaja sCaja = new ServiceCajaImpl();
            caja = sCaja.getBy("c.cajaId=" + caja.getCajaId());
            for (int i = 0; i < tablaProductos.getRowCount(); i++) {
                Orden orden = new Orden(); 
                orden.setCaja(caja);
                orden.setDetallepaquete(null);
                if (Integer.parseInt(tablaProductos.getValueAt(i, 0).toString()) == 0) {
                    Producto p = getProducto(tablaProductos.getValueAt(i, 1).toString());
                    productos.add(p);
                    orden.setProducto(p);
                    orden.setDetallepaquete(null);
                } else {
                    Detallepaquete dPaquete = getDetallePaquete(tablaProductos.getValueAt(i, 1).toString());
                    paquetes.add(dPaquete);
                    orden.setDetallepaquete(dPaquete);
                    orden.setProducto(null);
                }
                orden.setOrdenCambio(camb);
                orden.setOrdenFecha(hoy);
                orden.setOrdenHora(hoy);
                orden.setOrdenPacienteId(this.cliente.getClienteId());
                orden.setOrdenStatus("1");
                orden.setUser(this.frMenu.user);
                orden.setTipopago(tipoPago);
                orden.setFolio(null);
                orden.setTr(null);
                orden.setOrdenlab(null);
                ordenes.add(orden);
            }
            List<Producto> ordenesLab = getOrdenesLab(productos, paquetes);
            List<Producto> productosT = getProductosTotal(productos, paquetes);
            ServiceOrden sOrden = new ServiceOrdenImpl();
            param = sOrden.addList(ordenes, ordenesLab, this.cliente, this.lblTotal.getText(), totalLetra, productosT, this.frMenu.user, this.arrX);
            if (!param.isEmpty()) {
                float res = (caja.getCajaTotal() != null) ? caja.getCajaTotal() + Float.valueOf(lblTotal.getText().replaceAll(",", "")) : Float.valueOf(lblTotal.getText().replaceAll(",", ""));
                updateCash(caja, res);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LoggerUtil.registrarError(ex);
            if (ex.getCause().toString().contains("Duplicate entry")) {
                JOptionPane.showMessageDialog(this.frMenu, "Se esta intentando duplicar una orden, contacte al administrador. \nDetalle:\n" + ex.getCause(), "No se ha realizado la venta!!!", JOptionPane.WARNING_MESSAGE);
                LoggerUtil.registrarError(ex, this.frMenu.user);
            }
        }
        return param;
    }

    private Producto getProducto(String clave) {
        try {
            ServiceProducto sProducto = new ServiceProductoImpl();
            return sProducto.getBy("p.productoClave = '" + clave + "'");
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
        return null;
    }

    private Detallepaquete getDetallePaquete(String clave) {
        try {
            ServiceDetallePaquete sPaquete = new ServiceDetallePaqueteImpl();
            return sPaquete.getBy("p.detallePaqueteClave = '" + clave + "'");
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
        return null;
    }

    /**
     * Limpia la venta del pos
     */
    void vtaConcretada() {
        txtDireccion.setText("");
        txtPaciente.setText("");
        txtRfc.setText("");
        lblTotal.setText("0.00");
        this.cliente = null;

        tablaProductos.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "", "Clave", "Descripción", "C.M.R.U.", "C.M.R."
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        tablaProductos.getTableHeader().setReorderingAllowed(false);

        tablaProductos.addContainerListener(new java.awt.event.ContainerAdapter() {
            @Override
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                tablaProductosComponentAdded(evt);
            }
        });
        tablaProductos.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaProductosKeyPressed(evt);
            }
        });

        jScrollPane1.setViewportView(tablaProductos);
        JTableHeader jtableHeader = tablaProductos.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaProductos.setTableHeader(jtableHeader);

        tablaProductos.setRowHeight(20);

        tablaProductos.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.PK_ID_HIDDEN.toString()));
        tablaProductos.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tablaProductos.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tablaProductos.getColumnModel().getColumn(3).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));
        tablaProductos.getColumnModel().getColumn(4).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));

    }

    /**
     * Actualiza el total que se vendio en la caja
     *
     * @param caja Caja en la cual se hizo el cobro
     */
    private void updateCash(Caja caja, float total) {
        try {
            ServiceCaja sCaja = new ServiceCajaImpl();
            caja.setCajaFmodif(new java.util.Date());
            caja.setCajaTotal(total);
            sCaja.edit(caja);
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
    }

    /**
     * Obtiene solo los productos que pertenecen al laboratorio, los paquetes se
     * desintegran en productos
     *
     * @param productos Productos de la venta
     * @param paquetes Paquetes de la venta
     * @return Lista de productos
     */
    private List<Producto> getOrdenesLab(List<Producto> productos, List<Detallepaquete> paquetes) {
        try {
            List<Producto> productosLaboratorio = new ArrayList<>();
            ServiceApp sApp = new ServiceAppImpl();
            App app = sApp.getBy("a.appNombre ='"+ConstantConfig.CONFIG_LABORATORY_AREA+"'");
            if (app.getAppValor() != null) {
                /*for (Producto producto : productos) {
                    if (producto.getArea().getAreaId().equals(Integer.parseInt(app.getAppValor()))) {
                        productosLaboratorio.add(producto);
                    }
                }*/

                productos.stream().forEach(producto -> {
                    if (producto.getArea().getAreaId().equals(Integer.parseInt(app.getAppValor()))) {
                        productosLaboratorio.add(producto);
                    }
                });
                
                /*for (Detallepaquete paquete : paquetes) {
                    for (Object p : paquete.getPaquetes()) {
                        Paquete pq = (Paquete) p;
                        if (pq.getProducto().getArea().getAreaId().equals(Integer.parseInt(app.getAppValor()))) {
                            productosLaboratorio.add(pq.getProducto());
                        }
                    }
                }*/
                paquetes.stream().forEach(p -> {
                    p.getPaquetes().stream().forEach(pq -> {
                        if (pq.getProducto().getArea().getAreaId().equals(Integer.parseInt(app.getAppValor()))) {
                            productosLaboratorio.add(pq.getProducto());
                        }
                    });
                });
            } else {
                JOptionPane.showMessageDialog(null, "No se ha definido el rango de ordenes para enterprise.");
            }
            return productosLaboratorio;
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
        return null;
    }

    /**
     * *
     * Une tanto productos como paquetes en un arreglo de productos
     *
     * @param productos Productos de la venta
     * @param paquetes Paquetes de la venta
     * @return Lista de productos
     */
    private List<Producto> getProductosTotal(List<Producto> productos, List<Detallepaquete> paquetes) {
        ServiceProducto sProducto = new ServiceProductoImpl();
        paquetes.forEach(paquete -> {
            paquete.getPaquetes().stream().map(p -> (Paquete)p).forEachOrdered(pq -> {
                try {
                    productos.add(sProducto.getBy("p.productoClave = '" + pq.getProducto().getProductoClave() + "'"));
                } catch (Exception ex) {
                    LoggerUtil.registrarError(ex);
                }
            });
        });
        return productos;
    }

    /**
     * *
     * Reimprime la ultima venta realizada en la caja en cuestion
     *
     * @param idTr Id de la transacción
     */
    private void rePrintUltimaVenta(long idTr) {
        try {
            long idCliente = 0;
            java.util.Date date = new java.util.Date();
            ServiceTr sTr = new ServiceTrImpl();
            ServiceApp sApp = new ServiceAppImpl();
            ServiceSucursalLocal sSucursal = new ServiceSucursalLocalImp();
            ServiceCliente sCliente = new ServiceClienteImpl();

            Tr tr = sTr.getBy("t.id.trValor = " + idTr);
            App app = sApp.getBy("a.appNombre ='"+ConstantConfig.CONFIG_BRANCH_ID+"'");
            Sucursal sucursal = sSucursal.getBy("s.sucursalId = " + app.getAppValor());

            User usr = getUser(tr);
            Ordenlab ordenLab = getOrdenlab(tr);

            ClienteJRDataSource datasource = new ClienteJRDataSource();
            //Preparando lista de productos
            float total = 0;
            for (Object o : tr.getOrdens()) {
                Orden orden = (Orden)o;
                if (orden.getDetallepaquete() != null) {
                    Producto pr = new Producto();
                    pr.setProductoClave("1");
                    pr.setProductoNombre(orden.getDetallepaquete().getDetallePaqueteNombre() + " [" + orden.getDetallepaquete().getDetallePaqueteDesc() + "]");
                    total = total + orden.getDetallepaquete().getDetallePaquetePrecio();
                    datasource.addProducto(pr);
                }
                if (orden.getProducto() != null) {
                    Producto pr = new Producto();
                    pr.setProductoClave("1");
                    pr.setProductoNombre(orden.getProducto().getProductoNombre());
                    total = total + orden.getProducto().getProductoPrecio();
                    datasource.addProducto(pr);
                }
                idCliente = orden.getOrdenPacienteId();
            }
            Cliente customer = sCliente.obtenerCliente("p.clienteId=" + idCliente);
            Map param = new HashMap();
            param.put("CLIENTE", "[" + customer.getClienteId() + "] " + customer.getClienteNombre() + " " + customer.getClientePaterno() + " " + customer.getClienteMaterno());
            param.put("USUARIO", (usr != null) ? usr.getUserUsuario() : "");
            param.put("F_NAC", FormatDateUtil.SFD_DDMMYYYY.format(customer.getClienteFnac()));
            param.put("EDAD", String.valueOf(CalculateAgeUtil.calculate(FormatDateUtil.SFD_DMY.format(customer.getClienteFnac()))));
            param.put("FECHA", FormatDateUtil.SFD_DMY.format(date));
            param.put("HORA", FormatDateUtil.SFD_KKMMSS.format(date));
            param.put("SUCURSAL", sucursal.getSucursalNombre());
            param.put("TOTAL", String.valueOf(total));
            param.put("TRANSCC", (ordenLab != null) ? "LAB[" + String.valueOf(ordenLab.getId().getOrdenlabContDiario()) + "]" : "");
            param.put("MENSAJE", "");
            param.put("REIMPRESION", "REIMPRESIÓN");
            PrintReportImpl pr = new PrintReportImpl();
            pr.imprimeTiketCliente(param, datasource);
        } catch (Exception ex) {
            if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
        }
    }

    /**
     * *
     * Separa los productos de acuerdo al area a la que pertenecen
     *
     * @param ordenLab Orden de laboratorio
     * @param cliente Paciente
     */
    private void separaProductosPorArea(Ordenlab ordenLab, Cliente cliente) {
        try {
            List<Area> areas = new ArrayList<>();
            List<List<Producto>> prodEnAreas = new ArrayList<>();
            List<Map> mapProductos = new ArrayList<>();
            ServicePaquete sPaquete = new ServicePaqueteImpl();

            ServiceClave sClave = new ServiceClaveImpl();
            ServiceProducto sProducto = new ServiceProductoImpl();

            ServiceFolio sFolio = new ServiceFolioImpl();
            ServiceApp sApp = new ServiceAppImpl();
            List<Producto> productos = new ArrayList<>();
            ServiceSucursalLocal sSucursal = new ServiceSucursalLocalImp();
            App app = sApp.getBy("a.appNombre ='"+ConstantConfig.CONFIG_BRANCH_ID+"'");
            App app2 = sApp.getBy("a.appNombre='"+ConstantConfig.CONFIG_LABORATORY_AREA+"'");
            Sucursal sucursal = sSucursal.getBy("s.sucursalId = " + app.getAppValor());
            Folio folio = sFolio.getBy("f.id.folioValor = " + this.idFolioVentaAnterior);

            for (Object o : folio.getOrdens()) {
                Orden orden = (Orden)o;
                if (orden.getDetallepaquete() != null) {
                    List<Paquete> pqts = sPaquete.findBy("p.detallepaquete.detallePaqueteClave = '" + orden.getDetallepaquete().getDetallePaqueteClave() + "'");
                    for (Paquete paquete : pqts) {
                        productos.add(sProducto.getBy("p.productoClave = '" + paquete.getProducto().getProductoClave() + "'"));
                    }
                }
                if (orden.getProducto() != null) {
                    productos.add(orden.getProducto());
                }
            }

            //Obtiene las areas
            /*for (Producto producto : productos) {
               if (!isArea(producto.getArea(), areas)) {
                    areas.add(producto.getArea());
                }
            }*/
            productos.stream().forEach(producto -> {
                if (!isArea(producto.getArea(), areas)) {
                    areas.add(producto.getArea());
                }
            });

            for (Area area : areas) {
                List<Producto> produtoArea = new ArrayList<>();
                for (Producto pr : productos) {
                    if (area.getAreaId().equals(pr.getArea().getAreaId())) {
                        Map map = new HashMap();
                        map.put("PRODUCTO_ID", pr.getProductoClave());
                        map.put("PRODUCTO_AREA_ID", pr.getArea().getAreaId());
                        map.put("PRODUCTO_CLAVE", pr.getProductoClave());
                        map.put("PRODUCTO_NOMBRE", pr.getProductoNombre());
                        map.put("PRODUCTO_PRECIO", pr.getProductoPrecio());
                        map.put("PRODUCTO_DOCTOR_ID", "");
                        map.put("PRODUCTO_STATUS", "1");
                        if (pr.getArea().getAreaId() == Integer.parseInt(app2.getAppValor())) {
                            Clave clave = sClave.getBy("c.id.claveProductoClave = '" + pr.getProductoClave() + "'");
                            map.put("PRODUCTO_ENTERPRISE", clave.getEnterprise().getEnterpriseClave());
                        } else {
                            map.put("PRODUCTO_ENTERPRISE", "");
                        }
                        mapProductos.add(map);
                        produtoArea.add(pr);
                    }
                }
                prodEnAreas.add(produtoArea);
            }

            /*for (List<Producto> pEnArea : prodEnAreas) {
            for (Producto p : pEnArea) {
            int i = 0;
            for (List<Producto> pEnArea2 : prodEnAreas) {
            for (Producto p2 : pEnArea2) {
            if (p.getProductoClave().equals(p2.getProductoClave())) {
            i++;
            }
            }
            }
            p.setProductoStatus(String.valueOf(i));
            }
            }*/
            prodEnAreas.forEach(pEnArea -> {
                pEnArea.forEach(p -> {
                    int i = 0;
                    for (List<Producto> pEnArea2 : prodEnAreas) {
                        i = pEnArea2.stream().filter(p2 -> (p.getProductoClave().equals(p2.getProductoClave()))).map(_item -> 1).reduce(i, Integer::sum);
                    }
                    p.setProductoStatus(String.valueOf(i));
                });
            });
            
//        for (Map mp : mapProductos) {
//            int i = 0;
//            for (List<Producto> pEnArea2 : prodEnAreas) {
//                for (Producto p2 : pEnArea2) {
//                    if (p2.getProductoClave().equals(mp.get("PRODUCTO_ID"))) {
//                        i++;
//                    }
//                }
//            }
//            mp.put("PRODUCTO_STATUS", i);
//        }
//Quitar repetidos
            List<Map> mapProductosFinal = new ArrayList<>();

            HashSet hashset = new HashSet();
            hashset.addAll(mapProductos);
            mapProductosFinal.addAll(hashset);

            List<List<Map>> productosPorArea = new ArrayList<>();

            /*for (Area area : areas) {
                List<Map> maps = new ArrayList<>();
                for (Map mp : mapProductosFinal) {
                    if (area.getAreaId().equals(mp.get("PRODUCTO_AREA_ID"))) {
                        mp.put("PRODUCTO_AREA_NOMBRE", area.getAreaNombre());
                        maps.add(mp);
                    }
                }
                productosPorArea.add(maps);
            }*/
            
            for (Area area : areas) {
                List<Map> maps = new ArrayList<>();
                mapProductosFinal.stream().filter(mp -> (area.getAreaId().equals(mp.get("PRODUCTO_AREA_ID")))).map(mp -> {
                    mp.put("PRODUCTO_AREA_NOMBRE", area.getAreaNombre());
                    return mp;
                }).forEachOrdered(mp -> {
                    maps.add(mp);
                });
                productosPorArea.add(maps);
            }
            
            int i = 0;
            for (List<Map> ppa : productosPorArea) {
                if (ppa.size() > 0) {
                    reprintPapeletas(ppa, areas.size(), i + 1, ordenLab, cliente, sucursal);
                    i++;
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
        }
    }

    /**
     * *
     * Verifica si un area se encuenta en el arreglo de areas enviado
     *
     * @param area Area a buscar
     * @param areas Arreaglo de areas
     * @return True su ya se encuentra el area en el arreglo / False si no
     */
    private boolean isArea(Area area, List<Area> areas) {
        return areas.stream().anyMatch(a -> (Objects.equals(a.getAreaId(), area.getAreaId())));
    }

    /**
     * *
     *
     * @param datos Parametros para el reporte
     * @param areasTotal Numero total de areas
     * @param areaActual Area actual
     * @param ordenLab Número de orden de laboratorio
     * @param cliente Paciente al que se le realizaran los estudios de
     * laboratorio
     * @param sucursal Sucursal de donde se emite la orden de laboratorio
     * @return True si se imprime la papeleta / False si no
     */
    private boolean reprintPapeletas(List<Map> datos, int areasTotal, int areaActual, Ordenlab ordenLab, Cliente cliente, Sucursal sucursal) {
        ComandaJRDataSource datasource = new ComandaJRDataSource();
        ServiceApp sApp = new ServiceAppImpl();
        java.util.Date date = new java.util.Date();
        String area = null;

        for (Map p : datos) {
            try {
                area = p.get("PRODUCTO_AREA_NOMBRE").toString();
                App app = sApp.getBy("a.appNombre='"+ConstantConfig.CONFIG_LABORATORY_AREA+"'");
                if (p.get("PRODUCTO_AREA_ID").toString().equals(app.getAppValor())) {
                    Producto pr = new Producto();
                    //Si es un producto de laboratorio, agregar a la papeleta el codigo del producto en enterprise
                    //arr.add(p.get("PRODUCTO_STATUS") + "           " + "[" + p.get("PRODUCTO_ENTERPRISE") + "]  " + p.get("PRODUCTO_NOMBRE").toString());
                    pr.setProductoClave(String.valueOf(p.get("PRODUCTO_STATUS")));
                    pr.setProductoNombre("[" + p.get("PRODUCTO_ENTERPRISE").toString() + "]  " + p.get("PRODUCTO_NOMBRE").toString());
                    datasource.addProducto(pr);
                } else {
                    Producto pr = new Producto();
                    //arr.add(p.get("PRODUCTO_STATUS") + "           " + p.get("PRODUCTO_NOMBRE").toString());
                    pr.setProductoClave(p.get("PRODUCTO_STATUS").toString());
                    pr.setProductoNombre(p.get("PRODUCTO_NOMBRE").toString());
                    datasource.addProducto(pr);
                }
            } catch (Exception ex) {
                if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
            }
        }

        if (datos.size() > 0) {
            Map param = new HashMap();
            param.put("AREA", area);
            param.put("CLIENTE", "[" + cliente.getClienteId() + "] " + cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
            param.put("USUARIO", this.frMenu.user.getUserUsuario());
            param.put("EDAD", String.valueOf(CalculateAgeUtil.calculate(FormatDateUtil.SFD_DMY.format(cliente.getClienteFnac()))));
            param.put("PROGESS", areaActual + " / " + areasTotal);
            param.put("FECHA", FormatDateUtil.SFD_DMY.format(date));
            param.put("HORA", FormatDateUtil.SFD_KKMMSS.format(date));
            param.put("REIMPRESION", "REIMPRESIÓN");
            if (ordenLab != null) {
                param.put("FOLIO_ORDER", String.valueOf(ordenLab.getId().getOrdenlabContDiario()));
            } else {
                param.put("FOLIO_ORDER", "");
            }
            param.put("F_NAC", FormatDateUtil.SFD_DMY.format(cliente.getClienteFnac()));
            String genero = (cliente.getClienteGenero()) ? "MASCULINO" : "FEMENINO";
            param.put("GENERO", genero);
            param.put("SUCURSAL", sucursal.getSucursalNombre());
            PrintReportImpl pr = new PrintReportImpl();
            pr.papeletaComanda(param, datasource);
        }
        return true;
    }

    /**
     * 
     * Verifica si existe impresora predeterminada Si existe, activa el boton de
     * venta, si no, lo deshabilita
     */
    private void initData() {
        try {
            PrintReportImpl pr = new PrintReportImpl();
            ServiceX sX = new ServiceXImpl();
            this.arrX = sX.findAll();

            if (pr.getPrinters() <= 0) {
                JOptionPane.showMessageDialog(this, "No existe una impresora predeterminada.");
                btnOk.setEnabled(false);
                txtClave.selectAll();
                txtClave.requestFocus();
            }
            txtCajero.setText(this.frMenu.user.getUserNombre() + " " + this.frMenu.user.getUserPaterno() + " " + this.frMenu.user.getUserMaterno());
            ServiceApp sApp = new ServiceAppImpl();
            List<App> arr = sApp.findAll();
            for (App app : arr) {
                switch (app.getAppNombre()) {
                    case ConstantConfig.CONFIG_LABORATORY_AREA:
                        if (app.getAppValor() == null) {
                            JOptionPane.showMessageDialog(this, "Debe definir en que area se encuentran los estudios de laboratorio.");
                            btnOk.setEnabled(false);
                        }
                        break;
                    case ConstantConfig.CONFIG_BRANCH_ID:
                        if (app.getAppValor() == null) {
                            JOptionPane.showMessageDialog(this, "Debe definir el numero de sucursal.");
                            btnOk.setEnabled(false);
                        }
                        break;
                    case ConstantConfig.CONFIG_COMPANY_RFC:
                        if (app.getAppValor() == null) {
                            JOptionPane.showMessageDialog(this, "Debe definir el RFC de la fundación.");
                            btnOk.setEnabled(false);
                        }
                        break;
                    case ConstantConfig.CONFIG_COMPANY_BUSSINES_NAME:
                        if (app.getAppValor() == null) {
                            JOptionPane.showMessageDialog(this, "Debe definir la razón social de la fundación.");
                            btnOk.setEnabled(false);
                        }
                        break;
                    case ConstantConfig.CONFIG_COMPANY_BUSSINES_ADDRES:
                        if (app.getAppValor() == null) {
                            JOptionPane.showMessageDialog(this, "Debe definir el domicilio fiscal de la fundación.");
                            btnOk.setEnabled(false);
                        }
                        break;
                    case ConstantConfig.CONFIG_ORDER_INIT:
                        if (app.getAppValor() == null) {
                            JOptionPane.showMessageDialog(this, "Debe definir en que numero en que iniciaran las ordenes de laboratorio.");
                            //btnOk.setEnabled(false);
                        }
                        break;
                    case ConstantConfig.CONFIG_ORDER_FINALIZE:
                        if (app.getAppValor() == null) {
                            JOptionPane.showMessageDialog(this, "Debe definir en que numero en que finalizaran las ordenes de laboratorio.");
                            btnOk.setEnabled(false);
                        }
                        break;
                    case ConstantConfig.CONFIG_FOLIO_DEVOLUTION_INIT:
                        if (app.getAppValor() == null) {
                            JOptionPane.showMessageDialog(this, "Debe definir el numero de folio en que inciaran las devoluciones.");
                            btnOk.setEnabled(false);
                        }
                        break;
                    case ConstantConfig.CONFIG_FOLIO_SALE_INIT:
                        if (app.getAppValor() == null) {
                            JOptionPane.showMessageDialog(this, "Debe definir el numero de folio en que iniciaran las ventas.");
                            btnOk.setEnabled(false);
                        }
                        break;
                }
                btnBuscar.setVisible(false);
                btnExportar.setVisible(false);
                btnImportar.setVisible(false);
                btnLab.setVisible(false);
                btnLentes.setVisible(false);
                btnOk.setVisible(false);
                btnPaciente.setVisible(false);
                btnQuitar.setVisible(false);
                btnReimprimir.setVisible(false);
                btndev.setVisible(false);
            }
        } catch (Exception ex) {
            if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
        }
    }

    private void loadPermisos(User usuario) {
        Set<String> permisions = new HashSet<>();
        //quitar duplicados
        usuario.getRol().getPermisos().stream().forEach(p -> {
            permisions.add(p.getAccion().getAccionDesc());
        });
        
        permisions.stream().forEach(permiso -> {
            if ("BUSCAR PRODUCTOS".equals(permiso)) {
                btnBuscar.setVisible(true);
            }
            if ("BUSCAR PACIENTES".equals(permiso)) {
                btnPaciente.setVisible(true);
            }
            if ("ORDENES DE LABORATORIO".equals(permiso)) {
                btnLab.setVisible(true);
            }
            if ("COBRO DE DONATIVOS".equals(permiso)) {
                btnOk.setVisible(true);
            }
            if ("DEVOLUCION".equals(permiso)) {
                btndev.setVisible(true);
            }
            if ("LENTES COBRADOS".equals(permiso)) {
                btnLentes.setVisible(true);
            }
            if ("REIMPRESION".equals(permiso)) {
                btnReimprimir.setVisible(true);
            }
            if ("IMPORTAR ORDENES".equals(permiso)) {
                btnImportar.setVisible(true);
            }
            if ("EXPORTAR ORDENES".equals(permiso)) {
                btnExportar.setVisible(true);
            }
            if ("QUITAR PRODUCTO".equals(permiso)) {
                btnQuitar.setVisible(true);
            }
        });
    }

    public void imprimeUltima() {
        if (this.idTrVentaAnterior > 0) {
            try {
                ServiceOrden sOrden = new ServiceOrdenImpl();
                ServiceCliente sCliente = new ServiceClienteImpl();
                ServiceFolio sFolio = new ServiceFolioImpl();
                Ordenlab ordenLab = null;
                long idPaciente = 0;
                float total = 0;
                List<Orden> ordenes = sOrden.findBy("o.folio.id.folioValor = " + this.idFolioVentaAnterior);
                for (Orden ordene : ordenes) {
                    ordenLab = ordene.getOrdenlab();
                    idPaciente = ordene.getOrdenPacienteId();
                    total = (ordene.getProducto() != null) ? total + ordene.getProducto().getProductoPrecio() : total + ordene.getDetallepaquete().getDetallePaquetePrecio();
                }
                Cliente clte = sCliente.obtenerCliente("p.clienteId = " + idPaciente);
                rePrintUltimaVenta(this.idTrVentaAnterior);
                separaProductosPorArea(ordenLab, clte);
                Sucursal sucursal = GeneralGetters.getSucursal();
                Folio folio = sFolio.getBy("f.id.folioValor=" + this.idFolioVentaAnterior);
                PrintReportImpl pr = new PrintReportImpl();
                pr.printSimplificado(clte, sucursal, folio.getId().getFolioValor(), String.format("%.2f", total), NumberToLetterConverter.convertNumberToLetter(String.format("%.2f", total).replaceAll(",", "")), "REIMPRESIÓN");
            } catch (Exception ex) {
                if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "No existe una venta anterior.");
            txtClave.selectAll();
            txtClave.requestFocus();
        }
    }

    private User getUser(Tr tr) {
        User usr = null;
        for (Object o : tr.getOrdens()) {
            Orden orden = (Orden) o;
            usr = orden.getUser();
        }
        return usr;
    }

    private Ordenlab getOrdenlab(Tr tr) {
        Ordenlab or = null;
        for (Object o : tr.getOrdens()) {
            Orden orden = (Orden) o;
            or = orden.getOrdenlab();
        }
        return or;
    }
}
