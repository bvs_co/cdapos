/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.view;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.constants.ConstantReport;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import mx.com.cdadb.util.FormatDateUtil;

/**
 *
 * @author unknown
 */
public class TestReport {

    public static void main(String args[]) {
        new TestReport().report();
    }

    public void report() {
        try {
            Map param = new HashMap();
            param.put("FOLIO", "FOLIO");
            param.put("DONANTE", "BENITO MORALES RAMIREZ");
            param.put("CANTIDAD_NUMERO", "$ 1,500.00");
            param.put("CANTIDAD_LETRA", "UN MIL QUINIENTOS PESOS 00/100");
            param.put("FECHA_HORA", "2017-04-13 15:55:00");
            param.put("REIMPRESION", "rrrrrr");
            param.put("MATRIZ_RAZON_SOC", "CORAZON DE ANGEL A.C. PARA LA DETECCION DEL CANCER EN LA MUJER Y EL HOMBRE GUERRERENSE DE GUERRERO QUE SON DE GUERRERO");
            param.put("RFC_MATRIZ", "RFCE4556755RFC");
            param.put("DOMICILIO_MATRIZ", "DOMICILIO MATRIZ");
            param.put("DIRECCION_SUCURSAL", "DIRECCION DE LA SUCURSAL");
            param.put("SUCURSAL", "EJIDO");
            JasperReport jasperReport = null;

            URL in = this.getClass().getResource(ConstantReport.DONATION_VOUCHER);
            jasperReport = (JasperReport) JRLoader.loadObject(in);
            JRDataSource data = new JREmptyDataSource();
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, param, data);
            JasperViewer.viewReport(jasperPrint, true);
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
        }
    }
}
