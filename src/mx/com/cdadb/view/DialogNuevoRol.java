package mx.com.cdadb.view;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import mx.com.cdadb.entities.Accion;
import mx.com.cdadb.entities.Modulo;
import mx.com.cdadb.entities.Permiso;
import mx.com.cdadb.enums.CellRendererEnum;
import mx.com.cdadb.service.impl.ServiceBaseImpl;
import mx.com.cdadb.util.CellRendererUtil;
import mx.com.cdadb.util.HeaderCellRenderer;

/**
 *
 * @author unknown
 */
public class DialogNuevoRol extends javax.swing.JDialog {

    List<Permiso> permisos = new ArrayList<Permiso>();
    List<String> arrMenus = new ArrayList<String>();
    List<String> arrSubmenus = new ArrayList<String>();
    List<String> arrPermisos = new ArrayList<String>();
    private final ServiceBaseImpl service = new ServiceBaseImpl();
    private String menuSelected = "";

    public DialogNuevoRol(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setTitle("Nuevo rol.");
        this.setLocationRelativeTo(null);
        initMenu();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        panelMenus1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaMenus = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        panelSubmenues = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tablaSubMenus = new javax.swing.JTable();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaPermisos = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        panelMenus1.setBackground(new java.awt.Color(255, 255, 255));

        tablaMenus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "NOMBRE"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaMenus.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaMenus.getTableHeader().setReorderingAllowed(false);
        tablaMenus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaMenusMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tablaMenus);
        if (tablaMenus.getColumnModel().getColumnCount() > 0) {
            tablaMenus.getColumnModel().getColumn(0).setResizable(false);
            tablaMenus.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablaMenus.getColumnModel().getColumn(1).setResizable(false);
            tablaMenus.getColumnModel().getColumn(1).setPreferredWidth(300);
        }

        javax.swing.GroupLayout panelMenus1Layout = new javax.swing.GroupLayout(panelMenus1);
        panelMenus1.setLayout(panelMenus1Layout);
        panelMenus1Layout.setHorizontalGroup(
            panelMenus1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenus1Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
                .addGap(2, 2, 2))
        );
        panelMenus1Layout.setVerticalGroup(
            panelMenus1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("MENÚS");

        panelSubmenues.setBackground(new java.awt.Color(255, 255, 255));

        tablaSubMenus.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "NOMBRE"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaSubMenus.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaSubMenus.getTableHeader().setReorderingAllowed(false);
        tablaSubMenus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaSubMenusMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tablaSubMenus);
        if (tablaSubMenus.getColumnModel().getColumnCount() > 0) {
            tablaSubMenus.getColumnModel().getColumn(0).setResizable(false);
            tablaSubMenus.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablaSubMenus.getColumnModel().getColumn(1).setResizable(false);
            tablaSubMenus.getColumnModel().getColumn(1).setPreferredWidth(290);
        }

        javax.swing.GroupLayout panelSubmenuesLayout = new javax.swing.GroupLayout(panelSubmenues);
        panelSubmenues.setLayout(panelSubmenuesLayout);
        panelSubmenuesLayout.setHorizontalGroup(
            panelSubmenuesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        panelSubmenuesLayout.setVerticalGroup(
            panelSubmenuesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("SUBMENÚS");

        jLabel5.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("PERMISOS");

        tablaPermisos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "Nombre", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaPermisos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tablaPermisos.getTableHeader().setReorderingAllowed(false);
        tablaPermisos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaPermisosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaPermisos);
        if (tablaPermisos.getColumnModel().getColumnCount() > 0) {
            tablaPermisos.getColumnModel().getColumn(0).setResizable(false);
            tablaPermisos.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablaPermisos.getColumnModel().getColumn(1).setPreferredWidth(400);
            tablaPermisos.getColumnModel().getColumn(2).setResizable(false);
        }

        jButton1.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jButton1.setText("Guardar [F5]");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator9)
                    .addComponent(jSeparator8)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panelMenus1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator2)
                            .addComponent(jSeparator3)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(24, 24, 24)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(panelSubmenues, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jSeparator5)
                            .addComponent(jSeparator4)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addGap(338, 338, 338))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator4)
                    .addComponent(jSeparator2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator5)
                    .addComponent(jSeparator3))
                .addGap(5, 5, 5)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelSubmenues, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelMenus1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap())
        );

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 16)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Nombre:");

        jTextField1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablaMenusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaMenusMouseClicked
        menuSelected = String.valueOf(tablaMenus.getValueAt(tablaMenus.getSelectedRow(), 1));
        paintSubMenues(menuSelected);
        setVoidTablePermisos();
        //Cargar todos los permisos
        List<Accion> arr = (List<Accion>) service.findAll(new Accion());
        arrPermisos.clear();
        arr.forEach(p -> {
            arrPermisos.add("true|" + p.getAccionDesc() + "|" + p.getAccionId() + "|" + p.getModulo().getModuloSubmenu());
        });
    }//GEN-LAST:event_tablaMenusMouseClicked

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed

    }//GEN-LAST:event_jTextField1ActionPerformed

    private void tablaPermisosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaPermisosMouseClicked
        updatePermissions();
    }//GEN-LAST:event_tablaPermisosMouseClicked

    private void tablaSubMenusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaSubMenusMouseClicked
        paintPermissions(menuSelected, String.valueOf(tablaSubMenus.getValueAt(tablaSubMenus.getSelectedRow(), 1)));
        //addPermissions();
    }//GEN-LAST:event_tablaSubMenusMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        for (String arrMenu : arrMenus) {
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoRol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoRol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoRol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoRol.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogNuevoRol dialog = new DialogNuevoRol(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JPanel panelMenus1;
    public javax.swing.JPanel panelSubmenues;
    private javax.swing.JTable tablaMenus;
    private javax.swing.JTable tablaPermisos;
    private javax.swing.JTable tablaSubMenus;
    // End of variables declaration//GEN-END:variables

    private void initMenu() {
        try {
            List<Modulo> arr = (List<Modulo>) service.findAll(new Modulo());

            Set<String> menus = new HashSet<>();
            //quitar duplicados
            arr.stream().forEach(m -> {
                menus.add(m.getModuloMenu());
            });

            Object[][] datos = new Object[menus.size()][2];

            int i = 0;
            for (String menu : menus) {
                datos[i][1] = menu;
                datos[i][0] = true;
                i++;
            }
            setTableMenu(datos);
        } catch (Exception ex) {
            Logger.getLogger(DialogNuevoRol.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void initSubMenuProdServ(boolean op) {
        tablaSubMenus.setModel(new DefaultTableModel());
        Object[][] datos = new Object[5][2];
        datos[0][1] = "Productos";
        datos[0][0] = op;
        datos[1][1] = "Paquetes";
        datos[1][0] = op;
        datos[2][1] = "Servicios";
        datos[2][0] = op;
        datos[3][1] = "Enterprise";
        datos[3][0] = op;
        datos[4][1] = "Lentes";
        datos[4][0] = op;
        setTableSubMenu(datos);
    }

    private void initPermisosProductos(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Agregar producto";
        datos[0][0] = op;
        datos[1][1] = "Editar Producto";
        datos[1][0] = op;
        datos[2][1] = "Eliminar Producto";
        datos[2][0] = op;
        setTablePermisos(datos);
    }

    private void initPermisosPaquetes(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Agregar paquete";
        datos[0][0] = op;
        datos[1][1] = "Editar paquete";
        datos[1][0] = op;
        datos[2][1] = "Eliminar paquete";
        datos[2][0] = op;
        setTablePermisos(datos);
    }

    private void initSubMenuDonativos(boolean op) {
        tablaSubMenus.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Donativos";
        datos[0][0] = op;
        datos[1][1] = "Cortes";
        datos[1][0] = op;
        datos[2][1] = "Estados de caja";
        datos[2][0] = op;
        setTableSubMenu(datos);
    }

    private void initPermisosDantivos(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[9][2];
        datos[0][1] = "Buscar producto";
        datos[0][0] = op;
        datos[1][1] = "Pacientes";
        datos[1][0] = op;
        datos[2][1] = "Laboratorio";
        datos[2][0] = op;
        datos[3][1] = "Donativo";
        datos[3][0] = op;
        datos[4][1] = "Devoluciones";
        datos[4][0] = op;
        datos[5][1] = "Lentes";
        datos[5][0] = op;
        datos[6][1] = "Reimpresion";
        datos[6][0] = op;
        datos[7][1] = "Importar";
        datos[7][0] = op;
        datos[8][1] = "Exportar";
        datos[8][0] = op;

        tablaPermisos.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                new String[]{
                    "", "Nombre"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Boolean.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean[]{
                true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        tablaPermisos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);

        tablaPermisos.getTableHeader().setReorderingAllowed(false);

        jScrollPane1.setViewportView(tablaPermisos);

        if (tablaPermisos.getColumnModel().getColumnCount() > 0) {
            tablaPermisos.getColumnModel().getColumn(0).setResizable(false);
            tablaPermisos.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablaPermisos.getColumnModel().getColumn(1).setPreferredWidth(400);
        }

        JTableHeader jtableHeader = tablaPermisos.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaPermisos.setTableHeader(jtableHeader);

        tablaPermisos.setRowHeight(30);

        tablaPermisos.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));

    }

    private void initSubMenuConf(boolean op) {
        tablaSubMenus.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Roles";
        datos[0][0] = op;
        datos[1][1] = "Usuarios";
        datos[1][0] = op;
        datos[2][1] = "Sucursal";
        datos[2][0] = op;
        setTableSubMenu(datos);
    }

    private void initPermisosConfRoles(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Agrear rol";
        datos[0][0] = op;
        datos[1][1] = "Editar rol";
        datos[1][0] = op;
        datos[2][1] = "Eliminar rol";
        datos[2][0] = op;
        setTablePermisos(datos);
    }

    private void initPermisosServicios(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Agregar servicio";
        datos[0][0] = op;
        datos[1][1] = "Editar servicio";
        datos[1][0] = op;
        datos[2][1] = "Eliminar servicio";
        datos[2][0] = op;
        setTablePermisos(datos);
    }

    private void initPermisosEnterprise(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Agregar enterprise";
        datos[0][0] = op;
        datos[1][1] = "Editar enterprise";
        datos[1][0] = op;
        datos[2][1] = "Eliminar enterprise";
        datos[2][0] = op;
        setTablePermisos(datos);
    }

    private void setTablePermisos(Object[][] datos) {
        tablaPermisos.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                new String[]{
                    "", "Nombre", ""
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean[]{
                true, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        tablaPermisos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);

        tablaPermisos.getTableHeader().setReorderingAllowed(false);

//        tablaPermisos.addMouseListener(new java.awt.event.MouseAdapter() {
//            public void mouseClicked(java.awt.event.MouseEvent evt) {
//                tablaPermisosMouseClicked(evt);
//            }
//        });
        jScrollPane1.setViewportView(tablaPermisos);

        if (tablaPermisos.getColumnModel().getColumnCount() > 0) {
            tablaPermisos.getColumnModel().getColumn(0).setResizable(false);
            tablaPermisos.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablaPermisos.getColumnModel().getColumn(1).setPreferredWidth(400);
            tablaPermisos.getColumnModel().getColumn(2).setResizable(false);
            tablaPermisos.getColumnModel().getColumn(2).setPreferredWidth(100);
        }

        JTableHeader jtableHeader = tablaPermisos.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaPermisos.setTableHeader(jtableHeader);

        tablaPermisos.setRowHeight(30);

        tablaPermisos.getColumnModel().getColumn(1).setCellRenderer(new CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tablaPermisos.getColumnModel().getColumn(2).setCellRenderer(new CellRendererUtil(CellRendererEnum.PK_ID.toString()));

        //deleteVoidRows(tablaPermisos);
    }

    private void setVoidTablePermisos() {
        tablaPermisos.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{
                    "", "Nombre", ""
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Boolean.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean[]{
                true, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        tablaPermisos.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);

        tablaPermisos.getTableHeader().setReorderingAllowed(false);

//        tablaPermisos.addMouseListener(new java.awt.event.MouseAdapter() {
//            public void mouseClicked(java.awt.event.MouseEvent evt) {
//                tablaPermisosMouseClicked(evt);
//            }
//        });
        jScrollPane1.setViewportView(tablaPermisos);

        if (tablaPermisos.getColumnModel().getColumnCount() > 0) {
            tablaPermisos.getColumnModel().getColumn(0).setResizable(false);
            tablaPermisos.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablaPermisos.getColumnModel().getColumn(1).setPreferredWidth(400);
            tablaPermisos.getColumnModel().getColumn(2).setResizable(false);
        }

        JTableHeader jtableHeader = tablaPermisos.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaPermisos.setTableHeader(jtableHeader);

        tablaPermisos.setRowHeight(30);

        tablaPermisos.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
    }

    private void setTableMenu(Object[][] datos) {
        tablaMenus.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                new String[]{
                    "", "NOMBRE"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Boolean.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean[]{
                true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        tablaMenus.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);

        tablaMenus.getTableHeader().setReorderingAllowed(false);

        jScrollPane3.setViewportView(tablaMenus);

        if (tablaMenus.getColumnModel().getColumnCount() > 0) {
            tablaMenus.getColumnModel().getColumn(0).setResizable(false);
            tablaMenus.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablaMenus.getColumnModel().getColumn(1).setResizable(false);
            tablaMenus.getColumnModel().getColumn(1).setPreferredWidth(300);
        }

        JTableHeader jtableHeader = tablaMenus.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaMenus.setTableHeader(jtableHeader);

        tablaMenus.setRowHeight(30);

        tablaMenus.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
    }

    private void setTableSubMenu(Object[][] datos) {
        tablaSubMenus.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                new String[]{
                    "", "NOMBRE"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Boolean.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean[]{
                true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        tablaSubMenus.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);

        tablaSubMenus.getTableHeader().setReorderingAllowed(false);

        jScrollPane4.setViewportView(tablaSubMenus);

        if (tablaSubMenus.getColumnModel().getColumnCount() > 0) {
            tablaSubMenus.getColumnModel().getColumn(0).setResizable(false);
            tablaSubMenus.getColumnModel().getColumn(0).setPreferredWidth(50);
            tablaSubMenus.getColumnModel().getColumn(1).setResizable(false);
            tablaSubMenus.getColumnModel().getColumn(1).setPreferredWidth(290);
        }

        JTableHeader jtableHeader = tablaSubMenus.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaSubMenus.setTableHeader(jtableHeader);

        tablaSubMenus.setRowHeight(30);

        tablaSubMenus.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
    }

    private void initPermisosCortes(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Corte por usuario";
        datos[0][0] = op;
        datos[1][1] = "Corte final";
        datos[1][0] = op;
        datos[2][1] = "Arqueo de caja";
        datos[2][0] = op;
        setTablePermisos(datos);
    }

    private void initPermisosEstadosCaja(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Permiso 1";
        datos[0][0] = op;
        datos[1][1] = "Permiso 2";
        datos[1][0] = op;
        datos[2][1] = "Permiso 3";
        datos[2][0] = op;
        setTablePermisos(datos);
    }

    private void initPermisosRoles(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Agregar rol";
        datos[0][0] = op;
        datos[1][1] = "Editar rol";
        datos[1][0] = op;
        datos[2][1] = "Eliminar rol";
        datos[2][0] = op;
        setTablePermisos(datos);
    }

    private void initPermisosUsuarios(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Agregar usuario";
        datos[0][0] = op;
        datos[1][1] = "Editar usuario";
        datos[1][0] = op;
        datos[2][1] = "Eliminar usuario";
        datos[2][0] = op;
        setTablePermisos(datos);
    }

    private void initPermisosSucursal(boolean op) {
        tablaPermisos.setModel(new DefaultTableModel());
        Object[][] datos = new Object[3][2];
        datos[0][1] = "Permiso 1";
        datos[0][0] = op;
        datos[1][1] = "Permiso 2";
        datos[1][0] = op;
        datos[2][1] = "Permiso 3";
        datos[2][0] = op;
        setTablePermisos(datos);
    }

    private void addPermissions() {
        for (int i = 0; i < tablaPermisos.getRowCount(); i++) {
            arrPermisos.add(tablaPermisos.getValueAt(i, 0).toString() + "|" + tablaPermisos.getValueAt(i, 1).toString() + "|" + tablaPermisos.getValueAt(i, 2).toString());
        }
        System.out.println("----------- ADD PERMISOS ACTUALES ----------");
        arrPermisos.forEach(p -> {
            System.out.println(p);
        });
    }

    private void updatePermissions() {
        System.out.println("----------- UPDATE PERMISOS ACTUALES ANTES ----------");
        arrPermisos.forEach(p -> {
            System.out.println(p);
        });
        for (int i = 1; i < tablaPermisos.getRowCount(); i++) {
            for (String p : arrPermisos) {
                final String arr[] = p.split("\\|");
                if (arr[1].equals(tablaPermisos.getValueAt(i, 1).toString()) && arr[2].equals(tablaPermisos.getValueAt(i, 2).toString()) && !arr[0].equals(tablaPermisos.getValueAt(i, 0).toString())) {
                    updateArrPermissions(tablaPermisos.getValueAt(i, 0).toString(), tablaPermisos.getValueAt(i, 1).toString(), tablaPermisos.getValueAt(i, 2).toString());
                }
            }
        }

        System.out.println("----------- UPDATE PERMISOS ACTUALES DESPUES ----------");
        arrPermisos.forEach(p -> {
            System.out.println(p);
        });
    }

    private void removeMenus(String toString) {
        for (String arrMenu : arrMenus) {
            if (toString.equals(arrMenu)) {
                arrMenus.remove(arrMenu);
                break;
            }
        }
    }

    private void addSubMenus(String toString) {
        boolean is = false;
        for (String arrMenu : arrSubmenus) {
            if (toString.equals(arrMenu)) {
                is = true;
                break;
            }
        }
        if (!is) {
            arrSubmenus.add(toString);
        }
    }

    private void removeSubMenus(String toString) {
        for (String arrMenu : arrSubmenus) {
            if (toString.equals(arrMenu)) {
                arrSubmenus.remove(arrMenu);
                break;
            }
        }
    }

    private void addAllSubmenu(String toString) {

    }

    private void removeAllSubMenu(String toString) {

    }

    private void paintSubMenues(String _menuSelected) {
        try {
            List<Modulo> modules = (List<Modulo>) service.findBy(new Modulo(), "t.moduloMenu = '" + _menuSelected + "'");

            Object[][] datos = new Object[modules.size()][2];

            int i = 0;
            for (Modulo m : modules) {
                datos[i][1] = m.getModuloSubmenu();
                datos[i][0] = true;
                i++;
            }
            setTableSubMenu(datos);
        } catch (Exception ex) {
            Logger.getLogger(DialogNuevoRol.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void paintPermissions(String _menuSelected, String submenu) {
        try {
            //List<Modulo> modules = (List<Modulo>) service.findBy(new Modulo(), "t.moduloMenu = '" + _menuSelected + "'");

            List<String> aux = new ArrayList<>();
            arrPermisos.forEach(m -> {
                String[] split = m.split("\\|");
                if (split[3].equals(submenu)) {
                    aux.add(m);
                }
            });
            
            int i = 0;
            Object[][] datos = new Object[aux.size()][3];
            for (String m : aux) {
                String[] split = m.split("\\|");
                if (split[3].equals(submenu)) {
                    datos[i][1] = split[1];
                    datos[i][0] = Boolean.valueOf(split[0]);
                    datos[i][2] = split[2];
                    i++;
                }
            }
            setTablePermisos(datos);
        } catch (Exception ex) {
            Logger.getLogger(DialogNuevoRol.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void updateArrPermissions(String booleanValue, String permission, String idPermission) {
        for (String p : arrPermisos) {
            String[] arr = p.split("\\|");
            if (arr[1].equals(permission) && arr[2].equals(idPermission)) {
//                System.out.println("ID:"+arrPermisos.indexOf(p));
//                System.out.println("VALUE:"+arrPermisos.get(arrPermisos.indexOf(p)));
                arrPermisos.set(arrPermisos.indexOf(p), booleanValue + "|" + arr[1] + "|" + arr[2] + "|" + arr[3]);
            }
        }
    }

    private void deleteVoidRows(JTable tablaPermisos) {
        DefaultTableModel model = (DefaultTableModel) tablaPermisos.getModel();
        for (int i = 0; i < model.getRowCount(); i++) {
            if(model.getValueAt(i, 1) == null){
                model.removeRow(i);
            }
        }
        tablaPermisos.setModel(model);
        tablaPermisos.repaint();
    }
}
