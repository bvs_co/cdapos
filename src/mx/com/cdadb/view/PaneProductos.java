package mx.com.cdadb.view;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableRowSorter;
import mx.com.cdadb.entities.Area;
import mx.com.cdadb.entities.Detallepaquete;
import mx.com.cdadb.entities.Paquete;
import mx.com.cdadb.entities.PaqueteId;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.service.ServicePaquete;
import mx.com.cdadb.service.ServiceProducto;
import mx.com.cdadb.service.impl.ServicePaqueteImpl;
import mx.com.cdadb.service.impl.ServiceProductoImpl;
import mx.com.cdadb.util.DialogWait;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;
import mx.com.cdadb.util.PaneUtil;

/**
 *
 * @author unknown
 */
public class PaneProductos extends javax.swing.JPanel {

    private int opc = -1;
    public static Area area = null;
    private Detallepaquete detallePaquete = null;
    private ServiceProducto sProducto = new ServiceProductoImpl();
    private FrMenu frMenu2 = null;
    private PaneAreas paneAreas = null;
    private PaneProductos paneProductos = null;
    private PanePaquetes panePaquetes = null;

    public PaneProductos(int op, Area area, FrMenu fr) {
        initComponents();
        this.area = area;
        this.opc = op;
        this.frMenu2 = fr;
        lblInformacion.setText(area.getAreaNombre());
        DialogWait dw = new DialogWait(this.frMenu2, true, this);
        dw.setVisible(true);
        loadPermisos(this.frMenu2.user);
    }

    //Op = 4 viene de paquetes para agregar productos al mismo
    public PaneProductos(int op, FrMenu fr, Detallepaquete dt, Area area, PaneAreas a) {
        initComponents();
        this.opc = op;
        this.frMenu2 = fr;
        this.detallePaquete = dt;
        this.area = area;
        this.paneAreas = a;
        lblInformacion.setText(dt.getDetallePaqueteNombre());
        DialogWait dw = new DialogWait(this.frMenu2, true, this);
        dw.setVisible(true);
        loadPermisos(this.frMenu2.user);
    }

    //opcion 3: Muestra el contenido del paquete, y desde aqui se puede agregar productos
    public PaneProductos(int op, FrMenu fr, Detallepaquete dt, PanePaquetes p) {
        initComponents();
        this.opc = op;
        this.frMenu2 = fr;
        this.detallePaquete = dt;
        this.panePaquetes = p;
        lblInformacion.setText(dt.getDetallePaqueteNombre());
        DialogWait dw = new DialogWait(this.frMenu2, true, this);
        dw.setVisible(true);
        loadPermisos(this.frMenu2.user);
    }

    //Op 4, viene de paquete, para seleccionar productos a agregar en el paquete
    public PaneProductos(int op, FrMenu fr, Detallepaquete dt) {
        initComponents();
        this.opc = op;
        this.frMenu2 = fr;
        this.detallePaquete = dt;
        this.area = area;
        lblInformacion.setText(dt.getDetallePaqueteNombre());
        DialogWait dw = new DialogWait(this.frMenu2, true, this);
        dw.setVisible(true);
        loadPermisos(this.frMenu2.user);
    }

    //Opcion 6: viene de la lista de productos de un paquete
    public PaneProductos(int op, FrMenu fr, PaneProductos p, Detallepaquete dt) {
        initComponents();
        this.opc = op;
        this.frMenu2 = fr;
        this.paneProductos = p;
        this.detallePaquete = dt;
        lblInformacion.setText("Agregar productos a paquete: " + dt.getDetallePaqueteNombre());
        loadPermisos(this.frMenu2.user);
        DialogWait dw = new DialogWait(this.frMenu2, true, this);
        dw.setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBuscar = new javax.swing.JButton();
        txtBuscar = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        lblInformacion = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        btnBuscar.setToolTipText("Buscar [F5]");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        txtBuscar.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBuscarActionPerformed(evt);
            }
        });
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CLAVE", "NOMBRE", "PRECIO", "SELECC"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnCancelar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cancel_x16.png"))); // NOI18N
        btnCancelar.setText("Cancelar [Esc]");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        btnEliminar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/delete_x16.png"))); // NOI18N
        btnEliminar.setText("Eliminar [F3]");
        btnEliminar.setEnabled(false);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        btnEliminar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEliminarKeyPressed(evt);
            }
        });

        btnEditar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/pen_write_edit_x16.png"))); // NOI18N
        btnEditar.setText("Editar [F4]");
        btnEditar.setEnabled(false);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        btnEditar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEditarKeyPressed(evt);
            }
        });

        btnNuevo.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/add_x16.png"))); // NOI18N
        btnNuevo.setText("Nuevo [F2]");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        btnNuevo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnNuevoKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnNuevo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnNuevo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
                    .addComponent(btnEditar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        lblInformacion.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        lblInformacion.setText("...");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 843, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar)
                        .addGap(1, 1, 1)
                        .addComponent(btnBuscar))
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblInformacion)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addGap(5, 5, 5)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblInformacion)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleDescription("productos");
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        txtBuscarKeyReleased(null);
        txtBuscar.requestFocus();
        jTable1MouseClicked(null);
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void txtBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBuscarActionPerformed

    private void txtBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtBuscarKeyPressed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        txtBuscar.setText(txtBuscar.getText().toUpperCase());
        buscarProductos(txtBuscar.getText());
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        managedButtons();
        if (this.opc == 6) {
            if (Boolean.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 3).toString())) {
                txtBuscar.selectAll();
                txtBuscar.requestFocus();
            }
        }

    }//GEN-LAST:event_jTable1MouseClicked

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        // TODO add your handling code here:
        checkKeyPressed(evt);
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        if (jTable1.getSelectedRowCount() > 0) {
            managedButtons();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            txtBuscar.requestFocus();
        }
    }//GEN-LAST:event_jTable1KeyReleased

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        switch (this.opc) {
            case 1:
                PaneAreas paneAreas = new PaneAreas(this.frMenu2, null, 2);
                paneAreas.setSize(this.frMenu2.principalPane.getSize());
                this.frMenu2.principalPane.removeAll();
                this.frMenu2.principalPane.add(paneAreas, BorderLayout.CENTER);
                this.frMenu2.lblNavegador.setIcon(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/flask_x32.png")));
                this.frMenu2.lblNavegador.setText("Productos");
                this.frMenu2.lblSucursal.setText(" Doble clic en un registro, para editar.");
                this.frMenu2.principalPane.revalidate();
                this.frMenu2.principalPane.repaint();
                break;
            case 2:
                break;
            case 3://Regresa a paquetes
                //opcion 3: regresa a la ventana de paquetes
                //PanePaquetes panePaquetes = new PanePaquetes(this.frMenu2, 1);
                //this.panePaquetes.setSize(this.frMenu2.PrincipalPane.getSize());
                this.frMenu2.principalPane.removeAll();
                this.frMenu2.principalPane.add(this.panePaquetes, BorderLayout.CENTER);
                this.frMenu2.lblNavegador.setIcon(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/Package_x32.png")));
                this.frMenu2.lblNavegador.setText("Paquetes");
                this.frMenu2.lblSucursal.setText(" Doble clic en un registro, para ver su contenido.");
                this.frMenu2.principalPane.revalidate();
                this.frMenu2.principalPane.repaint();
                break;
            case 4:
                this.frMenu2.principalPane.removeAll();
                this.frMenu2.principalPane.add(this.paneAreas, BorderLayout.CENTER);
                this.frMenu2.lblNavegador.setIcon(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/Package_x32.png")));
                this.frMenu2.lblNavegador.setText("Paquetes");
                this.frMenu2.lblSucursal.setText(" Doble clic en un registro, para ver su contenido.");
                this.frMenu2.principalPane.revalidate();
                this.frMenu2.principalPane.repaint();
                break;
            case 6:
                this.frMenu2.principalPane.removeAll();
                this.frMenu2.principalPane.add(this.paneProductos, BorderLayout.CENTER);
                this.frMenu2.lblNavegador.setIcon(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/Package_x32.png")));
                this.frMenu2.lblNavegador.setText("Paquetes");
                this.frMenu2.lblSucursal.setText(" Doble clic en un registro, para ver su contenido.");
                this.frMenu2.principalPane.revalidate();
                this.frMenu2.principalPane.repaint();
                break;
        }
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        int op = -1;
        if (jTable1.getSelectedRowCount() > 0) {
            switch (this.opc) {
                case 1:
                    try {
                    op = JOptionPane.showConfirmDialog(this, "Esta seguro de eliminar el producto seleccionado.", "Confirme.", JOptionPane.YES_NO_OPTION);
                    if (op == 0) {
                        ServiceProducto serviceProduct = new ServiceProductoImpl();
                        Producto p = serviceProduct.getBy("p.productoClave = '" + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString() + "'");
                        if (serviceProduct.delete(p)) {
                            JOptionPane.showMessageDialog(this, "Producto eliminado correctamente.");
                            initData();
                            jTable1MouseClicked(null);
                        }
                    }
                } catch (Exception ex) {
                    LoggerUtil.registrarError(ex);
                }
                break;
                case 3://Eliminar productos de un paquete
                    op = JOptionPane.showConfirmDialog(this, "Esta seguro de eliminar el producto seleccionado.", "Confirme.", JOptionPane.YES_NO_OPTION);
                    try {
                        if (op == 0) {
                            ServicePaquete sPaquete = new ServicePaqueteImpl();
                            Paquete p = sPaquete.getBy("p.id.paqueteProductoClave = '" + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString() + "' and p.detallepaquete.detallePaqueteClave = '" + this.detallePaquete.getDetallePaqueteClave() + "'");
                            if (sPaquete.delete(p)) {
                                JOptionPane.showMessageDialog(this, "Producto eliminado correctamente, del paquete " + this.detallePaquete.getDetallePaqueteNombre() + ".");
                                initData();
                                jTable1MouseClicked(null);
                            }
                        }
                    } catch (Exception ex) {
                        LoggerUtil.registrarError(ex);
                    }
                    break;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione el registro a eliminar.");
            txtBuscar.selectAll();
            txtBuscar.requestFocus();
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnEliminarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEliminarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnEliminarKeyPressed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        if (jTable1.getSelectedRowCount() > 0) {
            try {
                Producto producto = sProducto.getBy("p.productoClave = '" + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString() + "'");
                DialogNuevoProducto frModificaProducto = new DialogNuevoProducto(this.frMenu2, true, 2, this.area, producto, this);
                frModificaProducto.setVisible(true);
            } catch (Exception ex) {
                LoggerUtil.registrarError(ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Seleccione el registro a editar.");
            txtBuscar.selectAll();
            txtBuscar.requestFocus();
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnEditarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEditarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnEditarKeyPressed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        switch (this.opc) {
            case 1:
                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                DialogNuevoProducto dialogNuevoProducto = new DialogNuevoProducto(this.frMenu2, true, 1, this.area, this);
                dialogNuevoProducto.setVisible(true);
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                break;
            case 2://Seleccionando productos para agregar a paquete
                if (getSeleccionados() > 0) {
                    if (guardar()) {
                        JOptionPane.showMessageDialog(this, "Productos agregados al paquete seleccionado, correctamente.");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Debe seleccionar por lo menos un producto.");
                }
                break;
            case 3://Agregar productos a paquete
                PaneProductos pane = new PaneProductos(6, this.frMenu2, this, detallePaquete);
                pane.loadPermisos(this.frMenu2.user);
                pane.setSize(this.frMenu2.principalPane.getSize());
                this.frMenu2.principalPane.removeAll();
                this.frMenu2.principalPane.add(pane, BorderLayout.CENTER);
                this.frMenu2.principalPane.revalidate();
                this.frMenu2.principalPane.repaint();
                PaneUtil.requesttFocus(frMenu2.principalPane);

                break;
            case 4:

                break;
            case 6://Agrega productos a un paquete seleccionado
                if (getSeleccionados() > 0) {
                    if (guardar()) {
                        JOptionPane.showMessageDialog(this, "Productos agregados al paquete seleccionado, correctamente.");
                        this.paneProductos.loadPermisos(this.frMenu2.user);
                        this.frMenu2.principalPane.removeAll();
                        this.frMenu2.principalPane.add(this.paneProductos, BorderLayout.CENTER);
                        this.frMenu2.principalPane.revalidate();
                        this.paneProductos.initData();
                        this.paneProductos.initData();
                        PaneUtil.requesttFocus(frMenu2.principalPane);
                        this.frMenu2.principalPane.repaint();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Debe seleccionar por lo menos un producto.");
                }
                break;
        }
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnNuevoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnNuevoKeyPressed
        // TODO add your handling code here:
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnNuevoKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lblInformacion;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables

//    private void add_event(final JTable b, final PaneProductos dlg) {
//        b.addMouseListener(new java.awt.event.MouseAdapter() {
//            @Override
//            public void mouseClicked(java.awt.event.MouseEvent evt) {
//                if (evt.getClickCount() == 2) {
//                    switch (dlg.opc) {
//                        case 1:
//                            break;
//                        case 2:
//                            break;
//                        case 3:
//                            break;
//                        case 4:
//                            break;
//                    }
//
//                }
//            }
//        });
//    }
    public void initData() {
        ServiceProducto serviceProducto = new ServiceProductoImpl();
        List<Producto> productos;
        ServicePaquete sp = new ServicePaqueteImpl();
        List<Paquete> paquetes;
        switch (this.opc) {
            case 1:
                try {
                productos = serviceProducto.findBy("p.area.areaId=" + this.area.getAreaId() + " and p.productoStatus = 'ACT'");
                fillTable(productos, false);
            } catch (Exception ex) {
                LoggerUtil.registrarError(ex);
            }
            break;
            case 2://Seleccionando productos para agregar a un paquete
                try {
                productos = serviceProducto.findBy("p.area.areaId=" + this.area.getAreaId() + " and p.productoStatus = 'ACT'");
                fillTable(productos, true);
            } catch (Exception ex) {
                LoggerUtil.registrarError(ex);
            }
            break;
            case 3://Muestra productos de un paquete
                try {
                paquetes = sp.findBy("p.id.detallePaqueteClave = '" + this.detallePaquete.getDetallePaqueteClave() + "'");
                fillTable2(paquetes, true);
            } catch (Exception ex) {
                LoggerUtil.registrarError(ex);
            }
            break;
            case 4:
                try {
                productos = serviceProducto.findBy("p.area.areaId=" + this.area.getAreaId() + " and p.productoStatus = 'ACT'");
                fillTable(productos, true);
                paquetes = sp.findBy("p.id.detallePaqueteClave = '" + this.detallePaquete.getDetallePaqueteClave() + "'");
                desactivaRow(paquetes);
            } catch (Exception ex) {
                LoggerUtil.registrarError(ex);
            }
            break;
            case 5:

                try {
                productos = serviceProducto.findBy("p.area.areaId=" + this.area.getAreaId() + " and p.productoStatus = 'ACT'");
                fillTable(productos, true);
                paquetes = sp.findBy("p.id.detallePaqueteClave = '" + this.detallePaquete.getDetallePaqueteClave() + "'");
                desactivaRow(paquetes);
            } catch (Exception ex) {
                LoggerUtil.registrarError(ex);
            }
            break;
            case 6://Muestra todos los productos para agregar a paquete
                try {
                productos = serviceProducto.findBy("p.productoStatus = 'ACT'");
                fillTable(productos, true);
            } catch (Exception ex) {
                LoggerUtil.registrarError(ex);
            }
            break;
        }
        btnNuevo.setVisible(false);
        btnEliminar.setVisible(false);
        btnEditar.setVisible(false);
    }

    public void initGui() {
        switch (this.opc) {
            case 1:
                break;
            case 2://Seleccionando productos para agregar a paquete
                btnNuevo.setText("Agregar  [F2]");
                btnNuevo.setMnemonic('A');
                btnEditar.setVisible(false);
                btnEliminar.setVisible(false);
                break;
            case 3://Productos de paquete
                btnNuevo.setText("Agregar [F2]");
                btnNuevo.setMnemonic('A');
                btnEditar.setVisible(false);
                break;
            case 4:
                btnNuevo.setText("Agregar  [F2]");
                btnNuevo.setMnemonic('A');
                btnEditar.setVisible(false);
                btnEliminar.setVisible(false);
                break;
            case 6:
                btnNuevo.setText("Agregar  [F2]");
                btnNuevo.setMnemonic('A');
                btnNuevo.setVisible(true);
                btnEditar.setVisible(false);
                btnEliminar.setVisible(false);
                break;
        }
    }

    private void fillTable(List<Producto> productos, boolean selecc) {
        int i = 0;
        Object[][] datos = new Object[productos.size()][4];
        for (Producto producto : productos) {
            datos[i][0] = producto.getProductoClave();
            datos[i][1] = producto.getProductoNombre();
            datos[i][2] = producto.getProductoPrecio();
            datos[i][3] = false;
            i++;
        }

        String titulos[] = (selecc) ? new String[]{"CLAVE", "NOMBRE", "CUOTA MÍNIMA DE RECUPERACIÓN", "SELECC"} : new String[]{"CLAVE", "NOMBRE", "PRECIO"};

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                titulos
        ) {
            Class[] types = (selecc) ? new Class[]{java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class} : new Class[]{java.lang.Object.class, java.lang.Object.class, java.lang.Object.class};
            boolean[] canEdit = new boolean[]{
                false, false, false, selecc
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        //jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        jTable1.getTableHeader().setReorderingAllowed(false);

        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });

        jScrollPane1.setViewportView(jTable1);

        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setMinWidth(150);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(150);
            jTable1.getColumnModel().getColumn(1).setResizable(true);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(400);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setMinWidth(100);
            jTable1.getColumnModel().getColumn(2).setMaxWidth(100);
            if (selecc) {
                jTable1.getColumnModel().getColumn(3).setResizable(false);
                jTable1.getColumnModel().getColumn(3).setMinWidth(100);
                jTable1.getColumnModel().getColumn(3).setMaxWidth(100);
            }
        }

        JTableHeader jtableHeader = jTable1.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        jTable1.setTableHeader(jtableHeader);

        jTable1.setRowHeight(30);

        jTable1.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ORDER_ID.toString()));
        jTable1.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        jTable1.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));

    }

    private void managedButtons() {
        if (jTable1.getSelectedRowCount() > 0) {
            btnEditar.setEnabled(true);
            btnEliminar.setEnabled(true);
        } else {
            btnEditar.setEnabled(false);
            btnEliminar.setEnabled(false);
        }
    }

    private int getSeleccionados() {
        int sel = 0;
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            if (Boolean.valueOf(jTable1.getValueAt(i, 3).toString())) {
                sel++;
            }
        }
        return sel;
    }

    private boolean guardar() {
        txtBuscar.setText("");
        txtBuscarKeyReleased(null);
        List<Paquete> paquetes = new ArrayList<>();
        for (int i = 0; i < jTable1.getRowCount(); i++) {
            if (Boolean.valueOf(jTable1.getValueAt(i, 3).toString())) {
                try {
                    Producto pr = sProducto.getBy("p.productoClave = '" + jTable1.getValueAt(i, 0).toString() + "'");
                    Paquete paq = new Paquete();
                    PaqueteId paqId = new PaqueteId();
                    paqId.setDetallePaqueteClave(this.detallePaquete.getDetallePaqueteClave());
                    paqId.setPaqueteProductoClave(pr.getProductoClave());
                    paq.setId(paqId);

                    paq.setDetallepaquete(this.detallePaquete);
                    paq.setPaqueteFcreacion(new java.util.Date());
                    paq.setPaqueteFmidif(new java.util.Date());
                    paq.setPaqueteStatus("ACT");
                    paq.setProducto(pr);
                    paquetes.add(paq);
                } catch (Exception ex) {
                    LoggerUtil.registrarError(ex);
                }
            }
        }
        try {
            ServicePaquete sdp = new ServicePaqueteImpl();
            if (sdp.add(paquetes)) {
                return true;
            }
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
        return false;
    }

    private void fillTable2(List<Paquete> paquetes, boolean selecc) {
        int i = 0;
        Object[][] datos = new Object[paquetes.size()][4];
        for (Paquete p : paquetes) {
            datos[i][0] = p.getProducto().getProductoClave();
            datos[i][1] = p.getProducto().getProductoNombre();
            datos[i][2] = p.getProducto().getProductoPrecio();
            datos[i][3] = false;
            i++;
        }

        String titulos[] = (selecc) ? new String[]{"CLAVE", "NOMBRE", "PRECIO", "SELECC"} : new String[]{"CLAVE", "NOMBRE", "PRECIO"};

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                titulos
        ) {
            Class[] types = (selecc) ? new Class[]{java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class} : new Class[]{java.lang.Object.class, java.lang.Object.class, java.lang.Object.class};
            boolean[] canEdit = new boolean[]{
                false, false, false, selecc
            };

            @Override
            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        //jTable1.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
        jTable1.getTableHeader().setReorderingAllowed(false);
        
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });

        jScrollPane1.setViewportView(jTable1);

        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setMinWidth(150);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(150);
            jTable1.getColumnModel().getColumn(1).setResizable(true);
            jTable1.getColumnModel().getColumn(1).setPreferredWidth(400);
            jTable1.getColumnModel().getColumn(2).setResizable(false);
            jTable1.getColumnModel().getColumn(2).setMinWidth(100);
            jTable1.getColumnModel().getColumn(2).setMaxWidth(100);
            if (selecc) {
                jTable1.getColumnModel().getColumn(3).setResizable(true);
                jTable1.getColumnModel().getColumn(3).setPreferredWidth(70);
            }
        }

        JTableHeader jtableHeader = jTable1.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        jTable1.setTableHeader(jtableHeader);

        jTable1.setRowHeight(30);

        jTable1.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.ORDER_ID.toString()));
        jTable1.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        jTable1.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));
    }

    /**
     * Desactiva los productos que ya se encuentran en el paquete en cuestion
     *
     * @param paquetes
     */
    private void desactivaRow(List<Paquete> paquetes) {
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();

        for (int i = 0; i < model.getRowCount(); i++) {
            for (Paquete p : paquetes) {
                if (model.getValueAt(i, 0).toString().equals(p.getProducto().getProductoClave())) {
                    model.removeRow(i);
                }
            }
        }
        jTable1.setModel(model);
        repaint();
    }

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 113://F2
                if (btnNuevo.isVisible()) {
                    btnNuevoActionPerformed(null);
                }
                break;
            case 114://F3 
                if (btnEliminar.isVisible()) {
                    if (btnEliminar.isEnabled()) {
                        btnEliminarActionPerformed(null);
                    }
                }
                break;
            case 115://F4
                if (btnEditar.isVisible()) {
                    if (btnEditar.isEnabled()) {
                        btnEditarActionPerformed(null);
                    }
                }
                break;
            case 116://F5 
                btnBuscarActionPerformed(null);
                break;
            case 27://Esc 
                btnCancelarActionPerformed(null);
                break;
        }
    }

    private void buscarProductos(String text) {
        List<RowFilter<Object, Object>> row = null;
        row = new ArrayList();
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>((DefaultTableModel) jTable1.getModel());
        try {
            row.add(RowFilter.regexFilter(text, 0));
            row.add(RowFilter.regexFilter(text, 1));
            row.add(RowFilter.regexFilter(text, 2));
        } catch (java.util.regex.PatternSyntaxException e) {
            JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + e.getMessage());
        }
        jTable1.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.orFilter(row));
    }

    private void loadPermisos(User usuario) {
        Set<String> permisions = new HashSet<>();
        //quitar duplicados
        usuario.getRol().getPermisos().stream().forEach(p -> {
            permisions.add(p.getAccion().getAccionDesc());
        });
        permisions.stream().forEach(permiso -> {
            if ("AGREGAR PRODUCTO".equals(permiso)) {
                btnNuevo.setVisible(true);
            }
            if ("MODIFICAR PRODUCTO".equals(permiso)) {
                btnEditar.setVisible(true);
            }
            if ("ELIMINAR PRODUCTO".equals(permiso)) {
                btnEliminar.setVisible(true);
            }
        });
    }
}
