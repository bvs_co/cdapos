package mx.com.cdadb.view;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableRowSorter;
import mx.com.cdadb.entities.Area;
import mx.com.cdadb.entities.Detallepaquete;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.service.ServiceArea;
import mx.com.cdadb.service.impl.ServiceAreaImpl;
import mx.com.cdadb.util.DialogWait;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;
import mx.com.cdadb.util.PaneUtil;

/**
 *
 * @author unknown
 */
public class PaneAreas extends javax.swing.JPanel {

    private final int option;
    private DialogNuevaArea dialogNuevaArea = null;
    private FrMenu frMenu2 = null;
    private SubProductsServices subProductosServicios = null;
    private Detallepaquete detallePaquete = null;
    public PaneProductos paneProductos = null;

    //Opcion 2: viene del submenu Productos y servicios y mostrará primero la lista de areas para e que el user
    //seleccione de que area quiere ver los productos
    public PaneAreas(FrMenu fr, SubProductsServices sub, int op) {
        initComponents();
        this.frMenu2 = fr;
        this.subProductosServicios = sub;
        this.option = op;
        DialogWait dw = new DialogWait(this.frMenu2, true, this);
        dw.setVisible(true);
    }

    //Op = 4
    public PaneAreas(FrMenu fr, int op, Detallepaquete dt, PaneProductos p) {
        initComponents();
        this.frMenu2 = fr;
        this.option = op;
        this.detallePaquete = dt;
        this.paneProductos = p;
        DialogWait dw = new DialogWait(this.frMenu2, true, this);
        dw.setVisible(true);
        loadPermisions(this.frMenu2.user);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        btnNuevo.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/add_x16.png"))); // NOI18N
        btnNuevo.setMnemonic('N');
        btnNuevo.setText("Nuevo [F2]");
        btnNuevo.setBorder(null);
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        btnNuevo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnNuevoKeyPressed(evt);
            }
        });

        btnEditar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/pen_write_edit_x16.png"))); // NOI18N
        btnEditar.setMnemonic('d');
        btnEditar.setText("Editar [F4]");
        btnEditar.setEnabled(false);
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });
        btnEditar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEditarKeyPressed(evt);
            }
        });

        btnEliminar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/delete_x16.png"))); // NOI18N
        btnEliminar.setMnemonic('E');
        btnEliminar.setText("Eliminar [F3]");
        btnEliminar.setEnabled(false);
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });
        btnEliminar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnEliminarKeyPressed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cancel_x16.png"))); // NOI18N
        btnCancelar.setText("Cancelar [Esc]");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(btnNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        txtBuscar.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jButton1.setToolTipText("Buscar [F5]");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "NOMBRE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setPreferredWidth(100);
            jTable1.getColumnModel().getColumn(1).setMinWidth(300);
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1)))
                .addContainerGap())
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)))
                .addGap(5, 5, 5)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE)
                .addContainerGap())
        );

        getAccessibleContext().setAccessibleDescription("servicios");
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        managedButtons();
    }//GEN-LAST:event_jTable1MouseClicked

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        if (jTable1.getSelectedRowCount() > 0) {
            managedButtons();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            txtBuscar.requestFocus();
        }
    }//GEN-LAST:event_jTable1KeyReleased

    private void txtBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtBuscarKeyPressed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        txtBuscar.setText(txtBuscar.getText().toUpperCase());
        searchProducts(txtBuscar.getText());
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        switch (this.option) {
            case 1:
                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                DialogNuevaArea dialogNewArea = new DialogNuevaArea(this.frMenu2, true, 1, this);
                dialogNewArea.setVisible(true);
                dialogNewArea.setLocationRelativeTo(null);
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                break;
            case 2:
                try {
                if (jTable1.getSelectedRowCount() > 0) {
                    setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                    ServiceArea sArea = new ServiceAreaImpl();
                    Area area = sArea.getBy("a.areaId = " + jTable1.getValueAt(jTable1.getSelectedRow(), 0));
                    PaneProductos paneProducts = new PaneProductos(1, area, this.frMenu2);
                    paneProducts.setSize(this.frMenu2.principalPane.getSize());
                    this.frMenu2.principalPane.removeAll();
                    this.frMenu2.principalPane.add(paneProducts, BorderLayout.CENTER);
                    this.frMenu2.principalPane.revalidate();
                    this.frMenu2.principalPane.repaint();
                    this.subProductosServicios.requesttFocus();
                    setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                } else {
                    JOptionPane.showMessageDialog(null, "Primero debe seleccionar el área a consultar.");
                    txtBuscar.requestFocus();
                }
            } catch (Exception ex) {
                LoggerUtil.registrarError(ex);
            }
            break;
        }
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnNuevoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnNuevoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnNuevoKeyPressed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        try {
            if (jTable1.getSelectedRowCount() > 0) {
                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                ServiceArea sArea = new ServiceAreaImpl();
                Area area = sArea.getBy("a.areaId=" + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString());
                dialogNuevaArea = new DialogNuevaArea(this.frMenu2, true, 2, area, this);
                dialogNuevaArea.setVisible(true);
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            } else {
                JOptionPane.showMessageDialog(this, "Seleccione el registro a eliminar.");
            }
        } catch (Exception ex) {
            Logger.getLogger(DialogNuevaArea.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnEditarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEditarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnEditarKeyPressed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        try {
            if (jTable1.getSelectedRowCount() > 0) {
                int op = JOptionPane.showConfirmDialog(this, "Esta seguro de eliminar el área seleccionada.", "Confirme.", JOptionPane.YES_NO_OPTION);
                if (op == 0) {
                    ServiceArea sArea = new ServiceAreaImpl();
                    Area area = sArea.getBy("a.areaId = " + jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString());
                    area.setAreaStatus("DEL");
                    if (sArea.edit(area)) {
                        JOptionPane.showMessageDialog(this, "Área eliminada, correctamente.");
                        initData();
                        jTable1MouseClicked(null);
                    }
                }
            } else {
                JOptionPane.showMessageDialog(this, "Seleccione el registro a eliminar.");
            }
        } catch (Exception ex) {
            Logger.getLogger(DialogNuevaArea.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnEliminarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnEliminarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnEliminarKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        txtBuscarKeyReleased(null);
        txtBuscar.requestFocus();
        jTable1MouseClicked(null);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        if (this.option == 4) {
            //PaneAreas paneAreas = new PaneAreas(this.frMenu2, 4, this.detallePaquete, this);
            //paneAreas.setSize(this.frMenu2.PrincipalPane.getSize());
            this.frMenu2.principalPane.removeAll();
            this.frMenu2.principalPane.add(this.paneProductos, BorderLayout.CENTER);
            this.frMenu2.principalPane.revalidate();
            this.frMenu2.principalPane.repaint();
        }
    }//GEN-LAST:event_btnCancelarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables

    public void initData() {
        try {
            ServiceArea sArea = new ServiceAreaImpl();
            fillTable(sArea.findBy("a.areaStatus='ACT'"));
            txtBuscar.selectAll();
            txtBuscar.requestFocus();

            btnNuevo.setVisible(true);
            btnEliminar.setVisible(false);
            btnEditar.setVisible(false);
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
    }

    private void fillTable(List<Area> arr) {
        int i = 0;
        Object[][] datos = new Object[arr.size()][2];
        for (Area area : arr) {
            datos[i][0] = area.getAreaId();
            datos[i][1] = area.getAreaNombre();
            i++;
        }

        jTable1 = new javax.swing.JTable();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
                datos,
                new String[]{
                    "ID", "NOMBRE"
                }
        ) {
            boolean[] canEdit = new boolean[]{
                false, false
            };

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        jTable1.getTableHeader().setReorderingAllowed(false);

        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }

            @Override
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
        });

        jScrollPane1.setViewportView(jTable1);

        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setResizable(false);
            jTable1.getColumnModel().getColumn(0).setMinWidth(100);
            jTable1.getColumnModel().getColumn(0).setMaxWidth(100);
            jTable1.getColumnModel().getColumn(1).setMinWidth(300);
        }

        JTableHeader jtableHeader = jTable1.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        jTable1.setTableHeader(jtableHeader);

        jTable1.setRowHeight(30);

        jTable1.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.PK_ID.toString()));
        jTable1.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));

        add_event(jTable1, this);
    }

    public void initGui() {
        switch (this.option) {
            case 1:
                btnCancelar.setVisible(false);
                break;
            case 2:
                btnEditar.setVisible(false);
                btnEliminar.setVisible(false);
                btnNuevo.setText("Ver [F6]");
                btnNuevo.setMnemonic('V');
                btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/eye_arrow_x16.png")));
                btnNuevo.setEnabled(false);
                btnCancelar.setVisible(false);
                break;
            case 3:
                //this.setTitle("Seleccione un servicio.");
                btnCancelar.setVisible(false);
                break;
            case 4://Agregar productos a un paquete
                //this.setTitle("Seleccione un servicio.");
                btnEditar.setVisible(false);
                btnEliminar.setVisible(false);
                btnNuevo.setVisible(false);
                btnCancelar.setVisible(true);
                break;
        }
        txtBuscar.selectAll();
        txtBuscar.requestFocus();
    }

    private void managedButtons() {
        switch (this.option) {
            case 1:
                if (jTable1.getSelectedRowCount() > 0) {
                    btnEditar.setEnabled(true);
                    btnEliminar.setEnabled(true);
                } else {
                    btnEditar.setEnabled(false);
                    btnEliminar.setEnabled(false);
                }
                break;
            case 2:
                if (jTable1.getSelectedRowCount() > 0) {
                    btnNuevo.setEnabled(true);
                } else {
                    btnNuevo.setEnabled(false);
                }
                break;
        }
    }

    private void add_event(JTable b, PaneAreas dlg) {
        b.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    switch (dlg.option) {
                        case 1:
                            break;
                        case 2://Muestra la lista de productos
                            try {
                            setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                            ServiceArea sArea = new ServiceAreaImpl();
                            Area area = sArea.getBy("a.areaId = " + jTable1.getValueAt(jTable1.getSelectedRow(), 0));

                            PaneProductos paneProductos = new PaneProductos(1, area, dlg.frMenu2);
                            paneProductos.setSize(dlg.frMenu2.principalPane.getSize());
                            dlg.frMenu2.principalPane.removeAll();
                            dlg.frMenu2.principalPane.add(paneProductos, BorderLayout.CENTER);
                            dlg.frMenu2.principalPane.revalidate();
                            dlg.frMenu2.principalPane.repaint();
                            dlg.frMenu2.lblAyuda.setText("...");
                            PaneUtil.requesttFocus(dlg.frMenu2.principalPane);
                            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        } catch (Exception ex) {
                            Logger.getLogger(DialogNuevaArea.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                        case 3://Seleccionando productos, para agregar a un paquete
//                            if (frProductos instanceof FrProductos) {
//                                JOptionPane.showMessageDialog(dlg, "Ya se esta ejecutando.");
//                                frProductos.grabFocus();
//                            } else {
//                                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
//                                ServiceArea sArea = new ServiceAreaImpl();
//                                Area area = sArea.obtenerArea("a.areaId = " + jTable1.getValueAt(jTable1.getSelectedRow(), 0));
//                                frProductos = new FrProductos(dlg, 2, area, dlg.detallePaquete);
//                                Frame.renderScreen(dlg.frPaquetes.menu.jDesktopPane1, frProductos);
//                                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
//                            }
                        case 4:
                            try {
                            setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                            ServiceArea sArea2 = new ServiceAreaImpl();
                            Area area2 = sArea2.getBy("a.areaId = " + jTable1.getValueAt(jTable1.getSelectedRow(), 0));
                            PaneProductos paneProductos2 = new PaneProductos(4, dlg.frMenu2, dlg.detallePaquete, area2, dlg);
                            paneProductos2.setSize(dlg.frMenu2.principalPane.getSize());
                            dlg.frMenu2.principalPane.removeAll();
                            dlg.frMenu2.principalPane.add(paneProductos2, BorderLayout.CENTER);
                            dlg.frMenu2.principalPane.revalidate();
                            dlg.frMenu2.principalPane.repaint();
                            dlg.frMenu2.lblAyuda.setText("...");
                            PaneUtil.requesttFocus(dlg.frMenu2.principalPane);
                            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        } catch (Exception ex) {
                            Logger.getLogger(DialogNuevaArea.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }

                }
            }
        });
    }

    //Evento para usar las teclas especiales 
    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 113://F2 Nuevo
                if (btnNuevo.isVisible()) {
                    btnNuevoActionPerformed(null);
                }
                break;
            case 114://F3 
                if (btnEliminar.isVisible()) {
                    if (btnEliminar.isEnabled()) {
                        btnEliminarActionPerformed(null);
                    }
                }
                break;
            case 115://F4
                if (btnEditar.isVisible()) {
                    if (btnEditar.isEnabled()) {
                        btnEditarActionPerformed(null);
                    }
                }
                break;
            case 116://F5 Buscar
                jButton1ActionPerformed(null);
                break;
            case 117://F6 
                //if (btnEditar.isVisible()) {
                if (this.option == 2) {
                    if (jTable1.getSelectedRowCount() > 0) {
                        try {
                            setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                            ServiceArea sArea = new ServiceAreaImpl();
                            Area area = sArea.getBy("a.areaId = " + jTable1.getValueAt(jTable1.getSelectedRow(), 0));
                            PaneProductos productsPane = new PaneProductos(1, area, this.frMenu2);
                            productsPane.setSize(this.frMenu2.principalPane.getSize());
                            this.frMenu2.principalPane.removeAll();
                            this.frMenu2.principalPane.add(productsPane, BorderLayout.CENTER);
                            this.frMenu2.principalPane.revalidate();
                            this.frMenu2.principalPane.repaint();
                            PaneUtil.requesttFocus(this.frMenu2.principalPane);
                            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        } catch (Exception ex) {
                            Logger.getLogger(DialogNuevaArea.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Primero debe seleccionar el área a consultar.");
                        txtBuscar.requestFocus();
                    }
                }
                //}
                break;
        }
    }

    private void searchProducts(String text) {
        List<RowFilter<Object, Object>> row = null;
        row = new ArrayList();
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<>((DefaultTableModel) jTable1.getModel());
        try {
            row.add(RowFilter.regexFilter(text, 0));
            row.add(RowFilter.regexFilter(text, 1));
        } catch (java.util.regex.PatternSyntaxException e) {
            JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + e.getMessage());
        }
        jTable1.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.orFilter(row));
    }

    private void loadPermisions(User usuario) {
        Set<String> permisions = new HashSet<>();
        //quitar duplicados
        usuario.getRol().getPermisos().stream().forEach(p -> {
            permisions.add(p.getAccion().getAccionDesc());
        });

        permisions.stream().forEach(permiso -> {
            if ("AGREGAR SERVICIO".equals(permiso)) {
                btnNuevo.setVisible(true);
            }
            if ("MODIFICAR SERVICIO".equals(permiso)) {
                btnEditar.setVisible(true);
            }
            if ("ELIMINAR SERVICIO".equals(permiso)) {
                btnEliminar.setVisible(true);
            }
        });
    }
}
