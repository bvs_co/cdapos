package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JList;
import javax.swing.JPanel;

/**
 *
 * @author unknown
 */
public class CheckListener implements MouseListener, KeyListener {

    protected PaneRoles paneRoles;
    protected JList jList;

    public CheckListener(PaneRoles parent, JList list) {
        paneRoles = parent;
        jList = list;
    }

    public void mouseClicked(MouseEvent e) {
//        if (e.getX() < 20) {
//            doCheck();
//        }
        doCheck();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == ' ') {
            doCheck();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    protected void doCheck() {
//        int index = jList.getSelectedIndex();
//        if (index < 0) {
//            return;
//        }
//        Menu data = (Menu) jList.getModel().
//                getElementAt(index);
//        if ("Productos & Servicios".equals(data.getName())) {
//            paneRoles.panelSubmenues.removeAll();
//            JPanel panelSubMenus = paneRoles.subMenuPS();
//            paneRoles.panelSubmenues.add(panelSubMenus);
//            paneRoles.panelSubmenues.revalidate();
//            paneRoles.repaint();
//        }
//        if ("Donativos".equals(data.getName())) {
//            paneRoles.panelSubmenues.removeAll();
//            JPanel panelDonativos = paneRoles.subMenuDonativos();
//            paneRoles.panelSubmenues.add(panelDonativos);
//            paneRoles.panelSubmenues.revalidate();
//            paneRoles.repaint();
//        } 
//        if ("Configuración".equals(data.getName())) {
//            paneRoles.panelSubmenues.removeAll();
//            JPanel panelConfig = paneRoles.subMenuConfig();
//            paneRoles.panelSubmenues.add(panelConfig);
//            paneRoles.panelSubmenues.revalidate();
//            paneRoles.repaint();
//        }
//        if ("Reportes".equals(data.getName())) {
//            paneRoles.panelSubmenues.removeAll();
//            JPanel panelReports = paneRoles.subMenuReports();
//            paneRoles.panelSubmenues.add(panelReports);
//            paneRoles.panelSubmenues.revalidate();
//        }
        //data.invertSelected();
        //m_list.repaint();
        //m_parent.recalcTotal();
    }
}
