package mx.com.cdadb.view;

import java.awt.Event;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Date;
import javax.swing.InputMap;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.service.impl.ServiceBaseImpl;
import mx.com.cdadb.util.DialogWait;
import mx.com.cdadb.util.KeyGenerator;
import mx.com.cdadb.util.LoggerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DialogIni extends javax.swing.JDialog {

    private FrMenu frMenu = null;
    private User user = null;
    private PaneCorteUsuario paneCorteUsuario = null;
    private java.util.Date fechaCorte = null;
    private ServiceBaseImpl service = new ServiceBaseImpl();
    private Logger log = LoggerFactory.getLogger(DialogIni.class);

    public DialogIni(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.frMenu = (FrMenu) parent;
        this.setLocationRelativeTo(null);
        this.getRootPane().setDefaultButton(btnIngresar);
    }

    public DialogIni(java.awt.Frame parent, boolean modal, User u, PaneCorteUsuario p, java.util.Date fecha) {
        super(parent, modal);
        initComponents();
        this.frMenu = (FrMenu) parent;
        this.user = u;
        this.paneCorteUsuario = p;
        initGui();
        this.fechaCorte = fecha;
        this.setLocationRelativeTo(null);
        this.getRootPane().setDefaultButton(btnIngresar);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtUser = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtPassw = new javax.swing.JPasswordField();
        btnIngresar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ingresar.");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel1KeyPressed(evt);
            }
        });
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        jLabel2.setText("Usuario:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 20, -1, 12));

        txtUser.setFont(new java.awt.Font("Courier New", 1, 16)); // NOI18N
        txtUser.setBorder(null);
        txtUser.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUserFocusGained(evt);
            }
        });
        txtUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUserKeyPressed(evt);
            }
        });
        jPanel1.add(txtUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(226, 39, 280, 20));

        jLabel3.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        jLabel3.setText("Contraseña:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 80, -1, -1));

        InputMap map2 = txtPassw.getInputMap(txtPassw.WHEN_FOCUSED);
        map2.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
        txtPassw.setFont(new java.awt.Font("Courier New", 1, 16)); // NOI18N
        txtPassw.setBorder(null);
        txtPassw.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPasswFocusGained(evt);
            }
        });
        txtPassw.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPasswKeyPressed(evt);
            }
        });
        jPanel1.add(txtPassw, new org.netbeans.lib.awtextra.AbsoluteConstraints(226, 101, 280, 20));

        btnIngresar.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        btnIngresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/accept_blue_x16.png"))); // NOI18N
        btnIngresar.setText("Ingresar [F5]");
        btnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIngresarActionPerformed(evt);
            }
        });
        btnIngresar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnIngresarKeyPressed(evt);
            }
        });
        jPanel1.add(btnIngresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 140, -1, 30));

        btnCancelar.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cancel_x16.png"))); // NOI18N
        btnCancelar.setText("Cancelar [Esc]");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });
        jPanel1.add(btnCancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(208, 138, -1, 35));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(211, 60, 295, -1));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(211, 122, 295, -1));

        jPanel2.setBackground(new java.awt.Color(153, 153, 255));

        jLabel1.setBackground(new java.awt.Color(153, 153, 153));
        jLabel1.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_6x128.png"))); // NOI18N
        jLabel1.setText("Ver.2.1.0.05.2018");
        jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jLabel1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 192, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 563, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 199, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIngresarActionPerformed
        if (this.user == null) {
            DialogWait dw = new DialogWait(this.frMenu, true, this);
            dw.setVisible(true);
        } else {
            if (valida(this.user, txtPassw.getText())) {
                if (this.user.getRol().getRolId() == 1 || this.user.getRol().getRolId() == 2) {
                    this.dispose();
                    DialogWait dw = new DialogWait(this.frMenu, true, this.paneCorteUsuario, 1, this.fechaCorte);
                    dw.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(null, "No está autorizado para realizar este corte.");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Contraseña incorrecta.");
                txtPassw.selectAll();
                txtPassw.requestFocus();
            }
        }
    }//GEN-LAST:event_btnIngresarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtUserKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUserKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtUserKeyPressed

    private void txtPasswKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPasswKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtPasswKeyPressed

    private void btnIngresarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnIngresarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnIngresarKeyPressed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void jPanel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jPanel1KeyPressed

    private void txtPasswFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPasswFocusGained
        txtPassw.selectAll();
    }//GEN-LAST:event_txtPasswFocusGained

    private void txtUserFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUserFocusGained
        txtUser.selectAll();
    }//GEN-LAST:event_txtUserFocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogIni.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogIni.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogIni.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogIni.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogIni dialog = new DialogIni(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnIngresar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JPasswordField txtPassw;
    private javax.swing.JTextField txtUser;
    // End of variables declaration//GEN-END:variables

    private boolean valida(User user, String passw) {
        return passw == null ? dx(user.getUserPassw()) == null : passw.equals(dx(user.getUserPassw()));
    }

    public static String dx(String cadena) {
        StandardPBEStringEncryptor s = new StandardPBEStringEncryptor();
        s.setPassword(KeyGenerator.PASSWORD_USER_CDA_KEY);
        String devuelve = "";
        try {
            devuelve = s.decrypt(cadena);
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
        return devuelve;
    }

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 116://F11
                btnIngresarActionPerformed(null);
                break;
            case 27://Esc
                this.dispose();
                break;
        }
    }

    public void ingresar() throws IOException {
        try {
            if (!txtUser.getText().isEmpty() && !txtPassw.getText().isEmpty()) {
                setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
                //ServiceUser sUsuario = new ServiceUserImpl();
                //User user = sUsuario.getBy("u.userUsuario = '" + txtUser.getText() + "'");
                User _user = (User)service.getBy(new User(), "t.userUsuario = '" + txtUser.getText() + "'");
                if (_user != null) {
                    if("1".equals(_user.getUserStatus())){
                        if (valida(_user, txtPassw.getText())) {
                            log.info("Ingresando user:{}|{}|{}|{}",_user.getUserUsuario(), _user.getUserStatus(), _user.getRol().getRolNombre(), new Date());
                            this.dispose();
                            JOptionPane.showMessageDialog(null, "Bienvenido " + _user.getUserUsuario() + ".");
                            this.frMenu.init(_user);
                            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        } else {
                            JOptionPane.showMessageDialog(null, "Usuario o contraseña incorrectos.","",JOptionPane.ERROR_MESSAGE);
                            txtPassw.selectAll();
                            setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        }
                    }else{
                        JOptionPane.showMessageDialog(null, "Error de conexión.","Error",JOptionPane.ERROR_MESSAGE);
                        log.info("Error user:{}|{}|{}|{}",_user.getUserUsuario(), _user.getUserStatus(), _user.getRol().getRolNombre(), new Date());
                        txtUser.selectAll();
                        txtUser.requestFocus();
                        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Error de conexión.","Error",JOptionPane.ERROR_MESSAGE);
                    log.info("------- Usuario "+txtUser.getText()+" no encontrado.");
                    txtUser.selectAll();
                    txtUser.requestFocus();
                    setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                }

            } else {
                JOptionPane.showMessageDialog(null, "Todos los campos son obligatorios.","",JOptionPane.WARNING_MESSAGE);
                txtUser.selectAll();
                txtUser.requestFocus();
                setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            }
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
    }

    private void initGui() {
        this.setTitle("Ingrese contraseña.");
        txtUser.setText(this.user.getUserUsuario());
        txtUser.setEnabled(false);
        txtPassw.requestFocus();
    }
}
