package mx.com.cdadb.view;

/**
 *
 * @author unknown
 */
public class Menu {

    protected String m_name;
    protected boolean m_selected;

    public Menu(String name) {
        m_name = name;
        m_selected = false;
    }

    public String getName() {
        return m_name;
    }

    public void setSelected(boolean selected) {
        m_selected = selected;
    }

    public void invertSelected() {
        m_selected = !m_selected;
    }

    public boolean isSelected() {
        return m_selected;
    }

    public String toString() {
        return m_name;
    }
}
