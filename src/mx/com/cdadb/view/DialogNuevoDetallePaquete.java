package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import mx.com.cdadb.entities.Detallepaquete;
import mx.com.cdadb.service.ServiceDetallePaquete;
import mx.com.cdadb.service.impl.ServiceDetallePaqueteImpl;
import mx.com.cdadb.util.TextfieldUtil;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.constants.ConstantMessages;

/**
 *
 * @author unknown
 */
public class DialogNuevoDetallePaquete extends javax.swing.JDialog {

    private int opc;
    private Detallepaquete detallePaquete = null;
    private ServiceDetallePaquete sDetallePaquete = new ServiceDetallePaqueteImpl();
    private PanePaquetes panePaquetes = null;

    public DialogNuevoDetallePaquete(java.awt.Frame parent, boolean modal, int op, PanePaquetes pane) {
        super(parent, modal);
        initComponents();
        this.setTitle("Nuevo paquete.");
        this.setLocationRelativeTo(null);
        this.opc = op;
        this.panePaquetes = pane;
    }

    public DialogNuevoDetallePaquete(java.awt.Frame parent, boolean modal, int op, PanePaquetes pane, Detallepaquete det) {
        super(parent, modal);
        initComponents();
        this.setTitle("Editar paquete.");
        this.setLocationRelativeTo(null);
        this.opc = op;
        this.panePaquetes = pane;
        this.detallePaquete = det;
        llenaDatos();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtFinicio = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        txtFTermina = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripcion = new javax.swing.JTextPane();
        btnGuardar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtClave = new javax.swing.JTextField();
        btnCancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel1.setText("Nombre");

        txtNombre.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtNombre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNombreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNombreFocusLost(evt);
            }
        });
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNombreKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel2.setText("Fecha de inicio");

        try {
            txtFinicio.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtFinicio.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtFinicio.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtFinicioFocusGained(evt);
            }
        });
        txtFinicio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtFinicioKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel3.setText("Fecha en que termina");

        try {
            txtFTermina.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtFTermina.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtFTermina.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtFTerminaFocusGained(evt);
            }
        });
        txtFTermina.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtFTerminaKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel4.setText("Cuota mínima de recuperación");

        txtPrecio.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));
        txtPrecio.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtPrecio.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPrecioFocusGained(evt);
            }
        });
        txtPrecio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPrecioKeyPressed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel5.setText("Descripción");

        txtDescripcion.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtDescripcion.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDescripcionFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDescripcionFocusLost(evt);
            }
        });
        txtDescripcion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDescripcionKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(txtDescripcion);

        btnGuardar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/save_accept_x16.png"))); // NOI18N
        btnGuardar.setMnemonic('G');
        btnGuardar.setText("Guardar [F5]");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        btnGuardar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnGuardarKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel6.setText("Clave");

        txtClave.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtClave.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtClaveFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtClaveFocusLost(evt);
            }
        });
        txtClave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtClaveKeyPressed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cancel_x16.png"))); // NOI18N
        btnCancelar.setText("Cancelar [Esc]");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombre)
                    .addComponent(txtFinicio)
                    .addComponent(txtFTermina)
                    .addComponent(txtPrecio)
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(0, 396, Short.MAX_VALUE))
                    .addComponent(txtClave))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(jLabel6)
                .addGap(5, 5, 5)
                .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtFinicio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtFTermina, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar)
                    .addComponent(btnCancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        switch (this.opc) {
            case 1://Alta
                try {
                List<Detallepaquete> paquetes = sDetallePaquete.findBy("p.detallePaqueteClave = '" + txtClave.getText() + "'");
                if (paquetes.isEmpty()) {
                    if (!txtClave.getText().isEmpty()
                            || !txtNombre.getText().isEmpty()
                            || !"  /  /    ".equals(txtFinicio.getText())
                            || !"  /  /    ".equals(txtFTermina.getText()) || !txtPrecio.getText().isEmpty()) {
                        Detallepaquete a = new Detallepaquete();
                        a.setDetallePaqueteClave(txtClave.getText());
                        a.setDetallePaqueteNombre(txtNombre.getText());
                        a.setDetallePaquetePrecio(Float.valueOf(txtPrecio.getText().replaceAll(",", "")));

                        String f[] = txtFinicio.getText().split("/");
                        String fecha1 = f[2] + "/" + f[1] + "/" + f[0];
                        a.setDetallePaqueteInicia(new java.util.Date(fecha1));

                        String f2[] = txtFTermina.getText().split("/");
                        String fecha2 = f2[2] + "/" + f2[1] + "/" + f2[0];
                        a.setDetallePaqueteTermina(new java.util.Date(fecha2));

                        a.setDetallePaqueteStatus("ACT");
                        a.setDetallePaqueteFcreacion(new java.util.Date());
                        a.setDetallePaqueteFmodif(new java.util.Date());
                        if (sDetallePaquete.add(a)) {
                            JOptionPane.showMessageDialog(this, "Se agregó correctamente el nuevo paquete.");
                            this.panePaquetes.initData();
                            this.dispose();
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Faltan datos importantes.");
                        txtClave.selectAll();
                        txtClave.grabFocus();
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Ya existe una paquete con la misma clave.");
                    txtClave.selectAll();
                    txtClave.grabFocus();
                }
            } catch (Exception ex) {
                if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
            }
            break;
            case 2:
                try {
                List<Detallepaquete> arr = sDetallePaquete.findBy("p.detallePaqueteClave = '" + txtClave.getText() + "'");
                if (arr.isEmpty() || arr.size() == 1) {
                    //this.producto.setProductoClave(txtClave.getText());
                    this.detallePaquete.setDetallePaqueteNombre(txtNombre.getText());
                    this.detallePaquete.setDetallePaquetePrecio(Float.valueOf(txtPrecio.getText().replaceAll(",", "")));

                    String f[] = txtFinicio.getText().split("/");
                    String fecha1 = f[2] + "/" + f[1] + "/" + f[0];
                    this.detallePaquete.setDetallePaqueteInicia(new java.util.Date(fecha1));

                    String f2[] = txtFTermina.getText().split("/");
                    String fecha2 = f2[2] + "/" + f2[1] + "/" + f2[0];
                    this.detallePaquete.setDetallePaqueteTermina(new java.util.Date(fecha2));
                    this.detallePaquete.setDetallePaqueteDesc(txtDescripcion.getText());
                    this.detallePaquete.setDetallePaqueteFmodif(new java.util.Date());
                    if (sDetallePaquete.edit(this.detallePaquete)) {
                        JOptionPane.showMessageDialog(this, "Se modificó correctamente el paquete seleccionado.");
                        this.panePaquetes.initData();
                        this.dispose();
                    }

                } else {
                    JOptionPane.showMessageDialog(this, "Ya existe un paquete con la misma clave.");
                    txtClave.selectAll();
                    txtClave.grabFocus();
                }
            } catch (Exception ex) {
                if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
            }
            break;
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void txtClaveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtClaveKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtClaveKeyPressed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtNombreKeyPressed

    private void txtFinicioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFinicioKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtFinicioKeyPressed

    private void txtFTerminaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFTerminaKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtFTerminaKeyPressed

    private void txtPrecioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtPrecioKeyPressed

    private void btnGuardarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnGuardarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnGuardarKeyPressed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void txtClaveFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtClaveFocusGained
        TextfieldUtil.textBoxFocus(txtClave);
    }//GEN-LAST:event_txtClaveFocusGained

    private void txtNombreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreFocusGained
        TextfieldUtil.textBoxFocus(txtNombre);
    }//GEN-LAST:event_txtNombreFocusGained

    private void txtFinicioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtFinicioFocusGained
        txtFinicio.selectAll();
        txtFinicio.requestFocus();
    }//GEN-LAST:event_txtFinicioFocusGained

    private void txtFTerminaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtFTerminaFocusGained
        txtFTermina.selectAll();
        txtFTermina.requestFocus();
    }//GEN-LAST:event_txtFTerminaFocusGained

    private void txtPrecioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPrecioFocusGained
        TextfieldUtil.textBoxFocus(txtPrecio);
    }//GEN-LAST:event_txtPrecioFocusGained

    private void txtDescripcionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDescripcionFocusGained
        txtDescripcion.selectAll();
        txtDescripcion.requestFocus();
    }//GEN-LAST:event_txtDescripcionFocusGained

    private void txtClaveFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtClaveFocusLost
        TextfieldUtil.textBoxMayus(txtClave);
    }//GEN-LAST:event_txtClaveFocusLost

    private void txtNombreFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreFocusLost
        TextfieldUtil.textBoxMayus(txtNombre);
    }//GEN-LAST:event_txtNombreFocusLost

    private void txtDescripcionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDescripcionFocusLost
        txtDescripcion.setText(txtDescripcion.getText().toUpperCase());
    }//GEN-LAST:event_txtDescripcionFocusLost

    private void txtDescripcionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDescripcionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ESCAPE) {
            txtClave.selectAll();
            txtClave.requestFocus();
        } else {
            checkKeyPressed(evt);
        }
    }//GEN-LAST:event_txtDescripcionKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoDetallePaquete.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoDetallePaquete.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoDetallePaquete.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogNuevoDetallePaquete.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogNuevoDetallePaquete dialog = new DialogNuevoDetallePaquete(new javax.swing.JFrame(), true, 0, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtClave;
    private javax.swing.JTextPane txtDescripcion;
    private javax.swing.JFormattedTextField txtFTermina;
    private javax.swing.JFormattedTextField txtFinicio;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JFormattedTextField txtPrecio;
    // End of variables declaration//GEN-END:variables

    private void llenaDatos() {
        txtClave.setText(this.detallePaquete.getDetallePaqueteClave());
        txtClave.setEditable(false);
        txtDescripcion.setText(this.detallePaquete.getDetallePaqueteDesc());
        txtFTermina.setText(FormatDateUtil.SFD_DDMMYYYY.format(this.detallePaquete.getDetallePaqueteTermina()));
        txtFinicio.setText(FormatDateUtil.SFD_DDMMYYYY.format(this.detallePaquete.getDetallePaqueteInicia()));
        txtNombre.setText(this.detallePaquete.getDetallePaqueteNombre());
        txtPrecio.setText(this.detallePaquete.getDetallePaquetePrecio().toString());
    }

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 116://F5 Guardar
                btnGuardarActionPerformed(null);
                break;
            case 27://Esc
                btnCancelarActionPerformed(null);
                break;
        }
    }
}
