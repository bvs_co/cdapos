package mx.com.cdadb.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import mx.com.cdadb.entities.Area;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.service.ServiceArea;
import mx.com.cdadb.service.impl.ServiceAreaImpl;

public class CorteJRDataSource implements JRDataSource {

    private List<Producto> listaProductos = new ArrayList<Producto>();
    private int indiceProductoActual = -1;
    private ServiceArea sa = new ServiceAreaImpl();
    private List<Area> areas = null;

    public CorteJRDataSource() throws Exception{
        areas = sa.findAll();
    }

    @Override
    public boolean next() throws JRException {
        return ++indiceProductoActual < listaProductos.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {

        Object valor = null;

        if ("productoNombre".equals(jrf.getName())) {
            valor = listaProductos.get(indiceProductoActual).getProductoNombre();
        } else if ("productoPrecio".equals(jrf.getName())) {
            valor = (double) listaProductos.get(indiceProductoActual).getProductoPrecio();
        } else if ("productoClave".equals(jrf.getName())) {
            valor = String.valueOf(listaProductos.get(indiceProductoActual).getProductoClave());
        } else if ("productoCantidad".equals(jrf.getName())) {
            //arr[0] = Num. de devoluciones
            //arr[1] = Num. de ventas
            List<String> arr = Arrays.asList(listaProductos.get(indiceProductoActual).getArea().getAreaStatus().split("\\,"));
            valor = Integer.parseInt(arr.get(1));
        } else if ("productoTotal".equals(jrf.getName())) {
            List<String> arr = Arrays.asList(listaProductos.get(indiceProductoActual).getArea().getAreaStatus().split("\\,"));
            
            double dev = Integer.parseInt(arr.get(0)) * (double) listaProductos.get(indiceProductoActual).getProductoPrecio();
            valor = ((double) listaProductos.get(indiceProductoActual).getProductoPrecio() * Integer.parseInt(arr.get(1)))-dev;
            
        } else if ("productoDev".equals(jrf.getName())) {
            List<String> arr = Arrays.asList(listaProductos.get(indiceProductoActual).getArea().getAreaStatus().split("\\,"));
            valor = arr.get(0);
        } else if ("totalDev".equals(jrf.getName())) {
            List<String> arr = Arrays.asList(listaProductos.get(indiceProductoActual).getArea().getAreaStatus().split("\\,"));
            if (listaProductos.get(indiceProductoActual).getProductoPrecio() < 0) {
                double precio = (double) listaProductos.get(indiceProductoActual).getProductoPrecio() * -1;
                int num = Integer.parseInt(arr.get(1));
                valor = num * precio;
            } else {
                valor = (double)0;
            }
            valor = (double) listaProductos.get(indiceProductoActual).getProductoPrecio() *Integer.parseInt(arr.get(0));
            //valor = Double.valueOf(listaProductos.get(indiceProductoActual).getArea().getAreaId()*listaProductos.get(indiceProductoActual).getArea().getAreaId());
        } else if ("productoAreaId".equals(jrf.getName())) {
            valor = listaProductos.get(indiceProductoActual).getArea().getAreaId();
        } else if ("productoAreaNombre".equals(jrf.getName())) {
            if (indiceProductoActual < listaProductos.size() - 1
                    && !listaProductos.get(indiceProductoActual).getArea().getAreaNombre().equals(listaProductos.get(indiceProductoActual + 1).getArea().getAreaNombre())) {
                if ("PAQUETE".equals(listaProductos.get(indiceProductoActual).getArea().getAreaNombre())) {
                    valor = "PAQUETES";
                } else {
                    valor = listaProductos.get(indiceProductoActual).getArea().getAreaNombre();
                }
            } else if (indiceProductoActual == listaProductos.size() - 1) {
                if ("PAQUETE".equals(listaProductos.get(indiceProductoActual).getArea().getAreaNombre())) {
                    valor = "PAQUETES";
                } else {
                    //Area a = BuscaEnAreas(listaProductos.get(indiceProductoActual).getArea().getAreaId());
                    valor = listaProductos.get(indiceProductoActual).getArea().getAreaNombre();
                }
            } else {
                valor = "";
            }
        }
        return valor;
    }

    public void addProducto(Producto producto) {
        this.listaProductos.add(producto);
    }

    public Area BuscaEnAreas(int id) {
        for (Area area : areas) {
            if (area.getAreaId() == id) {
                return area;
            }
        }
        return null;
    }
}
