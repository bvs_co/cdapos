package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.JTableHeader;
import mx.com.cdadb.entities.Detallepaquete;
import mx.com.cdadb.entities.Paquete;
import mx.com.cdadb.service.ServicePaquete;
import mx.com.cdadb.service.impl.ServicePaqueteImpl;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;
import mx.com.cdadb.util.MonedaUtil;

/**
 *
 * @author unknown
 */
public class DialogProductosPaq extends javax.swing.JDialog {

    private Detallepaquete dtp = null;

    public DialogProductosPaq(java.awt.Frame parent, boolean modal, Detallepaquete dtp) {
        super(parent, modal);
        initComponents();
        this.dtp = dtp;
        this.setTitle("Contenido de " + this.dtp.getDetallePaqueteNombre() + ".");
        this.setLocationRelativeTo(null);
        initData();
        actualizaTotales();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaProductos = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtPrecioPaquete = new javax.swing.JTextField();
        txtAhorro = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Seleccione estudio");
        setIconImage(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_x32.png")).getImage());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel1KeyPressed(evt);
            }
        });

        tablaProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CLAVE", "DESCRIPCIÓN", "C.M.R."
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaProductos.setColumnSelectionAllowed(true);
        tablaProductos.getTableHeader().setReorderingAllowed(false);
        tablaProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaProductosMouseClicked(evt);
            }
        });
        tablaProductos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaProductosKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaProductosKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaProductos);
        tablaProductos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        if (tablaProductos.getColumnModel().getColumnCount() > 0) {
            tablaProductos.getColumnModel().getColumn(0).setMinWidth(150);
            tablaProductos.getColumnModel().getColumn(1).setMinWidth(300);
            tablaProductos.getColumnModel().getColumn(2).setMinWidth(150);
        }
        JTableHeader jtableHeader = tablaProductos.getTableHeader();
        jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
        tablaProductos.setTableHeader(jtableHeader);

        tablaProductos.setRowHeight(30);

        tablaProductos.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tablaProductos.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
        tablaProductos.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));

        jButton1.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/accept_x32.png"))); // NOI18N
        jButton1.setText("Aceptar [F5]");
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.setVerifyInputWhenFocusTarget(false);
        jButton1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Precio de paquete:");

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Ahorro:");

        txtPrecioPaquete.setEditable(false);
        txtPrecioPaquete.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        txtPrecioPaquete.setForeground(new java.awt.Color(0, 0, 255));

        txtAhorro.setEditable(false);
        txtAhorro.setFont(new java.awt.Font("Courier New", 1, 14)); // NOI18N
        txtAhorro.setForeground(new java.awt.Color(0, 0, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 652, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPrecioPaquete)
                            .addComponent(txtAhorro))
                        .addGap(10, 10, 10)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtPrecioPaquete, javax.swing.GroupLayout.PREFERRED_SIZE, 26, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtAhorro, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jButton1))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jButton1KeyPressed

    private void tablaProductosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaProductosKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_tablaProductosKeyPressed

    private void jPanel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jPanel1KeyPressed

    private void tablaProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaProductosMouseClicked

    }//GEN-LAST:event_tablaProductosMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        dispose();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void tablaProductosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaProductosKeyReleased

    }//GEN-LAST:event_tablaProductosKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogProductosPaq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogProductosPaq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogProductosPaq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogProductosPaq.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogProductosPaq dialog = new DialogProductosPaq(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaProductos;
    private javax.swing.JTextField txtAhorro;
    private javax.swing.JTextField txtPrecioPaquete;
    // End of variables declaration//GEN-END:variables

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 116://F5 
                dispose();
                break;
            case 27://Esc
                this.dispose();
                break;
        }
    }

    public void initData() {
        try {
            ServicePaquete sPaquete = new ServicePaqueteImpl();
            List<Paquete> paquetes = sPaquete.findBy("p.detallepaquete.detallePaqueteClave='" + this.dtp.getDetallePaqueteClave() + "'");

            int cont = 0;
            for (Paquete p : paquetes) {
                if (p.getProducto().getProductoClave() != null) {
                    cont++;
                }
            }

            int i = 0;
            Object[][] datos = new Object[cont][3];
            for (Paquete p : paquetes) {
                datos[i][0] = p.getProducto().getProductoClave();
                datos[i][1] = p.getProducto().getProductoNombre();
                datos[i][2] = p.getProducto().getProductoPrecio();
                i++;
            }

            tablaProductos.setModel(new javax.swing.table.DefaultTableModel(
                    datos,
                    new String[]{
                        "CLAVE", "DESCRIPCIÓN", "C.M.R."
                    }
            ) {
                boolean[] canEdit = new boolean[]{
                    false, false, false
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            });

            tablaProductos.setColumnSelectionAllowed(true);

            tablaProductos.getTableHeader().setReorderingAllowed(false);

            jScrollPane1.setViewportView(tablaProductos);

            tablaProductos.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
            if (tablaProductos.getColumnModel().getColumnCount() > 0) {
                tablaProductos.getColumnModel().getColumn(0).setMinWidth(150);
                tablaProductos.getColumnModel().getColumn(1).setMinWidth(300);
                tablaProductos.getColumnModel().getColumn(2).setMinWidth(150);
            }
            JTableHeader jtableHeader = tablaProductos.getTableHeader();
            jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
            tablaProductos.setTableHeader(jtableHeader);

            tablaProductos.setRowHeight(30);

            tablaProductos.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
            tablaProductos.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
            tablaProductos.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);;
        }
    }

    private void actualizaTotales() {
        float total = 0;
        for (int i = 0; i < tablaProductos.getRowCount(); i++) {
            total = total + Float.valueOf(tablaProductos.getValueAt(i, 2).toString());
        }
        txtPrecioPaquete.setText("$ " + MonedaUtil.separaMiles(String.format("%.2f", this.dtp.getDetallePaquetePrecio())));
        txtAhorro.setText("$ " + MonedaUtil.separaMiles(String.format("%.2f", total - this.dtp.getDetallePaquetePrecio())));

    }
}
