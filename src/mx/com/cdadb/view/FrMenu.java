package mx.com.cdadb.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.font.TextAttribute;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Caja;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.util.EncryptCashbox;
import mx.com.cdadb.constants.ConstantFiles;
import mx.com.cdadb.service.impl.ServiceBaseImpl;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author unknown
 */
public class FrMenu extends javax.swing.JFrame {

    private final Color GRAY_COLOR = new Color(240, 240, 240);
    private final Color BLUE_COLOR = new Color(153, 153, 255);
    private final String[] ARR_SUB_PRODUCTS_SERVICES = {"productos", "paquetes", "servicios", "enterprise", "lentes"};
    private final String[] ARR_SUB_DONATIVES = {"donativos", "cortes", "arqueo", "EstadosCajas"};
    private final String[] ARR_SUB_CONFIG = {"roles", "usuarios", "sucursal", "config"};
    private static final String SESSION_INIT = "Iniciar sesión [F10]";
    private static final String ICON_OPEN_X16 = "/mx/com/cdadb/resources/icon/open_x16.png";
    private static final String ICON_CLOSE_X16 = "/mx/com/cdadb/resources/icon/close_x16.png";
    private static final String ICON_OPEN_X24 = "/mx/com/cdadb/resources/icon/open_x24.png";
    private static final String ICON_CLOSE_X24 = "/mx/com/cdadb/resources/icon/close_x24.png";
    public User user = null;
    private int option = -1;
    private final ServiceBaseImpl service = new ServiceBaseImpl();
    private Logger log = LoggerFactory.getLogger(FrMenu.class);

    public FrMenu() {
        initComponents();
        this.setExtendedState(MAXIMIZED_BOTH);
        checkUsers();
        txtFocus.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        contentPane = new javax.swing.JPanel();
        menuPane = new javax.swing.JPanel();
        informationPane = new javax.swing.JPanel();
        lblCaja = new javax.swing.JLabel();
        lblUsuario = new javax.swing.JLabel();
        donativosPane = new javax.swing.JPanel();
        lblDonativos = new javax.swing.JLabel();
        productosServiciosPane = new javax.swing.JPanel();
        lblProductosServicios = new javax.swing.JLabel();
        configuracionPane = new javax.swing.JPanel();
        lblConfiguracion = new javax.swing.JLabel();
        reportesPane = new javax.swing.JPanel();
        lblReportes = new javax.swing.JLabel();
        navigationPane = new javax.swing.JPanel();
        lblNavegador = new javax.swing.JLabel();
        stateBar = new javax.swing.JToolBar();
        lblSession = new javax.swing.JLabel();
        separatorBar = new javax.swing.JToolBar.Separator();
        lblTextSucursal = new javax.swing.JLabel();
        lblSucursal = new javax.swing.JLabel();
        separatorBar1 = new javax.swing.JToolBar.Separator();
        lblTextAyuda = new javax.swing.JLabel();
        lblAyuda = new javax.swing.JLabel();
        txtFocus = new javax.swing.JTextField();
        lblVersion = new javax.swing.JLabel();
        scroll = new javax.swing.JScrollPane();
        principalPane = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("CDA Ver.2.0.0.07.04.2018.");
        setIconImage(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_x32.png")).getImage());

        contentPane.setBackground(new java.awt.Color(255, 255, 255));

        menuPane.setBackground(new java.awt.Color(153, 153, 255));
        menuPane.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblCaja.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblCaja.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/pos_x16.png"))); // NOI18N
        lblCaja.setText(".......");
        lblCaja.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblCaja.setEnabled(false);
        lblCaja.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblCajaMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblCajaMouseEntered(evt);
            }
        });

        lblUsuario.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cajero_x16.png"))); // NOI18N
        lblUsuario.setText(".....");
        lblUsuario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblUsuario.setEnabled(false);
        lblUsuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblUsuarioMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblUsuarioMouseExited(evt);
            }
        });

        javax.swing.GroupLayout informationPaneLayout = new javax.swing.GroupLayout(informationPane);
        informationPane.setLayout(informationPaneLayout);
        informationPaneLayout.setHorizontalGroup(
            informationPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(informationPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCaja, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        informationPaneLayout.setVerticalGroup(
            informationPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(informationPaneLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(informationPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCaja)
                    .addComponent(lblUsuario))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lblCaja.setToolTipText(lblCaja.getText());

        menuPane.add(informationPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 270, -1));

        donativosPane.setBackground(new java.awt.Color(153, 153, 255));

        lblDonativos.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblDonativos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDonativos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/donate_x40.png"))); // NOI18N
        lblDonativos.setText("Donativos");
        lblDonativos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblDonativos.setEnabled(false);
        lblDonativos.setIconTextGap(1);
        lblDonativos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDonativos.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        lblDonativos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblDonativos.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        lblDonativos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblDonativosMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblDonativosMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout donativosPaneLayout = new javax.swing.GroupLayout(donativosPane);
        donativosPane.setLayout(donativosPaneLayout);
        donativosPaneLayout.setHorizontalGroup(
            donativosPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblDonativos, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
        );
        donativosPaneLayout.setVerticalGroup(
            donativosPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblDonativos, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
        );

        menuPane.add(donativosPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 0, 130, 70));

        productosServiciosPane.setBackground(new java.awt.Color(153, 153, 255));

        lblProductosServicios.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblProductosServicios.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblProductosServicios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/laboratory_x40.png"))); // NOI18N
        lblProductosServicios.setText("Productos & Servicios");
        lblProductosServicios.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblProductosServicios.setEnabled(false);
        lblProductosServicios.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        lblProductosServicios.setIconTextGap(1);
        lblProductosServicios.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblProductosServicios.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        lblProductosServicios.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblProductosServicios.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        lblProductosServicios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblProductosServiciosMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblProductosServiciosMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout productosServiciosPaneLayout = new javax.swing.GroupLayout(productosServiciosPane);
        productosServiciosPane.setLayout(productosServiciosPaneLayout);
        productosServiciosPaneLayout.setHorizontalGroup(
            productosServiciosPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(productosServiciosPaneLayout.createSequentialGroup()
                .addComponent(lblProductosServicios, javax.swing.GroupLayout.PREFERRED_SIZE, 152, Short.MAX_VALUE)
                .addContainerGap())
        );
        productosServiciosPaneLayout.setVerticalGroup(
            productosServiciosPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblProductosServicios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        menuPane.add(productosServiciosPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 0, 150, 70));

        configuracionPane.setBackground(new java.awt.Color(153, 153, 255));

        lblConfiguracion.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblConfiguracion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/Config_x40.png"))); // NOI18N
        lblConfiguracion.setText("Configuración");
        lblConfiguracion.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblConfiguracion.setEnabled(false);
        lblConfiguracion.setIconTextGap(1);
        lblConfiguracion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblConfiguracion.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        lblConfiguracion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblConfiguracion.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        lblConfiguracion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblConfiguracionMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblConfiguracionMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout configuracionPaneLayout = new javax.swing.GroupLayout(configuracionPane);
        configuracionPane.setLayout(configuracionPaneLayout);
        configuracionPaneLayout.setHorizontalGroup(
            configuracionPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblConfiguracion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        configuracionPaneLayout.setVerticalGroup(
            configuracionPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblConfiguracion, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
        );

        menuPane.add(configuracionPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 0, 130, 70));

        reportesPane.setBackground(new java.awt.Color(153, 153, 255));

        lblReportes.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblReportes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/reports_x40.png"))); // NOI18N
        lblReportes.setText("Reportes");
        lblReportes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblReportes.setEnabled(false);
        lblReportes.setIconTextGap(1);
        lblReportes.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblReportes.setVerticalAlignment(javax.swing.SwingConstants.CENTER);
        lblReportes.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        lblReportes.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        lblReportes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblReportesMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblReportesMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout reportesPaneLayout = new javax.swing.GroupLayout(reportesPane);
        reportesPane.setLayout(reportesPaneLayout);
        reportesPaneLayout.setHorizontalGroup(
            reportesPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblReportes, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        reportesPaneLayout.setVerticalGroup(
            reportesPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblReportes, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
        );

        menuPane.add(reportesPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 0, 120, 70));

        navigationPane.setBackground(new java.awt.Color(153, 153, 255));

        lblNavegador.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblNavegador.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNavegador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_x32.png"))); // NOI18N
        lblNavegador.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblNavegador.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblNavegadorMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblNavegadorMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout navigationPaneLayout = new javax.swing.GroupLayout(navigationPane);
        navigationPane.setLayout(navigationPaneLayout);
        navigationPaneLayout.setHorizontalGroup(
            navigationPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblNavegador, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 270, Short.MAX_VALUE)
        );
        navigationPaneLayout.setVerticalGroup(
            navigationPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblNavegador, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        menuPane.add(navigationPane, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 270, -1));

        stateBar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        stateBar.setFloatable(false);
        stateBar.setRollover(true);

        lblSession.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblSession.setForeground(new java.awt.Color(0, 0, 255));
        lblSession.setIcon(new javax.swing.ImageIcon(getClass().getResource(ICON_OPEN_X16)));
        lblSession.setText("Iniciar sesión [F10]");
        lblSession.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblSession.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblSessionMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblSessionMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblSessionMouseEntered(evt);
            }
        });
        stateBar.add(lblSession);
        stateBar.add(separatorBar);

        lblTextSucursal.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblTextSucursal.setText("Sucursal:");
        stateBar.add(lblTextSucursal);

        lblSucursal.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblSucursal.setForeground(new java.awt.Color(255, 0, 0));
        lblSucursal.setText("....                                                                                                    ");
        stateBar.add(lblSucursal);
        stateBar.add(separatorBar1);

        lblTextAyuda.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblTextAyuda.setText("Ayuda:");
        stateBar.add(lblTextAyuda);

        lblAyuda.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        lblAyuda.setForeground(new java.awt.Color(255, 0, 0));
        lblAyuda.setText("...");
        stateBar.add(lblAyuda);

        txtFocus.setEditable(false);
        txtFocus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtFocusKeyPressed(evt);
            }
        });
        stateBar.add(txtFocus);

        lblVersion.setFont(new java.awt.Font("Courier New", 1, 10)); // NOI18N
        lblVersion.setText("Ver_Comp_2.11052020");
        stateBar.add(lblVersion);

        scroll.setBorder(null);

        principalPane.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout principalPaneLayout = new javax.swing.GroupLayout(principalPane);
        principalPane.setLayout(principalPaneLayout);
        principalPaneLayout.setHorizontalGroup(
            principalPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1224, Short.MAX_VALUE)
        );
        principalPaneLayout.setVerticalGroup(
            principalPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 605, Short.MAX_VALUE)
        );

        scroll.setViewportView(principalPane);

        javax.swing.GroupLayout contentPaneLayout = new javax.swing.GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(menuPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(stateBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(scroll)
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(contentPaneLayout.createSequentialGroup()
                .addComponent(menuPane, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scroll, javax.swing.GroupLayout.DEFAULT_SIZE, 594, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stateBar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(contentPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(contentPane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblProductosServiciosMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblProductosServiciosMouseEntered
        if (lblProductosServicios.isEnabled()) {
            if (principalPane.getComponentCount() > 0 && isInstaceOf(principalPane.getComponent(0), ARR_SUB_PRODUCTS_SERVICES) && lblProductosServicios.isEnabled()) {
                productosServiciosPane.setBackground(new Color(255, 255, 255));
            } else {
                productosServiciosPane.setBackground(GRAY_COLOR);
            }
            lblProductosServicios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/laboratory_x48.png")));
        }
    }//GEN-LAST:event_lblProductosServiciosMouseEntered

    private void lblProductosServiciosMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblProductosServiciosMouseExited
        if (lblProductosServicios.isEnabled()) {
            if (principalPane.getComponentCount() > 0 && isInstaceOf(principalPane.getComponent(0), ARR_SUB_PRODUCTS_SERVICES)) {
                productosServiciosPane.setBackground(new Color(240, 240, 240));
            } else {
                productosServiciosPane.setBackground(BLUE_COLOR);
            }
            lblProductosServicios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/laboratory_x40.png")));
        }
    }//GEN-LAST:event_lblProductosServiciosMouseExited

    private void lblDonativosMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblDonativosMouseEntered
        if (lblDonativos.isEnabled()) {
            if (principalPane.getComponentCount() > 0 && isInstaceOf(principalPane.getComponent(0), ARR_SUB_DONATIVES)) {
                donativosPane.setBackground(new Color(255, 255, 255));
            } else {
                donativosPane.setBackground(GRAY_COLOR);
            }
            lblDonativos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/donate_x48.png")));
        }
    }//GEN-LAST:event_lblDonativosMouseEntered

    private void lblDonativosMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblDonativosMouseExited
        if (lblDonativos.isEnabled()) {
            if (principalPane.getComponentCount() > 0 && isInstaceOf(principalPane.getComponent(0), ARR_SUB_DONATIVES)) {
                donativosPane.setBackground(new Color(240, 240, 240));
            } else {
                donativosPane.setBackground(BLUE_COLOR);
            }
            lblDonativos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/donate_x40.png")));
        }
    }//GEN-LAST:event_lblDonativosMouseExited

    private void lblCajaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCajaMouseEntered
        if (lblCaja.isEnabled()) {
            underline(lblCaja, 1);
            lblCaja.setToolTipText(lblCaja.getText());
        }
    }//GEN-LAST:event_lblCajaMouseEntered

    private void lblCajaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCajaMouseExited
        if (lblCaja.isEnabled()) {
            underline(lblCaja, 2);
        }
    }//GEN-LAST:event_lblCajaMouseExited

    private void lblUsuarioMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblUsuarioMouseEntered
        if (lblUsuario.isEnabled()) {
            lblUsuario.setToolTipText(lblUsuario.getText());
            underline(lblUsuario, 1);
        }
    }//GEN-LAST:event_lblUsuarioMouseEntered

    private void lblNavegadorMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblNavegadorMouseEntered
        underline(lblNavegador, 1);
    }//GEN-LAST:event_lblNavegadorMouseEntered

    private void lblUsuarioMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblUsuarioMouseExited
        if (lblUsuario.isEnabled()) {
            underline(lblUsuario, 2);
        }
    }//GEN-LAST:event_lblUsuarioMouseExited

    private void lblNavegadorMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblNavegadorMouseExited
        underline(lblNavegador, 2);
    }//GEN-LAST:event_lblNavegadorMouseExited

    private void lblConfiguracionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblConfiguracionMouseEntered
        if (lblConfiguracion.isEnabled()) {
            if (principalPane.getComponentCount() > 0 && isInstaceOf(principalPane.getComponent(0), ARR_SUB_CONFIG)) {
                configuracionPane.setBackground(new Color(255, 255, 255));
            } else {
                configuracionPane.setBackground(GRAY_COLOR);
            }
            lblConfiguracion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/Config_x48.png")));
        }
    }//GEN-LAST:event_lblConfiguracionMouseEntered

    private void lblReportesMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblReportesMouseEntered
        if (lblReportes.isEnabled()) {
            if (principalPane.getComponentCount() > 0 && isInstaceOf(principalPane.getComponent(0), ARR_SUB_CONFIG)) {
                reportesPane.setBackground(new Color(255, 255, 255));
            } else {
                reportesPane.setBackground(GRAY_COLOR);
            }
            lblReportes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/reports_x48.png")));
        }
    }//GEN-LAST:event_lblReportesMouseEntered

    private void lblConfiguracionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblConfiguracionMouseExited
        if (lblConfiguracion.isEnabled()) {
            if (principalPane.getComponentCount() > 0 && isInstaceOf(principalPane.getComponent(0), ARR_SUB_CONFIG)) {
                configuracionPane.setBackground(new Color(240, 240, 240));
            } else {
                configuracionPane.setBackground(BLUE_COLOR);
            }
            lblConfiguracion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/Config_x40.png")));
        }
    }//GEN-LAST:event_lblConfiguracionMouseExited

    private void lblReportesMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblReportesMouseExited
        if (lblReportes.isEnabled()) {
            if (principalPane.getComponentCount() > 0 && isInstaceOf(principalPane.getComponent(0), ARR_SUB_CONFIG)) {
                reportesPane.setBackground(new Color(240, 240, 240));
            } else {
                reportesPane.setBackground(BLUE_COLOR);
            }
            lblReportes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/reports_x40.png")));
        }
    }//GEN-LAST:event_lblReportesMouseExited

    private void lblSessionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSessionMouseClicked
        if (lblSession.getText().equals(SESSION_INIT)) {
            DialogIni di = new DialogIni(this, true);
            di.setLocationRelativeTo(this);
            di.setVisible(true);
        } else if (lblSession.getText().equals("Cerrar sesión [F10]")) {
            int opc = JOptionPane.showConfirmDialog(null, "Está seguro que desea cerrar sesión?", "Confirme.", JOptionPane.YES_NO_OPTION);
            closeSession(opc);
        }
    }//GEN-LAST:event_lblSessionMouseClicked

    private void lblSessionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSessionMouseEntered
        if (lblSession.getText().equals(SESSION_INIT)) {
            iconAnimation(lblSession, 1, new javax.swing.ImageIcon(getClass().getResource(ICON_OPEN_X16)), new javax.swing.ImageIcon(getClass().getResource(ICON_OPEN_X24)));
        } else {
            iconAnimation(lblSession, 1, new javax.swing.ImageIcon(getClass().getResource(ICON_CLOSE_X16)), new javax.swing.ImageIcon(getClass().getResource(ICON_CLOSE_X24)));
        }
    }//GEN-LAST:event_lblSessionMouseEntered

    private void lblSessionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSessionMouseExited
        if (lblSession.getText().equals(SESSION_INIT)) {
            iconAnimation(lblSession, 2, new javax.swing.ImageIcon(getClass().getResource(ICON_OPEN_X16)), new javax.swing.ImageIcon(getClass().getResource(ICON_OPEN_X24)));
        } else {
            iconAnimation(lblSession, 2, new javax.swing.ImageIcon(getClass().getResource(ICON_CLOSE_X16)), new javax.swing.ImageIcon(getClass().getResource(ICON_CLOSE_X24)));
        }
    }//GEN-LAST:event_lblSessionMouseExited

    private void txtFocusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFocusKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtFocusKeyPressed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel configuracionPane;
    private javax.swing.JPanel contentPane;
    private javax.swing.JPanel donativosPane;
    private javax.swing.JPanel informationPane;
    public javax.swing.JLabel lblAyuda;
    private javax.swing.JLabel lblCaja;
    private javax.swing.JLabel lblConfiguracion;
    private javax.swing.JLabel lblDonativos;
    public javax.swing.JLabel lblNavegador;
    private javax.swing.JLabel lblProductosServicios;
    private javax.swing.JLabel lblReportes;
    private javax.swing.JLabel lblSession;
    public javax.swing.JLabel lblSucursal;
    private javax.swing.JLabel lblTextAyuda;
    private javax.swing.JLabel lblTextSucursal;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JLabel lblVersion;
    private javax.swing.JPanel menuPane;
    private javax.swing.JPanel navigationPane;
    public javax.swing.JPanel principalPane;
    private javax.swing.JPanel productosServiciosPane;
    private javax.swing.JPanel reportesPane;
    private javax.swing.JScrollPane scroll;
    private javax.swing.JToolBar.Separator separatorBar;
    private javax.swing.JToolBar.Separator separatorBar1;
    private javax.swing.JToolBar stateBar;
    private javax.swing.JTextField txtFocus;
    // End of variables declaration//GEN-END:variables

    /**
     * 
     * @param label
     * @param op 
     */
    private void underline(JLabel label, int op) {
        switch (op) {
            case 1:
                Font fuente = label.getFont();
                Font fuenteNueva = new Font(fuente.getName(), Font.BOLD, fuente.getSize());
                Map atributos = new java.util.HashMap<>();
                atributos.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                label.setFont(fuenteNueva.deriveFont(atributos));
                break;
            case 2:
                label.setFont(new java.awt.Font("Consolas", 1, 11));
                break;
        }
    }

    /**
     * Muestra menú Productos&Servicios, en caso de que se tenga permitido
     */
    public void paintMenuProductsServices() {
        if (principalPane.getComponentCount() > 0 && principalPane.getComponent(0) instanceof SubProductsServices) {

        } else {
            SubProductsServices subPS = new SubProductsServices(this);
            subPS.setLayout(new FlowLayout());
            subPS.setSize(principalPane.getWidth(), principalPane.getHeight());
            principalPane.removeAll();
            principalPane.add(subPS, BorderLayout.CENTER);
            setLblNavigation();
            principalPane.revalidate();
            principalPane.repaint();
            lblAyuda.setText(" ...");
            productosServiciosPane.setBackground(Color.WHITE);
            donativosPane.setBackground(BLUE_COLOR);
            configuracionPane.setBackground(BLUE_COLOR);
            reportesPane.setBackground(BLUE_COLOR);
        }
    }

    /**
     * Busca el nombre del submenu seleccionado en la lista de submenues,
     * asignados al menú
     *
     * @param component Panel submenu a buscar
     * @param arr Lista de nombres de submenu
     * @return true en caso de encontrarlo
     */
    private boolean isInstaceOf(Component component, String arr[]) {
        JPanel panel = (JPanel) component;
        for (String arr1 : arr) {
            if (arr1.equals(panel.getAccessibleContext().getAccessibleDescription())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Maneja el icono de la barra de navegación
     */
    private void setLblNavigation() {
        lblNavegador.setIcon(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_x32.png")));
        lblNavegador.setText("");
    }

    /**
     * Muestra menú configuración, en caso de que se tenga permitido
     */
    private void paintMenuCofiguracion() {
        if (principalPane.getComponentCount() > 0 && principalPane.getComponent(0) instanceof SubConfiguracion) {

        } else {
            SubConfiguracion subConf = new SubConfiguracion(this);
            subConf.setLayout(new FlowLayout());
            subConf.setSize(principalPane.getWidth(), principalPane.getHeight());
            principalPane.removeAll();
            principalPane.add(subConf, BorderLayout.CENTER);
            setLblNavigation();
            principalPane.revalidate();
            principalPane.repaint();
            lblAyuda.setText(" ...");
            productosServiciosPane.setBackground(BLUE_COLOR);
            donativosPane.setBackground(BLUE_COLOR);
            configuracionPane.setBackground(Color.WHITE);
            reportesPane.setBackground(BLUE_COLOR);
        }
    }

    /**
     * Cierra la session del usuario
     *
     * @param opc Opción de donde viene la petición
     */
    private void closeSession(int opc) {
        try {
            App app = (App) service.getBy(new App(), "t.appNombre = 'app.ver'");
            if (opc == 0) {
                log.info("Saliendo de session.");
                this.setDefaultCloseOperation(EXIT_ON_CLOSE);
                this.setTitle(app.getAppValor());
                lblProductosServicios.setEnabled(false);
                lblConfiguracion.setEnabled(false);
                lblDonativos.setEnabled(false);
                lblReportes.setEnabled(false);
                lblUsuario.setText("......");
                lblUsuario.setEnabled(false);
                lblCaja.setText("......");
                lblCaja.setEnabled(false);
                lblSession.setText(SESSION_INIT);
                lblSession.setIcon(new javax.swing.ImageIcon(getClass().getResource(ICON_OPEN_X16)));
                this.user = null;
                this.option = 0;
                principalPane.removeAll();
                setLblNavigation();
                lblSucursal.setText("......");
                this.repaint();
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
        }
    }

    /**
     * Maneja la animación de los menús
     *
     * @param label Label donde se mostrará la imagen
     * @param op Opción que permite poner o quitar la animación
     * @param icon24 Icono a 24px
     * @param icon32 Icono a 32px
     */
    private void iconAnimation(JLabel label, int op, Icon icon24, Icon icon32) {
        switch (op) {
            case 1:
                Font fuente = label.getFont();
                Font fuenteNueva = new Font(fuente.getName(), Font.BOLD, fuente.getSize());
                Map atributos = new java.util.HashMap<>();
                atributos.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                label.setFont(fuenteNueva.deriveFont(atributos));
                label.setIcon(icon32);
                break;
            case 2:
                label.setFont(new java.awt.Font("Consolas", 1, 12));
                label.setIcon(icon24);
                break;
        }
    }

    /**
     * Inicializa menú principal, una vez que el usuario ha iniciado session
     *
     * @param user Usuario
     * @throws IOException Excepción generada cuando no se encuentra archivo de
     * configuración de la caja
     */
    public void init(User user) throws IOException {
        try {
            log.info("Iniciando menu principal.");
            EncryptCashbox encryptCashbox = new EncryptCashbox();
            Caja cashbox = encryptCashbox.decrypt(ConstantFiles.POS_PROPERTIES, this, user);
            if (cashbox.getCajaId() > 0) {
                lblCaja.setText(cashbox.getCajaNombre());
                lblCaja.setEnabled(true);

                this.setDefaultCloseOperation(0);
                seTtitle();
                setBranchInformation();
                this.option = 1;
                this.user = user;

                lblUsuario.setText(this.user.getUserUsuario() + " - [" + this.user.getRol().getRolNombre() + "]");
                lblUsuario.setEnabled(true);
                loadPermissions(this.user);

                lblSession.setText("Cerrar sesión [F10]");
                lblSession.setIcon(new javax.swing.ImageIcon(getClass().getResource(ICON_CLOSE_X16)));
            }
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
    }

    /**
     * Muestra menú donativos, en caso de que se tenga permitido
     */
    private void paintDonativesMenu() {
        if (principalPane.getComponentCount() > 0 && principalPane.getComponent(0) instanceof SubDonations) {

        } else {
            SubDonations subPS = new SubDonations(this);
            subPS.setLayout(new FlowLayout());
            subPS.setSize(principalPane.getWidth(), principalPane.getHeight());
            principalPane.removeAll();
            principalPane.add(subPS, BorderLayout.CENTER);
            setLblNavigation();
            principalPane.revalidate();
            principalPane.repaint();
            lblAyuda.setText(" ...");
            productosServiciosPane.setBackground(BLUE_COLOR);
            donativosPane.setBackground(Color.WHITE);
            configuracionPane.setBackground(BLUE_COLOR);
            reportesPane.setBackground(BLUE_COLOR);
        }
    }

    /**
     * Maneja el titulo de la ventana de la aplicación
     */
    public void seTtitle() {
        try {
            App appName = (App) service.getBy(new App(), "t.appNombre='" + ConstantConfig.CONFIG_APP_NAME + "'");
            App appVersion = (App) service.getBy(new App(), "t.appNombre='" + ConstantConfig.CONFIG_APP_VERSION + "'");
            java.util.Date now = new java.util.Date();
            if (appName != null && appVersion != null) {
                this.setTitle(appName.getAppValor() + " " + appVersion.getAppValor() + " - [" + FormatDateUtil.SDF_EXTENDED_DATE.format(now) + "].");
            } else {
                this.setTitle("CDA " + " - [" + FormatDateUtil.SDF_EXTENDED_DATE.format(now) + "].");
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
        }
    }

    /**
     * Setea la información de la sucursal en la barra de titulo de la ventana
     */
    private void setBranchInformation() {
        try {
            App app = (App) service.getBy(new App(), "t.appNombre='" + ConstantConfig.CONFIG_BRANCH_ID + "'");
            if (app.getAppValor() != null) {
                Sucursal branch = (Sucursal) service.getBy(new Sucursal(), "t.sucursalId = " + app.getAppValor());
                lblSucursal.setText(branch.getSucursalNombre());
            }
        } catch (Exception ex) {
            Log.error(ex.getMessage());
        }
    }

    /**
     * Verifica si hay usuarios creados y activos
     */
    private void checkUsers() {
        Log.info("Checkusers.");
        try {
            List<User> arr = service.findBy(new User(), "t.userStatus = '1'");
            if (arr.size() <= 0) {
                JOptionPane.showMessageDialog(null, "No existe ningún usuario creado y activo.");
                NuevoUser nuevoUser = new NuevoUser(this, true, 2, this);
                nuevoUser.setVisible(true);
            }
        } catch (Exception e) {
            log.error("No existe ningún usuario creado y activo.");
            System.exit(0);
        }
    }

    /**
     * Carga los permisos del usuario, activando los menús que se tienen
     * disponibles para el usuario
     *
     * @param usuario Usuario
     */
    private void loadPermissions(User usuario) {
        Set<String> permisions = new HashSet<>();
        //quitar duplicados
        usuario.getRol().getPermisos().stream().forEach(p -> {
            permisions.add(p.getAccion().getModulo().getModuloMenu());
        });
        log.info("Cargando permisos de usuario:{}", permisions.toString());
        permisions.stream().forEach(p -> {
            if ("PRODUCTOS & SERVICIOS".equals(p)) {
                lblProductosServicios.setEnabled(true);
                lblProductosServicios.addMouseListener(new java.awt.event.MouseAdapter() {
                    @Override
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        if (option == 1 && lblProductosServicios.isEnabled()) {
                            paintMenuProductsServices();
                        }
                    }
                });
            }
            if ("CONFIGURACION".equals(p)) {
                lblConfiguracion.setEnabled(true);
                lblConfiguracion.addMouseListener(new java.awt.event.MouseAdapter() {
                    @Override
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        if (option == 1 && lblConfiguracion.isEnabled()) {
                            paintMenuCofiguracion();
                        }
                    }
                });
            }
            if ("DONATIVOS".equals(p)) {
                lblDonativos.setEnabled(true);
                lblDonativos.addMouseListener(new java.awt.event.MouseAdapter() {
                    @Override
                    public void mouseClicked(java.awt.event.MouseEvent evt) {
                        if (option == 1 && lblDonativos.isEnabled()) {
                            paintDonativesMenu();
                        }
                    }
                });
            }
        });
    }

    /**
     * Evento para las teclas de función
     *
     * @param evt
     */
    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            /*case 113://F2
                break;
            case 114://F3
                break;
            case 115://F4
                break;
            case 116://F5 
                break;
            case 117://F6 
                break;
            case 118://F7
                break;
            case 119://F8
                break;
            case 120://F9
                break;*/
            case 121://F10
                lblSessionMouseClicked(null);
                break;
            /*case 122://F11
                break;
            case 127://Suprimir
                break;*/
        }
    }
}
