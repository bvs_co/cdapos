package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableRowSorter;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Area;
import mx.com.cdadb.entities.Clave;
import mx.com.cdadb.entities.Detalledevolucion;
import mx.com.cdadb.entities.Devolucion;
import mx.com.cdadb.entities.Folio;
import mx.com.cdadb.entities.Foliodevolucion;
import mx.com.cdadb.entities.Orden;
import mx.com.cdadb.entities.Ordenlab;
import mx.com.cdadb.entities.Paquete;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.entities.Tr;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.entities.customer.Cliente;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceClave;
import mx.com.cdadb.service.ServiceFolio;
import mx.com.cdadb.service.ServiceFolioDevolucion;
import mx.com.cdadb.service.ServiceOrden;
import mx.com.cdadb.service.ServicePaquete;
import mx.com.cdadb.service.ServiceProducto;
import mx.com.cdadb.service.ServiceSucursalLocal;
import mx.com.cdadb.service.ServiceTr;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.service.impl.ServiceClaveImpl;
import mx.com.cdadb.service.impl.ServiceFolioDevolucionImpl;
import mx.com.cdadb.service.impl.ServiceFolioImpl;
import mx.com.cdadb.service.impl.ServiceOrdenImpl;
import mx.com.cdadb.service.impl.ServicePaqueteImpl;
import mx.com.cdadb.service.impl.ServiceProductoImpl;
import mx.com.cdadb.service.impl.ServiceSucursalLocalImp;
import mx.com.cdadb.service.impl.ServiceTrImpl;
import mx.com.cdadb.service.customer.impl.ServiceClienteImpl;
import mx.com.cdadb.service.customer.ServiceCliente;
import mx.com.cdadb.util.CalculateAgeUtil;
import mx.com.cdadb.util.DialogWait;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.GeneralGetters;
import mx.com.cdadb.util.HeaderCellRenderer;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.util.NumberToLetterConverter;
import mx.com.cdadb.util.PrintReportImpl;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.enums.CellRendererEnum;

/**
 *
 * @author unknown
 */
public class FrReprint extends javax.swing.JDialog {

    private PanePos panePos = null;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    public FrReprint(java.awt.Frame parent, boolean modal, PanePos pane) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.panePos = pane;
        DialogWait dw = new DialogWait(this.panePos.frMenu, true, this, 0);
        dw.setVisible(true);
        txtBuscar.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txtBuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        btnImprimir = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Reimpresión de ventas.");
        setIconImage(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_x32.png")).getImage());
        setResizable(false);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel2KeyPressed(evt);
            }
        });

        txtBuscar.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtBuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarKeyReleased(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "FOLIO", "CLIENTE", "IMPORTE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setMinWidth(100);
            jTable1.getColumnModel().getColumn(1).setMinWidth(350);
            jTable1.getColumnModel().getColumn(2).setMinWidth(100);
        }

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/search_x16.png"))); // NOI18N
        jLabel1.setText("Buscar:");

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        btnCancelar.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cancel_x32.png"))); // NOI18N
        btnCancelar.setText("Cancelar [Esc]");
        btnCancelar.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        btnCancelar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnCancelarKeyPressed(evt);
            }
        });

        btnImprimir.setFont(new java.awt.Font("Consolas", 1, 11)); // NOI18N
        btnImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/print_x32.png"))); // NOI18N
        btnImprimir.setText("Imprimir [F5]");
        btnImprimir.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirActionPerformed(evt);
            }
        });
        btnImprimir.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnImprimirKeyPressed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jButton1.setText("Detalle [F6]");
        jButton1.setEnabled(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnImprimir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar)
                .addContainerGap(405, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnImprimir)
                            .addComponent(btnCancelar))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 839, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtBuscar)))
                .addContainerGap())
            .addComponent(jSeparator1)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jTable1KeyPressed

    private void txtBuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtBuscarKeyPressed

    private void btnCancelarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnCancelarKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnCancelarKeyPressed

    private void btnImprimirKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnImprimirKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_btnImprimirKeyPressed

    private void jPanel2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel2KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jPanel2KeyPressed

    private void btnImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirActionPerformed
        DialogWait dw = new DialogWait(this.panePos.frMenu, true, this, 1);
        dw.setVisible(true);
    }//GEN-LAST:event_btnImprimirActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtBuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarKeyReleased
        setCursor(new java.awt.Cursor(java.awt.Cursor.WAIT_CURSOR));
        txtBuscar.setText(txtBuscar.getText().toUpperCase());
        buscarVentas(txtBuscar.getText());
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_txtBuscarKeyReleased

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        if (evt.getClickCount() == 2) {
            DialogWait dw = new DialogWait(this.panePos.frMenu, true, this, 1);
            dw.setVisible(true);
        }
    }//GEN-LAST:event_jTable1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrReprint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrReprint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrReprint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrReprint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrReprint dialog = new FrReprint(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnImprimir;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField txtBuscar;
    // End of variables declaration//GEN-END:variables

    public void initData() {
        try {
            int i = 0;
            Date current = new Date();
            ServiceFolio sFolio = new ServiceFolioImpl();
            mx.com.cdadb.service.ServiceCliente sCliente = new mx.com.cdadb.service.impl.ServiceClienteImpl();
            List<Folio> folios = sFolio.findBy("f.id.folioFecha = '" + sdf.format(current) + "'");

            List<Folio> foliosAux = new ArrayList<Folio>();
            for (Folio folio : folios) {
                if (folio.getOrdens().size() > 0) {
                    foliosAux.add(folio);
                }
            }

            Object[][] datos = new Object[foliosAux.size()][4];
            for (Folio folio : foliosAux) {
                datos[i][0] = String.format("%08d", folio.getId().getFolioValor());
                datos[i][1] = FormatDateUtil.SFD_DDMMYYYY_HHMMSS_GUION.format(folio.getFolioFcreacion());
                String cliente = "";
                float total = 0;
                for (Object o : folio.getOrdens()) {
                    Orden orden = (Orden) o;
                    mx.com.cdadb.entities.Cliente clienteObj = sCliente.getBy("p.clienteId = " + orden.getOrdenPacienteId());
                    total = (orden.getProducto() != null) ? total + orden.getProducto().getProductoPrecio() : total + orden.getDetallepaquete().getDetallePaquetePrecio();
                    cliente = clienteObj.getClienteNombre() + " " + clienteObj.getClientePaterno() + " " + clienteObj.getClienteMaterno();
                }
                datos[i][2] = cliente;
                datos[i][3] = total;
                total = 0;
                i++;
            }

            jTable1.setModel(new javax.swing.table.DefaultTableModel(
                    datos,
                    new String[]{
                        "FOLIO", "FEHA", "CLIENTE", "IMPORTE"
                    }
            ) {
                boolean[] canEdit = new boolean[]{
                    false, false, false, false
                };

                public boolean isCellEditable(int rowIndex, int columnIndex) {
                    return canEdit[columnIndex];
                }
            });

            jTable1.getTableHeader().setReorderingAllowed(false);

            jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
                public void keyPressed(java.awt.event.KeyEvent evt) {
                    jTable1KeyPressed(evt);
                }
            });

            jScrollPane1.setViewportView(jTable1);

            if (jTable1.getColumnModel().getColumnCount() > 0) {
                jTable1.getColumnModel().getColumn(0).setMinWidth(90);
                jTable1.getColumnModel().getColumn(1).setMinWidth(120);
                jTable1.getColumnModel().getColumn(2).setMinWidth(300);
                jTable1.getColumnModel().getColumn(3).setMinWidth(100);
            }
            JTableHeader jtableHeader = jTable1.getTableHeader();
            jtableHeader.setDefaultRenderer(new HeaderCellRenderer());
            jTable1.setTableHeader(jtableHeader);

            jTable1.setRowHeight(30);

            jTable1.getColumnModel().getColumn(0).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.FOLIO_ID.toString()));
            jTable1.getColumnModel().getColumn(1).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_CENTER.toString()));
            jTable1.getColumnModel().getColumn(2).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.TEXT_LEFT.toString()));
            jTable1.getColumnModel().getColumn(3).setCellRenderer(new mx.com.cdadb.util.CellRendererUtil(CellRendererEnum.COIN_ID.toString()));
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }

    }

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 116://F5 Imprimir
                btnImprimirActionPerformed(null);
                break;
            case 27://Esc
                this.dispose();
                break;
        }
    }

    private void reimprimir() {
        try {
            ServiceOrden sOrden = new ServiceOrdenImpl();
            ServiceCliente sCliente = new ServiceClienteImpl();
            Ordenlab ordenLab = null;
            long idPaciente = 0;
            long idTr = 0;
            long folioValor = Long.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(), 0).toString());
            long folioId = 0;
            float total = 0;
            String hoy = FormatDateUtil.SFD_YYYYMMDD_S.format(new java.util.Date());
            User usr = null;

            List<Orden> ordenes = sOrden.findBy("o.folio.id.folioValor = " + folioValor + "and o.folio.id.folioFecha = '" + hoy + "'");
            if (ordenes.size() > 0) {
                for (Orden ordene : ordenes) {
                    idTr = ordene.getTr().getId().getTrValor();
                    folioId = ordene.getFolio().getId().getFolioValor();
                    ordenLab = ordene.getOrdenlab();
                    idPaciente = ordene.getOrdenPacienteId();
                    usr = ordene.getUser();
                    total = (ordene.getProducto() != null) ? total + ordene.getProducto().getProductoPrecio() : total + ordene.getDetallepaquete().getDetallePaquetePrecio();
                }
                Cliente clte = sCliente.obtenerCliente("p.clienteId = " + idPaciente);
                rePrintTicket(idTr, usr, hoy);
                separaProductosPorArea(ordenLab, clte, folioId, usr, hoy);
                Sucursal sucursal = GeneralGetters.getSucursal();
                PrintReportImpl pr = new PrintReportImpl();
                pr.printSimplificado(clte, sucursal, folioValor, String.format("%.2f", total), NumberToLetterConverter.convertNumberToLetter(String.format("%.2f", total).replaceAll(",", "")), "");
                //Busca en las devoluciones, para saber si la venta tiene asociada alguna devolución.
                List<Foliodevolucion> f = BuscaFoliosDevueltos(ordenes);
                if (f.size() > 0) {
                    String motivo = "";
                    int op = JOptionPane.showConfirmDialog(this, "Esta venta tiene devolución(es), ¿Desea reimprimir(las)?", "Devolución asociada.", JOptionPane.YES_NO_OPTION);
                    if (op == 0) {
                        for (Foliodevolucion foliodevolucion : f) {
                            ClienteJRDataSource datasource = new ClienteJRDataSource();
                            for (Object d : foliodevolucion.getDevolucions()) {
                                Devolucion dev = (Devolucion) d;
                                total = 0;
                                motivo = dev.getDevolucionDesc();
                                for (Object dd : dev.getDetalledevolucions()) {
                                    Detalledevolucion detDev = (Detalledevolucion) dd;
                                    folioId = detDev.getDevolucion().getFoliodevolucion().getId().getFolioDevolucionValor();
                                    if (detDev.getOrden().getProducto() != null) {
                                        detDev.getOrden().getProducto().setProductoClave("1");
                                        datasource.addProducto(detDev.getOrden().getProducto());
                                        total = total + detDev.getOrden().getProducto().getProductoPrecio();
                                    } else if (detDev.getOrden().getDetallepaquete() != null) {
                                        Producto p = new Producto();
                                        p.setProductoClave("1");
                                        p.setProductoNombre(detDev.getOrden().getDetallepaquete().getDetallePaqueteNombre());
                                        datasource.addProducto(p);
                                        total = total + detDev.getOrden().getDetallepaquete().getDetallePaquetePrecio();
                                    }
                                }
                            }
                            PrintReportImpl p = new PrintReportImpl();
                            usr = getUserDevolucion(foliodevolucion);
                            clte.setClienteNombre(clte.getClienteNombre() + " " + clte.getClientePaterno() + " " + clte.getClienteMaterno());
                            p.printDevolution(clte, folioId, String.valueOf(total), "REIMPRESIÓN", motivo, datasource, usr);
                        }
                    }
                }

                JOptionPane.showMessageDialog(this, "Reimpresión correcta.");
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(this, "Este folio no tiene ninguna venta asociada.");
                txtBuscar.grabFocus();
            }
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }

    private void separaProductosPorArea(Ordenlab ordenLab, Cliente cl, long folioId, User usr, String fecha) throws Exception {
        List<Area> areas = new ArrayList<Area>();
        List<List<Producto>> prodEnAreas = new ArrayList<List<Producto>>();
        List<Map> mapProductos = new ArrayList<Map>();
        ServicePaquete sPaquete = new ServicePaqueteImpl();

        ServiceClave sClave = new ServiceClaveImpl();
        ServiceProducto sProducto = new ServiceProductoImpl();

        ServiceFolio sFolio = new ServiceFolioImpl();
        ServiceApp sApp = new ServiceAppImpl();
        List<Producto> productos = new ArrayList<Producto>();
        ServiceSucursalLocal sSucursal = new ServiceSucursalLocalImp();
        App app = sApp.getBy("a.appNombre ='" + ConstantConfig.CONFIG_BRANCH_ID + "'");
        App app2 = sApp.getBy("a.appNombre='" + ConstantConfig.CONFIG_LABORATORY_AREA + "'");
        Sucursal sucursal = sSucursal.getBy("s.sucursalId = " + app.getAppValor());
        Folio folio = sFolio.getBy("f.id.folioValor = " + folioId + " and f.id.folioFecha = '" + fecha + "'");

        for (Object o : folio.getOrdens()) {
            Orden orden = (Orden) o;
            if (orden.getDetallepaquete() != null) {
                List<Paquete> pqts = sPaquete.findBy("p.detallepaquete.detallePaqueteClave = '" + orden.getDetallepaquete().getDetallePaqueteClave() + "'");
                for (Paquete paquete : pqts) {
                    productos.add(sProducto.getBy("p.productoClave = '" + paquete.getProducto().getProductoClave() + "'"));
                }
            }
            if (orden.getProducto() != null) {
                productos.add(orden.getProducto());
            }
        }

        //Obtiene las areas
        for (Producto producto : productos) {
            if (!isArea(producto.getArea(), areas)) {
                areas.add(producto.getArea());
            }
        }

        for (Area area : areas) {
            List<Producto> produtoArea = new ArrayList<Producto>();
            for (Producto pr : productos) {
                if (area.getAreaId().equals(pr.getArea().getAreaId())) {
                    Map map = new HashMap();
                    map.put("PRODUCTO_ID", pr.getProductoClave());
                    map.put("PRODUCTO_AREA_ID", pr.getArea().getAreaId());
                    map.put("PRODUCTO_CLAVE", pr.getProductoClave());
                    map.put("PRODUCTO_NOMBRE", pr.getProductoNombre());
                    map.put("PRODUCTO_PRECIO", pr.getProductoPrecio());
                    map.put("PRODUCTO_DOCTOR_ID", "");
                    map.put("PRODUCTO_STATUS", "1");
                    if (pr.getArea().getAreaId() == Integer.parseInt(app2.getAppValor())) {
                        Clave clave = sClave.getBy("c.id.claveProductoClave = '" + pr.getProductoClave() + "'");
                        map.put("PRODUCTO_ENTERPRISE", clave.getEnterprise().getEnterpriseClave());
                    } else {
                        map.put("PRODUCTO_ENTERPRISE", "");
                    }
                    mapProductos.add(map);
                    produtoArea.add(pr);
                }
            }
            prodEnAreas.add(produtoArea);
        }

        for (List<Producto> pEnArea : prodEnAreas) {
            for (Producto p : pEnArea) {
                int i = 0;
                for (List<Producto> pEnArea2 : prodEnAreas) {
                    for (Producto p2 : pEnArea2) {
                        if (p.getProductoClave().equals(p2.getProductoClave())) {
                            i++;
                        }
                    }
                }
                p.setProductoStatus(String.valueOf(i));
            }
        }
        //Quitar repetidos
        List<Map> mapProductosFinal = new ArrayList<Map>();

        HashSet hashset = new HashSet();
        hashset.addAll(mapProductos);
        mapProductosFinal.addAll(hashset);

        List<List<Map>> productosPorArea = new ArrayList<List<Map>>();

        for (Area area : areas) {
            List<Map> maps = new ArrayList<Map>();
            for (Map mp : mapProductosFinal) {
                if (area.getAreaId().equals(mp.get("PRODUCTO_AREA_ID"))) {
                    mp.put("PRODUCTO_AREA_NOMBRE", area.getAreaNombre());
                    maps.add(mp);
                }
            }
            productosPorArea.add(maps);
        }
        int i = 0;
        for (List<Map> ppa : productosPorArea) {
            if (ppa.size() > 0) {
                reprintPapeletas(ppa, areas.size(), i + 1, ordenLab, cl, sucursal, usr);
                i++;
            }
        }
    }

    private void rePrintTicket(long idTr, User u, String fecha) {
        try {
            long idCliente = 0;
            java.util.Date date = new java.util.Date();
            ServiceTr sTr = new ServiceTrImpl();
            ServiceApp sApp = new ServiceAppImpl();
            ServiceSucursalLocal sSucursal = new ServiceSucursalLocalImp();
            ServiceCliente sCliente = new ServiceClienteImpl();
            Tr tr = sTr.getBy("t.id.trValor = " + idTr + " and t.id.trFecha = '" + fecha + "'");
            App app = sApp.getBy("a.appNombre ='" + ConstantConfig.CONFIG_BRANCH_ID + "'");
            Sucursal sucursal = sSucursal.getBy("s.sucursalId = " + app.getAppValor());
            ClienteJRDataSource datasource = new ClienteJRDataSource();
            //Preparando lista de productos
            float total = 0;
            for (Object o : tr.getOrdens()) {
                Orden orden = (Orden) o;
                if (orden.getDetallepaquete() != null) {
                    Producto pr = new Producto();
                    pr.setProductoClave("1");
                    pr.setProductoNombre(orden.getDetallepaquete().getDetallePaqueteNombre() + " [" + orden.getDetallepaquete().getDetallePaqueteDesc() + "]");
                    total = total + orden.getDetallepaquete().getDetallePaquetePrecio();
                    datasource.addProducto(pr);
                }
                if (orden.getProducto() != null) {
                    Producto pr = new Producto();
                    pr.setProductoClave("1");
                    pr.setProductoNombre(orden.getProducto().getProductoNombre());
                    total = total + orden.getProducto().getProductoPrecio();
                    datasource.addProducto(pr);
                }
                idCliente = orden.getOrdenPacienteId();
            }
            Cliente cliente = sCliente.obtenerCliente("p.clienteId=" + idCliente);
            Map param = new HashMap();
            param.put("CLIENTE", "[" + cliente.getClienteId() + "] " + cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
            param.put("USUARIO", u.getUserNombre());
            param.put("F_NAC", FormatDateUtil.SFD_DDMMYYYY.format(cliente.getClienteFnac()));
            param.put("EDAD", String.valueOf(CalculateAgeUtil.calculate(FormatDateUtil.SFD_DMY.format(cliente.getClienteFnac()))));
            param.put("FECHA", FormatDateUtil.SFD_DMY.format(date));
            param.put("HORA", FormatDateUtil.SFD_KKMMSS.format(date));
            param.put("SUCURSAL", sucursal.getSucursalNombre());
            param.put("TOTAL", String.valueOf(total));
            param.put("TRANSCC", String.valueOf(tr.getId().getTrValor()));
            param.put("MENSAJE", "");
            param.put("REIMPRESION", "REIMPRESIÓN");
            PrintReportImpl pr = new PrintReportImpl();
            pr.imprimeTiketCliente(param, datasource);
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
    }

    private boolean reprintPapeletas(List<Map> datos, int areasTotal, int areaActual, Ordenlab ordenLab, Cliente cliente, Sucursal sucursal, User usr) {
        ComandaJRDataSource datasource = new ComandaJRDataSource();
        ServiceApp sApp = new ServiceAppImpl();
        java.util.Date date = new java.util.Date();
        String area = null;

        for (Map p : datos) {
            try {
                area = p.get("PRODUCTO_AREA_NOMBRE").toString();
                App app = sApp.getBy("a.appNombre='" + ConstantConfig.CONFIG_LABORATORY_AREA + "'");
                if (p.get("PRODUCTO_AREA_ID").toString().equals(app.getAppValor())) {
                    Producto pr = new Producto();
                    //Si es un producto de laboratorio, agregar a la papeleta el codigo del producto en enterprise
                    //arr.add(p.get("PRODUCTO_STATUS") + "           " + "[" + p.get("PRODUCTO_ENTERPRISE") + "]  " + p.get("PRODUCTO_NOMBRE").toString());
                    pr.setProductoClave(String.valueOf(p.get("PRODUCTO_STATUS")));
                    pr.setProductoNombre("[" + p.get("PRODUCTO_ENTERPRISE").toString() + "]  " + p.get("PRODUCTO_NOMBRE").toString());
                    datasource.addProducto(pr);
                } else {
                    Producto pr = new Producto();
                    //arr.add(p.get("PRODUCTO_STATUS") + "           " + p.get("PRODUCTO_NOMBRE").toString());
                    pr.setProductoClave(p.get("PRODUCTO_STATUS").toString());
                    pr.setProductoNombre(p.get("PRODUCTO_NOMBRE").toString());
                    datasource.addProducto(pr);
                }
            } catch (Exception ex) {
                if (ex != null) {
                    JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
            }
        }

        if (datos.size() > 0) {
            Map param = new HashMap();
            param.put("AREA", area);
            param.put("CLIENTE", "[" + cliente.getClienteId() + "] " + cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
            param.put("USUARIO", usr.getUserUsuario());
            param.put("EDAD", String.valueOf(CalculateAgeUtil.calculate(FormatDateUtil.SFD_DMY.format(cliente.getClienteFnac()))));
            param.put("PROGESS", areaActual + " / " + areasTotal);
            param.put("FECHA", FormatDateUtil.SFD_DMY.format(date));
            param.put("HORA", FormatDateUtil.SFD_KKMMSS.format(date));
            param.put("REIMPRESION", "REIMPRESIÓN");
            if (ordenLab != null) {
                param.put("FOLIO_ORDER", String.valueOf(ordenLab.getId().getOrdenlabContDiario()));
            } else {
                param.put("FOLIO_ORDER", "");
            }
            param.put("F_NAC", FormatDateUtil.SFD_DMY.format(cliente.getClienteFnac()));
            String genero = (cliente.getClienteGenero()) ? "MASCULINO" : "FEMENINO";
            param.put("GENERO", genero);
            param.put("SUCURSAL", sucursal.getSucursalNombre());
            PrintReportImpl pr = new PrintReportImpl();
            pr.papeletaComanda(param, datasource);
        }
        return true;
    }

    private boolean isArea(Area area, List<Area> areas) {
        for (Area a : areas) {
            if (Objects.equals(a.getAreaId(), area.getAreaId())) {
                return true;
            }
        }
        return false;
    }

    private List<Foliodevolucion> BuscaFoliosDevueltos(List<Orden> ordenes) {
        List<Foliodevolucion> folios = new ArrayList<Foliodevolucion>();
        try {
            ServiceFolioDevolucion sFolioDev = new ServiceFolioDevolucionImpl();
            //REVISAR: Solo buscar los folios del dia
            List<Foliodevolucion> foliosDevs = sFolioDev.findAll();

            for (Foliodevolucion f : foliosDevs) {
                for (Object dev : f.getDevolucions()) {
                    Devolucion devolucion = (Devolucion) dev;
                    for (Object dd : devolucion.getDetalledevolucions()) {
                        Detalledevolucion d = (Detalledevolucion) dd;
                        for (Object oo : ordenes) {
                            Orden o = (Orden) oo;
                            if (Objects.equals(d.getOrden().getOrdenId(), o.getOrdenId())) {
                                folios.add(f);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        }
        return folios;
    }

    public void iniReimpresion() {
        if (jTable1.getSelectedRowCount() > 0) {
            reimprimir();
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar el registro que desea reimprimir.");
        }
    }

    private User getUserDevolucion(Foliodevolucion f) {
        User usr = null;
        for (Object dev : f.getDevolucions()) {
            Devolucion d = (Devolucion) dev;
            usr = d.getUser();
        }
        return usr;
    }

    private void buscarVentas(String text) {
        List<RowFilter<Object, Object>> row = null;
        row = new ArrayList();
        TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>((DefaultTableModel) jTable1.getModel());
        try {
            row.add(RowFilter.regexFilter(text, 0));
            row.add(RowFilter.regexFilter(text, 1));
            row.add(RowFilter.regexFilter(text, 2));
        } catch (java.util.regex.PatternSyntaxException e) {
            JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + e.getMessage());
        }
        jTable1.setRowSorter(sorter);
        sorter.setRowFilter(RowFilter.orFilter(row));
    }
}
