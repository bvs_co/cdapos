package mx.com.cdadb.view;

import java.util.ArrayList;
import java.util.List;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import mx.com.cdadb.entities.Producto;

public class ClienteJRDataSource implements JRDataSource {

    private List<Producto> listaProductos = new ArrayList<Producto>();
    private int indiceProdcutoActual = -1;

    @Override
    public boolean next() throws JRException {
        return ++indiceProdcutoActual < listaProductos.size();
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {

        Object valor = null;

        if ("cantidad".equals(jrf.getName())) {
            valor = listaProductos.get(indiceProdcutoActual).getProductoClave();
        } else if ("producto".equals(jrf.getName())) {
            valor = listaProductos.get(indiceProdcutoActual).getProductoNombre();
        }
        return valor;
    }
    
    public void addProducto(Producto pr) {
        this.listaProductos.add(pr);
    }
}
