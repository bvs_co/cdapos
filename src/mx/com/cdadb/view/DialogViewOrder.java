package mx.com.cdadb.view;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Clave;
import mx.com.cdadb.entities.Orden;
import mx.com.cdadb.entities.Ordenlab;
import mx.com.cdadb.entities.Paquete;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.entities.customer.Cliente;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceClave;
import mx.com.cdadb.service.ServiceOrdenlab;
import mx.com.cdadb.service.ServiceSucursalLocal;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.service.impl.ServiceClaveImpl;
import mx.com.cdadb.service.impl.ServiceSucursalLocalImp;
import mx.com.cdadb.service.impl.ServiceordenLabImpl;
import mx.com.cdadb.service.customer.impl.ServiceClienteImpl;
import mx.com.cdadb.service.customer.ServiceCliente;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.util.SendFile;
import mx.com.cdadb.constants.ConstantMessages;

/**
 *
 * @author unknown
 */
public class DialogViewOrder extends javax.swing.JDialog {

    private int opc;
    private Ordenlab ordenLab = null;
    private String datosSucursal = "";
    private List<Clave> claves = new ArrayList<Clave>();
    private DialogOrdenesLab dialogOrdenesLab = null;

    public DialogViewOrder(java.awt.Frame parent, boolean modal, int opc, Ordenlab or, DialogOrdenesLab fr) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.dialogOrdenesLab = fr;
        this.opc = opc;
        this.ordenLab = or;
        initData();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        txtPaterno = new javax.swing.JTextField();
        txtMaterno = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        comboGenero = new javax.swing.JComboBox<>();
        txtFNacimiento = new com.toedter.calendar.JDateChooser();
        txtFEmision = new javax.swing.JTextField();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listOrden = new javax.swing.JList<>();
        jButton1 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        lblOrden = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Edición de orden de laboratorio.");
        setIconImage(new ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/cda_x32.png")).getImage());
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel1KeyPressed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Nombre:");

        jLabel2.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("A. Paterno:");

        jLabel3.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("A. Materno:");

        txtNombre.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtNombre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNombreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNombreFocusLost(evt);
            }
        });
        txtNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNombreKeyPressed(evt);
            }
        });

        txtPaterno.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtPaterno.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPaternoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPaternoFocusLost(evt);
            }
        });
        txtPaterno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPaternoKeyPressed(evt);
            }
        });

        txtMaterno.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtMaterno.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtMaternoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtMaternoFocusLost(evt);
            }
        });
        txtMaterno.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtMaternoKeyPressed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Género:");

        jLabel5.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("F. nacimiento:");

        jLabel6.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("F. de emisión:");

        comboGenero.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        comboGenero.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione...", "Masculino", "Femenino" }));
        comboGenero.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                comboGeneroKeyPressed(evt);
            }
        });

        txtFNacimiento.setBackground(new java.awt.Color(255, 255, 255));
        txtFNacimiento.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtFNacimiento.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtFNacimientoFocusGained(evt);
            }
        });
        txtFNacimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtFNacimientoKeyPressed(evt);
            }
        });

        txtFEmision.setEditable(false);
        txtFEmision.setFont(new java.awt.Font("Consolas", 1, 14)); // NOI18N
        txtFEmision.setFocusable(false);
        txtFEmision.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtFEmisionFocusGained(evt);
            }
        });
        txtFEmision.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtFEmisionKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel6)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboGenero, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFNacimiento, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                    .addComponent(txtFEmision))
                .addContainerGap())
            .addComponent(jSeparator2)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(comboGenero, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(txtPaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5))
                    .addComponent(txtFNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtMaterno, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(txtFEmision, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        jLabel7.setFont(new java.awt.Font("Consolas", 1, 17)); // NOI18N
        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/laboratory_x16.png"))); // NOI18N
        jLabel7.setText("Estudios de la orden.");

        listOrden.setFont(new java.awt.Font("Consolas", 1, 12)); // NOI18N
        listOrden.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                listOrdenKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(listOrden);

        jButton1.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/accept_blue_x16.png"))); // NOI18N
        jButton1.setText("Guardar y Reenviar [F5]");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jButton1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton1KeyPressed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Consolas", 1, 17)); // NOI18N
        jLabel8.setText("# Orden:");

        lblOrden.setFont(new java.awt.Font("Consolas", 1, 17)); // NOI18N
        lblOrden.setForeground(new java.awt.Color(255, 0, 51));
        lblOrden.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOrden.setText("...");

        jLabel9.setFont(new java.awt.Font("Consolas", 1, 17)); // NOI18N
        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/icon/people_x16.png"))); // NOI18N
        jLabel9.setText("Datos del paciente.");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblOrden, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(lblOrden)
                    .addComponent(jLabel9))
                .addGap(3, 3, 3)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                .addGap(8, 8, 8)
                .addComponent(jButton1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jPanel1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jPanel1KeyPressed

    private void txtNombreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtNombreKeyPressed

    private void txtPaternoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaternoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtPaternoKeyPressed

    private void txtMaternoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMaternoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtMaternoKeyPressed

    private void comboGeneroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboGeneroKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_comboGeneroKeyPressed

    private void txtFEmisionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFEmisionKeyReleased
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtFEmisionKeyReleased

    private void txtFNacimientoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFNacimientoKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_txtFNacimientoKeyPressed

    private void listOrdenKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_listOrdenKeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_listOrdenKeyPressed

    private void jButton1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton1KeyPressed
        checkKeyPressed(evt);
    }//GEN-LAST:event_jButton1KeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        guardar();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtNombreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreFocusGained
        focus(txtNombre);
    }//GEN-LAST:event_txtNombreFocusGained

    private void txtPaternoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPaternoFocusGained
        focus(txtPaterno);
    }//GEN-LAST:event_txtPaternoFocusGained

    private void txtNombreFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreFocusLost
        Mayus(txtNombre);
    }//GEN-LAST:event_txtNombreFocusLost

    private void txtPaternoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPaternoFocusLost
        Mayus(txtPaterno);
    }//GEN-LAST:event_txtPaternoFocusLost

    private void txtMaternoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMaternoFocusGained
        focus(txtMaterno);
    }//GEN-LAST:event_txtMaternoFocusGained

    private void txtMaternoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtMaternoFocusLost
        Mayus(txtMaterno);
    }//GEN-LAST:event_txtMaternoFocusLost

    private void txtFNacimientoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtFNacimientoFocusGained
        txtFNacimiento.requestFocus();
    }//GEN-LAST:event_txtFNacimientoFocusGained

    private void txtFEmisionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtFEmisionFocusGained
        focus(txtFEmision);
    }//GEN-LAST:event_txtFEmisionFocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogViewOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogViewOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogViewOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogViewOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogViewOrder dialog = new DialogViewOrder(new javax.swing.JFrame(), true, 0, null, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> comboGenero;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblOrden;
    private javax.swing.JList<String> listOrden;
    private javax.swing.JTextField txtFEmision;
    private com.toedter.calendar.JDateChooser txtFNacimiento;
    private javax.swing.JTextField txtMaterno;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPaterno;
    // End of variables declaration//GEN-END:variables

    private void checkKeyPressed(KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case 113://F2
                break;
            case 114://F3
                break;
            case 115://F4
                break;
            case 116://F5
                jButton1ActionPerformed(null);
                break;
            case 117://F6
                break;
            case 118://F7
                break;
            case 119://F8
                break;
            case 120://F9
                break;
            case 121://F10
                break;
            case 27://Esc
                this.dispose();
                break;
        }
    }

    private void initData() {
        ServiceCliente sCliente = new ServiceClienteImpl();
        Cliente cliente = sCliente.obtenerCliente("p.clienteId = " + this.ordenLab.getOrdenLabPacienteId());
        String genero = (cliente.getClienteGenero()) ? "Masculino" : "Femenino";
        txtNombre.setText(cliente.getClienteNombre());
        txtPaterno.setText(cliente.getClientePaterno());
        txtMaterno.setText(cliente.getClienteMaterno());
        txtFNacimiento.setDate(cliente.getClienteFnac());
        txtFEmision.setText(FormatDateUtil.SFD_DDMMYYYY_HHMMSS.format(this.ordenLab.getOrdenlabFcreacion()));
        comboGenero.setSelectedItem(genero);
        lblOrden.setText(String.format("%05d", this.ordenLab.getId().getOrdenlabContDiario()));
        listOrden.setModel(getDesc(this.ordenLab));
    }

    private void focus(JTextField txt) {
        txt.selectAll();
        txt.grabFocus();
    }

    public static void Mayus(javax.swing.JTextField c) {
        String toUpperCase = c.getText().toUpperCase();
        c.setText(toUpperCase);
    }

    /**
     * Regresa los productos que fueron enviados al laboratorio en la orden
     *
     * @param ordenLab
     * @return
     */
    private ListModel<String> getDesc(Ordenlab ordenLab) {
        try {
            ServiceClave sclave = new ServiceClaveImpl();
            ServiceApp sApp = new ServiceAppImpl();
            App app = sApp.getBy("a.appNombre ='"+ConstantConfig.CONFIG_BRANCH_ID+"'");

            ServiceSucursalLocal sSucursal = new ServiceSucursalLocalImp();
            Sucursal sucursal = sSucursal.getBy("s.sucursalId = " + app.getAppValor());
            DefaultListModel modelo = new DefaultListModel();

            String prs = "";
            for (Orden or : ordenLab.getOrdens()) {
                if (or.getProducto() != null) {
                    if (prs.isEmpty()) {
                        prs = "'" + or.getProducto().getProductoClave();
                    } else {
                        prs = prs + "','" + or.getProducto().getProductoClave();
                    }
                } else if (or.getDetallepaquete() != null) {
                    for (Paquete paquete : or.getDetallepaquete().getPaquetes()) {
                        if (prs.isEmpty()) {
                            prs = "'" + paquete.getProducto().getProductoClave();
                        } else {
                            prs = prs + "','" + paquete.getProducto().getProductoClave();
                        }
                    }
                }
            }
            prs = prs + "'";

            String[] elementos = ordenLab.getOrdenlabDesc().split("\n");
            String hd = "L|" + String.valueOf(sucursal.getSucursalEnterpriseId()) + "|N";
            int j = 0;
            for (String elemento : elementos) {
                if (!hd.equals(elemento) && j >= 2 && j < elementos.length - 1) {
                    List<String> arr = Arrays.asList(elemento.split("\\|"));
                    for (int i = 0; i < arr.size(); i++) {
                        Clave clave = sclave.getBy("c.enterprise.enterpriseClave = '" + arr.get(4).replace("^", "") + "' and c.producto.productoClave in(" + prs + ")");
                        modelo.addElement((clave != null) ? "[" + clave.getProducto().getProductoClave() + "] " + clave.getProducto().getProductoNombre() + " [" + clave.getEnterprise().getEnterpriseClave() + "]" : "[SIN CLAVE ENTERPRISE]");
                        claves.add(clave);
                        break;
                    }
                }
                j++;
            }
            datosSucursal = elementos[0].toString();
            return modelo;
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Error: " + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex, this.dialogOrdenesLab.frMenu.user);
        }
        return null;
    }

    private void guardar() {
        try {
            ServiceCliente sCliente = new ServiceClienteImpl();
            mx.com.cdadb.service.ServiceCliente slCliente = new mx.com.cdadb.service.impl.ServiceClienteImpl();
            ServiceOrdenlab sOrden = new ServiceordenLabImpl();
            Cliente cliente = sCliente.obtenerCliente("p.clienteId = " + this.ordenLab.getOrdenLabPacienteId());
            mx.com.cdadb.entities.Cliente lcliente = slCliente.getBy("p.clienteId = " + this.ordenLab.getOrdenLabPacienteId());

            if (txtMaterno.getText().length() > 0 && txtPaterno.getText().length() > 0 && txtNombre.getText().length() > 0 && txtFNacimiento.getDate() != null && comboGenero.getSelectedIndex() > 0) {

                cliente.setClienteNombre(txtNombre.getText());
                cliente.setClientePaterno(txtPaterno.getText());
                cliente.setClienteMaterno(txtMaterno.getText());

                lcliente.setClienteNombre(txtNombre.getText());
                lcliente.setClientePaterno(txtPaterno.getText());
                lcliente.setClienteMaterno(txtMaterno.getText());
                if (comboGenero.getSelectedIndex() == 1) {
                    cliente.setClienteGenero(true);
                } else if (comboGenero.getSelectedIndex() == 2) {
                    cliente.setClienteGenero(false);
                }
                String sex = (cliente.getClienteGenero()) ? "M" : "F";
                cliente.setClienteFnac(txtFNacimiento.getDate());

                ServiceApp sApp = new ServiceAppImpl();
                App app = sApp.getBy("a.appNombre ='"+ConstantConfig.CONFIG_BRANCH_ID+"'");

                ServiceSucursalLocal sSucursal = new ServiceSucursalLocalImp();
                Sucursal sucursal = sSucursal.getBy("s.sucursalId = " + app.getAppValor());

                if (sCliente.modificarCliente(cliente) && slCliente.edit(lcliente)) {
                    String datosPaciente = "P|" + sucursal.getSucursalEnterpriseId() + "|" + this.ordenLab.getId().getOrdenlabContDiario() + "|" + cliente.getClienteId() + "||" + cliente.getClientePaterno() + "^" + cliente.getClienteMaterno() + "^" + cliente.getClienteNombre() + "||" + FormatDateUtil.SFD_YYYYMMDD_NS.format(cliente.getClienteFnac()).replaceAll("-", "") + "|" + sex + "||" + cliente.getClienteEdo() + "||" + cliente.getClienteTel() + "|||||||||||" + FormatDateUtil.SFD_YYYYMMDD.format(this.ordenLab.getId().getOrdenLabFecha()).replaceAll("-", "") + FormatDateUtil.SFD_KKMMSS_SIN.format(this.ordenLab.getOrdenHora()) + "|||||||||1";
                    String datosProductos = "";

                    if (claves.size() > 0) {
                        for (Clave clave : claves) {
                            if ("".equals(datosProductos)) {
                                datosProductos = "O|" + sucursal.getSucursalEnterpriseId() + "|" + this.ordenLab.getId().getOrdenlabContDiario() + "||^^^" + clave.getEnterprise().getEnterpriseClave() + "|||||||A||||||||||||||O";
                            } else {
                                datosProductos = datosProductos + "\n" + "O|" + sucursal.getSucursalEnterpriseId() + "|" + this.ordenLab.getId().getOrdenlabContDiario() + "||^^^" + clave.getEnterprise().getEnterpriseClave() + "|||||||A||||||||||||||O";
                            }
                        }
                    }

                    String lineaFinal = "L|" + sucursal.getSucursalEnterpriseId() + "|N";

                    this.ordenLab.setOrdenlabDesc(datosSucursal + "\n" + datosPaciente + "\n" + datosProductos + "\n" + lineaFinal);
                    this.ordenLab.setOrdenlabStatus("CORR");
                    //this.ordenLab.setOrdenClienteNombre(cliente.getClienteNombre());
                    this.ordenLab.setOrdenlabFmodif(new java.util.Date());
                    //this.ordenLab.setOrdenClienteNombre(cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
                    if (sOrden.edit(this.ordenLab)) {
                        new SendFile().outFile(ordenLab.getOrdenlabDesc(), ordenLab.getId().getOrdenlabContDiario());
                        JOptionPane.showMessageDialog(this, "Orden corregida y enviada al laboratorio, para su debido proceso.");
                        this.dialogOrdenesLab.initData();
                        this.dispose();
                    }
                }
            } else {
                JOptionPane.showMessageDialog(this, "Ningún campo puede quedar vacío, son de suma importancia.");
                txtNombre.selectAll();
                txtNombre.grabFocus();
            }
        } catch (Exception ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(this, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex, this.dialogOrdenesLab.frMenu.user);
        }
    }
}
