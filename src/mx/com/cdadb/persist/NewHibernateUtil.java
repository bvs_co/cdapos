/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.persist;

import java.util.Properties;
import mx.com.cdadb.constants.ConstantFiles;
import mx.com.cdadb.util.Log;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import mx.com.cdadb.util.PropertiesHibernateLocal;
import org.hibernate.HibernateException;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author unknown
 */
public class NewHibernateUtil {

    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            Properties dbConnectionProperties = PropertiesHibernateLocal.Decrypt(ConstantFiles.LOCAL_CONNECTION);
            Log.info("Abriendo hibernate.local.cfg.xml");
            return new AnnotationConfiguration().mergeProperties(dbConnectionProperties).configure("/mx/com/cdadb/persist/hibernate.local.cfg.xml").buildSessionFactory();

        } catch (HibernateException ex) {
            Log.fatal("Error al abrir la conexión:"+ex.getMessage());
        }
        return null;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
