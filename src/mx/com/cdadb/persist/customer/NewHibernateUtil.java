
package mx.com.cdadb.persist.customer;

import java.util.Properties;
import mx.com.cdadb.constants.ConstantFiles;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.util.PropertiesHibernateLocal;

public class NewHibernateUtil {
    
    private static final SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory() {
        try {
            Properties dbConnectionProperties = PropertiesHibernateLocal.Decrypt(ConstantFiles.CUSTOMER_CONNECTION);
            return new AnnotationConfiguration().mergeProperties(dbConnectionProperties).configure("/mx/com/cdadb/persist/customer/hibernate.cfg.xml").buildSessionFactory();

        } catch (Throwable ex) {
            LoggerUtil.registrarError(ex);
        }
        return null;
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
