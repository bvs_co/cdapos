/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.enums;

/**
 *
 * @author macbook
 */
public enum CellRendererEnum {
    ORDER_ID, PATIENT_ID, FOLIO_ID, COIN_ID, TEXT_LEFT, TEXT_CENTER, PK_ID_HIDDEN, PK_ID, ENTERPRISE_ID
}
