package mx.com.cdadb.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Caja;
import mx.com.cdadb.entities.Detalledevolucion;
import mx.com.cdadb.entities.Devolucion;
import mx.com.cdadb.entities.Folio;
import mx.com.cdadb.entities.Foliodevolucion;
import mx.com.cdadb.entities.FoliodevolucionId;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.entities.customer.Cliente;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceFolio;
import mx.com.cdadb.service.ServiceFolioDevolucion;
import mx.com.cdadb.view.ClienteJRDataSource;
import mx.com.cdadb.util.PrintReportImpl;

/**
 *
 * @author unknown
 */
public class ServiceFolioDevolucionImpl implements ServiceFolioDevolucion {

    @Override
    public List<Foliodevolucion> findAll() throws Exception {
        Services s = new Services();
        List<Foliodevolucion> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Foliodevolucion");
            lista = (List<Foliodevolucion>) query.list();
        } catch (org.hibernate.HibernateException e) {
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Foliodevolucion> findBy(String where) throws Exception {
        Services s = new Services();
        List<Foliodevolucion> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Foliodevolucion f where " + where);
            lista = q.list();
        } catch (org.hibernate.HibernateException e) {
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(Foliodevolucion folio, Cliente cliente, String total, String motivo, Folio folioVenta, Caja caja) throws Exception {
        Services s = new Services();
        List<Detalledevolucion> detalles = new ArrayList<Detalledevolucion>();
        ServiceFolioDevolucion sDev = new ServiceFolioDevolucionImpl();
        ServiceApp sApp = new ServiceAppImpl();
        ClienteJRDataSource datasource = new ClienteJRDataSource();
        java.util.Date hoy = new java.util.Date();
        User usr = null;
        //int idFolio = -1;
        boolean logrado = false;
        try {
            s.initOperation();
            List<Foliodevolucion> folios = sDev.findAll();
            FoliodevolucionId fId = new FoliodevolucionId();
            Long f = (folios.size() > 0) ? (long) s.getSesion().createQuery("select max(f.id.folioDevolucionValor) from Foliodevolucion f").uniqueResult() + 1 : 0;
            if (f <= 0) {
                App app = sApp.getBy("a.appNombre='app.pos.dev.ini'");
                fId.setFolioDevolucionValor(Long.valueOf(app.getAppValor()));
                fId.setFolioDevolucionFecha(hoy);
                folio.setId(fId);
            } else {
                fId.setFolioDevolucionValor(f);
                fId.setFolioDevolucionFecha(hoy);
                folio.setId(fId);
            }
            //Revisar, puede guardarse el folio de venta sin que se guarde el detalle de la devolucion
            s.getSesion().persist(folio);
            //Agregó el id del folio a la devolución
            folio.setId(folio.getId());
            for (Object d : folio.getDevolucions()) {
                Devolucion devolucion = (Devolucion) d;
                usr = devolucion.getUser();
                devolucion.setFoliodevolucion(folio);
                //Guardo el registro devolucion, ya ligado con el folio de devolución
                s.getSesion().persist(devolucion);
                if (devolucion.getDevolucionId() > 0) {
                    for (Object dd : devolucion.getDetalledevolucions()) {
                        Detalledevolucion detalledevolucion = (Detalledevolucion) dd;
                        //Agrego el id de la devolución
                        detalledevolucion.setDevolucion(devolucion);
                        //Guardo cada detalle de devolución, es decir, cada producto seleccionado para la devolución
                        s.getSesion().persist(detalledevolucion);
                        if (detalledevolucion.getDetalleDevolucionId() > 0) {
                            detalles.add(detalledevolucion);
                            if (detalledevolucion.getOrden().getProducto() != null) {
                                detalledevolucion.getOrden().getProducto().setProductoClave("1");
                                datasource.addProducto(detalledevolucion.getOrden().getProducto());
                            } else if (detalledevolucion.getOrden().getDetallepaquete() != null) {
                                Producto prod = new Producto();
                                prod.setProductoNombre(detalledevolucion.getOrden().getDetallepaquete().getDetallePaqueteNombre());
                                prod.setProductoClave("1");
                                datasource.addProducto(prod);
                            }
                        }
                    }
                }
            }
            if (detalles.size() > 0) {
                s.getSesion().update(caja);

                s.getTx().commit();
                ServiceFolio sFolio = new ServiceFolioImpl();
                sFolio.edit(folioVenta);
                logrado = true;
                PrintReportImpl pr = new PrintReportImpl();
                pr.printDevolution(cliente, folio.getId().getFolioDevolucionValor(), total, " ", motivo, datasource, usr);
            } else {
                return false;
            }
        } catch (org.hibernate.HibernateException e) {
            //s.getTx().commit();
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Foliodevolucion folio) throws Exception {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(folio);
            s.getTx().commit();
            logrado = true;
        } catch (org.hibernate.HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Foliodevolucion folio) throws Exception {
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(folio);
            s.getTx().commit();
        } catch (org.hibernate.HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Foliodevolucion getBy(String where) throws Exception {
        Services s = new Services();
        Foliodevolucion folio = null;
        try {
            s.initOperation();
            folio = (Foliodevolucion) s.getSesion().createQuery("SELECT f FROM Foliodevolucion f where " + where).uniqueResult();
        } catch (org.hibernate.HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return folio;
    }

}
