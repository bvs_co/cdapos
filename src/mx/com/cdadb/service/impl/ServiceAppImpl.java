/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.util.LoggerUtil;

/**
 *
 * @author unknown
 */
public class ServiceAppImpl implements ServiceApp {

    @Override
    public List<App> findAll() throws Exception{
        Services s = new Services();
        List<App> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from App");
            lista = (List<App>) query.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<App> findBy(String where) throws Exception{
        Services s = new Services();
        List<App> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from App a where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(App app) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(app);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean editList(List<App> app) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            for (App ap : app) {
                s.getSesion().update(ap);
            }
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(App app) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(app);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(App app) throws Exception{
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(app);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public App getBy(String where) throws Exception{
        Services s = new Services();
        App app = null;
        try {
            s.initOperation();
            app = (App) s.getSesion().createQuery("SELECT a FROM App a where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return app;
    }
}
