package mx.com.cdadb.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.swing.JOptionPane;
import mx.com.cdadb.constants.ConstantConfig;
import net.sf.jasperreports.engine.JRException;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Area;
import mx.com.cdadb.entities.Caja;
import mx.com.cdadb.entities.Clave;
import mx.com.cdadb.entities.Devolucion;
import mx.com.cdadb.entities.Folio;
import mx.com.cdadb.entities.FolioId;
import mx.com.cdadb.entities.Foliodevolucion;
import mx.com.cdadb.entities.Orden;
import mx.com.cdadb.entities.Ordenlab;
import mx.com.cdadb.entities.OrdenlabId;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.entities.Tr;
import mx.com.cdadb.entities.TrId;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.entities.Xx;
import mx.com.cdadb.entities.customer.Cliente;
import mx.com.cdadb.exceptions.ServiceException;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceCaja;
import mx.com.cdadb.service.ServiceClave;
import mx.com.cdadb.service.ServiceFolio;
import mx.com.cdadb.service.ServiceOrden;
import mx.com.cdadb.service.ServiceOrdenlab;
import mx.com.cdadb.service.ServiceSucursalLocal;
import mx.com.cdadb.service.ServiceTr;
import mx.com.cdadb.service.ServiceX;
import mx.com.cdadb.view.ClienteJRDataSource;
import mx.com.cdadb.view.ComandaJRDataSource;
import mx.com.cdadb.util.CalculateAgeUtil;
import mx.com.cdadb.util.FormatDateUtil;
import mx.com.cdadb.util.GeneralGetters;
import mx.com.cdadb.util.LoggerUtil;
import mx.com.cdadb.util.PrintReportImpl;
import mx.com.cdadb.util.SendFile;

public class ServiceOrdenImpl implements ServiceOrden {

    @Override
    public List<Orden> findAll() throws Exception {
        Services s = new Services();
        List<Orden> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Orden");
            lista = (List<Orden>) query.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Orden> findBy(String where) throws Exception {
        Services s = new Services();
        List<Orden> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Orden o where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public Map addList(List<Orden> ordenes, List<Producto> ordenesLab, Cliente cliente, String total, String totalLetra, List<Producto> productosTotal, User usr, List<Xx> arrX) throws Exception {
        Services services = new Services();
        ServiceApp serviceApp = new ServiceAppImpl();
        ServiceFolio serviceFolio = new ServiceFolioImpl();
        ServiceTr serviceTr = new ServiceTrImpl();
        mx.com.cdadb.service.ServiceCliente sCliente = new ServiceClienteImpl();
        mx.com.cdadb.entities.Cliente clienteExist = sCliente.getBy("p.clienteId = " + cliente.getClienteId());

        java.util.Date fHoy = new java.util.Date();
        Map param = null;
        Tr tr = null;
        SendFile sendFile = new SendFile();
        boolean guardoOrdenLab = false;
        long idTr = 0;
        int idFolio = 0;

        try {
            services.initOperation();
            //Entidad donde se guardará el folio, una vez creado
            Folio folio = new Folio();
            //Lista de folios usados actualmente
            //REVISAR: Obtener solo los folios del día
            List<Folio> folios = serviceFolio.findAll();
            //Se obtiene el ultimo folio usado
            Long f = (folios.size() > 0) ? (long) services.getSesion().createQuery("select max(f.id.folioValor) from Folio f").uniqueResult() + 1 : 0;
            //Si es el primer folio de la tabla, entonces se obtiene de la tabla app, el folio en el que debe iniciar
            FolioId fId = new FolioId();
            if (f <= 0) {
                App app = serviceApp.getBy("a.appNombre='app.pos.ini'");
                fId.setFolioValor(Long.valueOf(app.getAppValor()));
            } else {//Se guarda el folio aumentado en una unidad
                fId.setFolioValor(f);
            }
            fId.setFolioFecha(fHoy);
            folio.setId(fId);
            folio.setFolioStatus("1");
            folio.setFolioFcreacion(fHoy);

            List<Tr> trs = serviceTr.findAll();
            Long t = (trs.size() > 0) ? (long) services.getSesion().createQuery("select max(t.id.trValor) from Tr t").uniqueResult() + 1 : 0;
            tr = new Tr();
            tr.setTrFcreacion(fHoy);

            TrId trId = new TrId();
            if (t <= 0 || t == null) {
                trId.setTrValor(1);
            } else {//Se guarda el folio aumentado en una unidad
                trId.setTrValor(t);
            }
            //trId.setTrValor((t <= 0) ? 1 : t);
            trId.setTrFecha(fHoy);
            tr.setId(trId);
            tr.setTrStatus("1");

            //idFolio = (Integer) services.getSesion().save(folio);
            //idTr = (Long) services.getSesion().save(tr);
            Ordenlab ordenLab = new Ordenlab();
            boolean ok = false;
            if (hayOrdenesLab(ordenesLab)) {
                int oi = -1;
                //Revisa si hay ordenes guardadas; si las hay, toma la ultima orden para aumentar el contador
                ServiceOrdenlab sOrdenLab = new ServiceordenLabImpl();
                List<Ordenlab> arr = sOrdenLab.findAll();
                App apFin = serviceApp.getBy("a.appNombre = 'app.order.fin'");

                int orderLabId = 0;
                for (Ordenlab lab : arr) {
                    orderLabId = lab.getId().getOrdenlabContDiario();
                }
                if (orderLabId > 0) {
                    //int orderLabId = (int) services.getSesion().createQuery("select max(orderLabId.ordenlabContDiario) from Ordenlab orderLabId").uniqueResult();
                    if (orderLabId > 0 && orderLabId < Integer.parseInt(apFin.getAppValor())) {
                        oi = orderLabId + 1;
                        ok = true;
                    } else {
                        App ap = serviceApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_ORDER_INIT+"'");
                        if (ap.getAppValor() != null) {
                            oi = Integer.parseInt(ap.getAppValor());
                            ok = true;
                        } else {
                            JOptionPane.showMessageDialog(null, "No se ha definido el rango de ordenes para enterprise.");
                            //Detener la generacion de la orden para laboratorio, debido a que la orden se enviaria como -1
                        }
                    }
                } else {//Si no hay ordenes, toma el valor de inicio de las ordenes, de la tabla app
                    App ap = serviceApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_ORDER_INIT+"'");
                    if (ap.getAppValor() != null) {
                        oi = Integer.parseInt(ap.getAppValor());
                        ok = true;
                    } else {
                        JOptionPane.showMessageDialog(null, "No se ha definido el rango de ordenes para enterprise.");
                        //Detener la generacion de la orden para laboratorio, debido a que la orden se enviaria como -1
                    }
                }
                guardoOrdenLab = true;
                ordenLab.setSucursal(GeneralGetters.getSucursal());
                ordenLab.setOrdenlabDesc(getDescripcion(ordenesLab, oi, cliente));
                OrdenlabId olId = new OrdenlabId();
                olId.setOrdenlabContDiario(oi);
                olId.setOrdenLabFecha(fHoy);
                ordenLab.setId(olId);
                ordenLab.setOrdenlabFcreacion(fHoy);
                ordenLab.setOrdenlabFmodif(fHoy);
                ordenLab.setOrdenHora(fHoy);
                ordenLab.setOrdenlabStatus("ENV");
                //ordenLab.setOrdenClienteNombre(cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
                ordenLab.setOrdenLabPacienteId(cliente.getClienteId());
                services.getSesion().persist(ordenLab);
                //ordenLab.setOrdenlabId(ordenLab.getOrdenlabId());

            }
            services.getSesion().persist(folio);
            services.getSesion().persist(tr);
            param = new HashMap();
            param.put("tr", tr.getId().getTrValor());
            param.put("folio", folio.getId().getFolioValor());
//            System.out.println("FOLIO: " + folio.getId().getFolioValor());
//            System.out.println("TR-VALOR: " + tr.getId().getTrValor());
//            System.out.println("TR-DATE: " + tr.getId().getTrFecha());
            for (Orden orden : ordenes) {
                //orden.setOrdenClienteNombre(cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
                orden.setFolio(folio);
                orden.setTr(tr);
                if (ok) {
                    orden.setOrdenlab(ordenLab);
                } else {
                    orden.setOrdenlab(null);
                }
                services.getSesion().flush();
                services.getSesion().persist(orden);
            }

            //Revisar que el cliente no exista en la BD local 
            mx.com.cdadb.entities.Cliente localCustomer = convertToLocalCustomer(cliente);
            if (clienteExist != null) {//Actualiza datos
                services.getSesion().update(localCustomer);
            } else {//nuevo registro
                services.getSesion().persist(localCustomer);
            }

            services.getTx().commit();
            //Si todo se guardó, se envian las ordenes al laboratorio
            if (ok) {
                try {
                    // Si se generó correctamente el numero de orden, entonces enviar la orden a laboradorio
                    sendFile.outFile(ordenLab.getOrdenlabDesc(), ordenLab.getId().getOrdenlabContDiario());
                } catch (Exception ex) {
                    LoggerUtil.registrarError(ex, usr);
                    if (ex.getMessage().contains("Connection refused")) {
                        JOptionPane.showMessageDialog(null, "Hay problemas para comunicarse con Monitor CDA Server v1.0.12.2017. \nContacte al administrador.", "Orden no enviada al laboratorio..", JOptionPane.ERROR_MESSAGE);
                    }
                }
                ok = true;
            }
            printTicket(ordenes, cliente, fHoy, GeneralGetters.getSucursal(), ordenLab, usr);
            separaProductosPorArea(productosTotal, ordenLab, cliente, fHoy, GeneralGetters.getSucursal(), usr);
            PrintReportImpl pr = new PrintReportImpl();
            pr.printSimplificado(cliente, GeneralGetters.getSucursal(), folio.getId().getFolioValor(), total, totalLetra, "");

        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
            services.manageException(e);
            throw e;
        } finally {
            services.getSesion().close();
        }
        return param;
    }

    @Override
    public boolean edit(Orden orden) throws Exception {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(orden);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean editList(List<Orden> ordenes, List<Foliodevolucion> fDevs) throws Exception {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            for (Orden o : ordenes) {
                s.getSesion().update(o);
            }
            for (Foliodevolucion fDev : fDevs) {
                for (Object d : fDev.getDevolucions()) {
                    Devolucion dev = (Devolucion) d;
                    s.getSesion().update(dev);
                }
            }
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
            s.getTx().commit();
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean editBy(String where) throws Exception {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().createQuery("update Orden o set " + where);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean delete(java.util.Date fecha) throws Exception {
        Services s = new Services();
        s.initOperation();
        try {
            ServiceX sX = new ServiceXImpl();
            ServiceOrden sOrden = new ServiceOrdenImpl();

            List<Orden> ordenes = sOrden.findAll();
            List<Xx> arrX = sX.findAll();

            java.util.Date hoy = new java.util.Date();

            s.getSesion().createSQLQuery("delete from Detalledevolucion").executeUpdate();
            s.getSesion().createSQLQuery("ALTER TABLE Detalledevolucion AUTO_INCREMENT = 1").executeUpdate();

            s.getSesion().createSQLQuery("delete from Devolucion").executeUpdate();
            s.getSesion().createSQLQuery("ALTER TABLE Devolucion AUTO_INCREMENT = 1").executeUpdate();

            s.getSesion().createSQLQuery("delete from Orden").executeUpdate();
            s.getSesion().createSQLQuery("ALTER TABLE Orden AUTO_INCREMENT = 1").executeUpdate();

            s.getSesion().createSQLQuery("delete from Tr").executeUpdate();
            s.getSesion().createSQLQuery("ALTER TABLE Tr AUTO_INCREMENT = 1").executeUpdate();

            s.getSesion().createSQLQuery("delete from OrdenLab").executeUpdate();

            s.getSesion().createSQLQuery("delete from Cliente").executeUpdate();

            //Pidieron que los domingos ya no se borrarán los folios(16052018)
//            if (hoy.getDay() == 0) {
//                for (Orden orderLabId : ordenes) {
//                    services.getSesion().createSQLQuery("delete from folio where folio.folioValor = " + orderLabId.getFolio().getId().getFolioValor()+" and folio.folioFecha = '"+FormatDateUtil.SFD_YYYYMMDD.format(hoy)+"'").executeUpdate();
//                }
//            } else {
            for (Xx x : arrX) {
                if (FormatDateUtil.SFD_YYYYMMDD.format(x.getXxDate()).equals(FormatDateUtil.SFD_YYYYMMDD.format(hoy))) {
                    for (Orden o : ordenes) {
                        s.getSesion().createSQLQuery("delete from folio where folio.folioValor = " + o.getFolio().getId().getFolioValor() + " and folio.folioFecha = '" + FormatDateUtil.SFD_YYYYMMDD.format(hoy) + "'").executeUpdate();
                    }
                }
            }
//            }

            //Resetear el total de la caja
            ServiceCaja sCaja = new ServiceCajaImpl();
            List<Caja> cajas = sCaja.findAll();
            for (Caja caja : cajas) {
                caja.setCajaTotal(new Float(0));
            }
            for (Caja caja : cajas) {
                updateCash(caja, 0);
            }

            s.getTx().commit();
            return true;
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
            s.manageException(e);
            s.getTx().commit();
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Orden getBy(String where) {
        Services s = new Services();
        Orden orden = null;
        try {
            s.initOperation();
            orden = (Orden) s.getSesion().createQuery("SELECT o FROM Orden o where " + where).uniqueResult();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return orden;
    }

    private String getDescripcion(List<Producto> productosLaboratorio, int folioComanda, Cliente cliente) throws Exception {
        java.util.Date fecha = new java.util.Date();
        ServiceApp sApp = new ServiceAppImpl();
        App app = sApp.getBy("a.appNombre ='"+ConstantConfig.CONFIG_BRANCH_ID+"'");

        ServiceSucursalLocal sSucursal = new ServiceSucursalLocalImp();
        Sucursal sucursal = sSucursal.getBy("s.sucursalId = " + app.getAppValor());

        String sex = (cliente.getClienteGenero()) ? "M" : "F";
        String datosSucursal = "H|\\^&|||CORAZON DE ANGEL SUCURSAL " + sucursal.getSucursalNombre() + "|||||ECLIPSE||T||" + FormatDateUtil.SFD_YYYYMMDD.format(fecha).replaceAll("-", "") + FormatDateUtil.SFD_KKMMSS.format(fecha).replaceAll(":", "");
        String datosPaciente = "P|" + sucursal.getSucursalEnterpriseId() + "|" + folioComanda + "|" + cliente.getClienteId() + "||" + cliente.getClientePaterno() + "^" + cliente.getClienteMaterno() + "^" + cliente.getClienteNombre() + "||" + FormatDateUtil.SFD_YYYYMMDD.format(cliente.getClienteFnac()).replaceAll("-", "") + "|" + sex + "||" + cliente.getClienteEdo() + "||" + cliente.getClienteTel() + "|||||||||||" + FormatDateUtil.SFD_YYYYMMDD.format(fecha).replaceAll("-", "") + FormatDateUtil.SFD_KKMMSS.format(fecha).replaceAll(":", "") + "|||||||||1";
        String datosProductos = "";
        String lineaFinal = "L|" + sucursal.getSucursalEnterpriseId() + "|N";
        App app2 = sApp.getBy("a.appNombre ='"+ConstantConfig.CONFIG_LABORATORY_AREA+"'");
        for (Producto pr : productosLaboratorio) {
            if (pr.getArea().getAreaId() == Integer.parseInt(app2.getAppValor())) {
                if ("".equals(datosProductos)) {
                    datosProductos = "O|" + sucursal.getSucursalEnterpriseId() + "|" + folioComanda + "||^^^" + getClaveEnterprise(pr) + "|||||||A||||||||||||||O";
                } else {
                    datosProductos = datosProductos + "\nO|" + sucursal.getSucursalEnterpriseId() + "|" + folioComanda + "||^^^" + getClaveEnterprise(pr) + "|||||||A||||||||||||||O";
                }
            }
        }
        return datosSucursal + "\n" + datosPaciente + "\n" + datosProductos + "\n" + lineaFinal;
    }

    private String getClaveEnterprise(Producto pr) throws ServiceException{
        try{
        ServiceClave sclave = new ServiceClaveImpl();
        Clave clave = sclave.getBy("c.producto.productoClave='" + pr.getProductoClave() + "'");
        if (clave != null) {
            return clave.getEnterprise().getEnterpriseClave();
        } else {
            JOptionPane.showMessageDialog(null, "El producto [" + pr.getProductoClave() + "] " + pr.getProductoNombre() + ", no tiene asignada una clave para la comunicación con el laboratorio.");
            return "";
        }
        }catch(Exception e){
            throw new ServiceException("La clave enterprise para el estudio de laboratorio ["+pr.getProductoClave()+"] se encuentra duplicada.");
        }
    }

    private void printTicket(List<Orden> ordenes, Cliente cliente, Date hoy, Sucursal sucursal, Ordenlab or, User usr) throws JRException {
        ClienteJRDataSource datasource = new ClienteJRDataSource();
        //Preparando lista de productos
        float total = 0;
        for (Orden orden : ordenes) {
            if (orden.getDetallepaquete() != null) {
                Producto pr = new Producto();
                pr.setProductoClave("1");
                pr.setProductoNombre(orden.getDetallepaquete().getDetallePaqueteNombre() + " [" + orden.getDetallepaquete().getDetallePaqueteDesc() + "]");
                total = total + orden.getDetallepaquete().getDetallePaquetePrecio();
                datasource.addProducto(pr);
            }
            if (orden.getProducto() != null) {
                Producto pr = new Producto();
                pr.setProductoClave("1");
                pr.setProductoNombre(orden.getProducto().getProductoNombre());
                total = total + orden.getProducto().getProductoPrecio();
                datasource.addProducto(pr);
            }
        }

        Map param = new HashMap();
        param.put("CLIENTE", "[" + cliente.getClienteId() + "] " + cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
        param.put("USUARIO", usr.getUserUsuario());
        param.put("F_NAC", FormatDateUtil.SFD_DDMMYYYY.format(cliente.getClienteFnac()));
        param.put("EDAD", String.valueOf(CalculateAgeUtil.calculate(FormatDateUtil.SFD_DMY.format(cliente.getClienteFnac()))));
        param.put("FECHA", FormatDateUtil.SFD_DMY.format(hoy));
        param.put("HORA", FormatDateUtil.SFD_KKMMSS.format(hoy));
        param.put("SUCURSAL", sucursal.getSucursalNombre());
        param.put("TOTAL", String.valueOf(total));
        param.put("TRANSCC", (or.getId() != null) ? "LAB[" + String.valueOf(or.getId().getOrdenlabContDiario()) + "]" : "");
        param.put("MENSAJE", "");
        param.put("REIMPRESION", "");
        PrintReportImpl pr = new PrintReportImpl();
        pr.imprimeTiketCliente(param, datasource);
    }

    private void separaProductosPorArea(List<Producto> productoLab, Ordenlab ordenLab, Cliente cliente, java.util.Date date, Sucursal sucursal, User usr) throws Exception {
        List<Area> areas = new ArrayList<Area>();
        List<List<Producto>> prodEnAreas = new ArrayList<List<Producto>>();
        List<Map> mapProductos = new ArrayList<Map>();
        ServiceApp sApp = new ServiceAppImpl();
        ServiceClave sClave = new ServiceClaveImpl();
        App app = sApp.getBy("a.appNombre='"+ConstantConfig.CONFIG_LABORATORY_AREA+"'");

        //Obtiene las areas
        for (Producto producto : productoLab) {
            if (!isArea(producto.getArea(), areas)) {
                areas.add(producto.getArea());
            }
        }

        for (Area area : areas) {
            List<Producto> produtoArea = new ArrayList<Producto>();
            for (Producto pr : productoLab) {
                //JOptionPane.showMessageDialog(this, "\nArea: "+area.getAreaNombre()+"\nProducto: " + pc.getProductoNombre());
                Map map = new HashMap();
                map.put("PRODUCTO_ID", pr.getProductoClave());
                map.put("PRODUCTO_AREA_ID", pr.getArea().getAreaId());
                map.put("PRODUCTO_CLAVE", pr.getProductoClave());
                map.put("PRODUCTO_NOMBRE", pr.getProductoNombre());
                map.put("PRODUCTO_PRECIO", pr.getProductoPrecio());
                map.put("PRODUCTO_DOCTOR_ID", "");
                map.put("PRODUCTO_STATUS", "1");//Se deja en 1 por que un paciente nunca pedira dos estudios en la misma venta
                if (pr.getArea().getAreaId() == Integer.parseInt(app.getAppValor())) {
                    Clave clave = sClave.getBy("c.id.claveProductoClave = '" + pr.getProductoClave() + "'");
                    map.put("PRODUCTO_ENTERPRISE", (clave != null) ? clave.getEnterprise().getEnterpriseClave() : "S/C");
                } else {
                    map.put("PRODUCTO_ENTERPRISE", "");
                }
                mapProductos.add(map);
                produtoArea.add(pr);
            }
            prodEnAreas.add(produtoArea);
        }
        for (List<Producto> pEnArea : prodEnAreas) {
            for (Producto p : pEnArea) {
                int i = 0;
                for (List<Producto> pEnArea2 : prodEnAreas) {
                    for (Producto p2 : pEnArea2) {
                        if (p.getProductoClave().equals(p2.getProductoClave())) {
                            i++;
                        }
                    }
                }
                p.setProductoStatus(String.valueOf(i));
            }
        }

        //Agrega el numero de productos
//        for (Map mp : mapProductos) {
//            int i = 0;
//            for (List<Producto> pEnArea2 : prodEnAreas) {
//                for (Producto p2 : pEnArea2) {
//                    if (p2.getProductoClave().equals(mp.get("PRODUCTO_ID"))) {
//                        i++;
//                    }
//                }
//            }
//            mp.put("PRODUCTO_STATUS", i);
//        }
        //Quitar repetidos
        List<Map> mapProductosFinal = new ArrayList<Map>();

        HashSet hashset = new HashSet();
        hashset.addAll(mapProductos);
        mapProductosFinal.addAll(hashset);

        List<List<Map>> productosPorArea = new ArrayList<List<Map>>();

        for (Area area : areas) {
            List<Map> maps = new ArrayList<Map>();
            for (Map mp : mapProductosFinal) {
                if (area.getAreaId().equals(mp.get("PRODUCTO_AREA_ID"))) {
                    mp.put("PRODUCTO_AREA_NOMBRE", area.getAreaNombre());
                    maps.add(mp);
                }
            }
            productosPorArea.add(maps);
        }
        int i = 0;
        for (List<Map> ppa : productosPorArea) {
            if (ppa.size() > 0) {
                print(ppa, areas.size(), i + 1, ordenLab, cliente, date, sucursal, usr);
                i++;
            }
        }
    }

    private boolean isArea(Area area, List<Area> areas) throws Exception {
        for (Area a : areas) {
            if (Objects.equals(a.getAreaId(), area.getAreaId())) {
                return true;
            }
        }
        return false;
    }

    private boolean print(List<Map> datos, int areasTotal, int areaActual, Ordenlab ordenLab, Cliente cliente, java.util.Date date, Sucursal sucursal, User usr) throws Exception {
        ComandaJRDataSource datasource = new ComandaJRDataSource();
        ServiceApp sApp = new ServiceAppImpl();
        String area = null;

        for (Map p : datos) {
            area = p.get("PRODUCTO_AREA_NOMBRE").toString();
            App app = sApp.getBy("a.appNombre='"+ConstantConfig.CONFIG_LABORATORY_AREA+"'");
            if (p.get("PRODUCTO_AREA_ID").toString().equals(app.getAppValor())) {
                Producto pr = new Producto();
                //Si es un producto de laboratorio, agregar a la papeleta el codigo del producto en enterprise
                //arr.add(p.get("PRODUCTO_STATUS") + "           " + "[" + p.get("PRODUCTO_ENTERPRISE") + "]  " + p.get("PRODUCTO_NOMBRE").toString());
                pr.setProductoClave(String.valueOf(p.get("PRODUCTO_STATUS")));
                pr.setProductoNombre("[" + p.get("PRODUCTO_ENTERPRISE").toString() + "]  " + p.get("PRODUCTO_NOMBRE").toString());
                datasource.addProducto(pr);
            } else {
                Producto pr = new Producto();
                //arr.add(p.get("PRODUCTO_STATUS") + "           " + p.get("PRODUCTO_NOMBRE").toString());
                //JOptionPane.showMessageDialog(null, "PRODUCTO_STATUS: "+p.get("PRODUCTO_STATUS").toString()+" PRODUCTO_NOMBRE: "+p.get("PRODUCTO_NOMBRE").toString());
                pr.setProductoClave(p.get("PRODUCTO_STATUS").toString());
                pr.setProductoNombre(p.get("PRODUCTO_NOMBRE").toString());
                datasource.addProducto(pr);
            }
        }

        if (datos.size() > 0) {
            Map param = new HashMap();
            param.put("AREA", area);
            param.put("CLIENTE", "[" + cliente.getClienteId() + "] " + cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
            param.put("USUARIO", usr.getUserUsuario());
            param.put("EDAD", String.valueOf(CalculateAgeUtil.calculate(FormatDateUtil.SFD_DMY.format(cliente.getClienteFnac()))));
            param.put("PROGESS", areaActual + " / " + areasTotal);
            param.put("FECHA", FormatDateUtil.SFD_DMY.format(date));
            param.put("HORA", FormatDateUtil.SFD_KKMMSS.format(date));
            param.put("REIMPRESION", "");
            if (ordenLab.getId() != null) {
                param.put("FOLIO_ORDER", String.valueOf(ordenLab.getId().getOrdenlabContDiario()));
            } else {
                param.put("FOLIO_ORDER", "");
            }
            param.put("F_NAC", FormatDateUtil.SFD_DMY.format(cliente.getClienteFnac()));
            String genero = (cliente.getClienteGenero()) ? "MASCULINO" : "FEMENINO";
            param.put("GENERO", genero);
            param.put("SUCURSAL", sucursal.getSucursalNombre());
            PrintReportImpl pr = new PrintReportImpl();;
            pr.papeletaComanda(param, datasource);
        }
        return true;
    }

    
    private void updateCash(Caja caja, float total) throws Exception {
        ServiceCaja sCaja = new ServiceCajaImpl();
        caja.setCajaFmodif(new java.util.Date());
        caja.setCajaTotal(total);
        sCaja.edit(caja);
    }

    /**
     * Verifica si en el arreglo de productos hay estudios de laboratorio
     * @param productos Arreglo de productos
     * @return true si hay por lo menos un estudio de laboratorio
     * @throws Exception Excepcion
     */
    private boolean hayOrdenesLab(List<Producto> productos) throws Exception {
        ServiceApp sApp = new ServiceAppImpl();
        int num = 0;
        App app = sApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_LABORATORY_AREA+"'");
        num = productos.stream().filter(p -> (p.getArea().getAreaId() == Integer.parseInt(app.getAppValor()))).map(_item -> 1).reduce(num, Integer::sum);
        return num > 0;
    }

    /**
     * Convierte un cliente de la BD de clientes remota a un cliente local, para 
     * poder ser manipulado e insertado en la BD local
     * @param cliente Cliente
     * @return Cliente local
     */
    private mx.com.cdadb.entities.Cliente convertToLocalCustomer(Cliente cliente) {
        mx.com.cdadb.entities.Cliente clienteLocal = new mx.com.cdadb.entities.Cliente();
        clienteLocal.setClienteCalle(cliente.getClienteCalle());
        clienteLocal.setClienteCiudad(cliente.getClienteCiudad());
        clienteLocal.setClienteColonia(cliente.getClienteColonia());
        clienteLocal.setClienteCp(cliente.getClienteCp());
        clienteLocal.setClienteEdo(cliente.getClienteEdo());
        clienteLocal.setClienteEmail(cliente.getClienteEmail());
        clienteLocal.setClienteFechRegistro(cliente.getClienteFechRegistro());
        clienteLocal.setClienteFnac(cliente.getClienteFnac());
        clienteLocal.setClienteGenero(cliente.getClienteGenero());
        clienteLocal.setClienteId(cliente.getClienteId());
        clienteLocal.setClienteMaterno(cliente.getClienteMaterno());
        clienteLocal.setClienteMpio(cliente.getClienteMpio());
        clienteLocal.setClienteNombre(cliente.getClienteNombre());
        clienteLocal.setClienteNumExt(cliente.getClienteNumExt());
        clienteLocal.setClienteNumInt(cliente.getClienteNumInt());
        clienteLocal.setClientePais(cliente.getClientePais());
        clienteLocal.setClientePaterno(cliente.getClientePaterno());
        clienteLocal.setClienteRazonSoc(cliente.getClienteRazonSoc());
        clienteLocal.setClienteRfc(cliente.getClienteRfc());
        clienteLocal.setClienteStatus(cliente.getClienteStatus());
        clienteLocal.setClienteTalla(cliente.getClienteTalla());
        clienteLocal.setClienteTel(cliente.getClienteTel());

        return clienteLocal;

    }
}
