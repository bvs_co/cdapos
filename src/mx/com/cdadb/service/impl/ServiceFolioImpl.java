package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Folio;
import mx.com.cdadb.service.ServiceFolio;

/**
 *
 * @author unknown
 */
public class ServiceFolioImpl implements ServiceFolio{
    @Override
    public List<Folio> findAll() throws Exception{
        Services s = new Services();
        List<Folio> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Folio");
            lista = (List<Folio>) query.list();
        } catch (HibernateException e) {
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Folio> findBy(String where) throws Exception{
        Services s = new Services();
        List<Folio> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Folio f where " + where);
            lista = q.list();
        } catch (HibernateException e) {
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(Folio folio) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            //s.getSesion().persist(folio.getOrden());
            s.getSesion().persist(folio);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Folio folio) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(folio);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Folio folio) throws Exception{
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(folio);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Folio getBy(String where) throws Exception{
        Services s = new Services();
        Folio folio = null;
        try {
            s.initOperation();
            folio = (Folio) s.getSesion().createQuery("SELECT f FROM Folio f where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return folio;
    }
}
