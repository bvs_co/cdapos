/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Ordenlab;
import mx.com.cdadb.entities.OrdenlabId;
import mx.com.cdadb.service.ServiceOrdenlab;

/**
 *
 * @author unknown
 */
public class ServiceordenLabImpl implements ServiceOrdenlab{

    @Override
    public List<Ordenlab> findAll() {
        Services s = new Services();
        List<Ordenlab> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Ordenlab");
            lista = (List<Ordenlab>) query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Ordenlab> findBy(String where) {
        Services s = new Services();
        List<Ordenlab> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Ordenlab o where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public OrdenlabId add(Ordenlab orden) {
        Services s = new Services();
        OrdenlabId id = null;
        try {
            s.initOperation();
            id = (OrdenlabId)s.getSesion().save(orden);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return id;
    }

    @Override
    public boolean edit(Ordenlab orden) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(orden);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Ordenlab orden) {
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(orden);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Ordenlab getBy(String where) {
        Services s = new Services();
        Ordenlab orden = null;
        try {
            s.initOperation();
            orden = (Ordenlab) s.getSesion().createQuery("SELECT o FROM Ordenlab o where " + where).uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return orden;
    }
}
