/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.service.ServiceProducto;

/**
 *
 * @author unknown
 */
public class ServiceProductoImpl implements ServiceProducto {

    @Override
    public List<Producto> findAll() {
        Services s = new Services();
        List<Producto> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Producto");
            lista = (List<Producto>) query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Producto> findBy(String where) {
        Services s = new Services();
        List<Producto> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Producto p where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(Producto area) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(area);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Producto producto) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(producto);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean delete(Producto producto) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().delete(producto);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public Producto getBy(String where) {
        Services s = new Services();
        Producto area = null;
        try {
            s.initOperation();
            area = (Producto) s.getSesion().createQuery("SELECT p FROM Producto p where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return area;
    }

}
