package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.service.ServiceSucursalLocal;

/**
 *
 * @author unknown
 */
public class ServiceSucursalLocalImp implements ServiceSucursalLocal{
    @Override
    public List<Sucursal> findAll() {
        Services s = new Services();
        List<Sucursal> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Sucursal");
            lista = (List<Sucursal>) query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Sucursal> findBy(String where) {
        Services s = new Services();
        List<Sucursal> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Sucursal s where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(Sucursal sucursal) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(sucursal);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Sucursal sucursal) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(sucursal);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Sucursal sucursal) {
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(sucursal);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Sucursal getBy(String where) {
        Services s = new Services();
        Sucursal sucursal = null;
        try {
            s.initOperation();
            sucursal = (Sucursal) s.getSesion().createQuery("SELECT s FROM Sucursal s where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return sucursal;
    }
}
