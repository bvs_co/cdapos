/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.impl;

import java.util.List;
import mx.com.cdadb.util.LoggerUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;

/**
 *
 * @author macbook
 * @param <T>
 */
public class ServiceBaseImpl<T>{
    
    public List<T> findAll(T t){
//        System.out.println("CLASS:"+t.getClass().getSimpleName());
        Services s = new Services();
        List<T> list = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from "+t.getClass().getSimpleName());
            list = (List<T>) query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            //LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
    return list;
    }
    
     public List<T> findBy(T t, String where) throws Exception{
        Services s = new Services();
        List<T> list = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from "+t.getClass().getSimpleName()+" t where " + where);
            list = q.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            //LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return list;
    }
     
      public boolean add(List<T> objects) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            for(T object : objects){
                s.getSesion().persist(object);
            }
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    public boolean edit(T object) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(object);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    public void delete(T object) throws Exception{
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(object);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    public T getBy(T t,String where) throws Exception{
        Services s = new Services();
        T object = null;
        try {
            s.initOperation();
            object = (T) s.getSesion().createQuery("SELECT t FROM "+t.getClass().getSimpleName()+" t where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return object;
    }
}
