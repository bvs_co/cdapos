/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.Query;
import mx.com.cdadb.entities.Rol;
import mx.com.cdadb.service.ServiceRol;


/**
 *
 * @author unknown
 */
public class ServiceRolImpl implements ServiceRol {
     @Override
    public List<Rol> findAll() {
        Services s = new Services();
        List<Rol> lista = null;
        s.initOperation();
        Query query = s.getSesion().createQuery("from Rol");
        lista = (List<Rol>) query.list();
        s.getSesion().close();
        return lista;
    }

    @Override
    public List<Rol> findBy(String where) {
        Services s = new Services();
        List<Rol> lista = null;
        s.initOperation();
        Query q = s.getSesion().createQuery("from Rol r where " + where);
        lista = q.list();
        s.getSesion().close();
        return lista;
    }

    @Override
    public boolean add(Rol user) {
        Services s = new Services();
        boolean logrado = false;
        s.initOperation();
        s.getSesion().save(user);
        s.getTx().commit();
        logrado = true;
        s.getSesion().close();
        return logrado;
    }

    @Override
    public boolean edit(Rol rol) {
        Services s = new Services();
        boolean logrado = false;
        s.initOperation();
        s.getSesion().update(rol);
        s.getTx().commit();
        logrado = true;
        s.getSesion().close();
        return logrado;
    }

    @Override
    public boolean delete(Rol rol) {
        Services s = new Services();
        boolean logrado = false;
        s.initOperation();
        s.getSesion().delete(rol);
        s.getTx().commit();
        logrado = true;
        s.getSesion().close();
        return logrado;
    }

    @Override
    public Rol getBy(String where) {
        Services s = new Services();
        Rol rol = null;
        s.initOperation();
        rol = (Rol) s.getSesion().createQuery("SELECT r FROM Rol r where " + where).uniqueResult();
        s.getSesion().close();
        return rol;
    }
}
