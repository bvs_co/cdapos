package mx.com.cdadb.service.impl;

import java.util.List;
import javax.swing.JOptionPane;
import mx.com.cdadb.constants.ConstantMessages;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import mx.com.cdadb.entities.Cliente;
import mx.com.cdadb.service.ServiceCliente;

/**
 *
 * @author unknown
 */
public class ServiceClienteImpl implements ServiceCliente {

    private Session sesion;
    private Transaction tx;

    @Override
    public List<Cliente> findAll() {
        List<Cliente> lista = null;
        try {
            iniciaOperacion();
            Query query = sesion.createQuery("from Cliente");
            lista = (List<Cliente>) query.list();
        } catch (HibernateException e) {
            JOptionPane.showMessageDialog(null, "Error de conexión: " + e.getMessage());
        } finally {
            sesion.close();
        }
        return lista;
    }

    @Override
    public List<Cliente> findBy(String where) {
        List<Cliente> lista = null;
        try {
            iniciaOperacion();
            Query q = sesion.createQuery("from Cliente e where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + e.getMessage());
            manejaExcepcion(e);
        } finally {
            sesion.close();
        }
        return lista;
    }

    @Override
    public long add(Cliente cliente) {
        long logrado = -1;

        try {
            iniciaOperacion();
            logrado = (Long) sesion.save(cliente);
            tx.commit();
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Cliente cliente) {
        boolean logrado = false;
        try {
            iniciaOperacion();
            sesion.update(cliente);
            tx.commit();
            logrado = true;
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
        return logrado;
    }

    @Override
    public void delete(Cliente cliente) {
        try {
            iniciaOperacion();
            sesion.delete(cliente);
            tx.commit();
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Cliente getBy(String where) {
        Cliente cliente = null;
        try {
            iniciaOperacion();
            cliente = (Cliente) sesion.createQuery("SELECT p FROM Cliente p where " + where).uniqueResult();
        } catch (HibernateException e) {
            //manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
        return cliente;
    }

    private void iniciaOperacion() {
//        try {
//            sesion = NewHibernateUtil.getSessionFactory().openSession();
//            tx = sesion.beginTransaction();
//        } catch (Exception e) {
//            iniciaOperacion();
//        }
//        
        try {
            sesion = mx.com.cdadb.persist.NewHibernateUtil.getSessionFactory().openSession();
            tx = sesion.beginTransaction();
        } catch (HibernateException e) {
            //JOptionPane.showMessageDialog(null, "Error.getmessage: " + e.getMessage()+" \nError.getCause: "+e.getCause());
            //iniciaOperacion();
            if (JOptionPane.showConfirmDialog(null, "No hay conexión con el servidor. \n Desea volver a intentarlo?", "Aviso importante.", JOptionPane.YES_NO_OPTION) == 0) {
                sesion.clear();
                iniciaOperacion();
            } else {
                sesion.close();
                sesion.clear();
            }
        }
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        //throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
        //JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + he.getMessage());
    }
}
