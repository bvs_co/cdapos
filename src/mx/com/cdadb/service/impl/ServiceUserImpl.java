/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.Query;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.service.ServiceUser;
import org.hibernate.HibernateException;

/**
 *
 * @author unknown
 */
public class ServiceUserImpl implements ServiceUser{
    @Override
    public List<User> findAll() {
        Services s = new Services();
        List<User> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from User");
            lista = (List<User>) query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<User> findBy(String where) {
        Services s = new Services();
        List<User> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from User u where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(User user) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(user);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(User user) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(user);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean delete(User user) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().delete(user);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public User getBy(String where) {
        Services s = new Services();
        User user = null;
        try {
            s.initOperation();
            user = (User) s.getSesion().createQuery("SELECT u FROM User u where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return user;
    }
}
