/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Clave;
import mx.com.cdadb.service.ServiceClave;
import mx.com.cdadb.util.LoggerUtil;

/**
 *
 * @author unknown
 */
public class ServiceClaveImpl implements ServiceClave{
    @Override
    public List<Clave> findAll() throws Exception{
        Services s = new Services();
        List<Clave> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Clave");
            lista = (List<Clave>) query.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Clave> findBy(String where) throws Exception{
        Services s = new Services();
        List<Clave> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Clave c where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(List<Clave> claves) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            for(Clave clave : claves){
                s.getSesion().persist(clave);
            }
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Clave clave) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(clave);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Clave clave) throws Exception{
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(clave);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Clave getBy(String where) throws Exception{
        Services s = new Services();
        Clave clave = null;
        try {
            s.initOperation();
            clave = (Clave) s.getSesion().createQuery("SELECT c FROM Clave c where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return clave;
    }
}
