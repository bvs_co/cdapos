package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Paquete;
import mx.com.cdadb.service.ServicePaquete;

public class ServicePaqueteImpl implements ServicePaquete{
    @Override
    public List<Paquete> findAll() {
        Services s = new Services();
        List<Paquete> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Paquete");
            lista = (List<Paquete>) query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Paquete> findBy(String where) {
        Services s = new Services();
        List<Paquete> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Paquete p where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(List<Paquete> paquetes) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            for(Paquete paquete : paquetes){
                s.getSesion().persist(paquete);
            }
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Paquete paquete) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(paquete);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean delete(Paquete paquete) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().delete(paquete);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public Paquete getBy(String where) {
        Services s = new Services();
        Paquete paquete = null;
        try {
            s.initOperation();
            paquete = (Paquete) s.getSesion().createQuery("SELECT p FROM Paquete p where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return paquete;
    }
}
