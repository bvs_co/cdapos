package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Detallepaquete;
import mx.com.cdadb.service.ServiceDetallePaquete;
import mx.com.cdadb.util.LoggerUtil;

public class ServiceDetallePaqueteImpl implements ServiceDetallePaquete{
    @Override
    public List<Detallepaquete> findAll() throws Exception{
        Services s = new Services();
        List<Detallepaquete> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Detallepaquete");
            lista = (List<Detallepaquete>) query.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Detallepaquete> findBy(String where) throws Exception{
        Services s = new Services();
        List<Detallepaquete> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Detallepaquete p where " + where);
            lista = q.list();
        } catch (HibernateException e) {
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(Detallepaquete detallePaquete) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(detallePaquete);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Detallepaquete detallePaquete) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(detallePaquete);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Detallepaquete detallePaquete) throws Exception{
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(detallePaquete);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Detallepaquete getBy(String where) throws Exception{
        Services s = new Services();
        Detallepaquete detallePaquete = null;
        try {
            s.initOperation();
            detallePaquete = (Detallepaquete) s.getSesion().createQuery("SELECT p FROM Detallepaquete p where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return detallePaquete;
    }
}
