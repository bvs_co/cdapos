/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.Query;
import mx.com.cdadb.entities.Tr;
import mx.com.cdadb.service.ServiceTr;

/**
 *
 * @author unknown
 */
public class ServiceTrImpl implements ServiceTr{

    @Override
    public List<Tr> findAll() {
        Services s = new Services();
        List<Tr> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Tr");
            lista = (List<Tr>) query.list();
        } catch (org.hibernate.HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Tr> findBy(String where) {
        Services s = new Services();
        List<Tr> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Tr t where " + where);
            lista = q.list();
        } catch (org.hibernate.HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(Tr tr) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(tr);
            s.getTx().commit();
            logrado = true;
        } catch (org.hibernate.HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Tr tr) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(tr);
            s.getTx().commit();
            logrado = true;
        } catch (org.hibernate.HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Tr tr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Tr getBy(String where) {
        Services s = new Services();
        Tr tr = null;
        try {
            s.initOperation();
            tr = (Tr) s.getSesion().createQuery("SELECT t FROM Tr t where " + where).uniqueResult();
        } catch (org.hibernate.HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return tr;
    }
    
}
