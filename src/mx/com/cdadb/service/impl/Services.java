package mx.com.cdadb.service.impl;

import mx.com.cdadb.persist.NewHibernateUtil;
import javax.swing.JOptionPane;
import mx.com.cdadb.util.Log;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import mx.com.cdadb.util.LoggerUtil;

/**
 *
 * @author unknown
 */
public class Services {

    private Session sesion;
    private Transaction tx;

    public Services() {
    }

    public void initOperation() {
        try {
            sesion = NewHibernateUtil.getSessionFactory().openSession();
            tx = sesion.beginTransaction();
        } catch (HibernateException e) {
            JOptionPane.showMessageDialog(null, "Error.getmessage: " + e.getMessage() + " \nError.getCause: " + e.getCause());
            //iniciaOperacion();
//            LoggerUtil.registrarError(e);
            Log.fatal(e.getMessage());
        }
    }

    public void manageException(HibernateException he) throws HibernateException {
        tx.rollback();
//        JOptionPane.showMessageDialog(null, "Error: \n" + he.getMessage()
//                + "\n" + he.getCause());
        LoggerUtil.registrarError(he);
    }

    public Session getSesion() {
        return sesion;
    }

    public void setSesion(Session sesion) {
        this.sesion = sesion;
    }

    public Transaction getTx() {
        return tx;
    }

    public void setTx(Transaction tx) {
        this.tx = tx;
    }
}
