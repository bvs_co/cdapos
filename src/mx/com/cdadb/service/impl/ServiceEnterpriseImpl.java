package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Clave;
import mx.com.cdadb.entities.ClaveId;
import mx.com.cdadb.entities.Enterprise;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.service.ServiceClave;
import mx.com.cdadb.service.ServiceEnterprise;

public class ServiceEnterpriseImpl implements ServiceEnterprise {

    @Override
    public List<Enterprise> findAll() throws Exception{
        Services s = new Services();
        List<Enterprise> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Enterprise");
            lista = (List<Enterprise>) query.list();
        } catch (HibernateException e) {
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Enterprise> findBy(String where) throws Exception{
        Services s = new Services();
        List<Enterprise> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Enterprise e where " + where);
            lista = q.list();
        } catch (HibernateException e) {
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public int add(Enterprise enterprise) throws Exception{
        Services s = new Services();
        int idd;
        //boolean logrado = false;
        try {
            s.initOperation();
            idd = (int) s.getSesion().save(enterprise);
            s.getTx().commit();
            //logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return idd;
    }

    @Override
    public boolean edit(Enterprise enterprise, List<Producto> nuevos, List<Producto> eliminar) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(enterprise);
            ServiceClave sClave = new ServiceClaveImpl();
            if (eliminar.size() > 0) {
                for (Producto p : eliminar) {
                    Clave clv = sClave.getBy("c.id.claveProductoClave = '"+p.getProductoClave()+"' and c.id.calveEnterpriseId = "+enterprise.getEnterpriseId());
                    //s.getSesion().delete(clv);
                    sClave.delete(clv);
                }
            }
            
            if (nuevos.size() > 0) {
                for (Producto product : nuevos) {
                    Clave clave = new Clave();
                    ClaveId idd = new ClaveId();
                    idd.setCalveEnterpriseId(enterprise.getEnterpriseId());
                    idd.setClaveProductoClave(product.getProductoClave());
                    clave.setId(idd);
                    clave.setProducto(product);
                    clave.setClaveStatus("ACT");
                    clave.setClaveFcreacion(new java.util.Date());
                    clave.setClaveFmodif(new java.util.Date());
                    s.getSesion().persist(clave);
                }
            }
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean delete(Enterprise ent) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().delete(ent);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public Enterprise getBy(String where) throws Exception{
        Services s = new Services();
        Enterprise area = null;
        try {
            s.initOperation();
            area = (Enterprise) s.getSesion().createQuery("SELECT e FROM Enterprise e where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return area;
    }
}
