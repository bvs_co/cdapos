package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Caja;
import mx.com.cdadb.service.ServiceCaja;
import mx.com.cdadb.util.LoggerUtil;

/**
 *
 * @author unknown
 */
public class ServiceCajaImpl implements ServiceCaja{
    @Override
    public List<Caja> findAll() throws Exception{
        Services s = new Services();
        List<Caja> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Caja");
            lista = (List<Caja>) query.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Caja> findBy(String where) throws Exception{
        Services s = new Services();
        List<Caja> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Caja c where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(Caja caja) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(caja);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Caja caja) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(caja);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Caja caja) throws Exception{
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(caja);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Caja getBy(String where) throws Exception{
        Services s = new Services();
        Caja caja = null;
        try {
            s.initOperation();
            caja = (Caja) s.getSesion().createQuery("SELECT c FROM Caja c where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return caja;
    }
}
