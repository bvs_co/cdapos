/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Tipopago;
import mx.com.cdadb.service.ServiceTipoPago;

/**
 *
 * @author bny
 */
public class ServiceTipopagoImpl implements ServiceTipoPago {

    @Override
    public List<Tipopago> findAll() throws Exception {
        Services s = new Services();
        List<Tipopago> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Tipopago");
            lista = (List<Tipopago>) query.list();
        } catch (HibernateException e) {
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Tipopago> findBy(String where) throws Exception {
        Services s = new Services();
        List<Tipopago> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Tipopago t where " + where);
            lista = q.list();
        } catch (HibernateException e) {
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(Tipopago tipopago) throws Exception {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(tipopago);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Tipopago tipopago) throws Exception {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(tipopago);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Tipopago tipopago) throws Exception {
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(tipopago);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Tipopago getBy(String where) throws Exception {
        Services s = new Services();
        Tipopago tipopago = null;
        try {
            s.initOperation();
            tipopago = (Tipopago) s.getSesion().createQuery("SELECT t FROM Tipopago t where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return tipopago;
    }
}
