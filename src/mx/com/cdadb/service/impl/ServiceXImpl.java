/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Xx;
import mx.com.cdadb.service.ServiceX;

/**
 *
 * @author unknown
 */
public class ServiceXImpl implements ServiceX {

    @Override
    public List<Xx> findAll() {
        Services s = new Services();
        List<Xx> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Xx");
            lista = (List<Xx>) query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Xx> findBy(String where) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(Xx x) {
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(x);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Xx x) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Xx x) {
        boolean logrado;
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(x);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public Xx getBy(String where) {
        Services s = new Services();
        Xx x = null;
        try {
            s.initOperation();
            x = (Xx) s.getSesion().createQuery("SELECT x FROM Xx x where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return x;
    }

}
