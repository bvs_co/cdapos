package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Detalledevolucion;
import mx.com.cdadb.service.ServiceDetalleDevolucion;
import mx.com.cdadb.util.LoggerUtil;

public class ServiceDetalleDevolucionImpl implements ServiceDetalleDevolucion{

    @Override
    public List<Detalledevolucion> findAll() throws Exception{
        Services s = new Services();
        List<Detalledevolucion> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Detalledevolucion");
            lista = (List<Detalledevolucion>) query.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Detalledevolucion> findBy(String where) throws Exception{
        Services s = new Services();
        List<Detalledevolucion> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Detalledevolucion d where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(Detalledevolucion detalledevolucion) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(detalledevolucion);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Detalledevolucion detalledevolucion) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(detalledevolucion);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Detalledevolucion detalledevolucion) throws Exception{
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(detalledevolucion);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Detalledevolucion getBy(String where) throws Exception{
        Services s = new Services();
        Detalledevolucion dt = null;
        try {
            s.initOperation();
            dt = (Detalledevolucion) s.getSesion().createQuery("SELECT d FROM Detalledevolucion d where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return dt;
    }
    
}
