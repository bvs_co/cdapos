
package mx.com.cdadb.service.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import mx.com.cdadb.entities.Area;
import mx.com.cdadb.service.ServiceArea;
import mx.com.cdadb.util.LoggerUtil;

public class ServiceAreaImpl implements ServiceArea{
    @Override
    public List<Area> findAll() throws Exception{
        Services s = new Services();
        List<Area> lista = null;
        try {
            s.initOperation();
            Query query = s.getSesion().createQuery("from Area");
            lista = (List<Area>) query.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public List<Area> findBy(String where){
        Services s = new Services();
        List<Area> lista = null;
        try {
            s.initOperation();
            Query q = s.getSesion().createQuery("from Area a where " + where);
            lista = q.list();
        } catch (HibernateException e) {
            LoggerUtil.registrarError(e);
        } finally {
            s.getSesion().close();
        }
        return lista;
    }

    @Override
    public boolean add(Area area) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().save(area);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public boolean edit(Area area) throws Exception{
        Services s = new Services();
        boolean logrado = false;
        try {
            s.initOperation();
            s.getSesion().update(area);
            s.getTx().commit();
            logrado = true;
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return logrado;
    }

    @Override
    public void delete(Area area) throws Exception{
        Services s = new Services();
        try {
            s.initOperation();
            s.getSesion().delete(area);
            s.getTx().commit();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
    }

    @Override
    public Area getBy(String where) throws Exception{
        Services s = new Services();
        Area area = null;
        try {
            s.initOperation();
            area = (Area) s.getSesion().createQuery("SELECT a FROM Area a where " + where).uniqueResult();
        } catch (HibernateException e) {
            s.manageException(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.getSesion().close();
        }
        return area;
    }
}
