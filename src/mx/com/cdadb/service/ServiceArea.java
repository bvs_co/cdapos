package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Area;

public interface ServiceArea {

    public List<Area> findAll() throws Exception;

    public List<Area> findBy(String where) throws Exception;

    public boolean add(Area area) throws Exception;

    public boolean edit(Area area) throws Exception;

    public void delete(Area area) throws Exception;

    public Area getBy(String where) throws Exception;
}
