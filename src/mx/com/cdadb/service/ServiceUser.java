
package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.User;

/**
 *
 * @author unknown
 */
public interface ServiceUser {

    public List<User> findAll() throws Exception;

    public List<User> findBy(String where) throws Exception;

    public boolean add(User user) throws Exception;

    public boolean edit(User user) throws Exception;

    public boolean delete(User user) throws Exception;

    public User getBy(String where) throws Exception;
}
