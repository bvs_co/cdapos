/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Paquete;

/**
 *
 * @author unknown
 */
public interface ServicePaquete {

    public List<Paquete> findAll() throws Exception;

    public List<Paquete> findBy(String where) throws Exception;

    public boolean add(List<Paquete> paquetes) throws Exception;

    public boolean edit(Paquete paquete) throws Exception;

    public boolean delete(Paquete paquete) throws Exception;

    public Paquete getBy(String where) throws Exception;
}
