/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Rol;

/**
 *
 * @author unknown
 */
public interface ServiceRol {

    public List<Rol> findAll() throws Exception;

    public List<Rol> findBy(String where) throws Exception;

    public boolean add(Rol rol) throws Exception;

    public boolean edit(Rol rol) throws Exception;

    public boolean delete(Rol rol) throws Exception;

    public Rol getBy(String where) throws Exception;
}
