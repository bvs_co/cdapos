package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Clave;

public interface ServiceClave {

    public List<Clave> findAll() throws Exception;

    public List<Clave> findBy(String where) throws Exception;

    public boolean add(List<Clave> paquetes) throws Exception;

    public boolean edit(Clave paquete) throws Exception;

    public void delete(Clave paquete) throws Exception;

    public Clave getBy(String where) throws Exception;
}
