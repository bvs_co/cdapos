package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Caja;
import mx.com.cdadb.entities.Folio;
import mx.com.cdadb.entities.Foliodevolucion;
import mx.com.cdadb.entities.customer.Cliente;

/**
 *
 * @author unknown
 */
public interface ServiceFolioDevolucion {

    public List<Foliodevolucion> findAll() throws Exception;

    public List<Foliodevolucion> findBy(String where) throws Exception;

    public boolean add(Foliodevolucion folio, Cliente cliente, String total, String motivo, Folio folioVenta, Caja caja) throws Exception;

    public boolean edit(Foliodevolucion folio) throws Exception;

    public void delete(Foliodevolucion folio) throws Exception;

    public Foliodevolucion getBy(String where) throws Exception;
}
