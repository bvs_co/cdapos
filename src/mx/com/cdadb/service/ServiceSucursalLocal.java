package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Sucursal;

/**
 *
 * @author unknown
 */
public interface ServiceSucursalLocal {

    public List<Sucursal> findAll() throws Exception;

    public List<Sucursal> findBy(String where) throws Exception;

    public boolean add(Sucursal sucursal) throws Exception;

    public boolean edit(Sucursal sucursal) throws Exception;

    public void delete(Sucursal sucursal) throws Exception;

    public Sucursal getBy(String where) throws Exception;
}
