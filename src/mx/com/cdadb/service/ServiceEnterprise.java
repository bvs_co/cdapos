package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Enterprise;
import mx.com.cdadb.entities.Producto;

public interface ServiceEnterprise {

    public List<Enterprise> findAll() throws Exception;

    public List<Enterprise> findBy(String where) throws Exception;

    public int add(Enterprise enterprise) throws Exception;

    public boolean edit(Enterprise enterprise, List<Producto> nuevos, List<Producto> eliminar) throws Exception;

    public boolean delete(Enterprise enterprise) throws Exception;

    public Enterprise getBy(String where) throws Exception;
}
