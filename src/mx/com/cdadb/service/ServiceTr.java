package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Tr;

/**
 *
 * @author unknown
 */
public interface ServiceTr {

    public List<Tr> findAll() throws Exception;

    public List<Tr> findBy(String where) throws Exception;

    public boolean add(Tr tr) throws Exception;

    public boolean edit(Tr tr) throws Exception;

    public void delete(Tr tr) throws Exception;

    public Tr getBy(String where) throws Exception;
}
