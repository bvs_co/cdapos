package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Tipopago;

/**
 *
 * @author bny
 */
public interface ServiceTipoPago {

    public List<Tipopago> findAll() throws Exception;

    public List<Tipopago> findBy(String where) throws Exception;

    public boolean add(Tipopago tipopago) throws Exception;

    public boolean edit(Tipopago tipopago) throws Exception;

    public void delete(Tipopago tipopago) throws Exception;

    public Tipopago getBy(String where) throws Exception;
}
