package mx.com.cdadb.service.customer.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import javax.swing.JOptionPane;
import mx.com.cdadb.entities.customer.Doctor;
import mx.com.cdadb.persist.NewHibernateUtil;
import mx.com.cdadb.service.customer.ServiceDoctor;
import mx.com.cdadb.util.LoggerUtil;

public class ServiceDoctorImpl implements ServiceDoctor {

    private Session sesion;
    private Transaction tx;

    @Override
    public List<Doctor> mostrarDoctores() {
        List<Doctor> lista = null;
        try {
            iniciaOperacion();
            Query query = sesion.createQuery("from Doctor");
            lista = (List<Doctor>) query.list();
        } catch (HibernateException e) {
            if ("Could not open connection".equals(e.getMessage())) {
                JOptionPane.showMessageDialog(null, "No hay conexión con el servidor.", "Error de conexión.", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/seek/mx/pv/cliente/imagenes/iconos/iconLogo.png")));
            } else {
                JOptionPane.showMessageDialog(null, "Err: " + e.getMessage());
            }
        } finally {
            sesion.close();
        }
        return lista;
    }

    @Override
    public long insertarDoctor(Doctor doctor) {
        long logrado = -1;

        try {
            iniciaOperacion();
            logrado = (Long) sesion.save(doctor);
            tx.commit();
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
        return logrado;
    }

    @Override
    public boolean modificarDoctor(Doctor doctor) {
        boolean logrado = false;
        try {
            iniciaOperacion();
            sesion.update(doctor);
            tx.commit();
            logrado = true;
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
        return logrado;
    }

    @Override
    public void eliminarDoctor(Doctor doctor) {
        try {
            iniciaOperacion();
            sesion.delete(doctor);
            tx.commit();
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Doctor obtenerDoctor(String where) {
        Doctor doctor = null;
        try {
            iniciaOperacion();
            doctor = (Doctor) sesion.createQuery("SELECT p FROM Doctor p where " + where).uniqueResult();
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
        return doctor;
    }

    private void iniciaOperacion() {
        try {
            sesion = NewHibernateUtil.getSessionFactory().openSession();
            tx = sesion.beginTransaction();
        } catch (Exception e) {
            LoggerUtil.registrarError(e);
            iniciaOperacion();
        }
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }
}
