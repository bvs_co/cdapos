/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.customer.impl;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import mx.com.cdadb.entities.customer.Recomendacion;
import mx.com.cdadb.persist.customer.NewHibernateUtil;
import mx.com.cdadb.service.customer.ServiceRecomndacion;

/**
 *
 * @author unknown
 */
public class ServiceRecomendacionImpl implements ServiceRecomndacion{
    private Session sesion;
    private Transaction tx;

    @Override
    public List<Recomendacion> mostrarRecomendaciones() {
        List<Recomendacion> lista = null;
        try {
            iniciaOperacion();
            Query query = sesion.createQuery("from Recomendacion");
            lista = (List<Recomendacion>) query.list();
        } catch (HibernateException e) {
            manejaExcepcion(e);
        } finally {
            sesion.close();
        }
        return lista;
    }

    @Override
    public long insertarRecomendacion(Recomendacion recomendacion) {
        long logrado = -1;

        try {
            iniciaOperacion();
            logrado = (Long) sesion.save(recomendacion);
            tx.commit();
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
        return logrado;
    }

    @Override
    public boolean modificarRecomendacion(Recomendacion recomendacion) {
        boolean logrado = false;
        try {
            iniciaOperacion();
            sesion.update(recomendacion);
            tx.commit();
            logrado = true;
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
        return logrado;
    }

    @Override
    public void eliminarRecomendacion(Recomendacion recomendacion) {
        try {
            iniciaOperacion();
            sesion.delete(recomendacion);
            tx.commit();
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
    }

    @Override
    public Recomendacion obtenerRecomendacion(String where) {
        Recomendacion recom = null;
        try {
            iniciaOperacion();
            recom = (Recomendacion) sesion.createQuery("SELECT p FROM Recomendacion p where " + where).uniqueResult();
        } catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;
        } finally {
            sesion.close();
        }
        return recom;
    }
    
    private void iniciaOperacion() throws HibernateException {
        sesion = NewHibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException e) throws HibernateException {
        tx.rollback();
    }
}
