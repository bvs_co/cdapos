package mx.com.cdadb.service.customer.impl;

import java.sql.Connection;
import mx.com.cdadb.persist.NewHibernateUtil;
import javax.swing.JOptionPane;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author unknown
 */
public class Services_ {

    private Session sesion;
    private Transaction tx;

    public Services_() {
    }

    public void iniciaOperacion() {
        try {
            sesion = NewHibernateUtil.getSessionFactory().openSession();
            tx = sesion.beginTransaction();
        } catch (HibernateException e) {
            if ("Could not open connection".equals(e.getMessage())) {
                if (JOptionPane.showConfirmDialog(null, "No hay conexión con el servidor. \n Desea volver a intentarlo?", "Aviso importante.", JOptionPane.YES_NO_OPTION) == 0) {
                    sesion.clear();
                    iniciaOperacion();
                }else{
                    sesion.clear();
                }
            }
            //JOptionPane.showMessageDialog(null, "Services_.getmessage: " + e.getMessage()+" \nError.getCause: "+e.getCause());

        }
    }

    public void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        JOptionPane.showMessageDialog(null, "Error: \n" + he.getMessage()
                + "\n" + he.getCause());
    }

    public Session getSesion() {
        return sesion;
    }

    public void setSesion(Session sesion) {
        this.sesion = sesion;
    }

    public Transaction getTx() {
        return tx;
    }

    public void setTx(Transaction tx) {
        this.tx = tx;
    }
}
