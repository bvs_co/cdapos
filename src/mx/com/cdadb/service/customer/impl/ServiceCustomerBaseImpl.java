/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.customer.impl;

import java.util.List;
import javax.swing.JOptionPane;
import mx.com.cdadb.util.LoggerUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author macbook
 * @param <T>
 */
public class ServiceCustomerBaseImpl<T> {

    private Session s;
    private Transaction tx;

    public List<T> findAll(T t) {
//        System.out.println("CLASS:"+t.getClass().getSimpleName());
        List<T> list = null;
        try {
            iniciaOperacion();
            Query query = s.createQuery("from " + t.getClass().getSimpleName());
            list = (List<T>) query.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            //LoggerUtil.registrarError(e);
        } finally {
            s.close();
        }
        return list;
    }

    public List<T> findBy(T t, String where) throws Exception {
        List<T> list = null;
        try {
            iniciaOperacion();
            Query q = s.createQuery("from " + t.getClass().getSimpleName() + " t where " + where);
            list = q.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            //LoggerUtil.registrarError(e);
        } finally {
            s.close();
        }
        return list;
    }

    public boolean add(List<T> objects) throws Exception {
        boolean logrado = false;
        try {
            iniciaOperacion();
            for (T object : objects) {
                s.persist(object);
            }
            tx.commit();
            logrado = true;
        } catch (HibernateException e) {
            manejaExcepcion(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.close();
        }
        return logrado;
    }

    public boolean edit(T object) throws Exception {
        boolean logrado = false;
        try {
            iniciaOperacion();
            s.update(object);
            tx.commit();
            logrado = true;
        } catch (HibernateException e) {
            manejaExcepcion(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.close();
        }
        return logrado;
    }

    public void delete(T object) throws Exception {
        try {
            iniciaOperacion();
            s.delete(object);
            tx.commit();
        } catch (HibernateException e) {
            manejaExcepcion(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.close();
        }
    }

    public T getBy(T t, String where) throws Exception {
        T object = null;
        try {
            iniciaOperacion();
            object = (T) s.createQuery("SELECT t FROM " + t.getClass().getSimpleName() + " t where " + where).uniqueResult();
        } catch (HibernateException e) {
            manejaExcepcion(e);
            LoggerUtil.registrarError(e);
            throw e;
        } finally {
            s.close();
        }
        return object;
    }

    private void iniciaOperacion() {
//        try {
//            sesion = NewHibernateUtil.getSessionFactory().openSession();
//            tx = sesion.beginTransaction();
//        } catch (Exception e) {
//            iniciaOperacion();
//        }
//        
        try {
            s = mx.com.cdadb.persist.customer.NewHibernateUtil.getSessionFactory().openSession();
            tx = s.beginTransaction();
        } catch (HibernateException e) {
            //JOptionPane.showMessageDialog(null, "Error.getmessage: " + e.getMessage()+" \nError.getCause: "+e.getCause());
            //iniciaOperacion();
            if (JOptionPane.showConfirmDialog(null, "No hay conexión con el servidor. \n Desea volver a intentarlo?", "Aviso importante.", JOptionPane.YES_NO_OPTION) == 0) {
                s.clear();
                iniciaOperacion();
            } else {
                s.close();
                s.clear();
            }
        }
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        //throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
        //JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + he.getMessage());
    }
}
