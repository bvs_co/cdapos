package mx.com.cdadb.service.customer;

import java.util.List;
import mx.com.cdadb.entities.customer.Doctor;

public interface ServiceDoctor {

    public List<Doctor> mostrarDoctores();

    public long insertarDoctor(Doctor doctor);

    public boolean modificarDoctor(Doctor doctor);

    public void eliminarDoctor(Doctor doctor);

    public Doctor obtenerDoctor(String where);
}
