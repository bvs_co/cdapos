/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.customer;

import java.util.List;
import mx.com.cdadb.entities.customer.Recomendacion;

/**
 *
 * @author unknown
 */
public interface ServiceRecomndacion {
    public List<Recomendacion> mostrarRecomendaciones();
    public long insertarRecomendacion(Recomendacion recomendacion);
    public boolean modificarRecomendacion(Recomendacion recomendacion);
    public void eliminarRecomendacion(Recomendacion recomendacion);
    public Recomendacion obtenerRecomendacion(String where);
}
