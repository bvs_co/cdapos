/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service.customer;

import mx.com.cdadb.entities.customer.Cliente;
import java.util.List;

/**
 *
 * @author unknown
 */
public interface ServiceCliente {

    public List<Cliente> mostrarClientes();

    public List<Cliente> mostrarClientes(String where);

    public long insertarCliente(Cliente cliente);

    public boolean modificarCliente(Cliente cliente);

    public void eliminarCliente(Cliente cliente);

    public Cliente obtenerCliente(String where);
}
