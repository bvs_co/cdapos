package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Detalledevolucion;

/**
 *
 * @author unknown
 */
public interface ServiceDetalleDevolucion {

    public List<Detalledevolucion> findAll() throws Exception;

    public List<Detalledevolucion> findBy(String where) throws Exception;

    public boolean add(Detalledevolucion detalledevolucion) throws Exception;

    public boolean edit(Detalledevolucion detalledevolucion) throws Exception;

    public void delete(Detalledevolucion detalledevolucion) throws Exception;

    public Detalledevolucion getBy(String where) throws Exception;
}
