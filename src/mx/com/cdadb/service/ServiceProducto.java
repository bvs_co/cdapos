/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Producto;

/**
 *
 * @author unknown
 */
public interface ServiceProducto {

    public List<Producto> findAll() throws Exception;

    public List<Producto> findBy(String where) throws Exception;

    public boolean add(Producto producto) throws Exception;

    public boolean edit(Producto producto) throws Exception;

    public boolean delete(Producto producto) throws Exception;

    public Producto getBy(String where) throws Exception;
}
