/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service;

import mx.com.cdadb.entities.Cliente;
import java.util.List;

/**
 *
 * @author unknown
 */
public interface ServiceCliente {

    public List<Cliente> findAll();

    public List<Cliente> findBy(String where);

    public long add(Cliente cliente);

    public boolean edit(Cliente cliente);

    public void delete(Cliente cliente);

    public Cliente getBy(String where);
}
