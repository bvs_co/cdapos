package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Detallepaquete;

public interface ServiceDetallePaquete {

    public List<Detallepaquete> findAll()  throws Exception;

    public List<Detallepaquete> findBy(String where) throws Exception;

    public boolean add(Detallepaquete detallePaquete) throws Exception;

    public boolean edit(Detallepaquete detallePaquete) throws Exception;

    public void delete(Detallepaquete detallePaquete) throws Exception;

    public Detallepaquete getBy(String where) throws Exception;
}
