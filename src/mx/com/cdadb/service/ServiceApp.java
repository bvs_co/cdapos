/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.App;

/**
 *
 * @author unknown
 */
public interface ServiceApp {

    public List<App> findAll() throws Exception;

    public List<App> findBy(String where) throws Exception;

    public boolean add(App app) throws Exception;
    
    public boolean editList(List<App> apps) throws Exception;

    public boolean edit(App app) throws Exception;

    public void delete(App app) throws Exception;

    public App getBy(String where) throws Exception;
}
