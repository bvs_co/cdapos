/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Ordenlab;
import mx.com.cdadb.entities.OrdenlabId;

/**
 *
 * @author unknown
 */
public interface ServiceOrdenlab {

    public List<Ordenlab> findAll() throws Exception;

    public List<Ordenlab> findBy(String where) throws Exception;

    public OrdenlabId add(Ordenlab orden) throws Exception;

    public boolean edit(Ordenlab orden) throws Exception;

    public void delete(Ordenlab orden) throws Exception;

    public Ordenlab getBy(String where);
}
