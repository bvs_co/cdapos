/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Caja;

/**
 *
 * @author unknown
 */
public interface ServiceCaja {

    public List<Caja> findAll() throws Exception;

    public List<Caja> findBy(String where) throws Exception;

    public boolean add(Caja caja) throws Exception;

    public boolean edit(Caja caja) throws Exception;

    public void delete(Caja caja) throws Exception;

    public Caja getBy(String where) throws Exception;
}
