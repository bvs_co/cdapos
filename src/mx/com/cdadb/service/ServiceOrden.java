package mx.com.cdadb.service;

import java.util.List;
import java.util.Map;
import mx.com.cdadb.entities.Foliodevolucion;
import mx.com.cdadb.entities.Orden;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.entities.Xx;
import mx.com.cdadb.entities.customer.Cliente;

public interface ServiceOrden {

    public List<Orden> findAll() throws Exception;

    public List<Orden> findBy(String where) throws Exception;

    public Map addList(List<Orden> ordenes, List<Producto> ordenesLab, Cliente cliente, String total, String totalLetra, List<Producto> productostotal, User usr, List<Xx> arrX) throws Exception;

    public boolean edit(Orden orden) throws Exception;
    
    public boolean editBy(String where) throws Exception;
    
    public boolean editList(List<Orden> ordenes, List<Foliodevolucion> fDevs) throws Exception;

    public boolean delete(java.util.Date fecha) throws Exception;

    public Orden getBy(String where) throws Exception;
}
