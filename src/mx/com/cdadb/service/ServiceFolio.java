/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Folio;

/**
 *
 * @author unknown
 */
public interface ServiceFolio {
    public List<Folio> findAll() throws Exception;

    public List<Folio> findBy(String where) throws Exception;

    public boolean add(Folio folio) throws Exception;

    public boolean edit(Folio folio) throws Exception;

    public void delete(Folio folio) throws Exception;

    public Folio getBy(String where) throws Exception;
}
