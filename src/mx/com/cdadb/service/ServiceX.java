package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Xx;

/**
 *
 * @author unknown
 */
public interface ServiceX {

    public List<Xx> findAll()  throws Exception;

    public List<Xx> findBy(String where) throws Exception;

    public boolean add(Xx x) throws Exception;

    public boolean edit(Xx x) throws Exception;

    public boolean delete(Xx x) throws Exception;

    public Xx getBy(String where) throws Exception;
}
