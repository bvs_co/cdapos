/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.service;

import java.util.List;
import mx.com.cdadb.entities.Modulo;

/**
 *
 * @author macbook
 */
public interface ServiceModulo {

    public List<Modulo> findAll() throws Exception;

    public List<Modulo> findBy(String where) throws Exception;

    public boolean add(Modulo modulo) throws Exception;

    public boolean edit(Modulo modulo) throws Exception;

    public boolean delete(Modulo modulo) throws Exception;

    public Modulo getBy(String where) throws Exception;
}
