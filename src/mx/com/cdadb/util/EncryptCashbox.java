package mx.com.cdadb.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import javax.swing.JOptionPane;
import mx.com.cdadb.constants.ConstantMessages;
import org.jasypt.encryption.pbe.PBEStringEncryptor;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;
import org.jasypt.properties.PropertyValueEncryptionUtils;
import mx.com.cdadb.entities.Caja;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.view.NuevaCaja_;
import mx.com.cdadb.view.FrMenu;

public class EncryptCashbox {

    public EncryptCashbox(String dir, Caja c) throws FileNotFoundException, IOException {
        //cargamos en un objeto Properties los valores del fichero original.properties
        Properties props = new Properties();
        props.load(new FileInputStream(dir));
        String id = props.getProperty("seek.mx.pv.pos.Id");
        String nombre = props.getProperty("seek.mx.pv.pos.Nombre");
        String mac = props.getProperty("seek.mx.pv.pos.Mac");
        String ip = props.getProperty("seek.mx.pv.pos.Ip");
        String status = props.getProperty("seek.mx.pv.pos.Status");
        String modif = props.getProperty("seek.mx.pv.pos.Modif");
        String creacion = props.getProperty("seek.mx.pv.pos.Creacion");

        PBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(KeyGenerator.PASSWORD_ENCRYPTOR_BOX_PROPERTIES);

        //encriptamos el valor
        String id_ = PropertyValueEncryptionUtils.encrypt(id, encryptor);
        String nombre_ = PropertyValueEncryptionUtils.encrypt(nombre, encryptor);
        String mac_ = PropertyValueEncryptionUtils.encrypt(mac, encryptor);
        String ip_ = PropertyValueEncryptionUtils.encrypt(ip, encryptor);
        String status_ = PropertyValueEncryptionUtils.encrypt(status, encryptor);
        String modif_ = PropertyValueEncryptionUtils.encrypt(modif, encryptor);
        String creacion_ = PropertyValueEncryptionUtils.encrypt(creacion, encryptor);

        //almacenamos en el objeto de propiedades el valor que hemos encriptado
        props.setProperty("seek.mx.pv.pos.Id", id_);
        props.setProperty("seek.mx.pv.pos.Nombre", nombre_);
        props.setProperty("seek.mx.pv.pos.Mac", mac_);
        props.setProperty("seek.mx.pv.pos.Ip", ip_);
        props.setProperty("seek.mx.pv.pos.Status", status_);
        props.setProperty("seek.mx.pv.pos.Modif", modif_);
        props.setProperty("seek.mx.pv.pos.Creacion", creacion_);
        //guardamos el objeto de propiedades en un nuevo fichero properties
        OutputStream os = new FileOutputStream("src/mx/com/cdadb/view/CokGuRmkwYcfu+0tD3ouso9oyLi8znga.properties");
        props.store(os, "Fichero generado automaticamente");
    }

    public EncryptCashbox() {
    }

//    public static void main(String[] args) throws IOException {
//        PropertiesEncrypt p = new PropertiesEncrypt();
//        p.decrypt("src/seek/mx/pv/vistas");
//    }
    public Caja decrypt(String dir, FrMenu menu) throws Exception {
        PBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword("xxx");
        Caja caja = null;

        Properties props = new EncryptableProperties(encryptor);
        try {
            props.load(new FileInputStream(dir));

            String id = props.getProperty("seek.mx.pv.pos.Id");
            String nombre = props.getProperty("seek.mx.pv.pos.Nombre");
            String mac = props.getProperty("seek.mx.pv.pos.Mac");
            String ip = props.getProperty("seek.mx.pv.pos.Ip");
            String status = props.getProperty("seek.mx.pv.pos.Status");
            String modif = props.getProperty("seek.mx.pv.pos.Modif");
            String creacion = props.getProperty("seek.mx.pv.pos.Creacion");

            caja = new Caja();
            caja.setCajaId(Integer.parseInt(id));
            caja.setCajaFcreacion(new java.util.Date());
            caja.setCajaFmodif(new java.util.Date());
            caja.setCajaIp(ip);
            caja.setCajaMac(mac);
            caja.setCajaNombre(nombre);
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
            NuevaCaja_ nCaja = new NuevaCaja_(menu, true, this, 1, menu);
            nCaja.setVisible(true);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
            NuevaCaja_ nCaja = new NuevaCaja_(menu, true, this, 1, menu);
            nCaja.setVisible(true);
        }
        return caja;
    }

    public Caja decrypt(String dir, FrMenu menu, User u) throws Exception {
        PBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(KeyGenerator.PASSWORD_ENCRYPTOR_BOX_PROPERTIES);
        Caja caja = null;

        Properties props = new EncryptableProperties(encryptor);
        try {
            props.load(new FileInputStream(dir));

            String id = props.getProperty("seek.mx.pv.pos.Id");
            String nombre = props.getProperty("seek.mx.pv.pos.Nombre");
            String mac = props.getProperty("seek.mx.pv.pos.Mac");
            String ip = props.getProperty("seek.mx.pv.pos.Ip");
            //String status = props.getProperty("seek.mx.pv.pos.Status");
            //String modif = props.getProperty("seek.mx.pv.pos.Modif");
            //String creacion = props.getProperty("seek.mx.pv.pos.Creacion");

            caja = new Caja();
            caja.setCajaId(Integer.parseInt(id));
            caja.setCajaFcreacion(new java.util.Date());
            caja.setCajaFmodif(new java.util.Date());
            caja.setCajaIp(ip);
            caja.setCajaMac(mac);
            caja.setCajaNombre(nombre);
        } catch (FileNotFoundException ex) {
            Log.error(ex.getMessage());
            NuevaCaja_ nCaja = new NuevaCaja_(menu, true, this, 1, menu, u);
            nCaja.setVisible(true);
        } catch (IOException ex) {
            Log.error(ex.getMessage());
            NuevaCaja_ nCaja = new NuevaCaja_(menu, true, this, 1, menu, u);
            nCaja.setVisible(true);
        }
        return caja;
    }

    public Caja decrypt(String dir) {

        PBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(KeyGenerator.PASSWORD_ENCRYPTOR_BOX_PROPERTIES);

        Properties props = new EncryptableProperties(encryptor);
        try {
            props.load(new FileInputStream(dir));
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
            NuevaCaja_ nCaja = new NuevaCaja_(null, true, this, 2);
            nCaja.setVisible(true);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
            NuevaCaja_ nCaja = new NuevaCaja_(null, true, this, 2);
            nCaja.setVisible(true);
        }
        String id = props.getProperty("seek.mx.pv.pos.Id");
        String nombre = props.getProperty("seek.mx.pv.pos.Nombre");
        String mac = props.getProperty("seek.mx.pv.pos.Mac");
        String ip = props.getProperty("seek.mx.pv.pos.Ip");

        Caja caja = new Caja();
        caja.setCajaId(Integer.parseInt(id));
        caja.setCajaFcreacion(new java.util.Date());
        caja.setCajaFmodif(new java.util.Date());
        caja.setCajaIp(ip);
        caja.setCajaMac(mac);
        caja.setCajaNombre(nombre);
        return caja;
    }

    public boolean createdProperties(Caja c, String dir) throws FileNotFoundException, IOException {

        PBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(KeyGenerator.PASSWORD_ENCRYPTOR_BOX_PROPERTIES);
        Properties props = new Properties();

        String id_ = PropertyValueEncryptionUtils.encrypt(String.valueOf(c.getCajaId()), encryptor);
        String nombre_ = PropertyValueEncryptionUtils.encrypt(c.getCajaNombre(), encryptor);
        String mac_ = PropertyValueEncryptionUtils.encrypt(c.getCajaMac(), encryptor);
        String ip_ = PropertyValueEncryptionUtils.encrypt(c.getCajaIp(), encryptor);
        //String status_ = PropertyValueEncryptionUtils.EncryptCashbox(c.getCajaStatus(), encryptor);
        String modif_ = PropertyValueEncryptionUtils.encrypt(FormatDateUtil.SFD_DDMMYYYY_HHMMSS_GUION.format(c.getCajaFmodif()), encryptor);
        String creacion_ = PropertyValueEncryptionUtils.encrypt(FormatDateUtil.SFD_DDMMYYYY_HHMMSS_GUION.format(c.getCajaFcreacion()), encryptor);

        props.setProperty("seek.mx.pv.pos.Id", id_);
        props.setProperty("seek.mx.pv.pos.Nombre", nombre_);
        props.setProperty("seek.mx.pv.pos.Mac", mac_);
        props.setProperty("seek.mx.pv.pos.Ip", ip_);
        //props.setProperty("seek.mx.pv.pos.Status", status_);
        props.setProperty("seek.mx.pv.pos.Modif", modif_);
        props.setProperty("seek.mx.pv.pos.Creacion", creacion_);

        OutputStream os = new FileOutputStream(dir);
        props.store(os, "Fichero generado automaticamente");
        return true;
    }
}
