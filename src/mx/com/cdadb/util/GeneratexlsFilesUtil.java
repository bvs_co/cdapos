package mx.com.cdadb.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import mx.com.cdadb.constants.ConstantConfig;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Devolucion;
import mx.com.cdadb.entities.Folio;
import mx.com.cdadb.entities.Foliodevolucion;
import mx.com.cdadb.entities.Orden;
import mx.com.cdadb.entities.Ordenlab;
import mx.com.cdadb.entities.Cliente;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceFolioDevolucion;
import mx.com.cdadb.service.ServiceOrden;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.service.impl.ServiceFolioDevolucionImpl;
import mx.com.cdadb.service.impl.ServiceOrdenImpl;
import org.jasypt.util.text.BasicTextEncryptor;

public class GeneratexlsFilesUtil {

    private static final String ENCRYPTOR = "Fundacion.CDA$";

    /**
     * Crea el archvo 1 para la contabilidad
     *
     * @param url Dirección donde se generará el archivo
     * @param sucursal Nombre de la sucursal donde se está generando el archivo
     * @param date Fecha en que se genera el archivo
     * @param customers Lista de customers con que se generará el archivo
     * @return True, si se crea correctamente el archivo
     * @throws Exception
     */
    public boolean generatexlsOne(String url, String sucursal, java.util.Date date, List<Cliente> customers) throws Exception {
        boolean ok = false;

        ServiceOrden serviceOrder = new ServiceOrdenImpl();

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Hoja 1");

        //created File
        File fileone = new File(url + "/" + FormatDateUtil.SFD_DMY.format(date) + "_1.xls");
        //Created row 0
        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = row.createCell(0);//Put the text in the fisrt cell of the fisrt column

        cell.setCellValue(sucursal + " Hormonales " + FormatDateUtil.SFD_DMY.format(date));

        //Columns title
        String[] titulos = {"CU", "Paciente", "Nombre", "Paterno", "Materno", "Razon", "cp", "Calle", "Nointerior", "Noexterior", "Colonia", "Ciudad", "Delegacion", "Estado", "Pais", "Telefono", "Rfc", "Feha nac",
            "Edad", "Talla", "Sexo", "Correo", "Doctor", "Emaildoctor", "Teldoctor", "Click"};

        //create row 1
        HSSFRow rowAux = sheet.createRow(1);
        HSSFCell cellAux;

        //Add the title of columns in the row 1
        for (int i = 0; i < titulos.length; i++) {
            cellAux = rowAux.createCell(i);
            cellAux.setCellValue(titulos[i]);
        }

        //style cell
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle = workbook.createCellStyle();
        HSSFFont hSSFFont = workbook.createFont();
        hSSFFont.setFontName(HSSFFont.FONT_ARIAL);
        hSSFFont.setFontHeightInPoints((short) 16);
        hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        hSSFFont.setColor(HSSFColor.BLACK.index);
        cellStyle.setFont(hSSFFont);
        cell.setCellStyle(cellStyle);

        List<Orden> orders = serviceOrder.findBy("o.ordenFecha = '" + FormatDateUtil.SFD_YYYYMMDD.format(date) + "'");
        //fomat cell
        CreationHelper createHelper = workbook.getCreationHelper();

        int j = 2;
        for (Orden order : orders) {
            for (Cliente cliente : customers) {
                if ((long) cliente.getClienteId() == order.getOrdenPacienteId()/*&& (order.getOrdenTotal() > 0 || order.getOrdenTotal() < 0)*/) {
                    //new rowAux
                    rowAux = sheet.createRow(j);
                    cellAux = rowAux.createCell(0);
                    cellAux.setCellValue(order.getTr().getId().getTrValor());

                    cellAux = rowAux.createCell(1);
                    cellAux.setCellValue(cliente.getClienteId());

                    cellAux = rowAux.createCell(2);
                    cellAux.setCellValue(cliente.getClienteNombre());

                    cellAux = rowAux.createCell(4);
                    cellAux.setCellValue(cliente.getClientePaterno());

                    cellAux = rowAux.createCell(3);
                    cellAux.setCellValue(cliente.getClienteMaterno());

                    cellAux = rowAux.createCell(5);
                    cellAux.setCellValue(cliente.getClienteRazonSoc());

                    cellAux = rowAux.createCell(6);
                    cellAux.setCellValue("");//CP

                    cellAux = rowAux.createCell(7);
                    cellAux.setCellValue(cliente.getClienteCalle());

                    cellAux = rowAux.createCell(8);
                    cellAux.setCellValue(cliente.getClienteNumInt());

                    cellAux = rowAux.createCell(9);
                    cellAux.setCellValue(cliente.getClienteNumExt());

                    cellAux = rowAux.createCell(10);
                    cellAux.setCellValue(cliente.getClienteColonia());

                    cellAux = rowAux.createCell(11);
                    cellAux.setCellValue(cliente.getClienteCiudad());

                    cellAux = rowAux.createCell(12);
                    cellAux.setCellValue("");//Delegacion

                    cellAux = rowAux.createCell(13);
                    cellAux.setCellValue(cliente.getClienteEdo());

                    cellAux = rowAux.createCell(14);
                    cellAux.setCellValue("");//Pais

                    cellAux = rowAux.createCell(15);
                    cellAux.setCellValue(cliente.getClienteTel());

                    cellAux = rowAux.createCell(16);
                    cellAux.setCellValue(cliente.getClienteRfc());

                    //format cell aux
                    CellStyle dateStyle = workbook.createCellStyle();
                    dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd/MM/yyyy"));
                    cellAux = rowAux.createCell(17);
                    cellAux.setCellValue(cliente.getClienteFnac());
                    cellAux.setCellStyle(dateStyle);

                    //age calculation
                    cellAux = rowAux.createCell(18);
                    cellAux.setCellValue(CalculateAgeUtil.calculate(FormatDateUtil.SFD_DMY.format(cliente.getClienteFnac())));//Edad

                    cellAux = rowAux.createCell(19);
                    cellAux.setCellValue("");//height

                    if (cliente.getClienteGenero()) {
                        cellAux = rowAux.createCell(20);
                        cellAux.setCellValue("M");
                    } else {
                        cellAux = rowAux.createCell(20);
                        cellAux.setCellValue("F");
                    }

                    cellAux = rowAux.createCell(21);
                    cellAux.setCellValue(cliente.getClienteEmail());

                    //recomendations
                    String nombreDoctor = "";
                    String emailDoctor = "";
                    String telefonoDoctor = "";

                    nombreDoctor = "";
                    emailDoctor = "";
                    telefonoDoctor = "";

                    cellAux = rowAux.createCell(22);
                    cellAux.setCellValue(nombreDoctor);//Doctor

                    cellAux = rowAux.createCell(23);
                    cellAux.setCellValue(emailDoctor);//Email doctor

                    cellAux = rowAux.createCell(24);
                    cellAux.setCellValue(telefonoDoctor);//Phone doctor

                    cellAux = rowAux.createCell(25);
                    cellAux.setCellValue(FormatDateUtil.SFD_KKMMSS.format(order.getOrdenHora()));

                    nombreDoctor = "";
                    emailDoctor = "";
                    telefonoDoctor = "";

                    j++;
                }
            }
        }

        sheet.autoSizeColumn((short) 0);
        sheet.autoSizeColumn((short) 1);
        sheet.autoSizeColumn((short) 2);
        sheet.autoSizeColumn((short) 3);
        sheet.autoSizeColumn((short) 4);
        sheet.autoSizeColumn((short) 5);
        sheet.autoSizeColumn((short) 6);
        sheet.autoSizeColumn((short) 7);
        sheet.autoSizeColumn((short) 8);
        sheet.autoSizeColumn((short) 9);
        sheet.autoSizeColumn((short) 10);
        sheet.autoSizeColumn((short) 11);
        sheet.autoSizeColumn((short) 12);
        sheet.autoSizeColumn((short) 13);
        sheet.autoSizeColumn((short) 14);
        sheet.autoSizeColumn((short) 15);
        sheet.autoSizeColumn((short) 16);
        sheet.autoSizeColumn((short) 17);
        sheet.autoSizeColumn((short) 18);
        sheet.autoSizeColumn((short) 19);
        sheet.autoSizeColumn((short) 20);
        sheet.autoSizeColumn((short) 21);
        sheet.autoSizeColumn((short) 22);
        sheet.autoSizeColumn((short) 23);
        sheet.autoSizeColumn((short) 24);
        sheet.autoSizeColumn((short) 25);

        //write file
        try {
            FileOutputStream out = new FileOutputStream(fileone);
            workbook.write(out);
            //MessageOptionPane.information(null, "Aviso", "Se creó exitosamente el fileone, en la ruta especificada.");
            ok = true;
            out.close();
        } catch (IOException e) {
            LoggerUtil.registrarError(e);
        }
        return ok;
    }

    /**
     * Crea el archivo 2 para la contabilidad
     *
     * @param url Dirección donde se generará el archivo
     * @param sucursal Nombre de la sucursal donde se está generando el archivo
     * @param date Fecha en que se genera el archivo
     * @param customers Lista de customers con que se generará el archivo
     * @return True, si se crea correctamente el archivo
     * @throws Exception
     */
    public boolean generatexlsTwo(String url, String sucursal, java.util.Date date, List<Cliente> customers) throws Exception {
        boolean ok = false;

        ServiceOrden serviceOrder = new ServiceOrdenImpl();

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Hoja 1");

        //Crea archivo
        File fileTwo = new File(url + "/" + FormatDateUtil.SFD_DMY.format(date) + "_2.xls");
        //Crea la rowAux 0
        HSSFRow fila1 = sheet.createRow(0);
        HSSFCell celda1 = fila1.createCell(0);//Pon el texto en la primera columna

        celda1.setCellValue(sucursal + " BI-RADS " + FormatDateUtil.SFD_DMY.format(date));

        //Titulos de columnas
        String[] titulos = {"CU", "RX"};

        //Crea la rowAux 1
        HSSFRow fila = sheet.createRow(1);
        HSSFCell celda;

        //Agrega el titulo de las columnas en la rowAux 1
        for (int i = 0; i < titulos.length; i++) {
            celda = fila.createCell(i);
            celda.setCellValue(titulos[i]);
        }

        // Estilo para la celda1
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle = workbook.createCellStyle();
        HSSFFont hSSFFont = workbook.createFont();
        hSSFFont.setFontName(HSSFFont.FONT_ARIAL);
        hSSFFont.setFontHeightInPoints((short) 16);
        hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        hSSFFont.setColor(HSSFColor.BLACK.index);
        cellStyle.setFont(hSSFFont);
        celda1.setCellStyle(cellStyle);

        //Datos de BD
        List<Orden> ordenes = serviceOrder.findBy("o.ordenFecha = '" + FormatDateUtil.SFD_YYYYMMDD.format(date) + "'");

        int j = 2;
        for (Orden or : ordenes) {
            for (Cliente cliente : customers) {
                if ((long) cliente.getClienteId() == or.getOrdenPacienteId()/* && (order.getOrdenTotal() > 0 || order.getOrdenTotal() < 0)*/) {
                    // Nueva rowAux
                    fila = sheet.createRow(j);
                    celda = fila.createCell(0);
                    celda.setCellValue(or.getTr().getId().getTrValor());

                    celda = fila.createCell(1);
                    celda.setCellValue(or.getFolio().getId().getFolioValor());

                    j++;
                }
            }
        }
        // Escribimos el fileone
        try {
            FileOutputStream out = new FileOutputStream(fileTwo);
            workbook.write(out);
            //MessageOptionPane.information(null, "Aviso", "Se creó exitosamente el fileone, en la ruta especificada.");
            ok = true;
            out.close();
        } catch (IOException e) {
            LoggerUtil.registrarError(e);
        }
        return ok;
    }

    /**
     * Crea el archivo 2 para la contabilidad
     *
     * @param url Dirección donde se generará el archivo
     * @param sucursal Nombre de la sucursal donde se está generando el archivo
     * @param date Fecha en que se genera el archivo
     * @param customers Lista de customers con que se generará el archivo
     * @return True, si se crea correctamente el archivo
     * @throws Exception
     */
    public boolean generatexlsThree(String url, String sucursal, java.util.Date date, List<Cliente> customers) throws Exception {
        boolean logrado = false;
        ServiceApp serApp = new ServiceAppImpl();
        App app = serApp.getBy("a.appNombre = '" + ConstantConfig.CONFIG_POS_CREDIT_CARD_ID + "'");

        ServiceOrden so = new ServiceOrdenImpl();
        ServiceFolioDevolucion sFDevolucion = new ServiceFolioDevolucionImpl();

        List<Foliodevolucion> foliosDev = sFDevolucion.findBy("f.id.folioDevolucionFecha = '" + FormatDateUtil.SFD_YYYYMMDD.format(date) + "'");

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Hoja 1");

        //Crea fileone
        File archivo = new File(url + "/" + FormatDateUtil.SFD_DMY.format(date) + "_3.xls");
        //Crea la rowAux 0
        HSSFRow fila1 = sheet.createRow(0);
        HSSFCell celda1 = fila1.createCell(0);//Pon el texto en la primera columna

        celda1.setCellValue(sucursal + " QUIMICA CLINICA " + FormatDateUtil.SFD_DMY.format(date));

        //Titulos de columnas
        String[] titulos = {"CU", "NO.SERVICIO", "CS"};

        //Crea la rowAux 1
        HSSFRow fila = sheet.createRow(1);
        HSSFCell celda;

        //Agrega el titulo de las columnas en la rowAux 1
        for (int i = 0; i < titulos.length; i++) {
            celda = fila.createCell(i);
            celda.setCellValue(titulos[i]);
        }

        // Estilo para la celda1
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        cellStyle = workbook.createCellStyle();
        HSSFFont hSSFFont = workbook.createFont();
        hSSFFont.setFontName(HSSFFont.FONT_ARIAL);
        hSSFFont.setFontHeightInPoints((short) 16);
        hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        hSSFFont.setColor(HSSFColor.BLACK.index);
        cellStyle.setFont(hSSFFont);
        celda1.setCellStyle(cellStyle);

        //Datos de BD
        List<Orden> ordenes = so.findBy("o.ordenFecha = '" + FormatDateUtil.SFD_YYYYMMDD.format(date) + "'");
        int j = 2;
        for (Orden or : ordenes) {
            for (Cliente cliente : customers) {
                if ((long) cliente.getClienteId() == or.getOrdenPacienteId() /*&& (order.getOrdenTotal() > 0 || order.getOrdenTotal() < 0)*/) {
                    // Nueva rowAux
                    fila = sheet.createRow(j);
                    celda = fila.createCell(0);
                    celda.setCellValue(or.getTr().getId().getTrValor());

                    celda = fila.createCell(1);
                    if (or.getProducto() != null) {
                        celda.setCellValue(or.getProducto().getProductoClave());
                    } else if (or.getDetallepaquete() != null) {
                        celda.setCellValue(or.getDetallepaquete().getDetallePaqueteClave());
                    }
                    /*else if ("-2".equals(order.getOrdenTipoProducto()) && "2".equals(order.getOrdenStatusTipoProducto())) {
                        cellAux.setCellValue("LEN" + String.format("%04d", order.getOrdenId()));
                    } else if ("-1".equals(order.getOrdenTipoProducto())) {
                        cellAux.setCellValue("");
                    }*/

                    celda = fila.createCell(2);
                    if (findInDevolutions(foliosDev, or.getFolio())
                            || or.getTipopago().getTipoPagoId() == Integer.parseInt(app.getAppValor())) {
                        celda.setCellValue("0");
                    } else {
                        celda.setCellValue("1");
                    }

                    j++;
                }
            }
        }
        // Escribimos el fileone
        try {
            FileOutputStream out = new FileOutputStream(archivo);
            workbook.write(out);
            logrado = true;
            out.close();
        } catch (IOException e) {
            LoggerUtil.registrarError(e);
        }
        return logrado;
    }

    /**
     * Genera archivoxls con la información de las ordenes de la sucursal, para
     * exportarlas a otra sucursal.
     *
     * @param orders Ordenes de laboratorio a exportar
     * @param url Directorio donde se creará el archivo
     * @return true, si se creó correctamente el archivo
     */
    public boolean generatexlsOrdersForExport(List<Ordenlab> orders, String url, String encrypt) {
        boolean logrado = false;
        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        encryptor.setPassword(ENCRYPTOR);

        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Hoja 1");

        //Crea archivo
        File archivo = new File(url);
        //Crea la rowAux 0
        //HSSFRow fila1 = sheet.createRow(0);

        HSSFRow fila = sheet.createRow(1);
        HSSFCell celda;

        int j = 0;
        for (Ordenlab or : orders) {
            fila = sheet.createRow(j);
            celda = fila.createCell(0);
            celda.setCellValue(or.getId().getOrdenlabContDiario());
            celda = fila.createCell(1);
            celda.setCellValue(or.getSucursal().getSucursalId());
            celda = fila.createCell(2);
            if ("true".equals(encrypt)) {
                celda.setCellValue(encryptor.encrypt(or.getOrdenlabDesc()));
            }else{
                celda.setCellValue(or.getOrdenlabDesc());
            }
            celda = fila.createCell(3);
            celda.setCellValue(or.getOrdenLabPacienteId());
            celda = fila.createCell(4);
            celda.setCellValue("1");//Usuario, no reelevante
            celda = fila.createCell(5);
            celda.setCellValue(FormatDateUtil.SFD_YYYYMMDD.format(or.getId().getOrdenLabFecha()));
            celda = fila.createCell(6);
            celda.setCellValue(FormatDateUtil.SFD_KKMMSS.format(or.getOrdenHora()));
            celda = fila.createCell(7);
            celda.setCellValue(or.getOrdenlabStatus());
            celda = fila.createCell(8);
            celda.setCellValue(FormatDateUtil.SFD_DDMMYYYY_HHMMSS_GUION.format(or.getOrdenlabFmodif()));
            celda = fila.createCell(9);
            celda.setCellValue("1");//Tr, no rrelevante
            j++;
        }
        // Escribimos el archivo
        try {
            FileOutputStream out = new FileOutputStream(archivo);
            workbook.write(out);
            logrado = true;
            out.close();
        } catch (IOException e) {
            LoggerUtil.registrarError(e);
        }
        return logrado;
    }

    /**
     * Busca un folio de devolución en todos los foliso de devolución
     *
     * @param foliosDev Folios de devolución en los que se buscará
     * @param folio Folio a buscar
     * @return true, si encuentra el folio buscado
     */
    private boolean findInDevolutions(List<Foliodevolucion> foliosDev, Folio folio) {
        return foliosDev.stream().anyMatch((fd) -> (fd.getDevolucions().stream().map((o) -> (Devolucion) o).anyMatch((dev) -> (Objects.equals(dev.getFolio().getId().getFolioValor(), folio.getId().getFolioValor())))));
    }

}
