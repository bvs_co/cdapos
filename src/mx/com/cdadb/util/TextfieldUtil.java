package mx.com.cdadb.util;

import java.text.ParseException;
import java.util.Date;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;

/**
 *Utilidades para el manejo de los jTextField
 * @author unknown
 * @version 1.0
 */
public class TextfieldUtil {

    /**
     * Selecciona el texto una vez obtenido el focus en JTextField
     * @param t jTextField
     */
    public static void textBoxFocus(JTextField t) {
        t.selectAll();
        t.requestFocus();
    }

    /**
     * Convierte en mayusculas el contenido del JTextField
     * @param t JTextField
     */
    public static void textBoxMayus(JTextField t) {
        t.setText(t.getText().toUpperCase());
    }

    /**
     * Procesa la fecha que esta escrito en el JTextField
     * @param jft JTextField
     * @return Date
     */
    public static Date getDatte(JFormattedTextField jft) {
        Date date = null;
        try {
            date = FormatDateUtil.SFD_DDMMYYYY.parse(jft.getText());
        } catch (ParseException ex) {
            LoggerUtil.registrarError(ex);
        }
        return date;
    }
    
    private TextfieldUtil(){
        
    }
}
