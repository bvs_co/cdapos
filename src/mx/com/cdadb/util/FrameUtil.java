package mx.com.cdadb.util;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;

public class FrameUtil {

    /**
     * Redimensiona la ventana en toda la pantalla
     *
     * @param JF Jframe a redimensionar.
     */
    public static void renderScreen(JFrame JF) {
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        JF.setSize(d.width, d.height);
    }

    public static void renderScreen(JDesktopPane desktopPane, JInternalFrame j) {
        int x = (desktopPane.getWidth() / 2) - j.getWidth() / 2;
        int y = (desktopPane.getHeight() / 2) - j.getHeight() / 2;

        if (j.isShowing()) {
            j.setLocation(x, y);
        } else {
            desktopPane.add(j);
            j.setLocation(x, y);
            j.show();
        }
    }
}
