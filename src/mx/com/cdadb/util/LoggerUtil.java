package mx.com.cdadb.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import mx.com.cdadb.entities.User;

public class LoggerUtil {

    private static PrintWriter printWriter;

    public static boolean registrarError(Exception e, User u) {
        SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfKMS = new SimpleDateFormat("kkmmss");
        java.util.Date fHoy = new java.util.Date();
        File file2 = new File("");
        File var = new File(file2.getAbsolutePath() + "/Log");
        var.mkdirs();
        try {
            File file = new File(var.getAbsolutePath() + "/Log " + sdfYMD.format(fHoy) + " " + sdfKMS.format(fHoy).replaceAll(":", "") + ".log");
            printWriter = new PrintWriter(new FileWriter(file, true));
            printWriter.println("Excepción generada: " + sdfYMD.format(fHoy) + " " + sdfKMS.format(fHoy)+" Usuario: ["+u.getUserId()+"] "+u.getUserNombre()+" "+u.getUserPaterno()+" "+u.getUserMaterno()+" ["+u.getUserUsuario()+"]");
            e.printStackTrace(printWriter);
            printWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(LoggerUtil.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public static boolean registrarError(Exception e) {
        SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfKMS = new SimpleDateFormat("kkmmss");
        java.util.Date fHoy = new java.util.Date();
        File file2 = new File("");
        File var = new File(file2.getAbsolutePath() + "/Log");
        var.mkdirs();
        try {
            File file = new File(var.getAbsolutePath() + "/Log " + sdfYMD.format(fHoy) + " " + sdfKMS.format(fHoy).replaceAll(":", "") + ".log");
            printWriter = new PrintWriter(new FileWriter(file, true));
            printWriter.println("Excepción generada: " + sdfYMD.format(fHoy) + " " + sdfKMS.format(fHoy));
            e.printStackTrace(printWriter);
            printWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(LoggerUtil.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    public static boolean registrarError(Throwable e) {
        SimpleDateFormat sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfKMS = new SimpleDateFormat("kkmmss");
        java.util.Date fHoy = new java.util.Date();
        File file2 = new File("");
        File var = new File(file2.getAbsolutePath() + "/Log");
        var.mkdirs();
        try {
            File file = new File(var.getAbsolutePath() + "/Log " + sdfYMD.format(fHoy) + " " + sdfKMS.format(fHoy).replaceAll(":", "") + ".log");
            printWriter = new PrintWriter(new FileWriter(file, true));
            printWriter.println("Excepción generada: " + sdfYMD.format(fHoy) + " " + sdfKMS.format(fHoy));
            e.printStackTrace(printWriter);
            printWriter.close();
        } catch (IOException ex) {
            LoggerUtil.registrarError(ex);
            return false;
        }
        return true;
    }
}
