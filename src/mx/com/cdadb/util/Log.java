package mx.com.cdadb.util;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Log {

    private static Logger logger = Logger.getLogger(Log.class);

    public static void init() {
        PropertyConfigurator.configure("log4j.properties");
    }

    public static void info(String msg){
        logger.info(msg);
    }
    
    public static void debug(String msg) {
        logger.debug(msg);
    }

    public static void debug(String msg, Throwable ex) {
        logger.debug(msg, ex);
    }

    public static void error(String msg) {
        logger.error(msg);
    }

    public static void error(String msg, Throwable ex) {
        logger.error(msg, ex);
    }

    public static void trace(String msg) {
        logger.trace(msg);
    }

    public static void trace(String msg, Throwable ex) {
        logger.trace(msg, ex);
    }
    
    public static void fatal(String msg){
        logger.fatal(msg);
    }
}
