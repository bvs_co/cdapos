/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.util;

import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.ServiceSucursalLocal;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.service.impl.ServiceSucursalLocalImp;

/**
 *
 * @author unknown
 */
public class GeneralGetters {

    public static Sucursal getSucursal() throws Exception{
        ServiceSucursalLocal sSucursal = new ServiceSucursalLocalImp();
        ServiceApp sApp = new ServiceAppImpl();
        App app = sApp.getBy("a.appNombre ='"+ConstantConfig.CONFIG_BRANCH_ID+"'");
        Sucursal sucursal = sSucursal.getBy("s.sucursalId = " + app.getAppValor());
        return sucursal;
    }
}
