package mx.com.cdadb.util;

import java.awt.BorderLayout;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.print.Printer;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import static javax.print.PrintServiceLookup.lookupDefaultPrintService;
import javax.swing.JOptionPane;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.constants.ConstantFiles;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.constants.ConstantReport;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JRViewer;
import mx.com.cdadb.entities.Producto;
import mx.com.cdadb.entities.Sucursal;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.entities.customer.Cliente;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.view.CorteJRDataSource;
import mx.com.cdadb.view.ClienteJRDataSource;
import mx.com.cdadb.view.ComandaJRDataSource;
import mx.com.cdadb.view.ReportView;
import org.slf4j.LoggerFactory;

/**
 * Implementa los metodos para impresión de las papeletas
 *
 * @author unknown
 */
public class PrintReportImpl {

    private org.slf4j.Logger logger = LoggerFactory.getLogger(PrintReportImpl.class);
    private static final String VENTA_REALIZADA = ". \nVenta realizada, busque comprobante en ";
    private static final String APP_NOMBRE = "a.appNombre = '";
    private static final String PATH_DELIMITER = "/";
    private static final String ERROR_IMPRESORA_PREDETERMINADA = "No hay una impresora predeterminada";

    /**
     * Imprime la papeleta que se le entrega al cliente
     *
     * @param datos parametros para el reporte
     * @param dataSource Datasource cliente
     * @return true, si se imprimió correctamente
     */
    public boolean imprimeTiketCliente(Map datos, ClienteJRDataSource dataSource) {
        JasperReport jasperReport = null;
        try {
            URL in = this.getClass().getResource(ConstantReport.PATIENT_VOUCHER);
            jasperReport = (JasperReport) JRLoader.loadObject(in);
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION, ConstantMessages.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            LoggerUtil.registrarError(ex);
        }

        return printReportDefaultPrinter(jasperReport, datos, dataSource);
    }

    public void papeletaComanda(Map param, ComandaJRDataSource datasource) {
        JasperReport jasperReport = null;
        Directory dir = PropertiesDirectories.decryptDirectories(ConstantFiles.DIR_PROPERTIES);
        try {
            URL in = this.getClass().getResource(ConstantReport.SERVICE_ORDER);
            jasperReport = (JasperReport) JRLoader.loadObject(in);
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
        }
        if (getPrinters() > 0) {
            if (Printer.getDefaultPrinter() != null) //Si se selecciono una impresora
            {
                try {
                    JasperPrint jasperPrint;
                    jasperPrint = JasperFillManager.fillReport(jasperReport, param, datasource);
                    printReportDefault(jasperPrint);
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION + ex.getMessage() + VENTA_REALIZADA + dir.getDirTMP() + ".", ConstantMessages.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
                    LoggerUtil.registrarError(ex);
                }
            } else {
                JOptionPane.showMessageDialog(null, ERROR_IMPRESORA_PREDETERMINADA + VENTA_REALIZADA + dir.getDirTMP() + ".");
            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay impresoras instaladas" + VENTA_REALIZADA + dir.getDirTMP() + ".");
        }

    }

    /**
     * Imprime el reporte simplificado del cliente
     *
     * @param datos Parametros para el reporte
     * @return true, si se imprimio correctamente
     */
    public boolean ticketClienteSimplificado(Map datos) {
        JasperReport jasperReport = null;
        Directory dir = PropertiesDirectories.decryptDirectories(ConstantFiles.DIR_PROPERTIES);

        URL in = this.getClass().getResource(ConstantReport.DONATION_VOUCHER);

        if (getPrinters() > 0) {
            if (Printer.getDefaultPrinter() != null) //Si se selecciono una impresora
            {
                try {
                    jasperReport = (JasperReport) JRLoader.loadObject(in);
                    JasperPrint jasperPrint;
                    JRDataSource data = new JREmptyDataSource();
                    jasperPrint = JasperFillManager.fillReport(jasperReport, datos, data);
                    printReportDefault(jasperPrint);
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage() + VENTA_REALIZADA + dir.getDirTMP() + ".");
                }
            } else {
                JOptionPane.showMessageDialog(null, ERROR_IMPRESORA_PREDETERMINADA + VENTA_REALIZADA + dir.getDirTMP() + ".");
            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay impresoras instaladas" + VENTA_REALIZADA + dir.getDirTMP() + ".");
        }
        return true;
    }

    /**
     * Obtiene el numero de impresoras conectadas actuales
     *
     * @return numero de impresoras
     */
    public int getPrinters() {
        PrintService[] printService = PrintServiceLookup.lookupPrintServices(null, null);
        return printService.length;
    }

    /**
     * Imprime el reporte en la impresora seleccionada por default
     *
     * @param jasperPrint jasperprint a imprimir
     * @return true, si se imprimió correctamente
     */
    private boolean printReportDefault(JasperPrint jasperPrint) {
        try {
            PrintService impresoraDefault = lookupDefaultPrintService();
            JRPrintServiceExporter jrprintServiceExporter = new JRPrintServiceExporter();
            jrprintServiceExporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            jrprintServiceExporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE, impresoraDefault);
            jrprintServiceExporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);//Cambiar a False
            jrprintServiceExporter.exportReport();
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION, ConstantMessages.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            LoggerUtil.registrarError(ex);
        }
        return true;
    }

    /**
     * Imprime la papeleta del recibo simplificado
     *
     * @param cliente cliente
     * @param sucursal sucursal
     * @param folio folio
     * @param total total de la venta
     * @param totalLetra total de la venta en letra
     * @param reimpresion Cadena en la que va la leyenda REIMPRESION en caso de
     * que se trate de una
     */
    public void printSimplificado(Cliente cliente, Sucursal sucursal, long folio, String total, String totalLetra, String reimpresion) {
        try {
            ServiceApp sApp = new ServiceAppImpl();

            String fol = String.format("%05d", folio);
            java.util.Date date = new java.util.Date();
            Map param = new HashMap();
            param.put("FOLIO", fol);
            param.put("DONANTE", cliente.getClienteNombre() + " " + cliente.getClientePaterno() + " " + cliente.getClienteMaterno());
            param.put("CANTIDAD_NUMERO", "$ " + total);
            param.put("CANTIDAD_LETRA", totalLetra);
            param.put("FECHA_HORA", FormatDateUtil.SFD_DMY.format(date).replace("-", "") + " " + FormatDateUtil.SFD_KKMMSS.format(date));
            param.put("REIMPRESION", reimpresion);
            param.put("MATRIZ_RAZON_SOC", sApp.getBy(APP_NOMBRE + ConstantConfig.CONFIG_COMPANY_BUSSINES_NAME + "'").getAppValor());
            param.put("RFC_MATRIZ", sApp.getBy(APP_NOMBRE + ConstantConfig.CONFIG_COMPANY_RFC + "'").getAppValor());
            param.put("DOMICILIO_MATRIZ", sApp.getBy(APP_NOMBRE + ConstantConfig.CONFIG_COMPANY_BUSSINES_ADDRES + "'").getAppValor());
            param.put("DIRECCION_SUCURSAL", sucursal.getSucursalDireccion());
            param.put("SUCURSAL", sucursal.getSucursalNombre());
            param.put("REIMPRESION", "REIMPRESIÓN");
            ticketClienteSimplificado(param);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION, ConstantMessages.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            LoggerUtil.registrarError(ex);
        }
    }

    /**
     * Llena los datos para imprimir una devolución
     *
     * @param cliente cliente
     * @param folio folio de venta
     * @param total total en numero
     * @param reimpresion Cadena en la que va la leyenda REIMPRESION en caso de
     * que se trate de una
     * @param motivo Motivo de la devolución
     * @param datasource Datasource de la venta
     * @param usr usuario que realizó la devolución
     */
    public void printDevolution(Cliente cliente, long folio, String total, String reimpresion, String motivo, ClienteJRDataSource datasource, User usr) {
        try {
            String fol = String.format("%05d", folio);
            java.util.Date date = new java.util.Date();
            Map param = new HashMap();
            param.put("P_CLIENTE", "[" + cliente.getClienteId() + "]" + cliente.getClienteNombre());
            param.put("P_USUARIO", usr.getUserUsuario());
            param.put("P_FECHA", FormatDateUtil.SFD_DMY.format(date) + " " + FormatDateUtil.SFD_KKMMSS.format(date));
            param.put("P_REIMPRESION", reimpresion);
            param.put("P_TOTAL", "$ " + total);
            param.put("P_SUCURSAL", GeneralGetters.getSucursal().getSucursalNombre());
            param.put("P_FOLIO", fol);
            param.put("P_MOTIVO", motivo);
            imprimeDevolucion(param, datasource);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION, ConstantMessages.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            LoggerUtil.registrarError(ex);
        }
    }

    /**
     * Imprime una devolución
     *
     * @param datos parametros para el reporte
     * @param dataSource Datasource
     * @return true, si se imprime correctamente
     */
    public boolean imprimeDevolucion(Map datos, ClienteJRDataSource dataSource) {
        JasperReport jasperReport = null;
        try {
            URL in = this.getClass().getResource(ConstantReport.DEVOLUTION_VOUCHER);
            jasperReport = (JasperReport) JRLoader.loadObject(in);
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION, ConstantMessages.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            LoggerUtil.registrarError(ex);
        }
        return printReportDefaultPrinter(jasperReport, datos, dataSource);
    }

    /**
     * Imprime el reporte de corte de usuario
     *
     * @param datos Parametrs del reporte
     * @param titulo Tiutulo del reporte
     * @param usr Usuario que realiza el corte
     * @param periodo Periodo de corte
     * @param num Numero de reporte
     * @return true, si se imprime correctamente el reporte
     */
    public boolean printReportCorteUsuario(Map datos, String titulo, String usr, String periodo, String num) {
        boolean ok = false;
        try {
            JasperReport jasperReport = null;
            Directory dir = PropertiesDirectories.decryptDirectories(ConstantFiles.DIR_PROPERTIES);

            File directorio = new File(dir.getDirTMP());
            directorio.mkdir();//Si no esta creado, se crea el directorio
            URL in = this.getClass().getResource(ConstantReport.CASHBOX_CUT);
            jasperReport = (JasperReport) JRLoader.loadObject(in);

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, datos);
            ReportView rv = new ReportView(null, true, jasperPrint, 1, null);
            rv.setTitle(titulo + ".");
            JRViewer jRViewer = new JRViewer(jasperPrint);
            //Se guarda copia PDF en Servidor
            String nombre = "CPARCIAL " + usr + " - " + periodo + num + ".pdf";
            //se crea el archivo PDF
            JasperExportManager.exportReportToPdfFile(jasperPrint, directorio + PATH_DELIMITER + nombre);

            SendFile sf = new SendFile(directorio + PATH_DELIMITER + nombre);
            sf.send();

            File ruta = new File(dir.getDirTMP() + PATH_DELIMITER + nombre);
            if (ruta.delete()) {
                logger.info("Temporal files clean.....");
            }

            reportViewInPanel(rv, jRViewer);
            ok = true;
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION, ConstantMessages.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            LoggerUtil.registrarError(ex);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION, ConstantMessages.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            LoggerUtil.registrarError(ex);
        }
        return ok;
    }

    public boolean printReport(Map datos, CorteJRDataSource dataSource, List<Producto> productos, java.util.Date fecha) {
        boolean ok = false;
        try {
            JasperReport jasperReport = null;
            Directory dir = PropertiesDirectories.decryptDirectories(ConstantFiles.DIR_PROPERTIES);

            File directorio = new File(dir.getDirTMP());
            directorio.mkdir();//Si no esta creado, se crea el directorio

            URL in = this.getClass().getResource(ConstantReport.DAY_SALES_REPORT);
            jasperReport = (JasperReport) JRLoader.loadObject(in);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, datos, dataSource);
            ReportView rv = new ReportView(null, true, jasperPrint, 2, productos);
            rv.setTitle("Reporte de ventas - " + FormatDateUtil.SFD_DDMMYYYY.format(fecha) + ".");
            JRViewer jRViewer = new JRViewer(jasperPrint);
            //Se guarda copia PDF en Servidor
            String nombre = "REPORTE DE VENTAS " + FormatDateUtil.SFD_DMY.format(fecha) + "_" + FormatDateUtil.SFD_KKMMSS_SIN.format(new java.util.Date()) + ".pdf";
            //se crea el archivo PDF
            JasperExportManager.exportReportToPdfFile(jasperPrint, directorio + PATH_DELIMITER + nombre);

            SendFile sf = new SendFile(directorio + PATH_DELIMITER + nombre);
            sf.send();

            reportViewInPanel(rv, jRViewer);
            ok = true;
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION, ConstantMessages.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            LoggerUtil.registrarError(ex);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION, ConstantMessages.ERROR_TITLE, JOptionPane.ERROR_MESSAGE);
            LoggerUtil.registrarError(ex);
        }
        return ok;
    }

    /**
     * Imprime reporte en impresora seleccionada por default
     *
     * @param jasperReport jasper
     * @param datos parametros
     * @param dataSource datasource
     * @return true, si se imprime correctamente
     */
    private boolean printReportDefaultPrinter(JasperReport jasperReport, Map datos, ClienteJRDataSource dataSource) {
        boolean ok = false;
        Directory dir = PropertiesDirectories.decryptDirectories(ConstantFiles.DIR_PROPERTIES);
        if (getPrinters() > 0) {
            if (Printer.getDefaultPrinter() != null) //Si se selecciono una impresora
            {
                try {
                    JasperPrint jasperPrint;
                    jasperPrint = JasperFillManager.fillReport(jasperReport, datos, dataSource);
                    printReportDefault(jasperPrint);
                    ok = true;
                } catch (JRException ex) {
                    JOptionPane.showMessageDialog(null, ConstantMessages.ERROR_DESCRIPTION + ex.getMessage() + VENTA_REALIZADA + dir.getDirTMP() + ".");
                }
            } else {
                JOptionPane.showMessageDialog(null, ERROR_IMPRESORA_PREDETERMINADA + VENTA_REALIZADA + dir.getDirTMP() + ".");
            }
        } else {
            JOptionPane.showMessageDialog(null, "No hay impresoras instaladas " + VENTA_REALIZADA + dir.getDirTMP() + ".");
        }
        return ok;
    }

    /**
     * Muestra el reporte en el viewer
     *
     * @param rv ReportView
     * @param jRViewer JRView
     */
    private void reportViewInPanel(ReportView rv, JRViewer jRViewer) {
        //se elimina elementos del contenedor JPanel
        rv.panelReport.removeAll();
        //para el tamaño del reporte se agrega un BorderLayout
        rv.panelReport.setLayout(new BorderLayout());
        rv.panelReport.add(jRViewer, BorderLayout.CENTER);
        jRViewer.setVisible(true);
        rv.panelReport.repaint();
        rv.panelReport.revalidate();
        rv.setVisible(true);
    }
}
