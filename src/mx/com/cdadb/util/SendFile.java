package mx.com.cdadb.util;

import java.net.*;
import java.io.*;
import javax.swing.JOptionPane;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.constants.ConstantFiles;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.impl.ServiceAppImpl;
import mx.com.cdadb.constants.ConstantMessages;
import mx.com.cdadb.service.impl.ServiceBaseImpl;
import org.slf4j.LoggerFactory;

public class SendFile {

    private org.slf4j.Logger logger = LoggerFactory.getLogger(SendFile.class);
    private String nombreArchivo = "";
    private final ServiceBaseImpl service = new ServiceBaseImpl();

    public SendFile(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public SendFile() {
    }

    public void send() throws Exception {
        ServiceApp sApp = new ServiceAppImpl();
        App app = sApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_ORDER_HOST+"'");
        App appPuerto = sApp.getBy("a.appNombre = '"+ConstantConfig.CONFIG_ORDER_PORT+"'");
        

        // Creamos la direccion IP de la maquina que recibira el archivo
        InetAddress direccion = InetAddress.getByName(app.getAppValor());
        // Creamos el Socket con la direccion y elpuerto de comunicacion
        Socket socket = new Socket(direccion, Integer.parseInt(appPuerto.getAppValor()));
        socket.setSoTimeout(2000);
        socket.setKeepAlive(true);
        // Creamos el archivo que vamos a enviar
        File archivo = new File(nombreArchivo);
        //archivo.mkdir();
        // Obtenemos el tamaño del archivo
        int tamañoArchivo = (int) archivo.length();
        // Creamos el flujo de salida, este tipo de flujo nos permite 
        // hacer la escritura de diferentes tipos de datos tales como
        // Strings, boolean, caracteres y la familia de enteros, etc.
        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
        // Enviamos el nombre del archivo 
        dos.writeUTF(archivo.getName());
        // Enviamos el tamaño del archivo
        dos.writeInt(tamañoArchivo);
        // Creamos flujo de entrada para realizar la lectura del archivo en bytes
        FileInputStream fis = new FileInputStream(nombreArchivo);
        BufferedInputStream bis = new BufferedInputStream(fis);
        // Creamos el flujo de salida para enviar los datos del archivo en bytes
        BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
        // Creamos un array de tipo byte con el tamaño del archivo 
        byte[] buffer = new byte[tamañoArchivo];

        // Leemos el archivo y lo introducimos en el array de bytes 
        bis.read(buffer);
        // Realizamos el envio de los bytes que conforman el archivo
        for (int i = 0; i < buffer.length; i++) {
            bos.write(buffer[i]);
        }
        // Cerramos socket y flujos
        bis.close();
        bos.close();
        socket.close();
    }

    public void sendOrder(String text, int numOrder, String ext) throws Exception {
        Directory dir = PropertiesDirectories.decryptDirectories(ConstantFiles.DIR_PROPERTIES);

        File directorio = new File(dir.getDirTMP());
        directorio.mkdir();

        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(directorio.getAbsolutePath() + "/order" + numOrder + ext);
            pw = new PrintWriter(fichero);

            pw.println(text);

        } catch (IOException ex) {
            if (ex != null) {
                JOptionPane.showMessageDialog(null, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
            }
            LoggerUtil.registrarError(ex);
        } finally {
            try {
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception ex) {
                if (ex != null) {
                    JOptionPane.showMessageDialog(null, ConstantMessages.UNEXPECTED_ERROR, "Error.", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage(), "Error.", JOptionPane.ERROR_MESSAGE);
                }
                LoggerUtil.registrarError(ex);
            }
        }

        SendFile ea = new SendFile(directorio.getAbsolutePath() + "/order" + numOrder + ext);
        ea.send();

        /*App sendDouble = (App) service.getBy(new App(), "t.appNombre = '" + ConstantConfig.CONFIG_ORDER_SEND_DOUBLE + "'");
        App sendDoublePath = (App) service.getBy(new App(), "t.appNombre = '" + ConstantConfig.CONFIG_ORDER_SEND_DOUBLE_PATH + "'");
        System.out.println("DOBLE ENVÍO:"+sendDouble.getAppValor());
        if ("true".equals(sendDouble.getAppValor())) {//Si el doble envío está activado
            try{
            FileWriter fileWriter = new FileWriter(sendDoublePath.getAppValor()+"/order" + numOrder + ext);
                try (PrintWriter printWriter = new PrintWriter(fileWriter)) {
                    printWriter.println(text);
                    printWriter.close();
                }
            }catch(IOException ex){
                logger.error("Ruta ["+sendDoublePath.getAppValor()+"] no encontrada, para el segundo envío.");
                LoggerUtil.registrarError(ex);
            }
        }*/

        File ruta = new File(directorio.getAbsolutePath() + "/order" + numOrder + ext);
        //Elimina los archivos temporales
        if (ruta.delete()) {
            logger.info("Temporal files clean.....");
        }
    }

    public void outFile(String desc, int order) throws Exception {
        sendOrder(desc, order, ".ok");
        sendOrder(desc, order, ".res");
    }
}
