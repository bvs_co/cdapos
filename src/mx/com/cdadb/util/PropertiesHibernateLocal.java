package mx.com.cdadb.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import javax.swing.JOptionPane;
import mx.com.cdadb.constants.ConstantMessages;
import org.jasypt.encryption.pbe.PBEStringEncryptor;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;

public class PropertiesHibernateLocal {

    public PropertiesHibernateLocal() {

    }

    public static Properties Decrypt(String prop) {
        PBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(KeyGenerator.PASSWORD_ENCRYPTOR_HIBERNATE_LOCAL_PROPERTIES);

        Properties props = new EncryptableProperties(encryptor);
        try {
            props.load(new FileInputStream(prop));
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
        }
        Properties propDev = new Properties();
        propDev.setProperty("hibernate.connection.url", props.getProperty("hibernate.connection.url"));
        propDev.setProperty("hibernate.connection.driver_class", props.getProperty("hibernate.connection.driver_class"));
        propDev.setProperty("hibernate.connection.username", props.getProperty("hibernate.connection.username"));
        propDev.setProperty("hibernate.connection.password", props.getProperty("hibernate.connection.password"));
        return propDev;
    }
}
