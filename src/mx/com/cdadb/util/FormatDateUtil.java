package mx.com.cdadb.util;

import java.text.SimpleDateFormat;

public class FormatDateUtil {
    public static final SimpleDateFormat SFD_DDMMYYYY_HHMMSS = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    public static final SimpleDateFormat SFD_DDMMYYYY_HHMMSS_GUION = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    public static final SimpleDateFormat SFD_DDMMYYYY = new SimpleDateFormat("dd/MM/yyyy");
    public static final SimpleDateFormat SFD_YYYYMMDD_S = new SimpleDateFormat("yyyy/MM/dd");
    public static final SimpleDateFormat SFD_DMY = new SimpleDateFormat("dd-MM-yyyy");
    public static final SimpleDateFormat SFD_KKMMSS = new SimpleDateFormat("kk:mm:ss");
    public static final SimpleDateFormat SFD_KKMMSS_SIN = new SimpleDateFormat("kkmmss");
    public static final SimpleDateFormat SFD_HHMMSS = new SimpleDateFormat("hh:mm:ss");
    public static final SimpleDateFormat SFD_YYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
    public static final SimpleDateFormat SFD_YYYYMMDD_NS = new SimpleDateFormat("yyyyMMdd");
    public static final SimpleDateFormat SDF_EXTENDED_DATE = new SimpleDateFormat("dd 'de' MMMMMMMMMM 'de' yyyy");
}
