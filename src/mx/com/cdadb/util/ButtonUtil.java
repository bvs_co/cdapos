/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.util;

import java.awt.Color;

/**
 *
 * @author macbook
 */
public class ButtonUtil {
    public static final Color COLOR_SELECCION = new java.awt.Color(0, 153, 153);
    public static final Color COLOR_NORMAL = new java.awt.Color(204, 204, 255);
    public static final Color COLOR_PANEL_FONDO = new java.awt.Color(244, 254, 254);
    public static final Color COLOR_BTN_NVO = new java.awt.Color(255, 204, 204);
    public static final Color COLOR_BTN_MODF = new java.awt.Color(204, 255, 204);
    public static final java.awt.Font fontButton = new java.awt.Font("Consolas", 1, 12);
}
