package mx.com.cdadb.util;

import com.sun.awt.AWTUtilities;
import mx.com.cdadb.view.PaneAreas;
import mx.com.cdadb.view.PaneConnEnterprise;
import mx.com.cdadb.view.PaneArqueo;
import mx.com.cdadb.view.PaneCorteFinal;
import mx.com.cdadb.view.PaneCorteUsuario;
import mx.com.cdadb.view.DialogIni;
import mx.com.cdadb.view.FrMenu;
import mx.com.cdadb.view.SubDonations;
import mx.com.cdadb.view.DialogOrdenesLab;
import mx.com.cdadb.view.DialogAltaRapidaPaciente;
import mx.com.cdadb.view.PanePaquetes;
import mx.com.cdadb.view.FrCobro;
import mx.com.cdadb.view.PanePos;
import mx.com.cdadb.view.FrDevolucion;
import mx.com.cdadb.view.FrReprint;
import mx.com.cdadb.view.DialogProductos;
import mx.com.cdadb.view.PaneProductos;

/**
 *
 * @author unknown
 */
public class DialogWait extends javax.swing.JDialog {

    private final MyWorker worker;

    public DialogWait(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setSize(1366, 1024);
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        //jLabel1.setBounds(0,0,this.getWidth()/2, this.getHeight()/2);
        worker = new MyWorker(jLabel1, this);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, PaneAreas pane) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, pane);
        worker.execute();
    }
        
    public DialogWait(java.awt.Frame parent, boolean modal, PanePaquetes pane) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, pane);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, PanePos pane) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, pane);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, PaneProductos pane) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, pane);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, PaneConnEnterprise pane) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, pane);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, SubDonations s) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, s);
        worker.execute();
    }
        
    public DialogWait(java.awt.Frame parent, boolean modal, PaneCorteUsuario pane, int op, java.util.Date fecha) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, pane, op, fecha);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, PaneCorteFinal pane, int op, java.util.Date fecha) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, pane, op, fecha);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, PaneArqueo pane, int op) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, pane, op);
        worker.execute();
    }
   
    public DialogWait(java.awt.Frame parent, boolean modal, DialogProductos dlg, String clave) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, dlg, clave);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, FrDevolucion dlg, String clave) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, dlg, clave);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, DialogOrdenesLab dlg, int op) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, dlg, op);
        worker.execute();
    }
    
    
    public DialogWait(java.awt.Frame parent, boolean modal, FrCobro dlg) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, dlg);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, FrReprint dlg, int op) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, dlg, op);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, DialogAltaRapidaPaciente dlg, int opc) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, dlg, opc);
        worker.execute();
    }
    
    public DialogWait(java.awt.Frame parent, boolean modal, DialogIni dlg) {
        super(parent, modal);
        initComponents();
        FrMenu fr = (FrMenu) parent;
        this.setSize(fr.getWidth(), fr.getHeight());
        this.setLocationRelativeTo(null);
        AWTUtilities.setWindowOpacity(this, 0.6f);
        worker = new MyWorker(jLabel1, this, dlg);
        worker.execute();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);

        jLabel1.setFont(new java.awt.Font("Consolas", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("...");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 730, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(210, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(243, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DialogWait.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DialogWait.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DialogWait.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DialogWait.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DialogWait dialog = new DialogWait(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
