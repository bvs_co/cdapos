package mx.com.cdadb.util;

/**
 *
 * @author unknown
 */
public class MonedaUtil {

    public static final int MXP = 0;
    public static final int USD = 1;

    public static String numeroEnLetra(int num) {
        String numEnLetra = "";

        String[] uni = {"CERO", "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE"};

        String[] dec10 = {"DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE"};

        String[] dec20 = {"VEINTE", "VEINTIUN", "VEINTIDOS", "VEINTITRES", "VEINTICUATRO", "VEINTICINCO", "VEINTISEIS", "VEINTISIETE", "VEINTIOCHO", "VEINTINUEVE"};

        String[] dec = {"TREINTA", "CUARENTA", "CINCUENTA", "SESENTA", "SETENTA", "OCHENTA", "NOVENTA"};

        String[] cen = {"DOSCIENTOS", "TRESCIENTOS", "CUATROCIENTOS", "QUINIENTOS", "SEISCIENTOS", "SETECIENTOS", "OCHOCIENTOS", "NOVECIENTOS"};
        if (num >= 100) {
            if (num < 200) {
                if (num == 100) {
                    numEnLetra = numEnLetra + "CIEN";
                } else {
                    numEnLetra = numEnLetra + "CIENTO";
                }
            } else {
                numEnLetra = numEnLetra + cen[(num / 100 - 2)];
            }
            num %= 100;
            if (num != 0) {
                numEnLetra = numEnLetra + " ";
            }
        }
        if (num >= 10) {
            if (num < 20) {
                numEnLetra = numEnLetra + dec10[(num % 10)];
                num = 0;
            } else if (num < 30) {
                numEnLetra = numEnLetra + dec20[(num % 10)];
                num = 0;
            } else {
                numEnLetra = numEnLetra + dec[(num / 10 - 3)];
                num %= 10;
                if (num != 0) {
                    numEnLetra = numEnLetra + " Y ";
                }
            }
        }
        if (num > 0) {
            numEnLetra = numEnLetra + uni[num];
        }
        return numEnLetra;
    }

    public static String monedaEnLetra(String num, String monSin, String monPlu, String suf)
            throws Exception {
        String monEnLetra = "";
        String n = "";
        for (int i = 0; i < num.length(); i++) {
            if (Character.isDigit(num.charAt(i))) {
                n = n + String.valueOf(num.charAt(i));
            } else {
                if (num.charAt(i) == '.') {
                    break;
                }
            }
        }
        int numero = Integer.parseInt(n);
        if (numero == 0) {
            monEnLetra = monEnLetra + "CERO ";
        } else {
            if (numero > 999999999) {
                return "-";
            }
            if (numero >= 1000000) {
                String m = numeroEnLetra(numero / 1000000);
                numero %= 1000000;
                if (m.equals("UN")) {
                    monEnLetra = monEnLetra + "UN MILLON ";
                    if (numero == 0) {
                        monEnLetra = monEnLetra + " DE ";
                    }
                } else {
                    monEnLetra = monEnLetra + m + " MILLONES ";
                }
            }
            if (numero >= 1000) {
                String m = numeroEnLetra(numero / 1000);
                numero %= 1000;
                if (m.equals("UN")) {
                    monEnLetra = monEnLetra + "MIL ";
                } else {
                    monEnLetra = monEnLetra + m + " MIL ";
                }
            }
            if (numero > 0) {
                String m = numeroEnLetra(numero);

                monEnLetra = monEnLetra + m + " ";
            }
        }
        String dec = num.substring(num.length() - 2, num.length());

        monEnLetra = "(" + monEnLetra + (monEnLetra.trim().equals("UN") ? monSin : monPlu) + " " + dec + "/100 " + suf + ")";

        return monEnLetra;
    }

    public static String monedaEnLetra(String num, int mon)
            throws Exception {
        String monEnLetra = "";
        String n = "";
        for (int i = 0; i < num.length(); i++) {
            if (Character.isDigit(num.charAt(i))) {
                n = n + String.valueOf(num.charAt(i));
            } else {
                if (num.charAt(i) == '.') {
                    break;
                }
            }
        }
        int numero = Integer.parseInt(n);
        if (numero == 0) {
            monEnLetra = monEnLetra + "CERO ";
        } else {
            if (numero > 999999999) {
                return "-";
            }
            if (numero >= 1000000) {
                String m = numeroEnLetra(numero / 1000000);
                numero %= 1000000;
                if (m.equals("UN")) {
                    monEnLetra = monEnLetra + "UN MILLON ";
                    if (numero == 0) {
                        monEnLetra = monEnLetra + " DE ";
                    }
                } else {
                    monEnLetra = monEnLetra + m + " MILLONES ";
                }
            }
            if (numero >= 1000) {
                String m = numeroEnLetra(numero / 1000);
                numero %= 1000;
                if (m.equals("UN")) {
                    monEnLetra = monEnLetra + "MIL ";
                } else {
                    monEnLetra = monEnLetra + m + " MIL ";
                }
            }
            if (numero > 0) {
                String m = numeroEnLetra(numero);

                monEnLetra = monEnLetra + m + " ";
            }
        }
        String dec = num.substring(num.length() - 2, num.length());

        monEnLetra = "(" + monEnLetra;
        if (0 == mon) {
            monEnLetra = monEnLetra + "PESOS " + dec + "/100 MN)";
        } else if (1 == mon) {
            monEnLetra = monEnLetra + "DOLARES " + dec + "/100 USD)";
        }
        return monEnLetra;
    }
    
    public static String separaMiles(String cifra) {
        String[] cantidades = cifra.split("\\.");
        StringBuilder sb = new StringBuilder(cantidades[0]).append(".");
        for (int i = 1, len = sb.length(); i < len; i++) {
            if (i % 4 == 0) {
                sb.insert(len = sb.length() - i, ',');
                len = sb.length();
            }
        }
        return sb + cantidades[1];
    }
    
//    public static void main(String args[]){
//        System.out.println("OUT: "+MonedaUtil.separaMiles("800.00"));
//    }
}
