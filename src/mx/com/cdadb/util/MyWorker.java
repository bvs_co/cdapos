package mx.com.cdadb.util;

import java.io.IOException;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.view.PaneAreas;
import mx.com.cdadb.view.PaneConnEnterprise;
import mx.com.cdadb.view.PaneArqueo;
import mx.com.cdadb.view.PaneCorteFinal;
import mx.com.cdadb.view.PaneCorteUsuario;
import mx.com.cdadb.view.DialogIni;
import mx.com.cdadb.view.SubDonations;
import mx.com.cdadb.view.DialogOrdenesLab;
import mx.com.cdadb.view.DialogAltaRapidaPaciente;
import mx.com.cdadb.view.PanePaquetes;
import mx.com.cdadb.view.FrCobro;
import mx.com.cdadb.view.PanePos;
import mx.com.cdadb.view.FrDevolucion;
import mx.com.cdadb.view.FrReprint;
import mx.com.cdadb.view.DialogProductos;
import mx.com.cdadb.view.PaneProductos;

/**
 *
 * @author unknown
 */
public class MyWorker extends SwingWorker<Void, Void> {

    private final JLabel estado;
    private final JDialog dialog;
    private PaneAreas paneAreas = null;
    private PanePaquetes panePaquetes = null;
    private PaneProductos paneProductos = null;
    private PaneCorteUsuario paneCorteUsuario = null;
    private PaneCorteFinal paneCorteFinal = null;
    private PaneArqueo paneArqueo = null;
    private PaneConnEnterprise paneConnEnterprise = null;
    private PanePos panePos = null;
    private DialogProductos dialogProductos = null;
    private DialogIni dialogIni = null;
    private DialogAltaRapidaPaciente dAltaRapida = null;
    private DialogAltaRapidaPaciente dAltaRapida2 = null;
    private String claveAltaRapida = "";
    private DialogOrdenesLab dialogOrdenesLab = null;
    private FrReprint frReprint = null;
    private FrCobro frCobro = null;
    private String clave = "";
    private int opCorteUser = -1;
    private int opCorteFinal = -1;
    private int opcOrdenesLab = -1;
    private int opcPaneArqueo = -1;
    private int opcAltaRpida = -1;
    private int opcReprint = -1;
    private java.util.Date fechaCorte = null;
    private FrDevolucion frDevolucion = null;
    private String textDevolucion = "";
    private SubDonations subDonations = null;

    public MyWorker(JLabel lbl, JDialog dlg, DialogProductos pane, String clave) {
        this.estado = lbl;
        this.dialog = dlg;
        this.dialogProductos = pane;
        this.clave = clave;
    }

    public MyWorker(JLabel lbl, JDialog dlg, DialogAltaRapidaPaciente dlg2, int op) {
        this.estado = lbl;
        this.dialog = dlg;
        this.dAltaRapida = dlg2;
        this.opcAltaRpida = op;
    }

    public MyWorker(JLabel lbl, JDialog dlg, DialogOrdenesLab d, int op) {
        this.estado = lbl;
        this.dialog = dlg;
        this.dialogOrdenesLab = d;
        this.opcOrdenesLab = op;
    }

    public MyWorker(JLabel lbl, JDialog dlg, FrCobro d) {
        this.estado = lbl;
        this.dialog = dlg;
        this.frCobro = d;
    }

    public MyWorker(JLabel lbl, JDialog dlg, SubDonations s) {
        this.estado = lbl;
        this.dialog = dlg;
        this.subDonations = s;
    }

    public MyWorker(JLabel lbl, JDialog dlg) {
        this.estado = lbl;
        this.dialog = dlg;
    }

    public MyWorker(JLabel lbl, JDialog dlg, PaneAreas pane) {
        this.estado = lbl;
        this.dialog = dlg;
        this.paneAreas = pane;
    }

    public MyWorker(JLabel lbl, JDialog dlg, PanePaquetes pane) {
        this.estado = lbl;
        this.dialog = dlg;
        this.panePaquetes = pane;
    }

    public MyWorker(JLabel lbl, JDialog dlg, PanePos pane) {
        this.estado = lbl;
        this.dialog = dlg;
        this.panePos = pane;
    }

    public MyWorker(JLabel lbl, JDialog dlg, PaneCorteUsuario pane, int op, java.util.Date fecha) {
        this.estado = lbl;
        this.dialog = dlg;
        this.opCorteUser = op;
        this.paneCorteUsuario = pane;
        this.fechaCorte = fecha;
    }

    public MyWorker(JLabel lbl, JDialog dlg, PaneArqueo pane, int op) {
        this.estado = lbl;
        this.dialog = dlg;
        this.opcPaneArqueo = op;
        this.paneArqueo = pane;
    }

    public MyWorker(JLabel lbl, JDialog dlg, PaneCorteFinal pane, int op, java.util.Date fecha) {
        this.estado = lbl;
        this.dialog = dlg;
        this.opCorteFinal = op;
        this.paneCorteFinal = pane;
        this.fechaCorte = fecha;
    }

    public MyWorker(JLabel lbl, JDialog dlg, PaneProductos pane) {
        this.estado = lbl;
        this.dialog = dlg;
        this.paneProductos = pane;
    }

    public MyWorker(JLabel lbl, JDialog dlg, PaneConnEnterprise pane) {
        this.estado = lbl;
        this.dialog = dlg;
        this.paneConnEnterprise = pane;
    }

    public MyWorker(JLabel lbl, JDialog dlg, DialogIni pane) {
        this.estado = lbl;
        this.dialog = dlg;
        this.dialogIni = pane;
    }

    public MyWorker(JLabel lbl, JDialog dlg, FrReprint fr, int opc) {
        this.estado = lbl;
        this.dialog = dlg;
        this.frReprint = fr;
        this.opcReprint = opc;
    }

    public MyWorker(JLabel lbl, JDialog dlg, FrDevolucion fr, String text) {
        this.estado = lbl;
        this.dialog = dlg;
        this.frDevolucion = fr;
        this.textDevolucion = text;
    }

    @Override
    protected Void doInBackground() throws IOException {
        this.estado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/mx/com/cdadb/resources/images/wait.gif")));
        this.estado.setText("Cargando...");
        if (this.paneAreas != null) {
            this.paneAreas.initData();
            this.paneAreas.initGui();
        }
        if (this.panePaquetes != null) {
            this.panePaquetes.initData();
        }
        if (this.paneProductos != null) {
            this.paneProductos.initData();
            this.paneProductos.initGui();
        }
        if (this.paneConnEnterprise != null) {
            this.paneConnEnterprise.initData();
        }
        if (this.dialogProductos != null) {
            this.dialogProductos.initData(this.clave);
        }
        if (this.dialogIni != null) {
            this.dialogIni.ingresar();
        }
        if (this.paneCorteUsuario != null) {
            switch (this.opCorteUser) {
                case 0://Inicia corte de usuario
                    FormatDateUtil.SFD_DDMMYYYY.format(fechaCorte);
                    this.paneCorteUsuario.iniCorte(fechaCorte);
                    break;
                case 1://Inicia ventana de corte
                    try {
                        this.paneCorteUsuario.imprimeCorte(fechaCorte);
                        this.paneCorteUsuario.corteDb(fechaCorte);
                    } catch (Exception e) {
                        LoggerUtil.registrarError(e);
                        if (e.getMessage().contains("Connection refused")) {
                            JOptionPane.showMessageDialog(null, "Hay problemas para comunicarse con Monitor CDA Server. \nContacte al administrador.", "Corte no realizado.", JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    break;
                case 2://Selecciona un usuario y carga las ventas del mismo
                    this.paneCorteUsuario.iniVentasUsuario();
                    break;
                case 3://
                    this.paneCorteUsuario.initData();
                    break;
            }
//            if (this.opCorteUser == 0) {
//                this.paneCorteUsuario.iniCorte();
//            } else {
//                this.paneCorteUsuario.imprimeCorte();
//                this.paneCorteUsuario.corteDb();
//            }
        }
        if (paneCorteFinal != null) {
            switch (this.opCorteFinal) {
                case 0://Inicia proceso de corte final
                    this.paneCorteFinal.iniCorte(this.fechaCorte);
                    break;
                case 1://Carga todas las ventas
                    this.paneCorteFinal.cargaPeriodo();
                    break;
                case 2:
                    break;
            }
        }
        if (dAltaRapida != null) {
            switch (this.opcAltaRpida) {
                case 0:
                    this.dAltaRapida.findByNameOrderByPaternoMaternoAsc();
                    break;
                case 1:
                    this.dAltaRapida.findByNameAndPaternoOrderByPaternoMaternoAsc();
                    break;
            }
        }
        if (this.frCobro != null) {
            this.frCobro.iniVenta();
        }
        if (this.frReprint != null) {
            if (this.opcReprint == 0) {
                this.frReprint.initData();
            }
            if (this.opcReprint == 1) {
                this.frReprint.iniReimpresion();
            }
        }
        if (this.panePos != null) {
            this.panePos.imprimeUltima();
        }
        if (dialogOrdenesLab != null) {
            if (this.opcOrdenesLab == 0) {
                this.dialogOrdenesLab.initData();
            } else {
                this.dialogOrdenesLab.iniCargaOrden();
            }
        }
        if (frDevolucion != null) {
            if (this.textDevolucion != "") {
                frDevolucion.findFolio();
            } else {
                frDevolucion.devuelve();
            }
        }
        if (this.paneArqueo != null) {
            switch (this.opcPaneArqueo) {
                case 0://Impresion de detalle de ventas

                    break;
                case 1:
                    this.paneArqueo.initData();
                    break;
                case 2:
                    this.paneArqueo.iniVentasUsuario();
                    break;
                case 3:
                    this.paneArqueo.ventaGeneral();
                    break;
            }
        }
        if (this.subDonations != null) {
            this.subDonations.paintSubmenuDonations();
        }
        return null;
    }

    @Override
    protected void done() {
        this.dialog.dispose();
    }
}
