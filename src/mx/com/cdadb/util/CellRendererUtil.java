package mx.com.cdadb.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import mx.com.cdadb.enums.CellRendererEnum;

public class CellRendererUtil extends DefaultTableCellRenderer {

    private String tipo = CellRendererEnum.TEXT_LEFT.toString();
    private final Font FONT_COURIER_NEW_BOLD_X16 = new Font("Courier New", Font.BOLD, 16);
    private final Font FONT_COURIER_NEW_PLAIN_X16 = new Font("Courier New", Font.PLAIN, 16);
    private final Font FONT_COURIER_NEW_BOLD_X14 = new Font("Courier New", Font.BOLD, 14);
    private final Font FONT_COURIER_NEW_PLAIN_X14 = new Font("Courier New", Font.PLAIN, 14);

    /**
     * @param tipo
     * "id"
     */
    public CellRendererUtil(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        if (selected) {
            this.setBackground(new Color(50, 153, 254));
        } else {
            this.setBackground(Color.white);
        }
        
        if (tipo.equals(CellRendererEnum.ORDER_ID.toString())) {//ORDER
            this.setHorizontalAlignment(JLabel.CENTER);
            this.setText(String.valueOf(value));
            this.setForeground((selected) ? new Color(255, 255, 255) : new Color(153, 0, 0));
            this.setFont(FONT_COURIER_NEW_BOLD_X14);
            return this;
        }
        
        if (tipo.equals(CellRendererEnum.PATIENT_ID.toString())) {//PATIENT
            this.setHorizontalAlignment(JLabel.CENTER);
            this.setText(String.format(String.valueOf(value)));
            this.setForeground((selected) ? new Color(255, 255, 255) : new Color(32, 117, 32));
            this.setFont(FONT_COURIER_NEW_BOLD_X14);
            return this;
        }
        
        if (tipo.equals(CellRendererEnum.FOLIO_ID.toString())) {//FOLIO
            this.setHorizontalAlignment(JLabel.CENTER);
            this.setText(String.format(String.valueOf(value)));
            this.setForeground((selected) ? new Color(255, 255, 255) : new Color(255, 0, 0));
            this.setFont(FONT_COURIER_NEW_BOLD_X14);
            return this;
        }
        
        if (tipo.equals(CellRendererEnum.TEXT_CENTER.toString())) {//TEXT CENTER
            this.setHorizontalAlignment(JLabel.CENTER);
            this.setText((String) value);
            this.setForeground((selected) ? new Color(255, 255, 255) : new Color(0, 0, 0));
            this.setFont(FONT_COURIER_NEW_PLAIN_X14);
            return this;
        }
        
        if (tipo.equals(CellRendererEnum.TEXT_LEFT.toString())) {//TEXT LEFT
            this.setHorizontalAlignment(JLabel.LEFT);
            this.setText((String) value);
            this.setForeground((selected) ? new Color(255, 255, 255) : new Color(0, 0, 0));
            this.setFont(FONT_COURIER_NEW_PLAIN_X14);
            return this;
        }
        
        if (tipo.equals(CellRendererEnum.COIN_ID.toString())) {//COIN ID
            this.setHorizontalAlignment(JLabel.RIGHT);
            float f = Float.valueOf(String.valueOf(value));
            this.setText(MonedaUtil.separaMiles(String.format("%.2f", f)));
            this.setForeground((Float.valueOf(value.toString().replaceAll(",", "")) < 0) ? new Color(255, 0, 0) : new Color(0, 0, 255));
            //this.setForeground((selected) ? new Color(255, 255, 255) : new Color(0, 0, 255));
            this.setFont(FONT_COURIER_NEW_BOLD_X16);
            return this;
        }
        
        if (tipo.equals(CellRendererEnum.PK_ID_HIDDEN.toString())) {//HIDDEN PK
            this.setHorizontalAlignment(JLabel.LEFT);
            this.setText(String.valueOf(value));
            this.setForeground((selected) ? new Color(0, 153, 255) : new Color(255, 255, 255));
            this.setFont(FONT_COURIER_NEW_BOLD_X16);
            return this;
        }
        
        if (tipo.equals(CellRendererEnum.PK_ID.toString())) {//PK ID
            this.setHorizontalAlignment(JLabel.CENTER);
            this.setText(String.format(String.valueOf(value)));
            this.setForeground((selected) ? new Color(255, 255, 255) : new Color(0, 0, 0));
            this.setFont(FONT_COURIER_NEW_PLAIN_X16);
            return this;
        }
        
        if (tipo.equals(CellRendererEnum.ENTERPRISE_ID.toString())) {//ENTERPRISE ID
            this.setHorizontalAlignment(JLabel.CENTER);
            this.setText(String.format(String.valueOf(value)));
            this.setForeground((selected) ? new Color(255, 255, 255) : new Color(0,102,204));
            this.setFont(FONT_COURIER_NEW_BOLD_X14);
            return this;
        }

        if (Boolean.valueOf(String.valueOf(table.getValueAt(row, 3)))) {
            setForeground(Color.blue);
        } // SI NO ES ACTIVO ENTONCES COLOR ROJO
        else {
            setForeground(Color.red);
        }

        super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        return this;
    }
}
