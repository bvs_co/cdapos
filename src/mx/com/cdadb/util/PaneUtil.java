/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.util;

import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author macbook
 */
public class PaneUtil {
 
        public static void requesttFocus(JPanel principalPane) {
        JTextField txt = null;
        for (int i = 0; i < principalPane.getComponentCount(); i++) {
            if (principalPane.getComponent(i) instanceof JPanel) {
                JPanel panel = (JPanel) principalPane.getComponent(i);
                for (int j = 0; j < panel.getComponentCount(); j++) {
                    if (panel.getComponent(j) instanceof JTextField) {
                        txt = (JTextField) panel.getComponent(j);
                    }
                }
            }
        }
        if (txt != null) {
            txt.requestFocus();
        }
    }
}
