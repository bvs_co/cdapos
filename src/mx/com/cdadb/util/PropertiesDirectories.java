/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import javax.swing.JOptionPane;
import mx.com.cdadb.constants.ConstantMessages;
import org.jasypt.encryption.pbe.PBEStringEncryptor;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;

/**
 *
 * @author 
 */
public class PropertiesDirectories {
    
    public static Directory decryptDirectories(String prop) {
        PBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(KeyGenerator.PASSWORD_ENCRYPTOR_DIRECTORIES);

        Properties props = new EncryptableProperties(encryptor);
        try {
            props.load(new FileInputStream(prop));
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(PropertiesCycleEncrypt.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
        } catch (IOException ex) {
            //Logger.getLogger(PropertiesCycleEncrypt.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ConstantMessages.ERROR + ex.getMessage());
        }
        //obtenemos el valor desencriptado
        Directory d = new Directory();
        String tmp = props.getProperty("app.dir.tmp");
        d.setDirTMP(tmp);
        return d;
    }
}
