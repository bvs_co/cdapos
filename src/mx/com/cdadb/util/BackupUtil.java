/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.cdadb.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import mx.com.cdadb.constants.ConstantConfig;
import mx.com.cdadb.constants.ConstantFiles;
import mx.com.cdadb.entities.App;
import mx.com.cdadb.entities.User;
import mx.com.cdadb.service.ServiceApp;
import mx.com.cdadb.service.impl.ServiceAppImpl;

/**
 *
 * @author macbook
 */
public class BackupUtil {

    public static boolean backupCreate(User user) throws Exception {
        boolean ok = false;
        try {
            if (getActiveBackup()) {
                Directory dir = PropertiesDirectories.decryptDirectories(ConstantFiles.DIR_PROPERTIES);
                File testFile = new File(dir.getDirTMP() + "/token.bin");
                try (FileWriter write = new FileWriter(testFile, true)) {
                    write.write(FormatDateUtil.SFD_DDMMYYYY_HHMMSS.format(new Date()));
                    write.write("user: " + user.getUserUsuario());
                }
                SendFile ea3 = new SendFile(dir.getDirTMP() + "/token.bin");
                ea3.send();
                ok = true;
            }
        } catch (IOException ex) {
            LoggerUtil.registrarError(ex);
            return ok;
        }
        return ok;
    }

    public static boolean getActiveBackup() {
        boolean active = false;
        try {
            ServiceApp sApp = new ServiceAppImpl();
            App app = sApp.getBy("a.appNombre = '" + ConstantConfig.CONFIG_SQL_BACKUP + "'");
            active = Boolean.parseBoolean(app.getAppValor());
            return active;
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
            return active;
        }
    }
}
