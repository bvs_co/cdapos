package mx.com.cdadb.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalculateAgeUtil {

    public static Integer calculate(String stringBirthday) {
        Date dateBirthday = null;
        int año = 0;
        int mes = 0;
        int dia = 0;
        try {
            dateBirthday = new SimpleDateFormat("dd-MM-yyyy").parse(stringBirthday);
            Calendar fechaNacimiento = Calendar.getInstance();
            Calendar fechaActual = Calendar.getInstance();
            fechaNacimiento.setTime(dateBirthday);
            año = fechaActual.get(Calendar.YEAR) - fechaNacimiento.get(Calendar.YEAR);
            mes = fechaActual.get(Calendar.MONTH) - fechaNacimiento.get(Calendar.MONTH);
            dia = fechaActual.get(Calendar.DATE) - fechaNacimiento.get(Calendar.DATE);
            //Se ajusta el año dependiendo el mes y el día
            if (mes < 0 || (mes == 0 && dia < 0)) {
                año--;
            }
        } catch (Exception ex) {
            LoggerUtil.registrarError(ex);
        }
        return año;
    }
}
